;-----------------------------------------------------------------------
;+
; NAME:
;        CTM_READ_COARDS
;
; PURPOSE:
;        Reads data blocks from a COARDS-compliant netCDF file (such
;        as created by routine BPCH2COARDS) into GAMAP.  CTM_READ_COARDS 
;        is is an internal routine which is called by CTM_OPEN_FILE.
;
;        NOTE: COARDS is a formatting standard for netCDF files which
;        is widely used in both the atmospheric & climate communities.
;        COARDS-compliant netCDF files can be read by GAMAP, GrADS and
;        other plotting packages.
;        
;        See http://ferret.wrc.noaa.gov/noaa_coop/coop_cdf_profile.html
;        for more information about the COARDS standard.
;
; CATEGORY:
;        GAMAP Internals, Scientific Data Formats
;
; CALLING SEQUENCE:
;        CTM_READ_COARDS, [, Keywords ]
;
; INPUTS:
;        ILUN -> GAMAP file unit which will denote the netCDF file.
;
;        FILENAME -> Name of the netCDF grid file to be read.
; 
;        FILEINFO -> Array of FILEINFO structures which will be
;             returned to CTM_OPEN_FILE.  CTM_OPEN_FILE will 
;             append FILEINFO to the GAMAP global common block.
;
;        DATAINFO -> Array of DATAINFO structures (each element 
;             specifies a GAMAP data block) which will be returned
;             to CTM_OPEN_FILE.  CTM_OPEN_FILE will append FILEINFO 
;             to the GAMAP global common block.        
;
; KEYWORD PARAMETERS:
;        _EXTRA=e -> Picks up extra keywords
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        Internal Subroutines:
;        ====================================================
;        CRC_Get_DimInfo        CRC_Get_IndexVars   
;        CRC_Read_Global_Atts   CRC_Get_Tracer
;        CRC_Get_Data           CRC_Save_Data
;
;        External Subroutines Required:
;        ====================================================
;        CTM_GRID          (function)   CTM_TYPE (function)
;        CTM_MAKE_DATAINFO (function)   STRRIGHT (function)
;        STRREPL           (function)
;
; REQUIREMENTS:
;        Requires routines from both GAMAP and TOOLS packages.
;
; NOTES:
;        (1) Assumes that data blocks have the following dimensions:
;            (a) longitude, latitude, time  
;            (b) longitude, latitude, levels, time
;
;        (2) Assumes that times are given in GMT.
;
;        (3) If information about each tracer in the COARDS-compliant
;            netCDF file is stored in the GAMAP "tracerinfo.dat" file, 
;            then CTM_READ_COARDS will be able to read the file without 
;            having to ask the user to supply a GAMAP category and
;            tracer name.  
;       
; EXAMPLE:
;        ILUN     = 21
;        FILENAME = 'coards.20010101.nc'
;        CTM_READ_COARDS, ILUN, FILENAME, FILEINFO, DATAINFO
;
;             ; Reads data from the COARDS-compliant netCDF file 
;             ; coards.20010101.nc and stores it the FILEINFO and 
;             ; DATAINFO arrays of structures.  If you are calling 
;             ; CTM_READ_COARDS from CTM_OPEN_FILE, then CTM_OPEN_FILE 
;             ; will append FILEINFO and DATAINFO to the GAMAP global
;             ; common block.
;
; MODIFICATION HISTORY:
;        bmy, 21 Mar 2005: GAMAP VERSION 2.03
;        bmy, 21 Jul 2005: GAMAP VERSION 2.04
;                          - bug fix in CRC_SAVE_DATA
;        bmy, 06 Mar 2006: GAMAP VERSION 2.05
;                          - minor bug fix in CRC_READ_GLOBAL_ATTS
;                          - bug fix in CRC_SAVE_DATA: add a fake 4th
;                            dim to DATA array if needed
;  bmy & phs, 13 Jul 2007: GAMAP VERSION 2.10
;        bmy, 16 Sep 2008: GAMAP VERSION 2.13
;                          - Convert some global attributes to number
;                            types in case attributes were initially
;                            saved to the netCDF file as strings
;        phs, 15 Sep 2009: - Added check on reading tracerinfo.dat
;                          - Convert some tracer names created by
;                            BPCH2COARDS to their original value.
;        bmy, 14 Dec 2011: GAMAP VERSION 2.16
;                          - Updated to read the GAMAP category value
;                            from a netCDF variable attribute (if present)
;        bmy, 19 Dec 2011: - Generalized to handle several different
;                            vertical levels
;        bmy, 21 Dec 2011: - Now will interpret netCDF attributes
;                            "begin_date" and "begin_time" in the same
;                            way as "start_date" and "start_time"
;        bmy, 22 Dec 2011: - Now compute FIRST (nested datablock offsets)
;                            properly for nested grids.  For now assume
;                            that the data will always start on the first
;                            vertical level.
;                          - Bug fix: test for Latrev gt 0 to avoid
;                            INADVERTENTLY reversing latitudes
;        bmy, 03 Jan 2012: - Skip over Ap and Bp index arrays
;                          - Now use better error checks for
;                            the time and vertical level dimensions
;                            for each tracer in the netCDF file.
;        bmy, 05 Jan 2012: - Now interpret DELTA_TIME attribute
;                            correctly when specified in hhmmss format.
;        bmy, 10 Jan 2012: - Fix to interpret data blocks with multiple 
;                            vertical dimensions in the same file 
;        bmy, 13 Jan 2012: - When the time dimension in the netCDF
;                            file is 1 (esp. when time is an UNLIMITED
;                            variable, we need to add a fake dimension
;                            of 1 back onto the data block to avoid
;                            crashes.  This is a quirk in how the IDL
;                            NCDF_VARGET function works.
;        bmy, 23 Jan 2017: GAMAP VERSION 2.19
;                          - Add modifications to read netCDF restart files
;
;-
; Copyright (C) 2005-2017
; Bob Yantosca and Philippe Le Sager, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.harvard.edu with subject "IDL routine ctm_read_coards"
;-----------------------------------------------------------------------


pro CRC_Get_DimInfo, fId, Info, DimStru

   ;====================================================================
   ; Internal routine CRC_GET_DIMINFO returns the dimension names
   ; and sizes from the COARDS netCDF file.  We will assume that there
   ; are dimensions for longitude, latitude, levels, and time, which
   ; is true of most COARDS-compliant netCDF files. 
   ; (bmy, 3/21/05, 12/19/11)
   ;
   ; NOTES:
   ; (1) Added XDim, YDim, ZDim, TDim to list
   ; (2) Generalized; now return dimensions via an array of IDL
   ;      structures.  This allows for multiple vertical dimenisons.
   ;      (bmy, 12/19/11)
   ; (3) Add index to the DIMSTRU structure
   ;====================================================================

   ; Structure template
   Template =  { CRC_DimStru,  $
                 Name  : '',   $
                 Type  : '',   $
                 Size  : 0L,   $
                 Index : 0L   }
                
   ; Array of structures
   DimStru = Replicate( Template, Info.NDims )

   ; Loop over dimension names
   for dId = 0L, Info.NDims-1L do begin
   
      ; Get information about each dimension
      NCDF_DimInq, fId, dId, Name, Size

      ; Strip white space and make name lowercase
      Name = StrLowCase( StrTrim( Name, 2 ) )

      ; Store info about longitude dimensions
      if ( ( StrPos( Name, 'lon' )  ge 0     )   OR $
           ( StrPos( Name, 'x'   )  ge 0     ) ) then begin
         DimStru[dId].Name  = Name
         DimStru[dId].Type  = 'longitude'
         DimStru[dId].Size  = Size
         DimStru[dId].Index = dId
      endif

      ; Store info about latitude dimensions
      if ( ( StrPos( Name, 'lat' )  ge 0     )   OR $
           ( StrPos( Name, 'y'   )  ge 0     ) ) then begin
         DimStru[dId].Name  = Name
         DimStru[dId].Type  = 'latitude'
         DimStru[dId].Size  = Size
         DimStru[dId].Index = dId
      endif

      ; Store info about altitude dimensions
      if ( ( StrPos( Name, 'lev' )  ge 0     )   OR $
           ( StrPos( Name, 'edge')  ge 0     )   OR $
           ( StrPos( Name, 'alt' )  ge 0     ) ) then begin
         DimStru[dId].Name  = Name
         DimStru[dId].Type  = 'level centers'
         DimStru[dId].Size  = Size
         DimStru[dId].Index = dId
      endif

      ; Store info about altitude dimensions
      if ( ( StrPos( Name, 'edge')  ge 0     ) ) then begin
         DimStru[dId].Name  = Name
         DimStru[dId].Type  = 'level edges'
         DimStru[dId].Size  = Size
         DimStru[dId].Index = dId
      endif

      ; Store info about time dimensions
      if ( ( StrPos( Name, 'time' ) ge 0     )   OR $
           ( Name                   eq 't'   )   OR $
           ( Name                   eq 'tdim') ) then begin
         DimStru[dId].Name  = Name
         DimStru[dId].Type  = 'time'
         DimStru[dId].Size  = Size
         DimStru[dId].Index = dId
      endif

   endfor
end

;------------------------------------------------------------------------------

pro CRC_Get_IndexVars, fID, Info, DimStru, Lon,  Lat,      Lev,  $
                                  Edge,    Time, LonShift, LatRev

   ;====================================================================
   ; Internal routine CRC_GET_INDEXVARS returns the index variables
   ; (lon, lat, lev, time) from the COARDS netCDF file. (bmy, 3/21/05)
   ;
   ; The COARDS standard requires that index variables have the same
   ; name as their corresponding dimension.  Therefore all we have to
   ; do is to loop through each variable and see if its name matches
   ; one of the file dimensions. 
   ;
   ; The COARDS specification also states that one must explicitly
   ; include the "units" attribute for the time variable, which will
   ; define the zero point from which forward time is reckoned.  We 
   ; will use this to convert time data to TAU values (hours since
   ; 00 GMT on 1/1/1985).
   ;
   ; For GAMAP the first longitude must be on the date line, so the 
   ; LONSHIFT flags denotes if we have to shift the latitudes before 
   ; reading into the GAMAP common block.  GAMAP also assumes that 
   ; latitudes go from S -> N.  Therefore set the LONSHIFT and LATREV
   ; flags if we have to reorder longitude & latitudes.
   ;
   ; NOTES:
   ; (1) Totally rewrote in order to better handle multiple vertical
   ;      dimensions (bmy, 12/19/11)
   ; (2) Bug fix: test for Latrev gt 0 to avoid reversing nested
   ;      grid latitudes inadvertently (bmy, 12/22/11)
   ;====================================================================
   
   ; Initialize
   Lon      = -1L
   LonShift = -1L
   Lat      = -1L
   LatRev   = -1L
   Lev      = -1L
   Time     = -1L
   Found    =  0L

   ; Loop over variables
   for vId = 0L, Info.NVars-1L do begin
      
      ; Structure w/ info about this variable
      VarInfo = NCDF_VarInq( fId, vId )

      ; Variable name
      VarName = StrLowCase( StrTrim( VarInfo.Name, 2 ) )

      ; Loop over dimensions
      for dId = 0L, Info.NDims-1L do begin
         
         ; Array of dimension names in the netCDF file
         DimName = DimStru[dId].Name
         DimType = DimStru[dId].Type

         ; NOTE: The COARDS standard requires that each index array have
         ; the same name as its defining dimension.  Therefore, we need
         ; to match up each index array with its corresponding dimension.
         if ( DimName eq VarName ) then begin

            ;-----------------------------------------------------------
            ; Special handling for LONGITUDE: If the attribute
            ; specifies "degrees east",  then we need to put 
            ; longitude in the range of [-180,180] for GAMAP
            ;-----------------------------------------------------------
            if ( DimType eq 'longitude' ) then begin
               
               ; Get LON data
               NCDF_VarGet, fId, vId, Lon
               
               ; Do we have to shift the grid?
               if ( Lon[0] eq 0.0 ) then begin
                  LonShift = DimStru[did].Size / 2L
               
                  ; Convert [0,360] to the range of [-180,180]
                  Ind = Where( Lon gt 180.0 )
                  if ( Ind[0] ge 0 ) then Lon[Ind] = Lon[Ind] - 360.0
               
                  ; Represent 180 as -180
                  if ( Lon[0] gt 0 ) then Lon[0] = -Lon[0]
               endif

               ; Increment counter
               Found =  Found + 1

            endif $

            ;-----------------------------------------------------------
            ; Special handling for LATITUDE: GAMAP requires latitude
            ; to go from S -> N.  Reverse if necessary.
            ;-----------------------------------------------------------
            else if ( DimType eq 'latitude' ) then begin
               
               ; Get LAT data
               NCDF_VarGet, fId, vId, Lat
               
               ; If latitudes go from N -> S we need to reverse them
               if ( Lat[1] - Lat[0] lt 0 ) then LatRev = 1L
               
               ; Reverse latitudes
               if ( LatRev gt 0 ) then Lat = Reverse( Lat )

               ; Increment counter
               Found =  Found + 1

            endif $

            ;-----------------------------------------------------------
            ; LEVEL CENTERS
            ;-----------------------------------------------------------
            else if ( DimType eq 'level centers' ) then begin

               ; Read LEV data from file
               NCDF_VarGet, fId, vId, Lev

               ; Increment counter
               Found = Found + 1L
 
            endif $

            ;-----------------------------------------------------------
            ; LEVEL EDGES
            ;-----------------------------------------------------------
            else if ( DimType eq 'level edges' ) then begin

               ; Read data from file
               NCDF_VarGet, fId, vId, Edge

               ; Increment counter
               Found = Found + 1L

            endif $

            ;-----------------------------------------------------------
            ; Special handling for TIME: Make sure that it is a TAU 
            ; value (i.e. consecutive hours since 00:00 GMT on 
            ; 01/01/1985),  For now assume TIME is specified as GMT.  
            ;----------------------------------------------------------
            else if ( DimType eq 'time' ) then begin

               ; Get the TIME data
               NCDF_VarGet, fId, vId, Time
               
               ; COARDS requires a UNITS string for the TIME variable 
               NCDF_AttGet, fId, vId, 'units', Units
               Units    = String( Units )
         
               ; Extract information from the UNITS string
               R        = StrBreak( Units, ' ' )
               UnitStr  = StrLowCase( R[0] )
               DateStr  = R[2]
               HourStr  = R[3]
               
               ; Convert TIME into hours if necessary
               case ( UnitStr ) of
                  'seconds' : Time = Time / 3600.0
                  'minutes' : Time = Time / 60.0
                  'days'    : Time = Time * 24.0
                  else      : ; nothing
               endcase
               
               ; Convert starting date of data epoch to YYYYMMDD format
               R        = Long( StrBreak( DateStr, '-' ) )
               NYMD0    = Long( R[0]*10000L + R[1]*100L + R[2] )
               
               ; Convert starting time of data epoch to HHMMSS format
               R        = Long( StrBreak( HourStr, ':' ) )
               NHMS0    = Long( R[0]*10000L + R[1]*100L + R[2] )
               
               ; Compute TAU value at start of data epoch
               EpochTau = Nymd2Tau( NYMD0, NHMS0, /No_Y2K )
               
               ; Add TAU value at start of data epoch to TIME
               ; TIME is now a TAU value from 1985/01/01
               for T=0L, DimStru[dId].Size-1L do begin
                  Time[T] = Time[T] + EpochTau
               endfor

            endif

            ;-----------------------------------------------------------
            ; Exit after reading all 5 index arrays from disk
            ;-----------------------------------------------------------
            if ( Found eq 5 ) then goto, Quit

         endif

      endfor

      ; Undefine the structure
      UnDefine, VarInfo

   endfor
  
   ;--------------------------------------------------------------------
   ; Cleanup and return
   ;--------------------------------------------------------------------
Quit:

end

;------------------------------------------------------------------------------

pro CRC_Read_Global_Atts, fId, Info, Lon,        Lat,        $
                                     Lev,        Delta_Time, $
                                     ModelInfo,  GridInfo,   $
                                     First

   ;====================================================================
   ; Internal routine CRC_READ_GLOBAL_ATTS reads information from
   ; the global attributes of the netCDF file.  This information is 
   ; used to define the start & end time, as well as the model grid.
   ; (bmy, 3/21/05)
   ;
   ; For GEOS-CHEM or other model files created by BPCH2COARDS, then
   ; it will read model information from global attributes.  For COARDS
   ; files from other sources it will assume a generic grid.  This 
   ; should be good enough for most applications.
   ;
   ; NOTES:
   ; (1) Bug fix: Lev eq -1 should be Lev[0] eq -1 (bmy, 3/6/06)
   ; (2) Now read start_date, start_time, end_date, end_time 
   ;      global attributes. (bmy, 12/20/11)
   ; (3) Add "begin_date", "begin_time" as synonyms for "start_date"
   ;      and start_time. (bmy, 12/21/11)
   ; (4) Added FIRST output array to define data block offsets.
   ;      (bmy, 12/22/11)
   ;====================================================================

   ; Initialize
   Delta_Time = -1L

   ; Loop thru # of global attributes
   for N = 0L, Info.NGAtts-1L do begin

      ; Get name of the Nth global attribute
      AttName = StrTrim( NCDF_AttName( fId, /Global, N  ), 2 )
     
      ; Extract attribute info into IDL variables
      case ( StrLowCase( AttName ) ) of
         'model'      : NCDF_AttGet, fId, /Global, AttName, Model
         'delta_lon'  : NCDF_AttGet, fId, /Global, AttName, DI
         'delta_lat'  : NCDF_AttGet, fId, /Global, AttName, DJ
         'delta_time' : NCDF_AttGet, fId, /Global, AttName, Delta_Time
         'nlayers'    : NCDF_AttGet, fId, /Global, AttName, NLayers
         'start_date' : NCDF_AttGet, fId, /Global, AttName, Start_Date
         'start_time' : NCDF_AttGet, fId, /Global, AttName, Start_Time
         'begin_date' : NCDF_AttGet, fId, /Global, AttName, Start_Date
         'begin_time' : NCDF_AttGet, fId, /Global, AttName, Start_Time
         'end_date'   : NCDF_AttGet, fId, /Global, AttName, End_Date
         'end_time'   : NCDF_AttGet, fId, /Global, AttName, End_Time
         else         : ; Nothing
      endcase
   endfor
  
   ; Fix -- in case the attributes were written as strings (e.g. by
   ; Fortran code), convert them to numeric values here (bmy, 9/16/08)
   ; Make sure DI, DJ, NLayers exist before operating on them (bmy, 1/23/17)
   if ( N_Elements( DI      ) eq 1 ) then DI = Double( String( DI ) )
   if ( N_Elements( DJ      ) eq 1 ) then DJ = Double( String( DJ ) )
   if ( N_Elements( NLayers ) eq 1 ) then begin
      NLayers = Long( String( NLayers ) ) 
   endif else begin
      NLayers = N_ELements( Lev )
   endelse

   ; Get the delta_time increment
   if ( StrLen( StrTrim(Delta_Time,1) ) ge 5 ) then begin

      ; If DELTA_TIME is hhmmss, convert to decimal hours
      HHMMSS = Long( String( Delta_Time ) )
      Date2Ymd, HHMMSS, HH, MM, SS
      Delta_Time = Double( HH ) + Double( MM )/60D + Double( SS )/3600D
    
   endif else begin

      ; Otherwise assume DELTA_TIME is in hours
      Delta_Time = Double( String( Delta_Time ) )

   endelse

   ;====================================================================
   ; Attempt to obtain information about the grid or starting/ending
   ; times in alternate ways if global attributes aren't defined
   ;====================================================================

   ; Convert MODEL from BYTE to STRING
   if ( N_Elements( Model ) gt 0 ) then begin
      Model = StrUpCase( StrTrim( Model, 2 ) )
   endif else begin
      Model = 'generic'
   endelse

   ; Use lon & lat dimensions if we can't find DI & DJ tags
   if ( N_Elements( DI ) eq 0 ) then DI = Abs( Lon[3] - Lon[2] )
   if ( N_Elements( DJ ) eq 0 ) then DJ = Abs( Lat[3] - Lat[2] )

   ; If NLAYERS is undefined, then look to the MODEL string
   if ( N_Elements( NLayers ) eq 0 ) then begin
      if ( StrPos( Model, '_30L' ) ge 0 ) then NLayers = 30
      if ( StrPos( Model, '_47L' ) ge 0 ) then NLayers = 47
   endif

   ; If there is no LEVEL information then set NLAYERS=0
   if ( Lev[0] eq -1L ) then NLayers = 0
 
   ; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   ; %%% KLUDGE: In case the MODEL attribute is missing, guess the    %%%
   ; %%% vertical grid  from the number of layers. (bmy, 1/23/17)     %%%
   ; %%%                                                              %%%
   ; %%% NOTE: MERRA-2, MERRA, GEOS-5 have the same grid as GEOS-FP.  %%%
   ; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   case ( NLayers ) of
      72:   Model = 'GEOS-FP'
      47:   Model = 'GEOS-FP'
      55:   Model = 'GEOS-4'
      30:   Model = 'GEOS-4'
      else: Model = 'generic'
   endcase

   ; Define MODELINFO and GRIDINFO structures
   if ( NLayers gt 0 ) then begin

      ;---------------------------------
      ; Grid has vertical structure
      ;---------------------------------

      ; Define MODELINFO
      if ( Model eq 'generic' ) then begin
         ModelInfo = CTM_Type( Model, Res=[DI,DJ], NLay=NLayers, /Center180 )
      endif else begin
         ModelInfo = CTM_Type( Model, Res=[DI,DJ], NLay=NLayers )
      endelse

      ; Define GRIDINFO
      GridInfo = CTM_Grid( ModelInfo )

   endif else begin
      
      ;---------------------------------
      ; Grid has no vertical structure
      ;---------------------------------

      ; Define MODELINFO
      if ( Model eq 'generic' ) then begin
         ModelInfo = CTM_Type( Model, Res=[DI,DJ], /Center180 )
      endif else begin
         ModelInfo = CTM_Type( Model, Res=[DI,DJ]  )
      endelse

      ; Define 2-D GRIDINFO
      GridInfo = CTM_Grid( ModelInfo, /No_Vertical )
   endelse

   ; Flags for defining the starting epoch for NYMD2TAU
   Is_GEOS = ( ModelInfo.Family eq 'GEOS' OR ModelInfo.Family eq 'MOPITT' )
   Is_GISS = 1L - Is_GEOS

   ;====================================================================
   ; If the grid is nested, then determine the FIRST offsets
   ;====================================================================
   
   if ( N_Elements( Lon ) lt N_Elements( GridInfo.Xmid ) ) then begin

      ; Locate the first element of the data block in longitude
      ; Put into Fortran notation (starting from 1)
      Ind = Where( GridInfo.Xmid eq Lon[0] ) 
      if ( Ind[0] ge 0 )                                   $
        then IFirst = Ind[0] + 1L                          $
        else Message, 'Could not determine IFIRST offset!'

      ; Locate the first element of the data block in latitude
      ; Put into Fortran notation (starting from 1)
      Ind = Where( GridInfo.Ymid eq Lat[0] ) 
      if ( Ind[0] ge 0 )                                   $
        then JFirst = Ind[0] + 1L                          $
        else Message, 'Could not determine JFIRST offset!'

      ; For now, assume LFIRST=1.  Improve this later 
      LFirst = 1L

      ; define the first array
      First  = [ IFirst, JFirst, LFirst ]

   endif
end

;------------------------------------------------------------------------------

pro CRC_Get_Tracer, VarName, Category, TrcName, Tracer
 
   ;====================================================================
   ; Internal routine CRC_GET_TRACER splits the variable name into
   ; a GAMAP category and tracer name.  It also returns the GAMAP
   ; tracer number corresponding to the category & tracer name (as 
   ; defined by the input files "diaginfo.dat" and "tracerinfo.dat").
   ; (bmy, 3/21/05, 12/14/11)
   ;
   ; For COARDS netCDF files which were created from other sources
   ; then the GAMAP category and tracername will not be contained 
   ; within the variable name.  In that case ask the user to supply
   ; a GAMAP category and variable name.
   ;
   ; NOTES:
   ; (1) Modified so that the GAMAP category can now be specified by
   ;      the "gamap_category" netCDF variable attribute (bmy, 12/14/11)
   ;====================================================================

   if ( N_Elements( Category ) eq 1 ) then begin

      ;-----------------------------------------------------------------
      ; CATEGORY is specified with attribute "gamap_category"
      ; (passed from the main routine via the CATEGORY argument)
      ;-----------------------------------------------------------------
      
      ; The GAMAP tracer name is the same as the netCDF variable name
      TrcName = StrTrim( VarName, 2 )

      ; NOTE: Can strip tracername here

   endif else begin

      ; Initialize
      Category = ''
      TrcName  = ''
      
      ; If the file was created by BPCH2COARDS, then look for the "__" 
      ; separating CATEGORY from TRACERNAME.  Don't be spoofed
      ; by category names with multiple '__' in them; always take
      ; the last instance. (bmy, 12/19/11)
      Ind_F = StrPos ( VarName, '__' )
      Ind_R = RStrPos( VarName, '__' )
      Ind   = Max( [ Ind_F, Ind_R ] )
      UnDefine, Ind_F
      UnDefine, Ind_R

      ; If we find "__" ...
      if ( Ind ge 0 ) then begin

         ;-----------------------------------------------------------------
         ; CATEGORY and TRCNAME are part of the netCDF variable name
         ;-----------------------------------------------------------------
         
         ; Split GAMAP diagnostic category and tracer name from VARNAME
         if ( Ind[0] ge 0 ) then begin
            Category = StrMid( VarName, 0, Ind )
            TrcName  = StrMid( VarName, Ind+2, StrLen( VarName )-Ind+2 )
         endif
         
         ; Replace '=S' with '=$' in GAMAP category name
         if ( StrRight( Category, 2 ) eq '=S' ) $
            then StrPut, Category, '=$', StrLen( Category )-2L 
         
         ; Replace '_S' with '-$' in GAMAP category name
         if ( StrRight( Category, 2 ) eq '_S' ) $
            then StrPut, Category, '-$', StrLen( Category )-2L 
   
         ; Replace '-S' with '-$' in GAMAP category name
         if ( StrRight( Category, 2 ) eq '-S' ) $
            then StrPut, Category, '-$', StrLen( Category )-2L 

         ; Also replace the '_' with '-' in GAMAP category name
         Category = StrRepl( Category, '_', '-' )

         ; Reset certain fields which contain '=$' instead of '-$'
         ; (add more fields if necessary)
         case ( Category ) of
            'CO__SRCE' : Category = 'CO--SRCE'
            'CHEM-L-$' : Category = 'CHEM-L=$'
            'PORL-L-$' : Category = 'PORL-L=$'
            'ARSL-L-$' : Category = 'ARSL-L=$'
            'PNOY-L-$' : Category = 'PNOY-L=$'
            'PL-SUL-$' : Category = 'PL-SUL=$'
            'PL-BC-$'  : Category = 'PL-BC=$'
            'PL-OC-$'  : Category = 'PL-OC=$'
            'BXHT-L-$' : Category = 'BXHT-L=$'
            'AIRD-L-$' : Category = 'AIRD-L=$'
            'GMAO-2D'  : Category = 'GMAO-2D'
            'GMAO-3D$' : Category = 'GMAO-3D$'
            else       :           ; Nothing
         endcase

         ; Deal with special tracer names (phs, 09/15/09
         case ( TrcName ) of
            'N_AIR_'   : TrcName = 'N(AIR)'
            'TP_L'     : TrcName = 'TP-L'
            'TP_LEVEL' : TrcName = 'TP-LEVEL'
            'TP_H'     : TrcName = 'TP-H'
            'TP_HGHT'  : TrcName = 'TP-HGHT'
            'TP_P'     : TrcName = 'TP-P'
            'TP_PRESS' : TrcName = 'TP-PRESS'
            'TP_F'     : TrcName = 'TP-F'
            'PS_PTOP'  : TrcName = 'PS-PTOP'
            else       :           ; Nothing
         endcase

      endif else begin

         ;-----------------------------------------------------------------
         ; CATEGORY and TRCNAME aren't part of the netCDF variable name
         ;-----------------------------------------------------------------
         
         ; First assume that the category name is "COARDS-$"
         Category = 'COARDS-$'

         ; Get all tracer names in "tracerinfo.dat"
         CTM_TracerInfo, /All, Name=AllTracerNames
         
         ; Locate this tracer's name in the list of all tracer names
         Ind = Where( AllTracerNames eq StrUpCase( VarName ) )
         if ( Ind[0] ge 0 ) then TrcName = AllTracerNames[Ind] 

         ; Otherwise ask user to supply a CATEGORY and NAME
         if ( TrcName eq '' ) then begin
            
            ; Convert variable name to upper case and strip blanks
            VarUpCase = StrUpCase( StrTrim( VarName, 2 ) )

            ; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            ; %%% KLUDGE #1: Specify category DXYP for variable name AREA %%%
            ; %%% (bmy, 1/23/17)                                          %%%
            ; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if ( VarUpCase eq 'AREA' ) then begin

               ; Give AREA category DXYP and tracer name DXYP (bmy, 1/23/17)
               Category = 'DXYP'
               TrcName  = 'DXYP'

            ; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            ; %%% KLUDGE #2: Specify category IJ-AVG-$ for species whose  %%%
            ; %%% names begin with "SPC_" (bmy, 1/23/17)                  %%%
            ; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            endif else if ( StrPos( VarUpCase, 'SPC_' ) eq 0 ) then begin

               ; Strip the SPC_ from the variable name
               Category = 'IJ-AVG-$'
               TrcName  =  StrMid( VarName, 4, StrLen( VarName ) )
             
            ; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            ; %%% KLUDGE #3: Specify category IJ-AVG-$ for species whose  %%%
            ; %%% names begin with "TRC_" (bmy, 1/23/17)                  %%%
            ; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            endif else if ( StrPos( VarUpCase, 'TRC_' ) eq 0 ) then begin

               ; Strip the SPC_ from the variable name
               Category = 'IJ-AVG-$'
               TrcName  =  StrMid( VarName, 4, StrLen( VarName ) )

            ; If not found, ask user for category and tracer name
            endif else begin

               ; Ask user to supply GAMAP category 
               Prompt   = 'CATEGORY for variable ' + VarName + '? : '
               Read, Category, Prompt=Prompt, Format='(a)'
            
               ; Ask user to supply GAMAP tracer name
               Prompt   = 'TRACER NAME for variable ' + VarName + '? : '
               Read, TrcName, Prompt=Prompt, Format='(a)'

               ; Strip out any quotes that the user may have entered
               Category = StrTrim( StrRepl( Category, "'", " " ), 2 )
               Category = StrTrim( StrRepl( Category, "`", " " ), 2 )
               Category = StrTrim( StrRepl( Category, '"', " " ), 2 )
               TrcName  = StrTrim( StrRepl( TrcName,  "'", " " ), 2 )
               TrcName  = StrTrim( StrRepl( TrcName,  "`", " " ), 2 )
               TrcName  = StrTrim( StrRepl( TrcName,  '"', " " ), 2 )
         
            endelse

         endif

      endelse

   endelse

   ; Get the GAMAP diagnostic offset (from "diaginfo.dat")
   CTM_DiagInfo, Category, Offset=Offset, Spacing=Spacing

   ; Get all tracer numbers corresponding to TRCNAME
   CTM_TracerInfo, TrcName, Index=TrcIndex

   ; Check (phs, 9/15/09)
   if TrcIndex[0] eq -1 then begin
      message, 'Tracer '+ TrcName +'not found in tracerinfo.dat', /info
      print, 'You may need to add an entry in the CASE (TrcName) at ' + $
             'the begining of the routine. Check that Category in ' + $
             'tracerinfo.dat:'
   endif

   ; Find the matching tracer number for this diagnostic category
   Ind    = Where( TrcIndex gt Offset and TrcIndex lt Offset+Spacing, cc)

   if cc eq 0L then begin
      message, 'No matching number in Category '+ Category, /info
      stop
   endif

   Tracer = TrcIndex[Ind]

   ; If for some reason there are more than 2 matching tracer
   ; number (e.g. for regular Ox and Tagged Ox), just take the 1st
   Tracer = Tracer[0]

end
 
;------------------------------------------------------------------------------

pro CRC_Get_Data, fId, vId, VarInfo, Data, LongName, Unit, Category

   ;====================================================================
   ; Internal routine CRC_GET_DATA reads a variable from a netCDF file.
   ; It returns the variable name, data array, long tracer name and
   ; unit string to the main program. (bmy, 3/21/05)
   ;
   ; NOTES:
   ; (1) Now also search for the GAMAP category from the netCDF
   ;      variable attribute named "gamap_category" (bmy, 12/14/11)
   ;====================================================================


   ; Read netCDF variable
   NCDF_VarGet, fId, vId, Data, _EXTRA=e

   ; Read variable attributes
   for N = 0L, VarInfo.NAtts-1L do begin
         
      ; Attribute name
      AttName = NCDF_AttName( fId, vId, N )

      ; Search by attribute name
      case ( StrLowCase( AttName ) ) of

         'long_name' : begin
            NCDF_AttGet, fId, vId, AttName, LongName
            LongName = StrTrim( LongName, 2 )
         end

         'units' : begin
            NCDF_AttGet, fId, vId, AttName, Unit
            Unit = StrTrim( Unit, 2 )
         end

         'unit' : begin
            NCDF_AttGet, fId, vId, AttName, Unit
            Unit = StrTrim( Unit, 2 )
         end

         'gamap_category' : begin
            NCDF_AttGet, fId, vId, AttName, Category
            Category = StrTrim( Category, 2 )
         end

         else : ; Nothing
      endcase
   endfor

   ; Rename units: special cases
   if ( Unit eq 'mb' ) then Unit = 'hPa'
end

;------------------------------------------------------------------------------

pro CRC_Save_Data, InType,       InGrid,      VarInfo,  Time,  $
                   Delta_Time,   DimStru,     FileName, Ilun,  $
                   VarName,      LongName,    Unit,     Data,  $
                   LonShift,     LatRev,      Category, First, $
                   ThisFileInfo, ThisDataInfo

   ;====================================================================
   ; Internal function CRC_SAVE_DATA creates GAMAP-style data blocks 
   ; and returns the THISDATAINFO structure to the main program.
   ; (bmy, 3/21/05, 1/3/11)
   ;
   ; We assume that the lon, lat, dimensions are provided.  If a time
   ; dimension is not provided, we will define a "fake" one for the
   ; purposes of reading into GAMAP.  The altitude dimension is 
   ; determined by the size of the data block.
   ;
   ; Also works for nested-grid data blocks.
   ;
   ; Longitudes and latitudes will be shifted in order to correspond 
   ; with the GAMAP MODELINFO and GRIDINFO structures.
   ;
   ; NOTE: Assumes 
   ;
   ; NOTES:
   ; (1) Bug fix: add a fake 3rd dim to DATA if needed (bmy, 7/21/05)
   ; (2) Bug fix: add a fake 4th dim to DATA if needed (bmy, 3/6/06)
   ; (3) Now add CATEGORY variable (bmy, 12/14/11)
   ; (4) Add the FIRST argument for data block offsets (bmy, 12/22/11)
   ; (5) Special handling when the TIME dimension = 1 (bmy, 1/13/11)
   ;====================================================================

   ;-------------------------------------
   ; Get dimension information
   ;-------------------------------------

   ; Initialize
   DimLon  = -1
   DimLat  = -1
   DimLev  = -1
   DimTime = -1
   IndLon  = -1
   IndLat  = -1
   IndLev  = -1
   IndTime = -1

   ; Find longitude dimension
   Ind     = Where( DimStru[*].Type eq 'longitude' )
   DimLon  = DimStru[Ind].Size
   IndLon  = DimStru[Ind].Index

   ; Find latitude dimension
   Ind     = Where( DimStru[*].Type eq 'latitude'  )
   DimLat  = DimStru[Ind].Size
   IndLat  = DimStru[Ind].Index

   ; Find time dimension
   Ind     = Where( DimStru[*].Type eq 'time'      )
   DimTime = DimStru[Ind].Size
   IndTime = DimStru[Ind].Index

   ;-------------------------------------
   ; Get information about this tracer
   ;-------------------------------------

   ; Split netCDF variable name into CATEGORY and TRACERNAME
   CRC_Get_Tracer, VarName, Category, TrcName, Tracer

   ; Strip extraneous dimensions
   ;Data    = Reform( Data )
   
   ; Size of the DATA array
   SData   = Size( Data, /Dim )
   NData   = N_Elements( SData )

   ; Number of dimensions declared in the netCDF file
   NDims   = VarInfo.NDims

   ;%%% SPECIAL HANDLING!  For some reason, when the time dimension=1,
   ;%%% (or perhaps when the time dimension is unlimited), the
   ;%%% function NCDF_VARGET returns a data block that omits the time 
   ;%%% dimension.   For example a 72x46x1 data block gets returned
   ;%%% into a 72x46 array.  Therefore, for the code to work below, we 
   ;%%% use REFORM to add a "fake" dimension of 1 back onto the data block.
   if ( DimTime eq 1 ) then begin
      if ( NData eq 2 AND NDims eq 3 ) then begin
         Data = Reform( Data, SData[0], SData[1], 1 )
      endif else if ( NData eq 3 AND NDims eq 4 ) then begin
         Data = Reform( Data, SData[0], SData[1], SData[2], 1 )
      endif else if ( NData eq 4 AND NDims eq 5 ) then begin
         Data = Reform( Data, SData[0], SData[1], SData[2], SData[3], 1 )
      endif
   endif

   ; Recompute SDATA and NDATA
   SData = Size( Data, /Dim )
   NData = N_Elements( SData )

   ; Check which netCDF dimensions are defined for this tracer
   IndX    = Where( VarInfo.Dim eq IndLon  )
   IndX    = IndX[0]
   IndY    = Where( VarInfo.Dim eq IndLat  )
   IndY    = IndY[0]
   IndT    = Where( VarInfo.Dim eq IndTime )
   IndT    = IndT[0]

   ; Check that data block longitude dimension 
   ; matches the dimension defined in the netCDF file
   if ( IndX ge 0 ) then begin
      if ( SData[0] ne DimLon ) then Message, 'Invalid LON dimension!'
   endif

   ; Check that data block latitude dimension 
   ; matches the dimension defined in the netCDF file
   if ( IndY ge 0 ) then begin
      if ( SData[1] ne DimLat ) then Message, 'Invalid LAT dimension!'
   endif

   ; Check if there is a time dimension defined in the netCDF file
   if ( IndT ge 0 ) then begin

      ; This tracer has a defined time dimension.  Also test if this 
      ; tracer has a dimension for vertical levels or level edges.
      case ( N_Elements( SData ) ) of

         ;--------------------------------------------------------------
         ; We have 2 spatial dimensions + 1 time dimension, but the
         ; data block only has 2 dimensions.  Therefore, add the 
         ; a time dimension to the data block with REFORM.
         ;--------------------------------------------------------------
         2: begin
            Is_3D = 0L
            Data  = Reform( Data, SData[0], SData[1], 1 )
            SData = Size( Data, /Dim )
            if ( SData[IndT] ne DimTime ) $
               then Message, 'Invalid TIME dimension!'
         end

         ;--------------------------------------------------------------
         ; We have 2 spatial dimensions + 1 time dimension
         ;--------------------------------------------------------------
         3: begin
            Is_3D = 0L
            if ( SData[IndT] ne DimTime ) $
               then Message, 'Invalid TIME dimension!'
         end

         ;--------------------------------------------------------------
         ; We have 3 spatial dimensions + 1 time dimension
         ;--------------------------------------------------------------
         4: begin

            ; Use process of elimination to determine which dimension
            ; of the DATA array is the vertical dimension
            TmpArr = IndGen( NDims )
            IndZ   = Where( TmpArr ne IndX AND $
                            TmpArr ne IndY AND $
                            TmpArr ne IndT )
            
            if ( IndZ ge 0 ) then begin
               Is_3D  = 1L
               DimLev = DimStru[IndZ].Size
               if ( DimLev ne SData[2] ) then DimLev = SData[2]
            endif else begin
               Is_3D  = 0L
            endelse

            ; Check that the data block time dimension
            ; matches the dimension defined in the netCDF file
            if ( SData[IndT] ne DimTime ) $
               then Message, 'Invalid TIME dimension!'
         end

         else: ; Nothing

      endcase

   endif else begin

      ; This tracer does not have a defined time dimension, so we will
      ; create a "fake" one.  Also test if this tracer has a dimension 
      ; for vertical levels or level edges.
      case ( N_Elements( SData ) ) of

         ;--------------------------------------------------------------
         ; We have 2 spatial dimensions, but no time dimension,
         ; so add a "fake" time dimension for use below
         ;--------------------------------------------------------------
         2: begin
            Data  = Reform( Data, SData[0], SData[1], 1 )
            SData = Size( Data, /Dim )
            Is_3D = 0L
         end

         ; We have 3 spatial dimensions but no time dimension,
         ; so add a "fake" time dimension for use below
         3: begin
            Data  = Reform( Data, SData[0], SData[1], SData[2], 1 )
            SData = Size( Data, /Dim )
            Is_3D = 1L
         end

         else : ; Nothing

      endcase

   endelse

   ;--------------------------------------------------------------------
   ; Create a DATAINFO structure in the global GAMAP common block for 
   ; each individual time that is specified in the DATA array
   ;--------------------------------------------------------------------

   ; Get the dimension array for CTM_GET_DATABLOCK
   if ( Is_3D )                                      $
      then Dim = [ DimLon, DimLat, DimLev, DimTime ] $
      else Dim = [ DimLon, DimLat,         DimTime ]  

   ; First-time flag
   FirstTime = 1L

   ; Loop through each time
   for T = 0L, DimTime-1L do begin

      ; Save one time in DATA
      if ( Is_3D ) then begin

         ;--------------------------------------------------------------
         ; Data block w/ 3 spatial dimensions (+ 1 time dimension)
         ;--------------------------------------------------------------

         ; Shift longitudes if necessary
         if ( LonShift gt 0 )                                   $
            then Field = Shift( Data[*,*,*,T], LonShift, 0, 0 ) $
            else Field = Data[*,*,*,T] 

         ; Reverse latitudes if necessary
         if ( LatRev gt 0 ) then Field = Reverse( Field, 2 ) 

      endif else begin

         ;--------------------------------------------------------------
         ; Data block w/ 2 spatial dimensions (+ 1 time dimension)
         ;--------------------------------------------------------------

         ; Shift longitudes if necessary
         if ( LonShift gt 0 )                              $ 
            then Field = Shift( Data[*,*,T], LonShift, 0 ) $
            else Field = Data[*,*,T] 

         ; Reverse latitudes if necessary
         if ( LatRev gt 0 ) then Field = Reverse( Field, 2 ) 

      endelse

      ; Create a DATAINFO structure for this data block,
      ; which will be appended to the GAMAP global common block
      Success  = CTM_Make_DataInfo( Float( Field ),                  $
                                    OneDataInfo,                     $
                                    OneFileInfo,                     $
                                    ModelInfo  = InType,             $
                                    GridInfo   = InGrid,             $
                                    DiagN      = Category,           $
                                    Tracer     = Tracer,             $
                                    TrcName    = TrcName,            $
                                    Tau0       = Time[T],            $
                                    Tau1       = Time[T]+Delta_Time, $
                                    Unit       = Unit,               $
                                    Dim        = Dim,                $
                                    First      = First,              $
                                    FileName   = FileName,           $
                                    FileType   = 203,                $
                                    Format     = 'COARDS netCDF',    $
                                    Ilun       = Ilun,               $
                                    /No_Global )

      ; Error check
      if ( not Success ) then begin            
         S = 'Could not create DATAINFO structure for tracer ' + TrcName
         Message, S
      endif

      ; Save THISFILEINFO for return
      ThisFileInfo = OneFileInfo

      ; Save THISDATAINFO for return
      if ( FirstTime ) then begin
         ThisDataInfo = OneDataInfo
         FirstTime    = 0L
      endif else begin
         ThisDataInfo = [ ThisDataInfo, OneDataInfo ]
      endelse
   
      ; Undefine stuff
      UnDefine, Field
   
   endfor

   ; Undefine stuff 
   UnDefine, Dim
   
   ; Return to main program
   return 
end
 
;------------------------------------------------------------------------------
 
function CTM_Read_COARDS, Ilun, FileName, FileInfo, DataInfo, _EXTRA=e
 
   ;====================================================================
   ; Initialization
   ;====================================================================

   ; External functions
   FORWARD_FUNCTION CTM_Type, CTM_Grid, CTM_Make_DataInfo, $
                    Nymd2Tau, StrRight, StrRepl

   ; Keywords
   Debug = Keyword_Set( Debug )
 
   ; Error if FILENAME isn't passed (maybe later we'll
   ; open a DIALOG_PICKFI=LE box to get the filename)
   if ( N_Elements( FileName ) ne 1 ) then Message, 'FILENAME not passed!'

   ; Make sure ILUN is odd -- this will replicate 
   ; the behavior of GAMAP with binary files!
   if ( Ilun mod 2 eq 0 ) then Ilun = Ilun + 1

   ;====================================================================
   ; Open netCDF file: Get dimensions and index variables
   ;====================================================================
 
   ; Test if netCDF library ships w/ this version of IDL
   if ( not NCDF_Exists() ) then begin
      Message, 'netCDF is not supported in this IDL version!', /Continue
      return, -1
   endif
 
   ; Open netCDF file
   fId  = NCDF_Open( Expand_Path( FileName ) )
 
   ; Get a structure w/ the number of dimensions & variables
   Info = NCDF_Inquire( fId )

   ; Get the dimensions from the COARDS netCDF file
   CRC_Get_DimInfo,      fId, Info, DimStru

   ; Get the index variables (lon, lat, lev, edge, time) from the file
   ; Also get flags for shifting longitude & latitude if necessary
   CRC_Get_IndexVars,    fId, Info, DimStru,    Lon,      Lat,    $
                                    Lev,        Edge,     Time,   $
                                    LonShift,   LatRev

   ; Get MODELINFO and GRIDINFO structures from global attributes
   CRC_Read_Global_Atts, fId, Info, Lon,        Lat,      Lev,    $     
                                    Delta_Time, InType,   InGrid, $
                                    First

   ; If DELTA_TIME is undefined, compute it from 2 successive time values
;------------------------------------------------------------------------------
; Prior to 1/23/17:
; Make sure at least 2 elements of TIME exist before trying to compute it;
; otherwise, it will default to -1L as defined above. (bmy 1/23/17)
;   if ( Delta_Time lt 0 ) then Delta_Time = Time[1] - Time[0]
;------------------------------------------------------------------------------
   if ( N_Elements( Time ) ge 2 ) then begin
      if ( Delta_Time lt 0 ) then Delta_Time = Time[1] - Time[0]
   endif

   ;====================================================================
   ; Loop over all variables and save to DATAINFO & FILEINFO structures
   ;====================================================================

   ; First time flag
   FirstTime = 1L

   ; Loop over all netCDF variables
   for vId = 0L, Info.NVars-1L do begin

      ; Get structure w/ info about this variable
      VarInfo = NCDF_VarInq( fId, vId )

      ; Variable name
      VarName = VarInfo.Name

      ; Skip over index arrays for hybrid grid
      case ( StrLowCase( StrTrim( VarName, 2 ) ) ) of
         'ap' : goto, Next
         'bp' : goto, Next
         'ak' : goto, Next
         'bk' : goto, Next
         else : ; Nothing
      endcase

      ; Skip over index arrays: LON, LAT, LEV, EDGE, TIME
      Ind = Where ( DimStru[*].Name eq VarName )
      if ( Ind[0] ge 0 ) then goto, Next

      ; Read data from a variable
      CRC_Get_Data, fId, vId, VarInfo, Data, LongName, Unit, Category
      
      ; Get GAMAP-style THISFILEINFO and THISDATAINFO structures
      CRC_Save_Data, InType,       InGrid,      VarInfo,   Time,  $
                     Delta_Time,   DimStru,     FileName,  Ilun,  $
                     VarName,      LongName,    Unit,      Data,  $
                     LonShift,     LatRev,      Category,  First, $
                     ThisFileInfo, ThisDataInfo

      ; Append THISFILEINFO and THISDATAINFO into the
      ; FILEINFO and DATAINFO arrays of structures
      if ( FirstTime ) then begin
         FileInfo  = ThisFileInfo
         DataInfo  = ThisDataInfo        
         FirstTime = 0L
      endif else begin
         DataInfo  = [ DataInfo, ThisDataInfo ]
      endelse

      ; Undefine stuff
      UnDefine, Data
      UnDefine, LongName
      UnDefine, Unit
      UnDefine, Category
Next: 
      UnDefine, VarName
      UnDefine, VarInfo

   endfor

   ;====================================================================
   ; Cleanup and quit
   ;====================================================================
 
   ; Close netCDF file
   NCDF_Close, fId

   ; Return NEWDATAINFO to calling program
   return, 1
end
 
