;-----------------------------------------------------------------------
;+
; NAME:
;        GETETA
;
; PURPOSE:
;        Defines the eta levels for the various hybrid model grids.
;        GETETA is called by function CTM_GRID.
;
; CATEGORY:
;        GAMAP Internals
;
; CALLING SEQUENCE:
;        RESULT = GETETA( MNAME [, NLAYERS [, Keywords ] ] )
;
; INPUTS:
;        MNAME -> The name of the model for which eta level
;             information is desired ('GEOS4' or 'GEOS4_30L').
;
;        NLAYERS -> Specifies the number of eta layers for the 
;             model type.  Default is 55 layers.
;
; KEYWORD PARAMETERS:
;        PSURF -> Surface pressure in hPa.  If PSURF is not specified,
;             GETETA will use 1013.25 hPa (e.g. 1 atmosphere).
;
;        /CENTERS -> Returns to the calling program an array 
;             containing the eta coordinate at grid box centers.
;
;        /EDGES -> Returns to the calling program an array 
;             containing the eta coordinate at grid box edges.
;
;        A -> Returns to the calling program the "A" vector of
;             values that define the hybrid grid.  A has units of
;             [hPa].
;
;        B -> Returns to the calling program the "B" vector of
;             values that define the hybrid grid.  B is unitless.
;
;        PRESSURE -> Returns the pressure [hPa] at the grid box edges 
;             (if /EDGES is set) or the pressure at the grid box centers
;             (if /CENTERS is set).
;
; OUTPUTS:
;        RESULT -> contains the array of eta edges (if /EDGES is
;             set), or eta centers (if /CENTERS is set).
;
; SUBROUTINES:
;        None
;
; REQUIREMENTS:
;        Called by CTM_GRID.PRO
;
; NOTES:
;        Supported models:
;        -----------------------------------------------------------
;        (1 ) GCAP,         23-layer (name: "GCAP"         )
;        (2 ) GEOS-4,       55-layer (name: "GEOS4"        )
;        (3 ) GEOS-4        30-layer (name: "GEOS4_30L"    )
;        (4 ) GEOS-5,       72-layer (name: "GEOS5"        )
;        (5 ) GEOS-5        47-layer (name: "GEOS5_47L"    )
;        (6 ) GEOS-FP       72-layer (name: "GEOSFP"       )
;        (7 ) GEOS-FP       47-layer (name: "GEOSFP_47L"   )
;        (8 ) MERRA,        72-layer (name: "MERRA"        )
;        (9 ) MERRA         47-layer (name: "MERRA_47L"    )
;        (10) MERRA2,       72-layer (name: "MERRA2"       )
;        (11) MERRA2,       47-layer (name: "MERRA2_47L"   )
;        (12) GISS_II_PRIME 23-layer (name: "GISS_II_PRIME")
;        (13) MATCH         52-layer (name: "MATCH"        )
;
;        Computing pressure and eta coordinates:
;        -----------------------------------------------------------
;        In a vertical column, the pressure at the bottom edge of 
;        grid box (L) is given by:
;
;           Pe(L)   = A(L) + ( B(L) * Psurface )
;
;        and the pressure at the vertical midpoint of grid box (L)
;        is just a simple average of the pressures at the box edges:
;
;           Pc(L)   = ( Pe(L) + Pe(L+1) ) * 0.5
;
;        From PEDGE and PCENTER, we can construct the unitless coordinate 
;        ETA (which is very similar to the sigma coordinate) as follows:
;
;           ETAe(L) = ( Pe(L) - Ptop ) / ( Psurface - Ptop ) 
;
;           ETAc(L) = ( Pc(L) - Ptop ) / ( Psurface - Ptop )
; 
;        For GAMAP plotting routines, we will use the ETA coordinate 
;        for hybrid models instead of the sigma coordinate.
;
;        Psurface is a function of longitude and latitude.  GAMAP uses 
;        a default Psurface of 1013 hPa (1 atm).  However, you should 
;        always compute the pressure edges and centers from an accurate 
;        surface pressure field (i.e. from met field data files or from 
;        GEOS-Chem or other model output.
;   
; EXAMPLE:
;        EETA = GETETA( 'GEOS4' PSURF=984.0, /EDGES );
;             ; assigns GEOS-1 eta edges to array EETA
;
;        CETA = GETETA( 'GEOS4', /CENTERS, PRESSURE=CPRESS )
;             ; assigns GISS-II eta centers to array CETA
;             ; (Optional) also returns the pressure at level centers
;
; MODIFICATION HISTORY:
;        bmy, 06 Nov 2001: GAMAP VERSION 1.49
;                          - based on routine "getsigma.pro"
;        bmy, 04 Nov 2003: GAMAP VERSION 2.01
;                          - now supports "GEOS4_30L" grid
;                          - now tests for model name using STRPOS 
;                            instead of just a straight match
;                          - stop w/ an error for non-hybrid grids
;                          - now supports 23-layer GISS_II_PRIME model
;                          - now supports 52-layer MATCH model
;        bmy, 18 Jun 2004: GAMAP VERSION 2.02a
;                          - now supports GCAP 23-layer hybrid grid
;  bmy & phs, 13 Jul 2007: GAMAP VERSION 2.10
;                          - Updated comments
;        bmy, 15 Oct 2009: GAMAP VERSION 2.13
;                          - Now supports GEOS-5 grids    
;        bmy, 29 Nov 2010: GAMAP VERSION 2.15
;                          - Now returns hybrid-grid A and B 
;                            values via the A & B keywords      
;                          - Now returns the pressure corresponding 
;                            to ETA via the PRESSURE keyword 
;                          - Renamed /CENTER to /CENTERS  
;
;-
; Copyright (C) 2001-2010,
; Bob Yantosca and Philippe Le Sager, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of this 
; software. If this software shall be used commercially or sold as
; part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.as.harvard.edu with subject "IDL routine geteta"
;-----------------------------------------------------------------------


function GetEta, MName,       NLayers,                           $         
                 Psurf=Psurf, Centers=Centers, Edges=Edges,      $
                 A=A,         B=B,             Pressure=Pressure
 
   ; Keyword settings
   Edges  = Keyword_Set( Edges  )
   Center = Keyword_Set( Center )

   ; Default: pick centers
   if ( Center + Edges eq 0 ) then Center = 1 

   ;---------------------------------------------------------------------
   ; Prior to 11/29/10:
   ; Use global average surface pressure if PSURF is not passed
   ;if ( N_Elements( PSurf ) ne 1 ) then PSurf = 984.0
   ;---------------------------------------------------------------------

   ; Set default PSURF to 1 atmosphere (for consistency w/ CTM_TYPE)
   if ( N_Elements( PSurf ) ne 1 ) then PSurf = 1013.25
 
   ; Save model name
   ModelName = StrUpCase( StrTrim( MName, 2 ) )

   ;====================================================================
   ; GEOS-4 model grid
   ;====================================================================  
   if ( StrPos( ModelName, 'GEOS4' ) ge 0 ) then begin
 
      ; Model top pressure [hPa]
      PTop = 0.01
 
      ; Set default # of model layers
      if ( N_Elements( NLayers ) ne 1 ) then NLayers = 55
 
      ;-------------------------
      ; 55 vertical layer grid
      ;-------------------------
      if ( NLayers eq 55 ) then begin
 
         ; A parameter [hPa]
         A = [   0.000000,      0.000000,     12.704939,     35.465965, $ 
                66.098427,    101.671654,    138.744400,    173.403183, $
               198.737839,    215.417526,    223.884689,    224.362869, $ 
               216.864929,    201.192093,    176.929993,    150.393005, $ 
               127.837006,    108.663429,     92.365662,     78.512299, $ 
                66.603378,     56.387939,     47.643932,     40.175419, $ 
                33.809956,     28.367815,     23.730362,     19.791553, $ 
                16.457071,     13.643393,     11.276889,      9.292943, $ 
                 7.619839,      6.216800,      5.046805,      4.076567, $ 
                 3.276433,      2.620212,      2.084972,      1.650792, $ 
                 1.300508,      1.019442,      0.795134,      0.616779, $ 
                 0.475806,      0.365041,      0.278526,      0.211349, $ 
                 0.159495,      0.119703,      0.089345,      0.066000, $ 
                 0.047585,      0.032700,      0.020000,      0.010000 ]
         
         ; B parameter [unitless]
         B = [   1.000000,      0.985110,      0.943290,      0.867830, $ 
                 0.764920,      0.642710,      0.510460,      0.378440, $ 
                 0.270330,      0.183300,      0.115030,      0.063720, $ 
                 0.028010,      0.006960,      0.000000,      0.000000, $ 
                 0.000000,      0.000000,      0.000000,      0.000000, $ 
                 0.000000,      0.000000,      0.000000,      0.000000, $ 
                 0.000000,      0.000000,      0.000000,      0.000000, $ 
                 0.000000,      0.000000,      0.000000,      0.000000, $ 
                 0.000000,      0.000000,      0.000000,      0.000000, $ 
                 0.000000,      0.000000,      0.000000,      0.000000, $ 
                 0.000000,      0.000000,      0.000000,      0.000000, $ 
                 0.000000,      0.000000,      0.000000,      0.000000, $ 
                 0.000000,      0.000000,      0.000000,      0.000000, $ 
                 0.000000,      0.000000,      0.000000,      0.000000 ]

      ;-------------------------
      ; 30-layer vertical grid
      ;-------------------------
      endif else if ( NLayers eq 30 ) then begin

         ; A parameter [hPa]
         A = [   0.000000,      0.000000,     12.704939,     35.465965, $
                66.098427,    101.671654,    138.744400,    173.403183, $
               198.737839,    215.417526,    223.884689,    224.362869, $
               216.864929,    201.192093,    176.929993,    150.393005, $
               127.837006,    108.663429,     92.365662,     78.512299, $
                56.387939,     40.175419,     28.367815,     19.791553, $
                 9.292943,      4.076567,      1.650792,      0.616779, $
                 0.211349,      0.066000,      0.010000 ]

         ; B parameter [unitless]
         B = [   1.000000,      0.985110,      0.943290,      0.867830, $
                 0.764920,      0.642710,      0.510460,      0.378440, $
                 0.270330,      0.183300,      0.115030,      0.063720, $
                 0.028010,      0.006960,      0.000000,      0.000000, $
                 0.000000,      0.000000,      0.000000,      0.000000, $
                 0.000000,      0.000000,      0.000000,      0.000000, $
                 0.000000,      0.000000,      0.000000,      0.000000, $
                 0.000000,      0.000000,      0.000000 ]

      endif

   endif $

   ;====================================================================
   ; GEOS-5 / MERRA model grids
   ;====================================================================  
   else if ( ( StrPos( ModelName, 'GEOS5'  ) ge 0 )   OR $
             ( StrPos( ModelName, 'MERRA'  ) ge 0 )   OR $
             ( StrPos( ModelName, 'MERRA2' ) ge 0 )   OR $
             ( StrPos( ModelName, 'GEOSFP' ) ge 0 ) ) then begin
 
      ; Model top pressure [hPa]
      PTop = 0.01
 
      ; Set default # of model layers
      if ( N_Elements( NLayers ) ne 1 ) then NLayers = 72

      ;-------------------------
      ; 72 vertical layer grid
      ;-------------------------
      if ( NLayers eq 72 ) then begin
 
         ; A [hPa] for 72 levels (73 edges)
         A = [ 0.000000E+00, 4.804826E-02, 6.593752E+00, 1.313480E+01, $
               1.961311E+01, 2.609201E+01, 3.257081E+01, 3.898201E+01, $
               4.533901E+01, 5.169611E+01, 5.805321E+01, 6.436264E+01, $
               7.062198E+01, 7.883422E+01, 8.909992E+01, 9.936521E+01, $
               1.091817E+02, 1.189586E+02, 1.286959E+02, 1.429100E+02, $
               1.562600E+02, 1.696090E+02, 1.816190E+02, 1.930970E+02, $
               2.032590E+02, 2.121500E+02, 2.187760E+02, 2.238980E+02, $
               2.243630E+02, 2.168650E+02, 2.011920E+02, 1.769300E+02, $
               1.503930E+02, 1.278370E+02, 1.086630E+02, 9.236572E+01, $
               7.851231E+01, 6.660341E+01, 5.638791E+01, 4.764391E+01, $
               4.017541E+01, 3.381001E+01, 2.836781E+01, 2.373041E+01, $
               1.979160E+01, 1.645710E+01, 1.364340E+01, 1.127690E+01, $
               9.292942E+00, 7.619842E+00, 6.216801E+00, 5.046801E+00, $
               4.076571E+00, 3.276431E+00, 2.620211E+00, 2.084970E+00, $
               1.650790E+00, 1.300510E+00, 1.019440E+00, 7.951341E-01, $
               6.167791E-01, 4.758061E-01, 3.650411E-01, 2.785261E-01, $
               2.113490E-01, 1.594950E-01, 1.197030E-01, 8.934502E-02, $
               6.600001E-02, 4.758501E-02, 3.270000E-02, 2.000000E-02, $
               1.000000E-02 ]

         ; B [unitless] for 72 levels (73 edges)
         B = [ 1.000000E+00, 9.849520E-01, 9.634060E-01, 9.418650E-01, $
               9.203870E-01, 8.989080E-01, 8.774290E-01, 8.560180E-01, $
               8.346609E-01, 8.133039E-01, 7.919469E-01, 7.706375E-01, $
               7.493782E-01, 7.211660E-01, 6.858999E-01, 6.506349E-01, $
               6.158184E-01, 5.810415E-01, 5.463042E-01, 4.945902E-01, $
               4.437402E-01, 3.928911E-01, 3.433811E-01, 2.944031E-01, $
               2.467411E-01, 2.003501E-01, 1.562241E-01, 1.136021E-01, $
               6.372006E-02, 2.801004E-02, 6.960025E-03, 8.175413E-09, $
               0.000000E+00, 0.000000E+00, 0.000000E+00, 0.000000E+00, $
               0.000000E+00, 0.000000E+00, 0.000000E+00, 0.000000E+00, $
               0.000000E+00, 0.000000E+00, 0.000000E+00, 0.000000E+00, $
               0.000000E+00, 0.000000E+00, 0.000000E+00, 0.000000E+00, $
               0.000000E+00, 0.000000E+00, 0.000000E+00, 0.000000E+00, $
               0.000000E+00, 0.000000E+00, 0.000000E+00, 0.000000E+00, $
               0.000000E+00, 0.000000E+00, 0.000000E+00, 0.000000E+00, $
               0.000000E+00, 0.000000E+00, 0.000000E+00, 0.000000E+00, $
               0.000000E+00, 0.000000E+00, 0.000000E+00, 0.000000E+00, $
               0.000000E+00, 0.000000E+00, 0.000000E+00, 0.000000E+00, $
               0.000000E+00 ]

      ;-------------------------
      ; 47-layer vertical grid
      ;-------------------------
      endif else if ( NLayers eq 47 ) then begin

         ; A [hPa] for 47 levels (48 edges)
         A = [ 0.000000E+00, 4.804826E-02, 6.593752E+00, 1.313480E+01, $
               1.961311E+01, 2.609201E+01, 3.257081E+01, 3.898201E+01, $
               4.533901E+01, 5.169611E+01, 5.805321E+01, 6.436264E+01, $
               7.062198E+01, 7.883422E+01, 8.909992E+01, 9.936521E+01, $
               1.091817E+02, 1.189586E+02, 1.286959E+02, 1.429100E+02, $
               1.562600E+02, 1.696090E+02, 1.816190E+02, 1.930970E+02, $
               2.032590E+02, 2.121500E+02, 2.187760E+02, 2.238980E+02, $
               2.243630E+02, 2.168650E+02, 2.011920E+02, 1.769300E+02, $
               1.503930E+02, 1.278370E+02, 1.086630E+02, 9.236572E+01, $
               7.851231E+01, 5.638791E+01, 4.017541E+01, 2.836781E+01, $
               1.979160E+01, 9.292942E+00, 4.076571E+00, 1.650790E+00, $
               6.167791E-01, 2.113490E-01, 6.600001E-02, 1.000000E-02 ]

         ; Bp [unitless] for 47 levels (48 edges)
         B = [ 1.000000E+00, 9.849520E-01, 9.634060E-01, 9.418650E-01, $
               9.203870E-01, 8.989080E-01, 8.774290E-01, 8.560180E-01, $
               8.346609E-01, 8.133039E-01, 7.919469E-01, 7.706375E-01, $
               7.493782E-01, 7.211660E-01, 6.858999E-01, 6.506349E-01, $
               6.158184E-01, 5.810415E-01, 5.463042E-01, 4.945902E-01, $
               4.437402E-01, 3.928911E-01, 3.433811E-01, 2.944031E-01, $
               2.467411E-01, 2.003501E-01, 1.562241E-01, 1.136021E-01, $
               6.372006E-02, 2.801004E-02, 6.960025E-03, 8.175413E-09, $
               0.000000E+00, 0.000000E+00, 0.000000E+00, 0.000000E+00, $
               0.000000E+00, 0.000000E+00, 0.000000E+00, 0.000000E+00, $
               0.000000E+00, 0.000000E+00, 0.000000E+00, 0.000000E+00, $
               0.000000E+00, 0.000000E+00, 0.000000E+00, 0.000000E+00 ] 
         
      endif

   endif $

   ;====================================================================
   ; GISS-II-PRIME model or GCAP model grids
   ;====================================================================  
   else if ( StrPos( ModelName, 'GISS_II' ) ge 0   OR $
             StrPos( ModelName, 'GCAP'    ) ge 0 ) then begin

      ; Model top pressure [hPa]
      PTop = 150.0 

      ;-------------------------
      ; 23-layer vertical grid
      ;-------------------------      
      if ( NLayers eq 23 ) then begin

         ; A parameter [hPa]      
         A = [   0.000000,      4.316550,      9.892076,     17.985615, $
                29.676256,     49.280582,     74.460426,    100.539566, $
               120.503593,    132.913666,    142.446045,    150.000000, $
               116.999985,     86.199982,     56.200001,     31.599991, $
                17.800003,     10.000015,      4.630005,      1.459991, $
                 0.460999,      0.144989,      0.031204,      0.002075 ]

         ; B parameter [unitless]
         B = [   1.000000,      0.971223,      0.934053,      0.880096, $
                 0.802158,      0.671463,      0.503597,      0.329736, $
                 0.196643,      0.113909,      0.050360,      0.000000, $
                 0.000000,      0.000000,      0.000000,      0.000000, $
                 0.000000,      0.000000,      0.000000,      0.000000, $
                 0.000000,      0.000000,      0.000000,      0.000000 ] 
      endif

   endif $

   ;====================================================================
   ; NCAR MATCH model grid
   ;====================================================================  
   else if ( StrPos( ModelName, 'MATCH' ) ge 0 ) then begin

      ; Model top pressure [hPa]
      PTop = 0.00468101 

      ;-------------------------
      ; 52-layer vertical grid
      ;-------------------------      
      if ( NLayers eq 52 ) then begin

         ; A parameter [hPa]
         A = [   0.000000,      0.000000,      2.521400,      7.085000, $
                13.344399,     20.847401,     29.089499,     37.521999, $
                44.689598,     50.782299,     55.961098,     60.363201, $
                64.105103,     67.285698,     69.989304,     72.287399, $
                74.240898,     75.901299,     77.312698,     78.512398, $
                66.403999,     55.882801,     46.794102,     38.988098, $
                32.322300,     26.662500,     21.884001,     17.872400, $
                14.523300,     11.743000,      9.447599,      7.562900, $
                 6.024000,      4.774300,      3.765000,      2.954300, $
                 2.306600,      1.791900,      1.385100,      1.065300, $
                 0.815300,      0.620800,      0.470400,      0.339000, $
                 0.232400,      0.151600,      0.094021,      0.057026, $
                 0.034588,      0.020979,      0.012724,      0.007718, $
                 0.004681 ]

         ; B parameter [unitless]
         B = [   1.000000,      0.985112,      0.953476,      0.896215, $
                 0.817677,      0.723535,      0.620120,      0.514317, $
                 0.424382,      0.347936,      0.282956,      0.227722, $
                 0.180772,      0.140864,      0.106941,      0.078106, $
                 0.053596,      0.032762,      0.015053,      0.000000, $
                 0.000000,      0.000000,      0.000000,      0.000000, $
                 0.000000,      0.000000,      0.000000,      0.000000, $
                 0.000000,      0.000000,      0.000000,      0.000000, $
                 0.000000,      0.000000,      0.000000,      0.000000, $
                 0.000000,      0.000000,      0.000000,      0.000000, $
                 0.000000,      0.000000,      0.000000,      0.000000, $
                 0.000000,      0.000000,      0.000000,      0.000000, $
                 0.000000,      0.000000,      0.000000,      0.000000, $
                 0.000000 ]

      endif

   endif $

   ;====================================================================
   ; Stop with error msg if this is not a hybrid grid
   ;====================================================================
   else begin
      
      S = MName + ' is not a hybrid grid!  Cannot compute ETA coordinates!'
      Message, S

   endelse
 
   ;====================================================================
   ; Compute the ETA coordinate for the given grid and layers.
   ;
   ; For hybrid grids, the pressure at the box edges is given by
   ;
   ;    PEDGE(L) = A(L) + ( B(L) * PSURF(L) )
   ;
   ; Where A(L) and B(L) are specified at the bottom edge of level L.
   ; These are constants chosen to provide for a hybrid sigma/pressure
   ; vertical coordinate.  PSURF is the surface pressure in mb.
   ;
   ; The pressure at the center of layer L is given as the average 
   ; of the pressures at the bottom edge of layer L and the top edge
   ; of layer L (which is also the bottom edge of layer L+1):
   ;
   ;    PCENTER(L) = ( PEDGE(L) + PEDGE(L+1) ) / 2
   ;
   ; From PEDGE and PCENTER, we can construct the unitless coordinate
   ; ETA (which is very similar to the sigma coordinate) as follows:
   ;
   ;    ETA_EDGE(L)   = ( PEDGE(L)   - PTOP ) / ( PSURF - PTOP ) 
   ;
   ;    ETA_CENTER(L) = ( PCENTER(L) - PTOP ) / ( PSURF - PTOP )
   ; 
   ; For GAMAP plotting routines, we will use the ETA coordinate for 
   ; hybrid models instead of the sigma coordinate.
   ;====================================================================

   ; Overwrite NLAYERS w/ the number of layers for the grid 
   NLayers = N_Elements( A ) - 1L
      
   ; Compute pressure at box edges
   EPress = A + ( B * PSurf ) 

   ; Compute pressure at box centers
   N      = N_Elements( EPress )
   CPress = 0.5e0 * ( EPress[0:N-2] + EPress[1:N-1]) 
 
   ; Compute ETA on box edges
   if ( Edges  ) then begin
      Eta      = ( EPress - Ptop ) / ( PSurf - Ptop )
      Pressure = Epress
   endif

   ; Compute ETA on box centers
   if ( Center ) then begin
      Eta      = ( Cpress - Ptop ) / ( PSurf - Ptop )
      Pressure = CPress
   endif

   ; Return ETA to calling program
   return, Eta
   
end
