;------------------------------------------------------------------------
;+
; NAME:
; 	MODEL2AIRNUMDENS
;
; PURPOSE:
; 	Returns the monthly averaged Air Number Density in 
;       each grid box for one GEOS-Chem model. 
;
;       This let you get the Number Density from the average monthly
;       pressure of the model.
;
; CATEGORY:
;	Atmospheric Sciences
;
; CALLING SEQUENCE:
;	airNumDens = Model2AirNumDens( Model [, tau] )
;
; INPUTS:
;	Model : string for model name ("GEOS4_30L", "GEOS5_47L", ...)
;
; OPTIONAL INPUTS:
; 	Tau   : tau0 for which Air Numb density is look for. The year
; 	        does not matter, the month value is extracted. Default
; 	        is 0D0, which selects January.
;
; OUTPUTS:
;	A 3D array (same size as the model 3D grid) with Air
;	Density Number in [molec/cm3].
;
;
; PROCEDURE:
;	GetModelAndGridInfo, DataInfo, Model, Grid      
; 
;       airnumden = Model2AirNumDens( CTM_NamExt( Model ), DataInfo.tau0 )
;
;
; MODIFICATION HISTORY:
;       Wed Sep 2 10:54:41 2009, Philippe Le Sager
;       <phs@sol.as.harvard.edu>
;
;		Gamap v2.13 - Init version
;        bmy, 30 Nov 2010: GAMAP VERSION 2.15
;                          - Updated comments and category in header
;
;-
; Copyright (C) 2008, Philipe Le Sager, Harvard University
; This software is provided as is without any warranty whatsoever.
; It may be freely used, copied or distributed for non-commercial
; purposes.  This copyright notice must be kept with any copy of
; this software. If this software shall be used commercially or
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to phs@io.as.harvard.edu
; with subject "IDL routine model2airnumdens"
;-----------------------------------------------------------------------

function get_airNumDens_airMass, GridInfo, VEdge, Area, P

   ;====================================================================
   ; Internal function returns a 3-D array of air mass
   ; given the vertical edges, surface area, and surface pressure. 
   ; (bmy, 12/19/03). Added Units verbose for reference (phs, 9/2/09)
   ;====================================================================

   ; Number of vertical levels (1 less than edges)
   LMX     = N_Elements( VEdge ) - 1L

   ; Define airmass array
   AirMass = DblArr( GridInfo.IMX, GridInfo.JMX, LMX )

   ; Constant 100/g [100 converts mb=hPa into SI units : N/m2 ]
   g100    = 100d0 / 9.8d0 

   ; Loop over levels
   ;
   ; mass [ kg ] = Delta_P (= PSurf [mb] * Delta_Edge ) * S [m2] * 100 [N/m2/mb] / g [m/s2=N/kg]
   ;
   for L = 0L, LMX-1L do begin
      AirMass[*,*,L] = P[*,*] * Area[*,*] * ( VEdge[L] - VEdge[L+1] ) * g100
   endfor

   ; Return
   return, AirMass
end

;-----------------------------------------------------------------------------

function Model2AirNumDens, ModelType, Tau0

   ; Time values for each month
   Tau        = [ 0D,    744D,  1416D, 2160D, 2880D, 3624D, $
                  4344D, 5088D, 5832D, 6552D, 7296D, 8016D, 8760D ]

   ;-------------------
   ; SURFACE PRESSURE
   ;-------------------      

   ModelInfo = ctm_type( ModelType )
   GridInfo  = ctm_grid( ModelInfo )  
 
   ; Get current month index
   Result   = Tau2YYMMDD( Tau0 )
   MonInd   = Result.Month - 1L
   UnDefine, Result

   ; File name for surface pressure on INPUT GRID
   InPSFile = 'ps-ptop.' + CTM_NamExt( ModelInfo ) + $
               '.'       + CTM_ResExt( ModelInfo ) 
   
   InPSFile = file_which(inpsfile)

   ; Read surface pressure [mb]
   Success = CTM_Get_DataBlock( PSurf,   'PS-PTOP',  $
                                FileName=InPSFile,   $
                                Tracer=1L,           $
                                Tau0=Tau[MonInd],    $
                                /Quiet, /NoPrint )

   ; Error check
   if ( not Success ) then Message, 'Could not read INPSURF!'

   ;-------------------
   ; Air Mass
   ;-------------------

   ; Vertical edge coordinates
   if ( ModelInfo.Hybrid )                $
      then InVertEdge = GridInfo.EtaEdge $
      else InVertEdge = GridInfo.SigEdge 

   ; Surface area of grid
   Area = CTM_BoxSize( GridInfo, /GEOS, /M2 )
     
   ; Compute air mass from monthly mean surface pressure
   AirMass = get_airNumDens_airMass( GridInfo, InVertEdge, Area, PSurf )

   ;-------------------
   ; Air number density
   ;-------------------
   ;                       air mass [kg] * Avogadro [molec/mol]
   ; NumDens [molec/cm3] = -------------------------------------
   ;                       Molar Weight [kg/mol] * Volume [cm3]

   ; Volume [cm3]
   VolumeCm3 = CTM_BOXSIZE( GridInfo, /GEOS, /Volume, /cm3 )
 
   Avogadro = 6.022e23
   AirMw    = 0.02987

   airNumDens = Airmass * Avogadro / ( AirMw * VolumeCM3 )

; Debug
;   print, minmax(airmass*1000./volumecm3), ' g/cm3'


   return, airNumDens
end


















