; $Id: regridh_restart.pro,v 1.2 2007/08/03 18:59:50 bmy Exp $
;-----------------------------------------------------------------------
;+
; NAME:
;        REGRIDH_RESTART_NC
;
; PURPOSE:
;        Horizontally regrids data in [v/v] mixing ratio from one
;        model grid to another.  Data is converted to [kg] for 
;        regridding, and then converted back to [v/v].       
;
; CATEGORY:
;        Regridding
;
; CALLING SEQUENCE:
;        REGRIDH_RESTART [, Keywords ]
;
; INPUTS:
;        None
;
; KEYWORD PARAMETERS:
;        INFILENAME -> Name of the file containing data to be regridded.
;             If INFILENAME is not specified, then REGRIDH_RESTART
;             will prompt the user to select a file via a dialog box.
;
;        OUTFILENAME -> Name of the file which will contain the regridded 
;             data.  If OUTFILENAME is not specified, then REGRIDH_RESTART
;             will prompt the user to select a file via a dialog box.
;
;        OUTMODELNAME -> Name of the model grid onto which the data
;             will be regridded.  If OUTMODELNAME is not specified, 
;             then REGRIDH_RESTART will use the same model name as the
;             input grid.
;
;        OUTRESOLUTION -> Specifies the resolution of the model grid
;             onto which the data will be regridded.  OUTRESOLUTION
;             can be either a 2 element vector with [ DI, DJ ] or
;             a scalar (DJxDI: 8=8x10, 4=4x5, 2=2x2.5, 1=1x1, 
;             0.5=0.5x0.5).  Default for all models is 4x5.
;
;        DIAGN -> Diagnostic category of the data blocks that you 
;            wish to regrid.  Default is "IJ-AVG-$".
;
;        /GCAP -> Set this switch to regrid a 4x5 GEOS-3 or GEOS-4 
;            restart file to a "fake" GEOS grid containing 45 latitudes
;            but the same # of levels.  You can then regrid the file
;            vertically using regridv_restart.pro.
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        External Subroutines Required:
;        =================================================
;        CTM_GRID       (function)   CTM_TYPE    (function)
;        CTM_BOXSIZE    (function)   CTM_REGRIDH (function)
;        CTM_NAMEXT     (function)   CTM_RESEXT  (function)
;        CTM_WRITEBPCH               GETMODELANDGRIDINFO 
;        INTERPOLATE_2D (function)   UNDEFINE 
;
; REQUIREMENTS:
;        None
;
; NOTES:
;        None
;
; EXAMPLE:
;        REGRIDH_RESTART_NC, INFILENAME='geos3.2x25.nc', $
;                            OUTFILENAME='geos3.4x5.nc', $
;                            OUTRESOLUTION=4
;           
;             ; Regrids GEOS-3 data from 2 x 2.5 to 4 x 5 resolution.
;
; MODIFICATION HISTORY:
;        jaf, 01 May 2016: Initial version based on regridh_restart.pro and
;                          bpch2coards.pro
;        bmy, 21 Apr 2017: GAMAP VERSION 2.19
;                          - Add keyword /NETCDF4_FORMAT to NCDF_CREATE
;                            to make sure we write to a netCDF4 file,
;                            which can hold 0.25 degree data.
;
;-
; Copyright (C) 2017, 
; GEOS-Chem Support Team, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of this 
; software. If this software shall be used commercially or sold as 
; part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.as.harvard.edu with subject "IDL routine regridh_restart"
;-----------------------------------------------------------------------

function RHR_GetAirMass, GridInfo, VEdge, Area, P

   ;====================================================================
   ; Internal function RHR_GETAIRMASS returns a 3-D array of air mass
   ; given the vertical edges, surface area, and surface pressure. 
   ; (bmy, 12/19/03)
   ;====================================================================

   ; Number of lons & lats
   IMX     = GridInfo.IMX
   JMX     = GridInfo.JMX

   ; Number of vertical levels (1 less than edges)
   LMX     = N_Elements( VEdge ) - 1L

   ; Define airmass array
   AirMass = DblArr( IMX, JMX, LMX )

   ; Constant 100/g 
   g100    = 100d0 / 9.8d0 

   ; Average surface pressure at poles
   P[*,0     ] = Total( P[*,0     ] ) / Float( IMX )
   P[*,JMX-1L] = Total( P[*,JMX-1L] ) / Float( IMX )
   
   ; Loop over levels
   for L = 0L, LMX-1L do begin
      AirMass[*,*,L] = P[*,*] * Area[*,*] * ( VEdge[L] - VEdge[L+1] ) * g100
   endfor

   ; Return
   return, AirMass
end

;-----------------------------------------------------------------------------

pro RHR_Fix_Poles, InGrid, InData, OutGrid, OutData

   ;====================================================================
   ; Routine RHR_FIX_POLES adjusts OUTDATA so as to prevent a buildup
   ; of mixing ratio concentration at the poles -- this is an artifact
   ; of the coarse grid to fine grid regridding (bmy, 4/13/04)
   ;====================================================================

   ; Echo Info
   S = 'Fixing polar data for coarse to fine regridding!'
   Message, S, /Info

   ; Convenience variable
   Nx = Float( InGrid.IMX )

   ; Get size of output array
   SData = Size( OutData, /Dim )
   NData = N_Elements( SData )

   ; Test for dimension of data array
   case ( NData ) of

      ; Just take the value at the pole from 
      ; the data array on the input grid
      2: begin

         ; Polar averages of INDATA at NP and SP 
         AvgNP = Total( InData[*,InGrid.JMX-1L] ) / Nx
         AvgSP = Total( InData[*,0            ] ) / Nx

         ; Copy to first 2 polar latitudes -- NP and SP
         OutData[*,OutGrid.JMX-1L] = AvgNP
         OutData[*,OutGrid.JMX-2L] = AvgNP
         OutData[*,0             ] = AvgSP
         OutData[*,1             ] = AvgSP
      end

      3: begin

         ; Loop over levels
         for L=0L, SData[2]-1L do begin

            ; Polar averages of INDATA at NP and SP
            AvgNP = Total( InData[*,InGrid.JMX-1L,L] ) / Nx
            AvgSP = Total( InData[*,0,            L] ) / Nx
         
            ; Copy to first-2 polar latitudes -- NP and SP
            OutData[*,OutGrid.JMX-1L,L] = AvgNP
            OutData[*,OutGrid.JMX-2L,L] = AvgNP
            OutData[*,0,             L] = AvgSP
            OutData[*,1,             L] = AvgSP
         endfor
      end
   endcase
         
end

;-----------------------------------------------------------------------------

pro RegridH_Restart_NC, InFileName=InFileName,       OutModelName=OutModelName,$
                        OutResolution=OutResolution, OutFileName=OutFileName,  $
                        DiagN=DiagN,                 GCAP=GCAP,                $
                        _EXTRA=e
 
   ;====================================================================
   ; Initialization
   ;====================================================================

   ; External functions
   FORWARD_FUNCTION CTM_Type,       CTM_Grid,        CTM_BoxSize, $
                    CTM_RegridH,    CTM_NamExt,      CTM_ResExt,  $
                    InterPolate_2D, CTM_Make_DataInfo 

   ; Keywords
   if ( N_Elements( DiagN         ) ne 1 ) then DiagN         = 'IJ-AVG-$'
   if ( N_Elements( OutResolution ) eq 0 ) then OutResolution = 4

   ; For GCAP grid
   GCAP = Keyword_Set( GCAP )

   ; Time values for each month
   Tau        = [ 0D,    744D,  1416D, 2160D, 2880D, 3624D, $
                  4344D, 5088D, 5832D, 6552D, 7296D, 8016D, 8760D ]
      
   ; Set first time flag
   FirstTime  = 1L

   ;================================================================
   ; Process the data
   ;================================================================

   ; Read data into DATAINFO array of structures
   NCDF_Read, DataInfo, FileName=InFileName,/all, $
	      Dims=Dims,Attributes=Attributes
   Names = tag_names(DataInfo)

   ; Fix some names -- IDL reads in as upper case but should be mixed
   for nn = 0,n_elements(Names)-1 do begin

       loc1 = strpos(Names[nn],'BR')
       loc2 = strpos(Names[nn],'SO4S')
       loc3 = strpos(Names[nn],'NITS')
       if (loc1 ge 0) then begin
          tmpname = byte(Names[nn])
          tmpname[loc1+1] = byte('r')
          Names[nn] = string(tmpname)
       endif else if (loc2 ge 0) then begin
          tmpname = byte(Names[nn])
          tmpname[loc2+3] = byte('s')
          Names[nn] = string(tmpname)
       endif else if (loc3 ge 0) then begin
          tmpname = byte(Names[nn])
          tmpname[loc3+3] = byte('s')
          Names[nn] = string(tmpname)
       endif
   endfor

   ; Use tag names to find data to be regridded (SPC_ & TRC_)
   Types = strmid(Names,0,3)
   To_Regrid = where(Types eq 'SPC' or Types eq 'TRC',$
		     nRegrid,comp=Header)

   ; Get month from Time variable
   Result   = Tau2YYMMDD( DataInfo.Time )
   MonInd   = Result.Month - 1L
   UnDefine, Result

   ;-------------------
   ; INPUT GRID
   ;-------------------

   ; Get input area from netcdf file
   InArea = DataInfo.Area

   ; Check that this is in m2 as expected
   if ( strlowcase(Attributes.Area.Units) ne 'm2' ) then $
      Message, 'Input area not in m2 as expected!'

   ; Get resolution (horizontal & vertical) from dimensions
   if ( Dims[0] eq  72 and Dims[1] eq 46 ) then InRes=4 else $
   if ( Dims[0] eq 144 and Dims[1] eq 91 ) then InRes=2 else $
   Message,'Only 4x5 & 2x25 simulations can be regrdded for now! '+ $
           'Must be edited to accept other resolutions!'

   if ( Dims[2] eq 47 ) then append='_47L' else $
   if ( Dims[2] eq 72 ) then append=''     else $
   Message,'Only GEOS5/GEOSFP/MERRA simulations can be regridded for now! '+ $
           'Must be edited to accept other vertical resolutions!'
      
   ; Get MODELINFO and GRIDINFO structures
   ; Note: only use type for grid, so doesn't really matter whether GEOS5, etc.
   InType = CTM_TYPE('GEOS5'+append,Res=InRes)
   InGrid = CTM_GRID( InType)

   ; Vertical edge coordinates
   if ( InType.Hybrid )                                        $
      then InVertEdge = InGrid.EtaEdge[ 0:Dims[2] ] $
      else InVertEdge = InGrid.SigEdge[ 0:Dims[2] ]

   ;-------------------
   ; OUTPUT GRID
   ;-------------------      

   ; This routine is for horizontal regridding only, so use same model name as InType
   OutType = CTM_TYPE('GEOS5'+append,Res=OutResolution)
   OutGrid = CTM_GRID( OutType)

   ; Calculate output grid area
   OutArea = CTM_BoxSize( OutGrid, /M2 )

   ; Vertical edge coordinates on OUTPUT GRID
   if ( OutType.Hybrid )                                         $
      then OutVertEdge = OutGrid.EtaEdge[ 0:Dims[2] ] $
      else OutVertEdge = OutGrid.SigEdge[ 0:Dims[2] ]

   ;-------------------
   ; SET UP NETCDF
   ;-------------------      

   ; Open netCDF file for ouptut
   fId = NCDF_Create( OutFileName, /Clobber, /Netcdf4_Format )

   ;--------------------------
   ; Global Attributes
   ;--------------------------
   for n = 0, n_elements(tag_names(attributes.global))-1 do $
       NCDF_AttPut, fId, /Global, strlowcase((tag_names(attributes.global))[n]), $
		    string(attributes.global.([n]))

   ;--------------------------
   ; Define netcdf variables
   ;--------------------------
   vID_SAVE = intarr(n_elements(Names))
   for n = 0,n_elements(Names)-1 do begin

       ; Lon
       ;-----
       if (strupcase(Names[n]) eq 'LON') then begin

          ; Dimension for output lon array
          XDim = OutGrid.IMX

          ; Define the longitude dimension 
          NC_XDim = NCDF_DimDef( fId, 'lon', XDim )

          ; Define the variable which will hold the longitudes
          vLon = NCDF_VarDef( fId, 'lon', NC_XDim, /Double  )

          ; store variable
          vID_SAVE[n] = vLon

       ; Lat
       ;-----
       endif else if (strupcase(Names[n]) eq 'LAT') then begin

          ; Dimension for output lat array
          YDim = OutGrid.JMX

         ; Define the latitude dimension 
          NC_YDim = NCDF_DimDef( fId, 'lat', YDim )

          ; Define the variable which will hold the latitudes
          vLat = NCDF_VarDef( fId, 'lat', NC_YDim, /Double  )

          ; store variable
          vID_SAVE[n] = vLat

       ; Lev
       ;-----
       endif else if (strupcase(Names[n]) eq 'LEV') then begin

          ; Dimension for output level array
          ZDim = OutGrid.LMX

          ; Define the level dimension 
          NC_ZDim = NCDF_DimDef( fId, 'lev', ZDim )

          ; Define the variable which will hold the levels
          vLev = NCDF_VarDef( fId, 'lev', NC_ZDim, /LONG  )

          ; store variable
          vID_SAVE[n] = vLev

       ; Time
       ;-----
       ; Time dimension does not change
       endif else if (strupcase(Names[n]) eq 'TIME') then begin

          ; Dimension for output level array
          TDim = 1 ; restart files are for a single time only

          ; Define the time dimension 
          NC_TDim = NCDF_DimDef( fId, 'time', TDim )

          ; Define the variable which will hold the time
          vTime = NCDF_VarDef( fId, 'time', NC_TDim, /Float  )

          ; store variable
          vID_SAVE[n] = vTime

       ; Area - not a dimension but needs to be treated carefully
       ;-----
       endif else if (strupcase(Names[n]) eq 'AREA') then begin

          ; Define the variable which will hold the time
          vArea = NCDF_VarDef( fId, 'AREA', [NC_XDim,NC_YDim], /Double  )

          ; store variable
          vID_SAVE[n] = vArea

       ; All others - 3D, not dims, no special treatment
       endif else begin

          NC_Dims = [ NC_XDim, NC_YDim, NC_ZDim, NC_TDim ]
          vID = NCDF_VarDef( fId, Names[n], NC_Dims, /Float  )
          vID_SAVE[n] = vID

       endelse

        S = 'TmpAtt = attributes.'+Names[n]
        status = Execute(S)
        for nn = 0, n_elements(tag_names(TmpAtt))-1 do $
            NCDF_AttPut, fId, vID_SAVE[n], strlowcase((tag_names(TmpAtt))[nn]), $
 	    	         TmpAtt.([nn])

   endfor

   ; Exit from netCDF definition mode
   NCDF_Control, fId, /EnDef
      
   ; Put in non-regridded variables
   for n = Header[0],Header[-1] do begin

       if (strupcase(Names[n]) eq 'LON') then $
          NCDF_VarPut, fId, vLon, OutGrid.XMID else $
       if (strupcase(Names[n]) eq 'LAT') then $
          NCDF_VarPut, fId, vLat, OutGrid.YMID else $
       if (strupcase(Names[n]) eq 'LEV') then $
          NCDF_VarPut, fId, vLev, DataInfo.lev else $ ; doesn't change
       if (strupcase(Names[n]) eq 'TIME') then $
          NCDF_VarPut, fId, vTime, DataInfo.time else $ ; doesn't change
       if (strupcase(Names[n]) eq 'AREA') then $
          NCDF_VarPut, fId, vArea, OutArea else $
       Message, 'No info for treating non-regridded variable '+Names[n]+'!'

   endfor

   ;-------------------
   ; SURFACE PRESSURE
   ;-------------------      

   ; File name for surface pressure on INPUT GRID
   InPSFile = 'ps-ptop.' + CTM_NamExt( InType ) + $
              '.'        + CTM_ResExt( InType ) 
   
   ; Look for INPSFILE in this directory first, and then
   ; in the directories specified in the !PATH variable
   InPSFile = File_Which( InPsFile, /Include_Current_Dir )
   InPSFile = Expand_Path( InPSFile )

   ; Read surface pressure on INPUT GRID
   Success = CTM_Get_DataBlock( InPSurf, 'PS-PTOP',  $
                                FileName=InPSFile,   $
                                Tracer=1L,           $
                                Tau0=Tau[MonInd],    $
                                /Quiet, /NoPrint )

   ; Error check
   if ( not Success ) then Message, 'Could not read INPSURF!'

   ; File name for surface pressure on OUTPUT GRID
   OutPSFile = 'ps-ptop.' + CTM_NamExt( OutType ) + $
               '.'        + CTM_ResExt( OutType ) 
   
   ; Look for OUTPSFILE in this directory first, and then
   ; in the directories specified in the !PATH variable
   OutPSFile = File_Which( OutPSFile, /Include_Current_Dir )
   OutPSFile = Expand_Path( OutPSFile )

   ; Read surface pressure on INPUT GRID
   Success = CTM_Get_DataBlock( OutPSurf, 'PS-PTOP',  $
                                FileName=OutPSFile,   $
                                Tracer=1L,            $
                                Tau0=Tau[MonInd],     $
                                /Quiet, /NoPrint )

   ; Error check
   if ( not Success ) then Message, 'Could not read OUTPSURF!'

   ;-------------------
   ; DATA VARIABLES
   ;-------------------      
   ; Loop over all species/tracers
   for D = To_Regrid[0],To_Regrid[-1] do begin

      ; Echo info to screen
      S = 'Now processing ' + Names[D]
      Message, S, /Info

      ; Get the original data for this variable
      S = 'InData = DataInfo.' + Names[D]
      status = Execute( S )

      ;-------------------
      ; REGRID DATA
      ;------------------- 

      ; Convert data on INPUT GRID from [v/v] to [kg]
      InAirMass = RHR_GetAirMass( InGrid, InVertEdge, InArea, InPSurf )
      InData    = Temporary( InData ) * InAirMass

      ; Reuse saved mapping weights?
      US      = 1L - FirstTime

      ; Regrid data from INPUT GRID to OUTPUT GRID
      OutData = CTM_RegridH( InData, InGrid, OutGrid, $
                             /Double, Use_Saved=US )

      ; Convert data on OUTPUT GRID from [kg] to [v/v]
      OutAirMass = RHR_GetAirMass( OutGrid, OutVertEdge, OutArea, OutPSurf )
      OutData    = ( Temporary( OutData ) / OutAirMass )

      ;### Kludge -- prevent buildup of stuff at the poles caused by 
      ;### the fine --> coarse regridding algorithm (bmy, 4/9/04)
      if ( InGrid.IMX gt OutGrid.IMX ) then begin
         InData = Temporary( InData ) / InAirMass
         RHR_Fix_Poles, InGrid, InData, OutGrid, OutData
      endif
      
      ;-------------------
      ; SAVE DATA
      ;------------------- 
      NCDF_VarPut, fId, vID_Save[D], OutData

      ; Undefine variables
      UnDefine, InData
      Undefine, OutData
      UnDefine, InAirMass
      UnDefine, OutAirMass

      ; Reset the first time flag
      FirstTime = 0L
      
   endfor

   ;====================================================================
   ; Close file and quit
   ;====================================================================
   NCDF_Close, fId

end



