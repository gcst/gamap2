;-----------------------------------------------------------------------
;+
; NAME:
;        PLOT_GPROF
;
; PURPOSE:
;        Reads a GNU Profile (gprof) text output and creates a bar graph
;        of how long subroutines take to execute.  This is useful in
;        examining code for computational bottlenecks.
;
; CATEGORY:
;        Plotting
;
; CALLING SEQUENCE:
;        PLOT_GPROF, FILENAME [,keywords]
;
; INPUTS
;        FILENAME -> Text file containing profiling output from gprof.
;
; KEYWORD PARAMETERS:
;        N_DISPLAY -> The number of routines to display, from slowest
;             to fastest.  The default is 30.
;
;        BARCOLOR -> The color table value that will be used to plot
;             the bars.  Default is !MYCT.RED.
; 
;        /VERBOSE -> If set, will print a listing of the N_DISPLAY
;             slowest routines.  The routine name, execution time, 
;             and percentage of total run time will be printed.
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        None
;        
; REQUIREMENTS:
;        Uses these GAMAP routines
;        ==========================
;        STRPAD   (function)  
;        STRRIGHT (function)   
;
; NOTES:
;       For instructions on using the GNU Profiler (gprof) and creating
;       a text file with profiling output, please see this wiki page:
;       http://wiki.geos-chem.org/Profiling_GEOS-Chem.
;
; EXAMPLE:
;        PLOT_GPROF, 'Profile_Gfortran_Intel.txt', N_DISPLAY=40, /VERBOSE
;
;             ; Creates a bar plot showing the 40 slowest routines
;             ; contained in the GNU profiler output file named
;             ; "Profile_Gfortran_Intel.txt".  Also will print the
;             ; profiling info to the screen.
;
; MODIFICATION HISTORY:
;        bmy, 15 Dec 2016: GAMAP VERSION 2.19
;                          - Initial version
;
;-
; Copyright (C) 2016, Bob Yantosca, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.harvard.edu with subject "IDL routine bargraph"
;-----------------------------------------------------------------------


pro Plot_Gprof, FileName, N_Display = N_Display, $
                          BarColor  = BarColor,  $
                          Verbose   = Verbose,   $
                          _EXTRA = e

   ;======================================================================
   ; Initialization
   ;======================================================================

   ; Inline external functions
   FORWARD_FUNCTION StrPad, StrRight

   ; Stop if the file w/ profiling info is not found
   if ( N_Elements( FileName  ) ne 1 ) then Message, 'FILENAME not specified!'

   ; Number of subroutines to display (default is 25)
   if ( N_Elements( N_Display ) ne 1 ) then N_Display = 30

   if ( N_Elements( BarColor  ) ne 1 ) then BarColor  = !MYCT.RED

   ; Print out the subroutine times?
   Verbose      = Keyword_Set( Verbose )

   ; Constants
   MAX_ROUTINES = 5000
   
   ; Scalars
   LineCt       = 0L
   Pos          = 0L
   SubCt        = 0L
   
   ; Strings
   Line         = ''
   RoutineName  = ''

   ; Arrays (declare bigger than necessary)
   PctTime      = FltArr( MAX_ROUTINES )
   SelfSec      = FltArr( MAX_ROUTINES )
   SubName      = StrArr( MAX_ROUTINES )

   ;======================================================================
   ; Read the profile info from the file and save into arrays
   ;======================================================================

   ; Open the file
   Open_File, FileName, Ilun, _EXTRA = e

   ; Loop through the file
   while ( not EOF( Ilun ) ) do begin

      ; Read the line
      ReadF, Ilun, Line

      ; Increment the number of lines read in
      LineCt = LineCt +  1L

      ; Skip the first 5 lines, this is the header
      if ( LineCt lt 6 ) then goto, next

      ; Split the line
      Result   = StrBreak( Line, ' ' )
      N_Result = N_Elements( Result ) 

      ; Exit if we have passed the line
      if ( Result[0]      eq '%' ) then goto, exit_loop
      if ( StrLen( Line ) eq 0   ) then goto, exit_loop

      ; Archive the % of the total time
      PctTime[SubCt] = Float( Result[0] )

      ; Archive the time spent in this routine [s]
      SelfSec[SubCt] = Float( Result[2] )

      ; Get the routine name
      if ( N_Result eq 4 )                                       $
         then RoutineName = StrTrim( Result[3], 2 ) $
         else RoutineName = StrTrim( Result[6], 2 ) 

      ; GFORTRAN: The module name is denoted by MOD_, 
      ; so use this to try to shorten the routine name
      Pos = StrPos( RoutineName, 'MOD_' )
      if ( Pos[0] ge 0 ) then begin
         RoutineName = StrRight( RoutineName, $
                                 StrLen( RoutineName ) - ( Pos + 4 ) )
      endif

      ; IFORT: The module name is denoted by "MP_",
      ; so use this to try to shorten the routine name
      Pos = StrPos( RoutineName, 'mp_' )
      if ( Pos ge 0 ) then begin
         RoutineName = StrRight( RoutineName, $
                                 StrLen( RoutineName ) - ( Pos + 3 ) )
      endif

      ; Archive the routine name
      SubName[SubCt] = RoutineName
      
      ; Increment the subroutine count
      SubCt = SubCt +  1L

next:
   endwhile

exit_loop:

   ; Close the file
   Close,    Ilun
   Free_LUN, Ilun

   ;======================================================================
   ; Filter out the routines that report "self seconds" as 0.0;
   ; These are a very small part of the total and not the offenders
   ;======================================================================

   ; Make sure that N_DISPLAY does not exceed the number 
   ; of subroutines contained in the profile
   N_Display = N_Display < N_Elements( PctTime )

   ; Pick out the slowest routines (1..N_DISPLAY)
   Ind       = LIndGen( N_Display )

   ; Resize arrays and reverse them so that slowest 
   ; to fastest will be displayed from the top down
   PctTime   = Reverse( PctTime[Ind] )
   SelfSec   = Reverse( SelfSec[Ind] )
   SubName   = StrUpCase( Reverse( SubName[Ind] ) )

   ;======================================================================
   ; Plot and/or print the profiling information
   ;======================================================================

   ; Get the plot position. Leave extra room for the routine names at left
   MultiPanel, Margin = [0.17, 0.05, 0.05, 0.05 ], Position = P

   ; Format strings
   F1 = '("The ", a, " subroutines with the longest execution time:")'
   F2 = '(i4, 2x, a30, f11.3, " s  ", f8.3, " %")'

   ; Title for the plot and listing
   Title = "The " + StrTrim( String( N_Display ), 2 )          + $
           " slowest routines in profile: " + $
            StrTrim( FileName, 2 ) 

   ; Also print out the profiling information
   if ( Verbose ) then begin

      ; Print Title 
      print, Title

      ; Print list of subroutines in descending order of execution time
      for N = N_Display-1L, 0, -1L do begin
         print, Format = F2, N_Display-N, StrPad( SubName[N], 30 ),   $
                             SelfSec[N],  PctTime[N]
      endfor
   endif

   ; Print the percent that each subroutine took atop each bar 
   BarLabels = strTrim( String( PctTime, Format='(F10.2)' ), 2 ) + "%"
 
   ; Extend the max data range by 10%
   MaxSec = Max( SelfSec )
   Yrange = [ 0, MaxSec * 1.1 ]
   
   ; Create a bar graph
   BarGraph, SelfSec,                                  $
             BarColor   = BarColor,                    $
             BarWidth   = 0.9,                         $
             BarLabels  = BarLabels,                   $
             Horizontal = 1,                           $
             L_Format   = '(a)',                       $
             Position   = P,                           $
             Title      = Title,                       $
             XLabels    = SubName,                     $
             XMinor     = 5,                           $
             XStyle     = 1,                           $
             XTitle     = 'Time spent in routine (s)', $
             YRange     = YRange,                      $
             YTickLen   = 0,                           $
             _EXTRA     = e

end

