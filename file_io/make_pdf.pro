;-----------------------------------------------------------------------
;+
; NAME:
;        MAKE_PDF
;
; PURPOSE:
;        Wrapper program for the Unix utility "ps2pdf".  Creates
;        a PDF file for each PostScript file located in the
;        a user-specified directory.
;
; CATEGORY:
;        File & I/O
;
; CALLING SEQUENCE:
;        MAKE_PDF, DIR
;
; INPUTS:
;        DIR -> Directory where PostScript files reside.  
;             PDF files will be written out to this directory.
;
; KEYWORD PARAMETERS:
;        None
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        External Subroutines Required:
;        ===============================
;        ADD_SEPARATOR (function)
;        MFINDFILE     (function)
;        REPLACE_TOKEN (function)
;
; REQUIREMENTS:
;        You need "ps2pdf" installed on your system.  This ships
;        with most versions of Unix or Linux.
;
;        Also requires routines from the GAMAP package.
;
; NOTES:
;        PDF files will have the same names as the PostScript files
;        but with the *.pdf extension.  
;
; EXAMPLES:
;        MAKE_PDF, 'output'
;
;             ; Create *.pdf Files from all *.ps files 
;             ; in the "output" directory.
;      
;
; MODIFICATION HISTORY:
;        bmy, 30 Nov 2010: GAMAP VERSION 2.15
;                          - Initial version
;        bmy, 08 Jun 2011: - Make sure that the directory ends with a
;                            path separator
;
;-
; Copyright (C) 2010-2011,
; Bob Yantosca and Philippe Le Sager, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.harvard.edu with subject "IDL routine benchmark_1yr"
;-----------------------------------------------------------------------

pro Make_PDF, Dir

   ; External functions
   FORWARD_FUNCTION Add_Separator, MFindFile, Replace_Token

   ; Default directory
   if ( N_Elements( Dir ) ne 1 ) then Message, 'DIR not passed!'

   ; Make sure the directory ends with a path separator
   Dir = Add_Separator( Dir )

   ; Get a file listing of all PostScript files in the output directory
   List = MFindFile( Dir + '*.ps' )

   ; Loop over files
   for F = 0L, N_Elements( List )-1L do begin

      ; PostScript file name
      PsFile  = List[F]

      ; PDF file name
      PdfFile = Replace_Token( PsFile, '.ps', '.pdf', Delim='' )

      ; Call PS2PDF to create a PDF from each PS file
      print, 'Creating ', PdfFile
      Cmd = 'ps2pdf -dAutoRotatePages=/None ' + PsFile + ' ' + PdfFile
      Spawn, Cmd

   endfor
end
