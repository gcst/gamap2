; $Id: read_h5dataset.pro,v 1.1 2009/05/28 20:18:20 bmy Exp $
;-----------------------------------------------------------------------
;+
; NAME:
;        READ_H5DATASET
;
; PURPOSE: 
;        Convenience routine to read dataset variables 
;        from Hierarchical Data Format version 5 (HDF5) files.
;        Also works for HDF-EOS files!
;
; CATEGORY:
;        File & I/O, Scientific Data Formats
;
; CALLING SEQUENCE:
;        DATAFIELD = READ_H5DATASET( FID, DATASET_NAME )
;
; INPUTS:
;        FID -> HDF5 File ID, as returned by routine H5F_OPEN
;
;        DATASET_NAME -> Name of the scientific dataset variable that
;             you want to extract from the file.  
;
; KEYWORD PARAMETERS:
;        None
;
; OUTPUTS:
;        DATAFIELD -> Array containing extracted data from the HDF file.
;
; SUBROUTINES:
;        None
;
; REQUIREMENTS:
;        Need to use a version of IDL w/ HDF5 library installed.
;
; NOTES:
;        From Trevor Beck (trevor.beck@noaa.gov) for GOME-2.
;        
; EXAMPLE:
;
;        ; Specify the file name
;        FILE = 'GOME_xxx_1B_M02_20070105012056Z_20070105030556Z_R_O_20080613081807Z.337p4_356p1.brs.hcho.he5'
;
;        ; Make sure the file is a HDF5 file
;        IF ( H5F_IS_HDF5( FILE ) eq 0 ) then MESSAGE, 'Not an HDF-5 file!'
;
;        ; Open the HDF file and get the file ID # (FID)
;        FID = H5F_OPEN( FILE ) 
;        IF ( FID lt 0 ) then MESSAGE, 'Error opening file!'
;
;        ; Read the AMF field from disk
;        ; NOTE: the swath name is "Column"
;        AMF = READ_H5DATASET( FID, "/Column/Amf" )
;
;        ; Close the file 
;        H5_CLOSE, FID
;
; MODIFICATION HISTORY:
;        bmy, 28 May 2009: VERSION 1.00
;
;-
; Copyright (C) 2009,
; Bob Yantosca and Philippe Le Sager, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.as.harvard.edu with subject "IDL routine hdf_getsd"
;-----------------------------------------------------------------------


FUNCTION READ_H5DATASET, fid, dataset_name

   ; Reads HDF5 datasets
   ; From Trevor Beck (trevor.beck@noaa.gov) for GOME-2.

   dsid = H5D_OPEN(fid, dataset_name )
   datafield = H5D_READ(dsid)
   H5D_CLOSE, dsid
   RETURN, datafield
END

