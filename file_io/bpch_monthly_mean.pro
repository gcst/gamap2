;-----------------------------------------------------------------------
;+
; NAME:
;        BPCH_MONTHLY_MEAN
;
; PURPOSE:
;        Creates monthly means from GEOS-Chem output saved at less
;        than monthly intervals.  Ideal for working with output of 
;        high-resolution model output, especially if your queuing
;        system time limits do not permit a 1-month simulation to be
;        completed in a single run stage.

; CATEGORY:
;        File & I/O, BPCH Format
;
; CALLING SEQUENCE:
;        BPCH_MONTHLY_MEAN, FILES [, Keywords ]
;
; INPUTS:
;        FILES -> A vector containing the pathnames of the files from
;             which you would like to create monthly mean output.
;
; KEYWORD PARAMETERS:
;        OUTFILENAME -> Name of the file (bpch format) that will
;             contain the monthly mean output.  The default is 
;             "bpch_monthly_mean_output.bpch".
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        External Subroutines Required
;        ================================
;
; REQUIREMENTS:
;        Requires other routines from the GAMAP package.
;
; NOTES:
;        Assumes that each of the files passed via the FILES argument
;        contains an identical sequence of diagnostic data blocks.
;        This will normally be the case for GEOS-Chem simulations that 
;        have to be separated into several run stages for the queue
;        system.
;
;        Error checking is minimal, we will fix this later.  This
;        routine in intended for use with files that are created from
;        individual stages of a long GEOS-Chem simulation.  As such,
;        we can usually assume that all files will have the same
;        sequence of data blocks, and that all data blocks will be on
;        the same grid.
;
; EXAMPLE:
;        FILES = [ 'ctm.bpch.2011010100', 'ctm.bpch.2011011500' ]
;        BPCH_MONTHLY_MEAN, FILES, OUTFILENAME = 'monthly_mean.bpch'
;
;             ; Creates monthly-mean output from two GEOS-Chem bpch
;             ; files that contain 15-day averaged data.
;
; MODIFICATION HISTORY:
;        bmy, 21 Dec 2011: GAMAP VERSION 2.16
;                          - Initial version
;
;-
; Copyright (C) 2011, Bob Yantosca, Harvard University
; This software is provided as is without any warranty whatsoever.
; It may be freely used, copied or distributed for non-commercial
; purposes.  This copyright notice must be kept with any copy of
; this software. If this software shall be used commercially or
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to yantosca@seas.harvard.edu
; or plesager@seas.harvard.edu with subject "IDL routine bpch_monthly_mean"
;-----------------------------------------------------------------------


pro Bpch_Monthly_Mean, Files, OutFileName=OutFileName, _EXTRA=e
 
   ;========================================================================
   ; Initialization
   ;========================================================================
   
   ; Free all previously-read data blocks
   CTM_Cleanup
 
   ;--------------------------------------------------------------------------
   ; %%% HARDWIRE FILENAMES FOR TESTING
   ;files = [ $
   ;  '~mpayer/geos-chem/hippo/runs/fullchem/bpch/ctm.bpch.2x25.2009010100', $
   ;  '~mpayer/geos-chem/hippo/runs/fullchem/bpch/ctm.bpch.2x25.2009011500' ]
   ;OutFileName = '~/S/bpch_monthly_mean_output.bpch'
   ;--------------------------------------------------------------------------
 
   ; Number of files to process
   N_Files = N_Elements( Files )
   if ( N_Files lt 2 ) then Message, 'Need at least 2 files to process!'

   ; Double precision value for N_FILES
   Dble_N_Files = Double( N_Files )

   ; Output file name
   if ( N_Elements( OutFileName ) eq 0 ) $
      then OutFileName = 'bpch_monthly_mean_output.bpch'
 
   ; Template for DATAINFO structure
   DInfoTemplate = Create3DHstru( 2000 )
 
   ; Template for array of DATAINFO structures
   DStruTemplate = { DataInfo : DInfoTemplate }
 
   ; Array of DATAINFO structures for input & output data
   InDataStru    = Replicate( DStruTemplate, N_Files )
   OutDataStru   = Replicate( DStruTemplate, 1      )

   ;========================================================================
   ; Read data from each file into an array of DATAINFO structures
   ;========================================================================
 
   ; Loop over the # of files
   for F = 0L, N_Files-1L do begin
      
      ; Read data from each file into a DATAINFO structure
      CTM_Get_Data, DataInfo, File=Files[F], _EXTRA=e
      
      ; Number of data blocks in each bpch file
      if ( F eq 0L ) then begin

         ; Get the # of data blocks in the first file
         N_Data = N_Elements( DataInfo )

      endif else begin
         
         ; Stop w/ error msg if the other files do not have the same #
         ; of data blocks as the first data file.  This isn't
         ; the greatest error check but will suffice for now.
         if ( N_Elements( DataInfo ) ne N_Data ) then begin
            Str = 'ERROR: File '           + Extract_FileName( Files[F] ) + $
                  ' is incompatible with ' + Extract_FileName( Files[0] )
            Message, Str
         endif

      endelse

      ; Save the DATAINFO structure into the INDATASTRU data structure
      InDataStru[F].DataInfo[0L:N_Data-1L] = DataInfo[0:N_Data-1L]
 
      ; Undefine variables
      UnDefine, DataInfo
 
   endfor
 
   ; Get the MODELINFO and GRIDINFO structures from the first file
   ; For now, assume that the other files have the same MODELINFO and
   ; GRIDINFO structures.
   GetModelAndGridInfo, InDataStru[0].DataInfo[0], ModelInfo, GridInfo

   ;========================================================================
   ; Average all corresponding data blocks together
   ;========================================================================
 
   ; Index for picking the proper TAU1 value for CTM_MAKE_DATABLOCK
   FF = N_Files - 1L

   ; Loop over data blocks
   for D = 0L, N_Data-1L do begin
 
      ; Loop over each file in the data block
      for F = 0L, N_Files-1L do begin
 
         ; Data array for a given tracer & diagnostic
         Data = *( InDataStru[F].DataInfo[D].Data )
         Data = Double( Data )

         ; Sum the data for the given tracer & diagnostic
         ; across all of the other bpch files
         if ( F eq 0L )                   $
            then SumData =           Data $
            else SumData = SumData + Data
 
         ; Undefine variables
         UnDefine, Data
 
      endfor
 
      ; Average data from all files
      AvgData = SumData / Dble_N_Files
 
      ; Create a DATAINFO structure
      Success = CTM_Make_DataInfo( $
                   Float( AvgData ),                                  $
                   ThisDataInfo,                                      $
                   ThisFileInfo,                                      $
                   ModelInfo  = ModelInfo,                            $
                   GridInfo   = GridInfo,                             $
                   DiagN      = InDataStru[0 ].DataInfo[D].Category,  $
                   Tracer     = InDataStru[0 ].DataInfo[D].Tracer,    $
                   Tau0       = InDataStru[0 ].DataInfo[D].Tau0,      $
                   Tau1       = InDataStru[FF].DataInfo[D].Tau1,      $
                   Unit       = InDataStru[0 ].DataInfo[D].Unit,      $
                   Dim        = InDataStru[0 ].DataInfo[D].Dim,       $
                   First      = InDataStru[0 ].DataInfo[D].First,     $
                   /NO_GLOBAL )
  
      ; Error meassage
      if ( not Success ) then begin
         Str = 'ERROR: Could not create DATAINFO for '+               $
               ' category '   + InDataStru[0].DataInfo[D].Category +  $
               ' and tracer ' + InDataStru[0].DataInfo[D].Tracer
 
         Message, Str
      endif

      ; Save output data structure
      OutDataStru[0].DataInfo[D] = ThisDataInfo
 
      ; Undefine variables
      UnDefine, AvgData
      UnDefine, SumData
      
   endfor
 
   ;========================================================================
   ; Write averaged data to OUTFILENAME (bpch file format)
   ;========================================================================
 
   ; Write the average data to a bpch file
   CTM_WriteBpch, OutDataStru[0].DataInfo[0:N_Data-1L], $
                  ThisFileInfo,                         $
                  FileName=OutFileName,                 $
                  _EXTRA=e
 
   ; Undefine variables
   UnDefine, DStruTemplate
   UnDefine, InDataInfo
   UnDefine, OutDataInfo

end
