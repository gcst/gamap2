;-----------------------------------------------------------------------
;+
; NAME:
;        MAKE_MULTI_NC
;
; PURPOSE:
;        Convert bpch files (matching a mask) to netCDF format. 
;        The routine can work recursively : files in subdirectories
;        are also searched for.
;        
;        This is a convenient generic wrapper for GAMAP routine
;        Bpch2coards.pro, to work on several files and directories at
;        once.
;        
;        When working recursively, the directory tree in the input
;        directory is recreated as needed in the output directory.
;
; CATEGORY:
;        File & I/O, BPCH Format, Scientific Data Formats
;
; CALLING SEQUENCE:
;        MAKE_MULTI_NC
;
; INPUTS:
;        none
;
; KEYWORD PARAMETERS:
;        MASK -> string, default is '*', that is all files
;
;        INPUT_PARENT_DIR -> top directory of files to convert, if
;             not specified, a dialog window pops up
;
;        OUTPUT_PARENT_DIR -> top directory destination for netCDF
;             files, if not specified, a dialog window pops up.

;        RECURSION -> to search subdirectories. Default is 0
;             (OFF). Set to 1 to turn it on.
;
;        TOKEN -> set if you want to replace 'bpch' with 'nc' in all
;             part of the full name. Default is to have extension
;            ".nc" added to file name only.
;
; OUTPUTS:
;        None
;
; REQUIREMENTS:
;        References routines from the GAMAP package.
;
; NOTES:
;        To work recursively on the directories, I use FILE_SEARCH
;        with 2 arguments. This is not possible with MFINDFILE, and
;        works only with IDL 5.5 and above. 
;
; EXAMPLES:
;
;        indir  = '/as/data/geos/GEOS_1x1/EDGAR_200607/'         
;        outdir = '/home/phs/data/EDGAR_200607/nc/'             
;        mask   = '*1x1'                                          
;        make_multi_nc, input_parent_dir=indir, out=outdir, mask=mask, /r
;
; MODIFICATION HISTORY:
;
;       Thu Oct 29 16:40:28 2009, Philippe Le Sager
;       <phs@sol.as.harvard.edu>
;
;		now pass _extra keyword to BPCH2COARDS (eg /NAMER
;		needed for nested data)
;
;       Thu Feb 19 11:12:16 2009, Philippe Le Sager
;       <phs@sol.as.harvard.edu>
;
;               renamed MAKE_MULTI_NC. Added recursion keyword
;               (default is OFF). Now **AUTOMATICALLY** creates missing
;               subdirectory in output directory tree, when in
;               recursive mode.
;
;       Tue Jan 27 10:40:49 2009, Philippe Le Sager
;       <phs@sol.as.harvard.edu>
;
;		v1, based on make_c_nc.pro to work on several
;		directories at once.
;
;        bmy, 30 Nov 2010: GAMAP VERSION 2.15
;                          - Updated comments and category in header
;        bmy, 01 Feb 2012: GAMAP VERSION 2.16
;                          - Skip processing ASCII text files
;        mps, 04 Mar 2015: - Bug fix: Now assign InFileName before checking
;                            file type
;        
;-
; Copyright (C) 2009-2012, 
; Bob Yantosca and Philippe Le Sager, Harvard University. 
; This software is provided as is without any warranty
; whatsoever.  It may be freely used, copied or distributed for
; non-commercial purposes.  This copyright notice must be kept with any
; copy of this software. If this software shall be used commercially or
; sold as part of a larger package, please contact the author.  Bugs
; and comments should be directed to yantosca@seas.harvard.edu or
; plesager@seas.harvard.edu with subject "IDL routine make_multi_nc"
;-----------------------------------------------------------------------


pro make_multi_nc, input_parent_dir=dir,      mask=mask,   $
                   output_parent_dir=new_dir, token=token, $
                   recursion=recursion,  _extra=e

   ; -- check inputs
   IF n_elements(dir      ) eq 0 then dir = dialog_pickfile(/directory)
   IF n_elements(mask     ) eq 0 then mask = '*'
   if n_elements(new_dir  ) eq 0 then new_dir = dialog_pickfile(/directory, path=dir)
   recur = keyword_set(recursion) 

   ; take off the path separator
   dir     = file_expand_path( dir     )
   new_dir = file_expand_path( new_dir )
   
   sep     = path_sep() 

   ; -- Get filenames with a MASK in DIR (and its sub-dir)
   if recur then $
      list = File_Search( DIR, mask, count=count, /fully_qualify_path) $
   else $
      list = File_Search( DIR + sep + mask, count=count, /fully_qualify_path)
   
   if count eq 0 then begin
      print, 'No matching file! Returning...'
      return
   endif else print, 'Find '+strtrim(count, 2)+' matching files'


   TenChars = BytArr(10)
 
   ; Loop over files
   for F = 0L, count-1L do begin

      ; Input file
      InFileName  = List[F]

      ; Skip ASCII files (bmy, 2/1/11)
      cmd = 'file ' + InFileName
      spawn, cmd, result
      if ( StrPos( Result, 'ASCII' ) ge 0 ) then begin
         print, InfileName + ' is an ASCII file, skipping ...'
         goto, Next
      endif

      ; Purge the memory
      CTM_CleanUp
 
      ; Output file -- replace old parent dir with new one
      substring   = StrSplit(List[F], dir, /extract, /regex)
      OutFileName = new_dir + substring[0]+ '.nc'

      ; replace bpch in the path with nc
      if keyword_set(token) then $
         OutFileName = Replace_Token( OutFileName, 'bpch', 'nc', delim='' )

      ; check that the same directory tree exist in the new parent
      ; directory. If not create subdirectory as needed (this works
      ; nicely recursively)
      tempdir = file_dirname(outfilename)
      if ~file_test( tempdir ) then file_mkdir, tempdir
 
      ; Convert to netCDF
      Bpch2Coards, InFileName, OutFileName,  _extra=e

Next:
   endfor
end	
