;-----------------------------------------------------------------------
;+
; NAME:
;        MAKE_NC_RECURSIVE
;
; PURPOSE:
;        Converts files matching a mask to netCDF format. 
;        The routine works recursively. This is a convenience 
;        wrapper for GAMAP routine bpch2coards.pro, to work on 
;        several directories at once.
;
;        You just need to (1) create the same directory tree in the
;        output location, then (2) run the routine once.
;
; CATEGORY:
;        File & I/O, BPCH Format, Scientific Data Formats
;
; CALLING SEQUENCE:
;        MAKE_NC_RECURSIVE
;
; INPUTS:
;        none
;
; KEYWORD PARAMETERS:
;        MASK -> string, default is '*', that is all files
;
;        INPUT_PARENT_DIR -> top directory of files to convert, if
;             not specified, a dialog window pops up
;
;        NEW_PARENT_DIR -> top directory destination for netCDF
;             files, if not specified, a dialog window pops up
;        
;        TOKEN -> set if you want to replace 'bpch' with 'nc' in all
;             part of the full name 
;
; OUTPUTS:
;        None
;
; REQUIREMENTS:
;        References routines from the GAMAP package.
;
; NOTES:
;        To work recursively on the directories, I use FILE_SEARCH
;        with 2 arguments. This is not possible with MFINDFILE, and
;        works only with IDL 5.5 and above. 
;
; EXAMPLE:
;
;        ; after creating a subdirectories tree in outdir similar to the
;        ; one in indir: 
;        indir  = '/as/data/geos/GEOS_1x1/EDGAR_200607/'         
;        outdir = '/home/phs/data/EDGAR_200607/nc/'             
;        mask   = '*1x1'                                          
;        make_nc_recursive, input_parent_dir=indir, new=outdir, mask=mask
;
; MODIFICATION HISTORY:
;
;       Tue Jan 27 10:40:49 2009, Philippe Le Sager
;       <phs@sol.as.harvard.edu>
;
;		v1, based on make_c_nc.pro to work on several
;		directories at once.
;
;        bmy, 30 Nov 2010: GAMAP VERSION 2.15
;                          - Updated comments and categories in header
;
;-
; Copyright (C) 2008, Bob Yantosca and Philippe Le Sager, Harvard
; University This software is provided as is without any warranty
; whatsoever.  It may be freely used, copied or distributed for
; non-commercial purposes.  This copyright notice must be kept with any
; copy of this software. If this software shall be used commercially or
; sold as part of a larger package, please contact the author.  Bugs
; and comments should be directed to yantosca@seas.harvard.edu or
; plesager@seas.harvard.edu with subject "IDL routine make_nc_recursive"
;-----------------------------------------------------------------------


pro Make_nc_Recursive, input_parent_dir=dir,   mask=mask, $
                       new_parent_dir=new_dir, token=token

   ; -- check inputs
   IF n_elements(dir    ) eq 0 then dir = dialog_pickfile(/directory)
   IF n_elements(mask   ) eq 0 then mask = '*'
   if n_elements(new_dir) eq 0 then new_dir = dialog_pickfile(/directory)

   ; take off the path separator
   dir     = file_expand_path( dir     )
   new_dir = file_expand_path( new_dir )
   

   ; -- Get filenames with a MASK in DIR
   list = File_Search(DIR, mask, count=count, /fully_qualify_path)
 
   if count eq 0 then begin
      print, 'No matching file! Returning...'
      return
   endif else print, 'Find '+strtrim(count, 2)+' matching files'


 
   ; Loop over files
   for F = 0L, count-1L do begin
   
      ; Purge the memory
      CTM_CleanUp
 
      ; Input file
      InFileName  = List[F]
 
      ; Output file -- replace old parent dir with new one. We assume
      ;                that the same directory tree exist in the
      ;                output directory
      substring   = StrSplit(List[F], dir, /extract, /regex)
      OutFileName = new_dir + substring[0]+ '.nc'

      ; replace bpch in the path with nc
      if keyword_set(token) then $
         OutFileName = Replace_Token( OutFileName, 'bpch', 'nc', delim='' )
 
      ; Convert to netCDF
      Bpch2Coards, InFileName, OutFileName

   endfor
end	
