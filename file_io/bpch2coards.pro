;-----------------------------------------------------------------------
;+
; NAME:
;        BPCH2COARDS
;
; PURPOSE:
;        Reads data from a binary punch file and saves it in a
;        COARDS-compliant netCDF (network Common Data Format) file.
;
;        NOTE: COARDS is a formatting standard for netCDF files which
;        is widely used in both the atmospheric & climate communities.
;        COARDS-compliant netCDF files can be read by GAMAP, GrADS and
;        other plotting packages.
;        
;        See http://ferret.wrc.noaa.gov/noaa_coop/coop_cdf_profile.html
;        for more information about the COARDS standard.
;
; CATEGORY:
;        File & I/O, BPCH Format, Scientific Data Formats
;
; CALLING SEQUENCE:
;        BPCH2COARDS, INFILE, OUTFILE [, Keywords ]
;
; INPUTS:
;        INFILE -> Name of the binary punch file to read.  If INFILE
;             is not passed, the user will be prompted to supply a 
;             file name via a dialog box.
;
;        OUTFILE -> Name of the netCDF file to be written.  It is
;             recommended to insert the tokens %DATE% (or %date%)
;             into OUTFILE, since BPCH2COARDS will write a separate
;             netCDF file for each unique YYYYMMDD value contained
;             within the *.bpch file.  If OUTFILE is not specified,
;             BPCH2COARDS will use the default file name 
;             "coards.%DATE%.nc".
;
; KEYWORD PARAMETERS:
;        DIAGN -> Array of diagnostic categories from the bpch file
;             to save to netCDF format.  If omitted, BPCH2COARDS will 
;             save all diagnostic categories.  
; 
;        /VERBOSE -> If set, will print the name of each tracer
;             as it is being written to the netCDF file.  Useful
;             for debugging purposes.
;
;        /NC4 -> Write a netCDF4 file instead of netCDF3. Default is
;             NetCDF3. NetCDF4 support requires IDL 8.0 or later. 
;
;        COMPRESS -> Integer 0-9 specifies amount of compression in
;             netCDF4 files. Default is 2, with very little benefit
;             for higher compression.
;
;        /PCOORD -> Use mean pressure as the vertical coordinate rather
;             sigma or eta
;
;        /ALTCOORD -> Use mean altitude as the vertical coordinate
;             rather than sigma or eta
;
;        /TROPONLY -> Write only tropospheric layers
;
;        /ONEFILE -> Write all data to one netCDF output file.
;              Default is one file per calendar day.
;
;        _EXTRA=e -> Picks up additional keywords for NCDF_SET
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        External Subroutines Required:
;        ============================================
;        CTM_GET_DATA        TAU2YYMMDD    (function)  
;        UNDEFINE            REPLACE_TOKEN (function)
;        STRREPL (function)  GETMODELANDGRIDINFO
;    
; REQUIREMENTS:
;        (1) References routines from GAMAP and TOOLS packages.
;        (2) You must use a version of IDL containing the NCDF routines.
;
; NOTES:
;        (1) BPCH2COARDS assumes that each data block in the *.bpch
;            file is either 2-D (lon-lat) or 3-D (lon-lat-alt). 
;
;        (2) BPCH2COARDS assumes that the number type of each data 
;            block in the *.bpch file is REAL*4 (a.k.a. FLOAT). 
;
;        (3) BPCH2COARDS assumes that all data blocks in the *.bpch
;            file adhere to same horizontal grid.  This will always
;            be true for output files from the GEOS-CHEM model.
;
;        (4) BPCH2COARDS will write a separate COARDS-compliant netCDF
;            file corresponding to each unique YYYYMMDD date.  This 
;            prevents the files from becoming too large to be read
;            with IDL.
;
;        (5) BPCH2COARDS will replace the %DATE% (or %date%) token with
;            the current YYYYMMDD value.  Therefore, it is recommended
;            to insert this token into the string passed via OUTFILE.
;
;        (6) BPCH2COARDS will write arrays containing the latitudes,
;            longitudes to the netCDF file.  For 3-D data blocks,
;            the eta or sigma centers will also be written to the
;            file.  Time will be written as TAU values (i.e. hours
;            since 00:00 GMT on 01-Jan-1985.
;
;        (7) The netCDF library has apparently been updated in 
;            IDL 6.0+.  The result is that variable names containing
;            characters such as '$', '=', and ':' may now cause an
;            error in NCDF_VARDEF.  Therefore, we now pre-screen 
;            tracer names with function NCDF_VALID_NAME.
;           
; EXAMPLE:
;        BPCH2COARDS, 'myfile.bpch', 'myfile.%DATE%.nc'
;
;            ; Will write the contents of "myfile.bpch" to one
;            ; or more COARDS-compliant netCDF files adhering
;            ; to the filename convention "myfile.YYYYMMDD.nc"
;
; MODIFICATION HISTORY:
;  rjp & bmy, 17 Mar 2005: GAMAP VERSION 2.03
;                          - Based on bpch2nc.pro
;        bmy, 21 Jul 2005: GAMAP VERSION 2.04
;                          - Bug fix: 
;        bmy, 13 Jul 2006: GAMAP VERSION 2.05
;                          - Remove call to PTR_FREE
;  bmy & phs, 13 Jul 2007: GAMAP VERSION 2.10
;                          - Now count GCAP among the GEOS family
;                            for the purpose of converting the TAU
;                            date to a YYYY/MM/DD date.
;        phs, 29 Oct 2009: GAMAP VERSION 2.14
;                          - Can process files with 3D data on both
;                            centers and edges of the grid boxes.
;        bmy, 19 Dec 2011: GAMAP VERSION 2.16
;                          - Now handles multiple vertical dimensions
;                            in the bpch file properly.
;                          - Bug fix: now write vertical levels edges to 
;                            the file.
;        bmy, 27 Sep 2012: - Bug fix: Now handle data blocks that straddle
;                            the date line.
;        bmy, 05 Nov 2013: GAMAP VERSION 2.17
;                          - Change attributes for better COARDS compliance
;        bmy, 12 Feb 2014: GAMAP VERSION 2.18
;                          - Add more modifications for 4-D data blocks
;                            from bpch files created w/ GC_COMBINE_ND49
;        bmy, 03 Mar 2015: - Now define dims in order: time, lev, lon, lat,
;                            which is more COARDS compliant.
;        cdholmes, 29 Mar 2017 - Add support for NetCDF4 and
;                                compression
;                              - Add support for pressure and altitude
;                                as vertical coordinate
;                              - ONEFILE puts all data into a single
;                                output file
;                              - TROPONLY limits the output to
;                                troposphere layers
;                              - Singleton "altXXX" dimensions are avoided
;        bmy, 14 Mar 2018: - Define all index arrays as DOUBLE in order to
;                            match the GEOS-Chem HISTORY output
;        bmy, 28 Mar 2018: - Disable non-COARDS-compliant modifications
;
;-
; Copyright (C) 2002-2018,
; Bob Yantosca and Philippe Le Sager, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.as.harvard.edu with subject "IDL routine bpch2coards"
;-----------------------------------------------------------------------


pro Bpch2COARDS, InFile, OutFile, Verbose=Verbose, DiagN=DiagN, $
                 PCoord=PCoord, AltCoord=AltCoord, TropOnly=TropOnly,$
                 NC4=NC4, Compress=Compress,       OneFile=OneFile,  $
                 SimpleName=SimpleName, _EXTRA=e
 
   ;====================================================================
   ; Initialization
   ;====================================================================

   ; External functions
   FORWARD_FUNCTION Tau2YYMMDD, Replace_Token,                 $
                    ChkStru,    StrRepl,       NCDF_Valid_Name

   ; Keywords
   Verbose = Keyword_Set( Verbose )
   OneFile = Keyword_Set( OneFile )
   NC4     = Keyword_Set( NC4 )
   PCoord  = Keyword_Set( PCoord )
   AltCoord= Keyword_set( AltCoord )
   TropOnly= Keyword_Set( TropOnly )
   SimpleName= Keyword_Set( SimpleName )

   ; Set default compression level to 2
   if (~Keyword_set( Compress )) then Compress=2
   ; Compress must be an integer 0-9
   Compress = max( [min( [fix(Compress), 9] ), 0] )
   ; Compress must be zero for netCDF3 files 
   if (~NC4) then Compress = 0

   ; Put NetCDF4 and Compression in structure, 
   ; Allows them to be passed through _Extra which will 
   ; ignore them in IDL versions that don't support these features
   nc4config = {NetCDF4_Format: NC4}
   if (Compress gt 0) then $
      nc4config = create_struct( nc4config, 'GZip', Compress )

   ; Ouptut file name
   if ( N_Elements( OutFile ) eq 0 ) then OutFile = 'coards.%DATE%.nc'

   ; Make sure to substitute /users/ctm/bmy for ~bmy, etc...
   OutFile = Expand_Path( OutFile )

   ; Format strings for use below
   F1 = '(''Defined tracer '',i5, '' = '', a)'
   F2 = '(''Saving tracer  '',i5, '' = '', a)'

   ;====================================================================
   ; Read data from the *.bpch file
   ;====================================================================
   
   ; Make sure the NC library is supported
   if ( not NCDF_Exists() ) then begin
      Message, 'netCDF is not supported in this IDL version!', /Continue
      return
   endif

   ; Read all data blocks
   if ( N_Elements( DiagN ) gt 0 )                                       $
      then CTM_Get_Data, DataInfo, DiagN, File=InFile, /Quiet, _EXTRA=e  $
      else CTM_Get_Data, DataInfo,        File=InFile, /Quiet, _EXTRA=e

   ; Determine if this is a 4D data block that was created
   ; by GC_COMBINE_ND49 or GC_COMBINE_ND50 (bmy, 2/6/12)
   Is_4D = ( StrPos( DataInfo[0].Format, '4-D data blocks' ) ge 0 )

   ; Get unique starting YYYYMMDD, HHMMSS, TAU0 values from the bpch file
   Tau0    = DataInfo[*].Tau0
   Date    = Tau2YYMMDD( Tau0, /NFormat )
   N_Date  = N_Elements( Date )
   Date0   = Date[ 0         : N_Date/2L - 1L ]
   Hour0   = Date[ N_Date/2L : *              ]
   Date0_U = Date0[ Uniq( Date0, Sort( Date0 ) ) ]
   Hour0_U = Hour0[ Uniq( Hour0, Sort( Hour0 ) ) ] / 10000L
   Tau0_U  = Tau0[  Uniq( Tau0,  Sort( Tau0  ) ) ]

   ; Get unique ending YYYYMMDD, HHMMSS, TAU1 values from the bpch file
   Tau1    = DataInfo[*].Tau1
   Date    = Tau2YYMMDD( Tau1, /NFormat )
   N_Date  = N_Elements( Date )
   Date1   = Date[ 0         : N_Date/2L - 1L ]
   Hour1   = Date[ N_Date/2L : *              ]
   Date1_U = Date1[ Uniq( Date1, Sort( Date1 ) ) ]
   Hour1_U = Hour1[ Uniq( Hour1, Sort( Hour1 ) ) ] / 10000L
   Tau1_U  = Tau1[  Uniq( Tau1,  Sort( Tau1  ) ) ]

   if ( Is_4D ) then begin

      ;-----------------------------------------------------------------
      ; Bpch file has 4D data (i.e. created by GC_COMBINE_ND49 etc.)
      ;-----------------------------------------------------------------

      ; The # of times is stored as the last element of DIM
      N_Times         = DataInfo[0].Dim[3]      

      ; DELTA_TIME tag for COARDS [hours]
      Delta_Time      = Double( Tau1 - Tau0 ) / Double( N_Times-1 )

      ; TAU values for all timeseries blocks
      All_Tau         = Tau0 + ( FindGen( N_Times ) * Delta_Time )

      ; Convert TAU values to NYMD/NHMS
      Result          = Tau2YYMMDD( All_Tau, /NFormat )
      Date0           = Result[ 0:N_Times-1 ]
      Hour0           = Result[ N_Times: * ]

      ; Find the list of unique dates
      Date0_U         = Date0[ Uniq( Date0, Sort( Date0 ) ) ]
      Date1_U         = Date0_U

      ; Find the list of unique times      
      Hour0_U         = Hour0[ Uniq( Hour0, Sort( Hour0 ) ) ] / 10000L
      Hour1_U         = Hour0_U

      ; Declare an array of TAU0 values
      Tau0_U          = DblArr( N_Times )

      ; Populate TAU0_U
      for D = 0L, N_Times-1L do begin
         Tau0_U[D]    = Tau0 + ( D * Delta_Time )
      endfor

      ; Set TAU1_U to the same values as TAU0_U
      Tau1_U          = Tau0_U

   endif

   if ( Is_4D or OneFile ) then begin
      
      ; Start & end dates & times
      Date_Start      = Date0_U[0]
      Date_End        = Date1_U[N_Elements( Date1_U )-1L]
      Time_Start      = Hour0_U[0]
      Time_End        = Hour1_U[N_Elements( Hour1_U )-1L]

   endif       

   ; Number of unique YYYYMMDD dates in the file
   D_Dim              = N_Elements( Date0_U ) 

   ; Number of unique HHMMSS times for each YYYYMMDD date
   T_Dim              = N_Elements( Hour0_U )
   
   ;====================================================================
   ; Write a new NC file for each data block in the *.bpch file
   ; NOTE: Data for the same YYYYMMDD will be saved in the same file
   ;====================================================================
   
   ; Get the number data blocks
   if ( Is_4D )           $
      then N_Iter = 1     $
      else N_Iter = D_Dim

   ; Loop over the # of data blocks
   for T = 0L, D_Dim-1L do begin

      ; Define DELTA_TIME tag for COARDS [hours]
      if ( T_Dim gt 1 ) then begin

         ; For netCDF files w/ multiple times, DELTA_TIME is the 
         ; difference in hours computed from each specified time
         Delta_Time = ( Hour0_U[1] - Hour0_U[0] )

      endif else begin

         ; Get the astronomical Julian date corresponding to DATE0_U
         Date2YMD, Date0_U[T], Y, M, D
         Jd_0 = JulDay( M, D, Y )

         ; Get the astronomical Julian date corresponding to DATE1_U
         Date2YMD, Date1_U[T], Y, M, D
         Jd_1 = JulDay( M, D, Y )

         ; For netCDF files with a single time, DELTA_TIME is just the 
         ; # of hours in the interval between the start & end dates
         Delta_Time = Long( Jd_1 - Jd_0 ) * 24L 

      endelse 

      ; Define templates for DELTA_TIME string
      DT_Str  = '0000-%MM%-%DD% %HH%:00:00'

      ; Define structure for REPLACE_TOKEN
      if ( Delta_Time lt 24 ) then begin
         Stru = { MM:'00', DD:'00', HH:String( Delta_Time, Format='(i2.2)' ) }
      endif else if ( Delta_Time ge 24 AND Delta_Time lt 744 ) then begin
         Stru = { MM:'00', DD:'01', HH:'00' }
      endif else begin
         Stru = { MM:'01', DD:'00', HH:'00' }
      endelse

      ; Replace tokens with actual time 
      DT_Str = Replace_Token( DT_Str, Stru )

      if ( Is_4D or OneFile ) then begin
         ThisDataInfo = DataInfo
      endif else begin
         ; THISDATAINFO is an array of data blocks for time TAU0
         Ind          = Where( Date0 eq Date0_U[T] )
         ThisDataInfo = DataInfo[Ind]
      endelse

      ; Error check
      if ( N_Elements( ThisDataInfo ) eq 0 ) then begin
         S = 'Could not find data blocks for time ' + String( Date0_U[T] )
         Message, S
      endif

      ; Get MODELINFO and GRIDINFO structures, assume that
      ; all data blocks in the punch file are on the same grid
      ; (which is 99.999999999% true for most cases)
      GetModelAndGridInfo, ThisDataInfo[0], ModelInfo, GridInfo

      ; Does data block come from GEOS-CHEM model family?
      IsGEOS          = ( ModelInfo.Family eq 'GEOS'   OR $
                          ModelInfo.Family eq 'GCAP'   OR $
                          ModelInfo.Family eq 'MOPITT' )

      ; Does data block come from GISS model family?
      IsGISS          = 1L - IsGEOS

      ; Does the model use a hybrid sigma-pressure grid?
      IsHybrid        = ( ModelInfo.Hybrid eq 1 )

      ; Are any of the data blocks in the file 3-D? 
      ; If so, we'll need to save the vertical coordinate
      Ind             = Where( ThisDataInfo[*].Dim[2] gt 1 )
      Is_Any_3D       = ( Ind[0] ge 0 )

      ; Create a string for the YYYYMMDD date
      if ( Is_4D )                                              $
         then NymdStr = String( Date_Start, Format = '(i8.8)' ) $
         else NymdStr = String( Date0_U[T], Format = '(i8.8)' ) 

      ; Insert YYYYMMDD into output file name
      ThisFile     = Replace_Token( OutFile, '%DATE%', NymdStr )
         
      ; Write lon/lat/alt/time info here      
      F               = ThisDataInfo[0].First
      D               = ThisDataInfo[0].Dim
;------------------------------------------------------------------------------
; Prior to 9/27/12:
; Need to implement a fix for regions straddling the date line (bmy, 9/27/12)
;      Lon             = [ GridInfo.XMid[ F[0]-1L : F[0]+D[0]-2L ] ]
;------------------------------------------------------------------------------

      ; Indices describing the ends of the lon range (in IDL notation)
      I0              =  F[0] - 1L
      I1              =  F[0] + D[0] - 2L 

      ; Make sure I1 does not exceed the max # of longitudes
      if ( I1 gt GridInfo.IMX ) then I1 = I1 mod GridInfo.IMX

      ; Define longitude array (bmy, 9/27/12)
      if ( I0 gt I1 ) then begin

         ; Here we are straddling the date line.  NOTE: Make sure longitudes
         ; are monotonically increasing, this is required for COARDS!
         Lon          = [ GridInfo.Xmid[ I0 : GridInfo.IMX-1L ] - 360.0,  $
                          GridInfo.Xmid[  0 : I1              ]          ]
      endif else begin

         ; Here we are not straddling the date line
         Lon          = [ GridInfo.XMid[ I0 : I1              ]          ]

      endelse

      ; Latitude range
      Lat             = [ GridInfo.YMid[ F[1]-1L : F[1]+D[1]-2L ] ]

      ; Get X dims of all data blocks (only take unique dims)
      XDim            = ThisDataInfo[*].Dim[0]
      XDim            = XDim[ Uniq( XDim, Sort( XDim ) ) ]

      ; Get Y dims of all data blocks (only take unique dims)
      YDim            = ThisDataInfo[*].Dim[1]
      YDim            = YDim[ Uniq( YDim, Sort( YDim ) ) ]

      ; If at least one data block for this date is 3-D
      ; then we must also define the vertical coordinate
;      if ( Is_Any_3D ) then begin
      if ( Is_Any_3D OR Is_4D ) then begin

         ; Get maximum altitude dimension
         AllZDims     = ThisDataInfo[*].Dim[2] 
         AllZDims     = AllZDims[ Uniq( AllZDims, Sort( AllZDims ) ) ]
         ZDim         = Max( AllZDims )
         
         ; Drop all 1-layer vertical dimensions
         junk = where( AllZDims eq 1, complement=idx)
         AllZDims = AllZDims[idx]

         ; now check if there is 3D data on the box edges (phs, 10/29/09)
         if ZDIM gt ModelInfo.NLayers then begin
            ZEDGE     = ZDIM 
            ZDIM      = ModelInfo.NLayers
         endif else ZEDGE = 0

         ; Keep only tropospheric layers
         if (TropOnly) then begin
            ; ZDIM cannot be larger than NTrop
            ZDIM  = Min( [ZDIM,  ModelInfo.NTrop]   )
            ; ZEDGE cannot be larger than NTrop
            ZEDGE = Min( [ZEDGE, ModelInfo.NTrop+1] )
            ; No vertical dimension can be greater than the larger
            ; of ZDIM or ZEDGE
            AllZDims = AllZDims < max( [ZDIM, ZEDGE] )
            ; Drop any non-unique
            AllZDims = AllZDims[ Uniq( AllZDims, sort( AllZDims) ) ]
         endif

         ; Pressure at surface and model top, hPa
         PSurf = ModelInfo.PSurf
         PTop  = ModelInfo.PTop 

         ; Get ETA (hybrid) or SIGMA (non-hybrid) coordinates
         if ( IsHybrid ) then begin
            EtaC = GridInfo.EtaMid [ 0:ZDim-1L ]
            EtaE = GridInfo.EtaEdge[ 0:ZDim    ]
         endif else begin
            SigC = GridInfo.SigMid [ 0:ZDim-1L ]
            SigE = GridInfo.SigEdge[ 0:ZDim    ]
         endelse    

         ; Pressure at centers and edges
         PresC = GridInfo.PMid [ 0:ZDim-1L ]
         PresE = GridInfo.PEdge[ 0:ZDim    ]

         ; Altitude at centers and edges, km
         AltC = GridInfo.ZMid [ 0:ZDim-1L ]
         AltE = GridInfo.ZEdge[ 0:ZDim    ]

      endif

      ;=================================================================
      ; Define netCDF variables for lon, lat, lev, time coordinates
      ;=================================================================

      ; Open netCDF file for ouptut
      fId = NCDF_Create( ThisFile, /Clobber, _extra=nc4Config )

      ;--------------------------
      ; Global Attributes
      ;--------------------------
      ncType = NC4 ? 'NetCDF-4' : 'NetCDF-3' 

      TitleStr = 'COARDS/netCDF file created by BPCH2COARDS (GAMAP v2-17+)'
      NCDF_AttPut,    fId, /Global, 'Title',       TitleStr
      NCDF_AttPut,    fId, /Global, 'Conventions', 'COARDS'
      NCDF_AttPut,    fId, /Global, 'Format',      ncType
      NCDF_AttPut,    fId, /Global, 'Model',       ModelInfo.Name
      NCDF_AttPut,    fId, /Global, 'Delta_Lon',   ModelInfo.Resolution[0]
      NCDF_AttPut,    fId, /Global, 'Delta_Lat',   ModelInfo.Resolution[1]
      NCDF_AttPut,    fId, /Global, 'NLayers',     ModelInfo.NLayers
; NOTE: This section sometimes doesn't work, so comment it out
;      if ( Is_4d or OneFile ) then begin
;         NCDF_AttPut, fId, /Global, 'Start_Date',  Date_Start
;         NCDF_AttPut, fId, /Global, 'Start_Time',  Time_Start
;         NCDF_AttPut, fId, /Global, 'End_Date',    Date_End
;         NCDF_AttPut, fId, /Global, 'End_Time',    Time_End
;      endif else begin
;         NCDF_AttPut, fId, /Global, 'Start_Date',  Date0_U[T]
;         NCDF_AttPut, fId, /Global, 'Start_Time',  Hour0_U[0]
;         NCDF_AttPut, fId, /Global, 'End_Date',    Date1_U[T]
;         NCDF_AttPut, fId, /Global, 'End_Time',    Hour1_U[T_Dim-1L]
;      endelse
      NCDF_AttPut,    fId, /Global, 'Delta_Time',  Delta_Time

      ;--------------------------
      ; Time Dimension
      ;--------------------------

      ; Define the time dimension
      NC_TDim = NCDF_DimDef( fId, 'time', /UNLIMITED )

      ; Define the variable which will hold times
      vTim    = NCDF_VarDef( fId, 'time', NC_TDim, /Double, _extra=nc4Config )

      ; Save attributes for time variable
      if ( ModelInfo.Name eq 'GCAP' )                           $
         then NCDF_AttPut, fID, vTim, 'calendar', 'noleap'     $
         else NCDF_AttPut, fID, vTim, 'calendar', 'gregorian'
      NCDF_AttPut, fId, vTim, 'long_name', 'Time, instantaneous or at start of averaging period'
      NCDF_AttPut, fId, vTim, 'units',     'hours since 1985-1-1 00:00:0.0'
      NCDF_AttPut, fId, vTim, 'axis',      'T'
      NCDF_AttPut, fId, vTim, 'delta_t',   DT_Str
 
      if ( Verbose ) then Print, vTim, 'time', Format=F1

;-----------------------------------------------------------------------------
; Prior to 3/28/18:
; Skip defining date_length variable, this is not COARDS compliant
; (bmy, 3/28/18)
;     ; Define variable for date
;      if (NC4) then begin
;         ; String type
;         vDate   = NCDF_VarDef( fID, 'date', NC_TDim, /String, $
;                                _extra=nc4config )
;      endif else begin
;         ; NetCDF3 doesn't have strings, so define a fixed-length char
;         NC_SDim = NCDF_DimDef( fId, 'date_length', 19 )
;         vDate   = NCDF_VarDef( fID, 'date', [NC_SDim, NC_TDim], /Char, $
;                                _extra=nc4config )
;      endelse
;      NCDF_AttPut, fId, vDate, 'long_name', 'Date'
;      NCDF_AttPut, fId, vDate, 'units', 'YYYY-MM-DD hh:mm:ss'
;-----------------------------------------------------------------------------

      ;--------------------------
      ; Altitude Dimension
      ;--------------------------
;      if ( Is_Any_3D ) then begin
      if ( Is_Any_3D OR Is_4D ) then begin
         
         ; Make an array to hold dimension information
         AltDim = LonArr( N_Elements( AllZDims ) )
  
         ; Loop over each vertical dimension
         for LL = 0, N_Elements( AllZDims )-1 do begin
            
            ; Pick the right name for the netCDF variable declarations
            case ( AllZDims[LL] ) of
               ZEdge : name = 'ilev'
               ZDim  : name = 'lev'
               else  : name = 'alt' + string( AllZDims[LL], format='(i3.3)' )
            endcase

            ; Define a netCDF dimension for each # of unique
            ; number of vertical levels in the file
            AltDim[LL] = NCDF_DimDef( fId, name, AllZDims[LL] )
         endfor

;-----------------------------------------------------------------------------
; Prior to 3/28/18:
; This section can cause problems, so skip (bmy, 3/28/18)
;         ; COARDS requires that the index arrays be of the same
;         ; name as the dimensions used to define them. Therefore,
;         ; we need to define netCDF variables for the "lev" and
;         ; "ilev" index arrays.  "lev" is the max # of levels and
;         ; "ilev" is the max # of level edges in the file.
;
;         ; Loop over 3 different types of vertical coordinates: 
;         ; Pressure, Altitude, Eta/Sigma
;         ; One will be saved as "lev" and all as variables
;         vIDc_save = intarr(3)
;         vIDe_save = intarr(3)
;         for I=0,2 do begin
;            
;            doDim = 0L
;
;            ; Set the name and units for each vertical coordinate type
;            case I of
;               0:begin
;                  ; Pressure
;                  if (PCoord) then doDim=1L
;                  vname = 'Pressure'
;                  vunit = 'hPa'
;                  vpos  = 'up'      ;vpos  = 'down' (bmy, 1/8/18)
;                  LevC  = PresC
;                  LevE  = PresE
;                  end
;               1:begin
;                  if (AltCoord) then doDim=1L
;                  vname = 'Altitude'
;                  vunit = 'km'
;                  vpos  = 'up'
;                  LevC  = AltC
;                  LevE  = AltE
;                  end
;               2:begin
;                  ; 
;                  if ~(PCoord or AltCoord) then doDim=1L
;                  if (IsHybrid) then begin
;                     vname = 'Eta'
;                     vunit = 'eta_level'
;                     vpos  = 'up'   ;vpos  = 'down' (bmy, 1/8/18)
;                     LevC  = EtaC
;                     LevE  = EtaE
;                  endif else begin
;                     vname = 'Sigma'
;                     vunit = 'sigma_level'
;                     vpos  = 'up'   ;vpos  = 'down' (bmy, 1/8/18)
;                     LevC  = SigC
;                     LevE  = SigE
;                  endelse
;                  end
;               else: message, 'Not defined' 
;            endcase
;
;            ; Save this variable as the vertical dimension
;            if (doDim) then begin
;
;               ; Index array for level centers
;               Ind   = Where( AllZDims eq ZDim )
;               vLev  =  NCDF_VarDef( fId, 'lev', altDim[Ind], /Double, $
;                                     _extra=nc4Config  )
;      
;               ; Save attributes for vertical grid
;               NCDF_AttPut, fId, vLev, 'long_name', vname+' Centers'
;               NCDF_AttPut, fId, vLev, 'units',     vunit
;               NCDF_AttPut, fId, vLev, 'positive',  vpos
;         
;               ; Index array for level edges
;               if ( Zedge ne 0 ) then begin
;                  Ind   = Where( AllZDims eq Zedge ) 
;                  vEdge = NCDF_VarDef( fId, 'ilev', altDim[Ind], /Double, $
;                                       _extra=nc4Config  )
; 
;                  ; Save attributes for edges
;                  NCDF_AttPut, fId, vEdge, 'long_name', vname+' Edges'
;                  NCDF_AttPut, fId, vEdge, 'units',     vunit
;                  NCDF_AttPut, fId, vEdge, 'positive',  vpos
;               endif
;
;               ; /VERBOSE output
;               if ( Verbose ) then Print, vLev, 'lev', Format=F1         
;
;            endif
;
;            ; Also save this as a regular variable
;            Ind   = Where( AllZDims eq ZDim )
;            vIDc  =  NCDF_VarDef( fId, strlowcase(vname), altDim[Ind], $
;                                  /Double, _extra=nc4Config  )
;      
;               ; Save attributes for vertical grid
;               NCDF_AttPut, fId, vIDc, 'long_name', vname+' Centers'
;               NCDF_AttPut, fId, vIDc, 'units',     vunit
;               NCDF_AttPut, fId, vIDc, 'positive',  vpos
;               NCDF_AttPut, fId, vIDc, 'axis',      'Z'
;               
;               vIDc_save[I] = vIDc
;
;               ; Index array for level edges
;               if ( Zedge ne 0 ) then begin
;                  Ind   = Where( AllZDims eq Zedge ) 
;                  vIDe = NCDF_VarDef( fId, strlowcase(vname)+'_edge', $
;                                      altDim[Ind], /Double, _extra=nc4Config  )
; 
;                  ; Save attributes for edges
;                  NCDF_AttPut, fId, vIDe, 'long_name', vname+' Edges'
;                  NCDF_AttPut, fId, vIDe, 'units',     vunit
;                  NCDF_AttPut, fId, vIDe, 'positive',  vpos
;
;                  vIDe_save[I] = vIDe
;
;               endif
;            
;
;         endfor
;-----------------------------------------------------------------------------

         ;%%% NOTE: We really only need lev and ilev,
         ;%%% the other variables are not COARDS-compliant

         ; Index array for level centers
         Ind   = Where( AllZDims eq ZDim )
         vLev  =  NCDF_VarDef( fId, 'lev', altDim[Ind], /Double, $
                                     _extra = nc4Config  )
      
         ; Save attributes for vertical grid
         NCDF_AttPut, fId, vLev, 'long_name', 'hybrid level at midpoints'
         NCDF_AttPut, fId, vLev, 'units',     '1'
         NCDF_AttPut, fId, vLev, 'positive',  'up'
         
         ; Index array for level edges
         if ( Zedge ne 0 ) then begin
            Ind   = Where( AllZDims eq Zedge ) 
            vEdge = NCDF_VarDef( fId, 'ilev', altDim[Ind], /Double, $
                                       _extra=nc4Config  )
 
            ; Save attributes for edges
            NCDF_AttPut, fId, vEdge, 'long_name', 'hybrid level at interfaces'
            NCDF_AttPut, fId, vEdge, 'units',     '1'
            NCDF_AttPut, fId, vEdge, 'positive',  'up'
         endif

         ; /VERBOSE output
         if ( Verbose ) then Print, vLev, 'lev', Format = F1   

      endif

      ;--------------------------
      ; Latitude Dimension
      ;--------------------------
      
      ; Define the latitude dimension 
      NC_YDim = NCDF_DimDef( fId, 'lat', YDim )

      ; Define the variable which will hold the latitudes
      vLat    = NCDF_VarDef( fId, 'lat', NC_YDim, /Double, _extra=nc4Config  )

      ; Save attributes for latitude variable
      NCDF_AttPut, fId, vLat, 'long_name',  'Latitude'
      NCDF_AttPut, fId, vLat, 'units',      'degrees_north'
      NCDF_AttPut, fId, vLat, 'axis',       'Y'

      ; /VERBOSE output
      if ( Verbose ) then Print, vLat, 'lat', Format=F1

      ;--------------------------
      ; Longitude Dimension
      ;--------------------------

      ; Define the longitude dimension 
      NC_XDim = NCDF_DimDef( fId, 'lon', XDim )

      ; Define the variable which will hold the longitudes
      vLon    = NCDF_VarDef( fId, 'lon', NC_XDim, /Double, _extra=nc4Config  )

      ; Save attributes for longitude variable
      NCDF_AttPut, fId, vLon, 'long_name', 'Longitude'
      NCDF_AttPut, fId, vLon, 'units',     'degrees_east'
      NCDF_AttPut, fId, vLon, 'axis',      'X'

      ; /VERBOSE output
      if ( Verbose ) then Print, vLon, 'lon', Format=F1

      ; Undefine variables
      UnDefine, Stru
      UnDefine, Date
      UnDefine, Result
      UnDefine, Ind
      UnDefine, ModelInfo
      UnDefine, GridInfo
      UnDefine, F
      UnDefine, D
      UnDefine, Tmp
      UnDefine, Tmp2
      UnDefine, IsGISS
      UnDefine, IsGEOS
      
      ;=================================================================
      ; Define netCDF variables for BPCH file data blocks
      ;=================================================================
      
      ; Array to save vID's and tracer names
      vId_Save  = LonArr( N_Elements( ThisDataInfo ) )
      Name_Save = StrArr( N_Elements( ThisDataInfo ) )
      Name_Old  = 'notValidName'

      ; Get unique TAU0's for this
      if ( Is_4D ) then begin

         ;--------------------------------------------------------
         ; 4-D DATA BLOCKS (created by GC_COMBINE_ND49)
         ; Manually set TAU0TODAY from DATE0_U and HOUR0_U
         ;--------------------------------------------------------

         ; Define the array of TAU0 data blocks
         Tau0Today = Tau0_U
         
      endif else begin

         ;--------------------------------------------------------
         ; Normal bpch files: Take TAU0TODAY from THISDATAINFO
         ;--------------------------------------------------------
         Tau0Today = ThisDataInfo[*].Tau0
         Tau0Today = Tau0Today[ Uniq( Tau0Today, Sort( Tau0Today ) ) ]

      endelse

      ; Format time as date string
      nU = n_elements( Tau0Today )
      Date0Str = strarr( nU )
      dt = tau2yymmdd( Tau0Today )
      for T=0L, nU-1L do $
         Date0Str[T] = string( dt.Year[T], dt.Month[T], dt.Day[T], $
                               dt.Hour[T], dt.Minute[T],dt.Second[T],$
                    format='(I4,"-",I02,"-",I02," ",I02,":",I02,":",I02)' )

      
      ; Loop over all BPCH datablocks for this TAU0 value
      for D = 0L, N_Elements( ThisDataInfo ) - 1L do begin

         ; Simplify name
         if (simplename) then begin
            CatName = StrTrim( ThisDataInfo[D].Category, 2 )

            case CatName of
               'IJ-AVG-$': CatName = 'x'
               'IJ-24H-$': CatName = 'x'
               'PORL-L=$': CatName = 'PL'
               'CHEM-L=$': CatName = 'x'
               'JV-MAP-$': CatName = 'j'
               'OD-MAP-$': CatName = 'optic'
               'DAO-FLDS': CatName = 'met'
               'DAO-3D-$': CatName = 'met'
               'ANTHSRCE': CatName = 'em_anth'
               'BIOFSRCE': CatName = 'em_biof'
               'BIOGSRCE': CatName = 'em_biog'
               'BIOBSRCE': CatName = 'em_bb'
               'DRYD-VEL': CatName = 'vd'
               'DRYD-FLX': CatName = 'drydep'
               else:
            endcase

            ; Drop "-$", "=$", and "$" 
            CatName = replace_token( CatName, '=$', '', delim='' )
            CatName = replace_token( CatName, '-$', '', delim='' )
            CatName = replace_token( CatName, '$',  '', delim='' )

            ; Define tracer name as CATEGORY_TRACERNAME
            Name            = CatName + '_' + $
                              StrTrim( ThisDataInfo[D].TracerName, 2 )

            ; Avoid "J_J" in photolysis rates
            Name = replace_token( Name, 'j_J', 'j_', delim='' )

            ; For DRYD-FLX, remove redundant "df" from end
            if (CatName eq 'drydep') then $
               Name = replace_token( Name, 'df', '', delim='' )

            ; For DRYD-VEL, remove redundant "dv" from end
            if (CatName eq 'vd') then $
               Name = replace_token( Name, 'dv', '', delim='' )

         endif else begin
         
            ; Define tracer name as CATEGORY__TRACERNAME
            Name            = StrTrim( ThisDataInfo[D].Category,   2 ) + $
                              '__' + $
                              StrTrim( ThisDataInfo[D].TracerName, 2 )
         endelse
         
         ; Strip out bad characters for netCDF variable names
         ; or else the code may crash (bmy, 10/20/03)
         Name            = NCDF_Valid_Name( Name )

         ; Extra fix -- GrADS will interpret "-" as a minus sign
         ; so we need to replace this with an underscore (rjp, 3/18/05)
         Name            = StrRepl( Name, '-', '_' )

         ; Append variable name & into list of previously saved variables
         Name_Save[D]    = Name

         ; Long tracer number (offset applied!)
         Tracer          = ThisDataInfo[D].Tracer 
         
         ; Get the long descriptive tracer name
         CTM_TracerInfo, Tracer, FullName=LongName
 
         ; Unit string
         Unit            = StrTrim( ThisDataInfo[D].Unit, 2 )

         ; If the unit string is blank, read it from "tracerinfo.dat"
         if ( StrLen( Unit ) eq 0 ) $
            then CTM_TracerInfo, Tracer, Unit=Unit

         if ( ThisDataInfo[D].Dim[2] gt 1 OR Is_4D ) then begin

            ; 3-D data.  Locate the appropriate netCDF vertical
            ; dimension index (in ALTDIM) and store it in NC_DIMS
            Ind     = Where( AllZDims eq ThisDataInfo[D].Dim[2] )
            NC_Zdim = AltDim[Ind]
            NC_Dims = [ NC_XDim, NC_YDim, NC_ZDim, NC_TDim ]

         endif else begin
            
            ; Define surface data dimensions for netCDF file (bmy, 12/16/11)
            NC_Dims = [ NC_XDim, NC_YDim, NC_TDim ]

         endelse

         ; Bug fix: LONGNAME cannot be a null string (bmy, 5/22/03)
         if ( LongName eq '' ) then LongName = 'Unknown Tracer Name'

         ; Have we defined this variable before?
         Chk             = Where( Name eq Name_Old )

         if ( Chk[0] lt 0 ) then begin

            ;-----------------------------------------
            ; FIRST TIME WE DEFINE VARIABLE IN netCDF
            ;-----------------------------------------

            ; Define as a netCDF variable (bpch data is FLOAT)
            vId          = NCDF_VarDef( fId, Name, NC_Dims, /Float, _extra=nc4Config  )

            ; Save attributes to netCDF file
            NCDF_AttPut, fId, vId, 'long_name',      LongName
            NCDF_AttPut, fId, vId, 'units',          Unit
            NCDF_AttPut, fId, vId, 'add_offset',     0e0
            NCDF_AttPut, fId, vId, 'scale_factor',   1e0
            NCDF_AttPut, fId, vId, 'missing_value',  1e30
            NCDF_AttPut, fId, vId, '_FillValue',     1e30

            ; Append variable ID to list of defined variables
            vId_Save[D]  = vId

            ; Append variable name to list of defined variables
            Name_Old     = [ Name_Old, Name ]

         endif else begin

            ;-----------------------------------------
            ; WE HAVE DEFINED THIS VARIABLE ALREADY
            ; BUT IT IS A FOR DIFFERENT TIME OF DAY
            ;-----------------------------------------

            ; Locate this variable among list of defined variables
            Chk0         = Where( Name eq Name_Save )

            ; Append this variable name to the list of saved
            ; variables so that we can print it with /VERBOSE
            vId_Save[D]  = vId_Save[ Chk0[0] ]

         endelse

         ; /VERBOSE output
         if ( Verbose ) then print, vId_Save[D], Name, Format=F1

         ; Error check
         if ( vId_Save[D] lt 0 ) then Message, 'Could not define variable!'

         ; Undefine variables
         UnDefine, Name      
         UnDefine, Tracer
         UnDefine, LongName
         UnDefine, Unit
         UnDefine, NC_Dims
         UnDefine, vId
         
      endfor
  
      ;=================================================================
      ; Now save the data into all netCDF variables
      ;=================================================================

      ; Exit from netCDF definition mode
      NCDF_Control, fId, /EnDef
      
      ;---------------------------
      ; Longitude (deg E)
      ;---------------------------

      ;%%%% KLUDGE, COMMENT OUT THESE LINES OF CODE
      ;%%%% for better adherence to COARDS standard
      ;%%%% ckeller, 16 Oct 2014
      ;; Convert to degrees east if necessary
      ;Ind = Where( Lon lt 0.0 )
      ;if ( Ind[0] ge 0 ) then Lon[Ind] = Lon[Ind] + 360.0

      ; Save to netCDF file
      NCDF_VarPut, fId, vLon, Lon

      ; /VERBOSE output
      if ( Verbose ) then Print, vLon, 'lon', Format=F2

      ;----------------------------
      ; Latitude (deg N)
      ;----------------------------

      ; Save to netCDF file
      NCDF_VarPut, fId, vLat, Lat

      ; /VERBOSE output
      if ( Verbose ) then Print, vLat, 'lat', Format=F2

;-----------------------------------------------------------------------------
; Prior to 3/28/18:
; This section is problematic, so comment out (bmy, 3/28/18)
;      ;-----------------------------
;      ; Altitude (Pressure, Altitude, ETA or SIGMA)
;      ;-----------------------------
;      if ( Is_Any_3D OR Is_4D ) then begin
;         
;         ; Level Centers
;         NCDF_VarPut, fId, vLev, LevC
;         if ( Verbose ) then print, vLev, 'lev', Format=F2
;
;         ; Level edges
;         if ( Zedge ne 0 ) then begin
;            NCDF_VarPut, fId, vEdge, LevE
;            if ( Verbose ) then print, vLev, 'edge', Format=F2
;         endif
;
;         ; Also save the other vertical variables
;         for I=0,2 do begin
;            
;            case I of
;               0: begin
;                  ValC=PresC
;                  ValE=PresE
;                  end
;               1: begin
;                  ValC=AltC
;                  ValE=AltE
;                  end
;               2: begin
;                  if (IsHybrid) then begin
;                     ValC=EtaC
;                     ValE=EtaE
;                  endif else begin
;                     ValC=SigC
;                     ValE=SitE
;                  endelse
;                  end
;               else: message, 'Not defined'
;            endcase
;
;            ; vertical centers
;            NCDF_VarPut, fId, vIDc_save[I], ValC
;            if ( Verbose ) then print, vIDc_save[I], 'vert center', Format=F2
;
;            ; Vertical edges
;            if (Zedge ne 0) then begin
;               NCDF_VarPut, fId, vIDe_save[I], ValE
;               if ( Verbose ) then print, vIDe_save[I], 'vert edge', Format=F2
;            endif
;
;         endfor
;
;      endif
;-----------------------------------------------------------------------------

      ;-----------------------------
      ; Vertical (lev and/or ilev)
      ;-----------------------------
      if ( Is_Any_3D OR Is_4D ) then begin
         
         ; Level Centers
         NCDF_VarPut, fId, vLev, EtaC
         if ( Verbose ) then print, vLev, 'lev', Format=F2

         ; Level edges
         if ( Zedge ne 0 ) then begin
            NCDF_VarPut, fId, vEdge, EtaE
            if ( Verbose ) then print, vLev, 'ilev', Format=F2
         endif

      endif

      ;-----------------------------
      ; Time (hours from 1/1/1985)
      ;-----------------------------

      ; Save to netCDF file 
      NCDF_VarPut, fId, vTim, Float( Tau0Today )

      ; /VERBOSE output
      if ( Verbose ) then Print, vTim, 'time', Format=F2

;-----------------------------------------------------------------------------
; Prior to 3/28/18:
; Remove the DATE variable, it's not COARDS-compliant
;      ; Save date 
;      NCDF_VarPut, fId, vDate, Date0Str
;      
;      ; /VERBOSE output
;      if ( Verbose ) then Print, vDate, 'date', Format=F2
;-----------------------------------------------------------------------------
      
      ;-----------------------------
      ; BPCH data blocks
      ;-----------------------------

      ; Find unique variable ID numbers
      vID_Uniq = vId_Save[ Uniq( vId_Save, Sort( vId_Save ) ) ]

      ; Loop over all unique variables in the bpch file
      for N = 0L, N_Elements( vID_Uniq )-1L do begin

         if ( Is_4D ) then begin

            ;------------------------------------------------------------
            ; 4D DATA BLOCKS (created by GC_COMBINE_ND49)
            ;------------------------------------------------------------

            ; Index array of data blocks corresponding to this
            ; unique variable ID number
            pId   = Where( vId_Uniq[N] eq vId_Save )

            ; Number of times per YYYYMMDD for this unique variable ID #
            N_pId = N_Elements( pId )

            ; Loop over number of times per day
            for D = 0L, N_pId-1L do begin

               ; Define a pointer to data block
               Pointer = ThisDataInfo[ pID[D] ].Data 

               ; Error check
               if ( not Ptr_Valid( Pointer ) ) then begin
                  S = 'Invalid pointer for ' + Name_Save[ pID[D] ]
                  Message, S
               endif

               ; Dereference the pointer to get the data
               Data = *( Pointer )

               ; Get the actual # of dimensions of the data block
               SD   = Size( Data, /Dim   )
               ND   = Size( Data, /N_Dim )

               ; Keep only troposphere layers, if requested, can be edges or centers
               if (TropOnly) then begin
                  if (ZEDGE gt 0) then begin
                     SD[2] = Min( [SD[2], ZEDGE] )
                  endif else begin
                     SD[2] = Min( [SD[2], ZDIM] )
                  endelse
               endif

               ; Create data array to save to the netCDF file
               if ( D eq 0L ) then begin
                  Field = FltArr( XDim, YDim, SD[2], SD[3] )
               endif

               for X = 0, SD[3]-1L do begin
                  Field[ *, *,  0:SD[2]-1L, X ] = Data[ *, *, 0:SD[2]-1L, X ]
               endfor

               ; Undefine stuff
               UnDefine, Data
               UnDefine, SD
               UnDefine, ND

            endfor

         endif else begin

            ;------------------------------------------------------------
            ; NORMAL DATA BLOCKS
            ;------------------------------------------------------------

            ; Index array of data blocks corresponding to this
            ; unique variable ID number
            pId   = Where( vId_Uniq[N] eq vId_Save )

            ; Number of times per YYYYMMDD for this unique variable ID #
            N_pId = N_Elements( pId )

            ; Loop over number of times per day
            for D = 0L, N_pId-1L do begin

               ; Define a pointer to data block
               Pointer = ThisDataInfo[ pID[D] ].Data 

               ; Error check
               if ( not Ptr_Valid( Pointer ) ) then begin
                  S = 'Invalid pointer for ' + Name_Save[ pID[D] ]
                  Message, S
               endif

               ; Dereference the pointer to get the data
               Data = *( Pointer )

               ; Get the actual # of dimensions of the data block
               SD   = Size( Data, /Dim   )
               ND   = Size( Data, /N_Dim )

               ; Keep only troposphere layers, if requested, can be edges or centers
               if ((ND eq 3) and TropOnly) then begin
                  if (ZEDGE gt 0) then begin
                     SD[2] = Min( [SD[2], ZEDGE] )
                  endif else begin
                     SD[2] = Min( [SD[2], ZDIM] )
                  endelse
               endif

               ; Create data array to save to the netCDF file
               if ( D eq 0L ) then begin
                  case ( ND ) of 
                     2 : Field = FltArr( XDim, YDim,        N_pId )
                     3 : Field = FltArr( XDim, YDim, SD[2], N_pId )
                  endcase
               endif

               ; Store data into array
               case ( ND ) of 
                  3 : Field[ *, *,  0:SD[2]-1L, D ] = Data[ *, *, 0:SD[2]-1L ]  
                  2 : Field[ *, *,              D ] = Data
               endcase

               ; Undefine stuff
               UnDefine, Data
               UnDefine, SD
               UnDefine, ND

            endfor

         endelse

         ; /VERBOSE output
         if ( Verbose ) then Print, vId_Uniq[N], Name_Save[pID[0]], Format=F2

         ; Write the data to the netCDF file
         NCDF_VarPut, fId, vId_Uniq[N], Field

         ; Undefine variable
         UnDefine, Field

      endfor

      ;===================================================================
      ; Close file and quit
      ;===================================================================
Quit:
      ; Close netCDF file
      NCDF_Close, fId

      ; Undefine variables
      UnDefine, fId
      UnDefine, ThisFile
      UnDefine, ThisDataInfo
      UnDefine, Is_Any_3D
      UnDefine, vId_Save
      UnDefine, Lon
      UnDefine, Lat
      UnDefine, SigC
      UnDefine, EtaC

   endfor
 
   ; Quit
   return
end
