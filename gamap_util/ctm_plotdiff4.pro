;-----------------------------------------------------------------------
;+
; NAME:
;        CTM_PLOTDIFF4
;
; PURPOSE:
;        Prints a lat-lon map of 2 CTM fields, their absolute difference, 
;        and their percent difference.  This is a quick way of ensuring 
;        that two model versions are producing identical results.
;
;        The page will contain the following plot panels:
;
;             Panel #1               Panel #2
;             Data Block D1          Data Block D2
;
;             Panel #3               Panel #4
;             Abs Diff (D2 - D1)     % Diff (D2 - D1) / D1
;
;        CTM_PLOTDIFF4 is a lighter version of CTM_PLOTDIFF, and does
;        not use CTM_DIFF as an intermediary routine. 
;
; CATEGORY:
;        GAMAP Utilities, GAMAP Plotting
;
; CALLING SEQUENCE:
;        CTM_PLOTDIFF4, DIAGN, FILE2, FILE2 [, Keywords ]
;
; INPUTS:
;        DIAGN -> A diagnostic number or category to restrict
;             the record selection (default is: "IJ-AVG-$"). 
;
;        FILE1 -> Name of the first CTM output file.  If FILE1
;             is not given, or if it contains wildcard characters
;             (e.g. "*"), then the user will be prompted to supply
;             a file name via a pickfile dialog box.
;
;        FILE2 ->  Name of the second CTM output file.  If FILE2
;             is not given, or if it contains wildcard characters
;             (e.g. "*"), then the user will be prompted to supply
;             a file name via a pickfile dialog box.
;
; KEYWORD PARAMETERS:
;
;        LON -> A 2-element vector specifying the longitude range that
;             will be used to make the plot.  Default is [-180,180].
;
;        LAT -> A 2-element vector specifying the latitude range that
;             will be used to make the plot.  Default is [-88,88].
;
;        LEV -> A scalar or 2-element vector which specifies the
;             level(s) that will be used to make the plot.  Default
;             is [ 1, 1 ].
;
;        /PS -> Set this switch to print to a PostScript file.
; 
;        OUTFILENAME -> Name of the PostScript file if the /PS option 
;             is set.  Default is "idl.ps".
;
;        TRACER -> A scalar or 2-element vector which specifies the
;             tracer number(s) for each data block.  
;
;        TAU0 -> A scalar or 2-element vector which specifies the
;             TAU value(s) (hours since 0 GMT 1 Jan 1985) by which
;             each data block is timestamped.
;
;        DIVISIONS -> Specifies the number of colorbar divisions for 
;             the quantity plot (Panels #1 and #2).  Default is 5.
;             NOTE: The optimal # of divisions is !MYCT.NCOLORS/2 +1.
;
;        DATARANGE -> Allows you to manually specify the min and max
;             values of the data that will appear on the plot (Panels
;             # 1 and #2).  The default is to automatically compute
;             the overall min and max of both data blocks.
;
;        MIN_VALID -> Specifies the minimum valid data for the plot. 
;             Data lower than MIN_VALID will be rendered with color
;             !MYCT.WHITE.  For example, MIN_VALID is useful for
;             plotting emissions which only occur on land.
;
;        DIFFDIV -> Specifies the number of colorbar divisions for 
;             the difference plots (Panels #3 and #4).  Default is 8.
;             NOTE: The optimal # of divisions is !MYCT.NCOLORS/2 +1.
;
;        DIFFNCOLORS -> Sets the # of colors used to create the
;             difference plots (Panels #3 and #4).  Default is 13.
;
;        DIFFRANGE -> Allows you to manually specify the min and max
;             values that will appear on the absolute difference
;             plot (Panel #3).  Default is to use the dynamic range of
;             the absolute difference data (symmetric around zero).
;
;        /NODEVICE -> set to not call open_device, so you can do it
;             from outside
;
;        /NOMULTIPANEL -> set to not call multipanel, so you can do it
;             from outside
;
;        PCRANGE -> Allows you to manually specify the min and max
;             values that will appear on the percentage difference
;             plot (Panel #4).  Default is to use the dynamic range of
;             the percentage difference data (symmetric around zero).
;
;        PCDIV -> number of ticks in the %-Diff colorbar
;
;        PCNCOLORS -> number of colors for the %-Diff plot
;
;        TITLE_1 -> Allows you to override the default plot title for
;             the first data block (1st plot panel).
;
;        TITLE_1 -> Allows you to override the default plot title for
;             the 2nd data block (2nd plot panel).
;
;        DIFFTITLE -> Allows you to override the default plot title for
;             the absolute difference plot (3rd plot panel).
;
;        PCTITLE -> Allows you to override the default plot title for
;             the percent difference plot (4th plot panel).
;
;        _EXTRA=e -> Picks up extra keywords for CTM_DIFF,
;             OPEN_DEVICE, and MYCT
;
;
;   Keywords for the MASK feature:
;   ------------------------------
;        /MASK -> set to mask with a green color boxes where %-diff
;             and/or diff are below some threshold. The absolute value
;             of the % is plot.
;
;	 PC_THR, DIFF_THR : the %-Diff and Diff THResholds to mask
;	      data in the 4th plot (percent difference) if /MASK is
;	      set.  Boxes with %-Diff or/and Diff below the THResholds
;	      are masked. Default values are 1% and 1e4.
;
;        /ZAP -> set to skip all plots if /MASK is set and all boxes
;             are masked.;
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        External Subroutines Required:
;        ==========================================================
;        CHKSTRU          (function)  CLOSE_DEVICE
;        COLORBAR_NDIV    (function)  CTM_GET_DATABLOCK (function)
;        EXTRACT_FILENAME (function)  MULTIPANEL                
;        MYCT                         OPEN_DEVICE               
;        TVMAP                        UNDEFINE
;
; REQUIREMENTS:
;        Requires routines from the GAMAP package.
;
; NOTES:
;        (1) When plotting a subset of the globe, CTM_PLOTDIFF4
;            properly sets the colorbar ranges for the abs diff
;            and percent diff plots.  This was an issue in the
;            older CTM_PLOTDIFF.
;
;        (2) Longitude and latitude ranges specified by LON and LAT
;            will be applied to both data blocks.  
;
; EXAMPLE:
;         CTM_PLOTDIFF4, 'IJ-AVG-$',                      $
;                        'v8-03-02.bpch', 'v9-01-01.bpch, $
;                        TRACER=6,      LEVEL=1,          $
;                        LON=[-90,-30], LAT=[30,15]
; 
;             ; Creates a 4-panel difference plot for ISOPRENE
;             ; (IJ-AVG-$, tracer #6) at the surface for the Amazon
;             ; basin region from data in 2 different model versions.
;
; MODIFICATION HISTORY:
;        bmy, 16 May 2011: VERSION 1.00
;        bmy, 13 Jan 2014: GAMAP VERSION 2.17
;                          - Bug fix: For 2-D data blocks that lack the  
;                            GRIDINFO.ZMID tag name, display the altitude
;                            of the data as 0.0 km.
;
;-
; Copyright (C) 2011-2014 Bob Yantosca, Harvard University
; This software is provided as is without any warranty whatsoever.
; It may be freely used, copied or distributed for non-commercial
; purposes.  This copyright notice must be kept with any copy of
; this software. If this software shall be used commercially or
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to yantosca@seas.harvard.edu
; with subject "IDL routine ctm_plotdiff4"
;-----------------------------------------------------------------------


pro CTM_PlotDiff4, DiagN, File_1, File_2,       $
                   Tracer       = Tracer,       $
                   Lev          = Lev,          $ 
                   Lon          = Lon,          $ 
                   Lat          = Lat,          $
                   PS           = PS,           $
                   OutFileName  = OutFileName,  $
                   Tau0         = Tau0,         $
                   Min_Valid    = Min_Valid,    $
                   DataRange    = DataRange,    $
                   Divisions    = Divisions,    $
                   DiffDiv      = DiffDiv,      $
                   DiffNColors  = DiffNColors,  $
                   DiffRange    = DiffRange,    $
                   Diff_Thr     = Diff_Thr,     $
                   DiffTitle    = DiffTitle,    $
                   PcDiv        = PcDiv,        $
                   PcNColors    = PcNColors,    $
                   PcRange      = PcRange,      $
                   Pc_Thr       = Pc_Thr,       $
                   PcTitle      = PcTitle,      $
                   Mask         = mask,         $
                   Zap          = zap,          $
                   NoDevice     = NoDevice,     $
                   NoMultiPanel = NoMulti,      $
                   NoIso        = NoIso,        $
                   Title_1      = Title_1,      $
                   Title_2      = Title_2,      $
                   Success      = Success,      $
                   _EXTRA=e
 
   ; Pass external functions
   FORWARD_FUNCTION ChkStru,            ColorBar_NDiv, $
                    CTM_Get_DataBlock,  Extract_FileName  
 
   ;====================================================================
   ; Initialization and error checking
   ;====================================================================
 
   ; Arguments
   if ( N_Elements( Diagn  ) eq 0 ) then begin
      Message, 'DIAGN must be passed to CTM_PLOTDIFF4!'
   endif
 
   if ( N_Elements( File_1 ) eq 0 ) then begin
      Message, 'FILE_1 must be passed to CTM_PLOTDIFF4!'
   endif
 
   if ( N_Elements( File_2 ) eq 0 ) then begin
      Message, 'FILE_2 must be passed to CTM_PLOTDIFF4!'
   endif
 
   ; Keywords for data block plots
   if ( N_Elements( Lon         ) eq 0 ) then Lon         = [ -180, 180 ]
   if ( N_Elements( Lat         ) eq 0 ) then Lat         = [  -86,  86 ]
   if ( N_Elements( Lev         ) eq 0 ) then Lev         = [   1,    1 ]
   if ( N_Elements( Div         ) eq 0 ) then Div         = ColorBar_NDiv(6)
 
   ; Keywords for difference plot
   if ( N_Elements( DiffDiv     ) eq 0 ) then DiffDiv     = ColorBar_NDiv(6)
   if ( N_Elements( DiffNColors ) eq 0 ) then DiffNColors = 12
   if ( N_Elements( diff_thr    ) eq 0 ) then diff_thr    = 1.e4
 
   ; Keywords for percent plot

   if ( N_Elements( PcDiv       ) eq 0 ) then PcDiv       = ColorBar_NDiv(6)
   if ( N_Elements( PcNColors   ) eq 0 ) then PcNColors   = 12
   if ( N_Elements( pc_thr      ) eq 0 ) then pc_thr      = 1.
 
   ; Keywords for device settings
   Dev      = ~Keyword_Set( NoDevice )
   Iso      = ~Keyword_Set( NoIso    )
   MultiPan = ~Keyword_set( NoMulti  ) 
 
   ; Save elements of TAU0 to individual variables
   case ( N_Elements( Tau0 ) ) of
      0: ; Null command
 
      1: begin
         Tau0_1 = Tau0[0]
         Tau0_2 = Tau0[0] 
      end
 
      2: begin
         Tau0_1 = Tau0[0]
         Tau0_2 = Tau0[1]   
      end
 
      else: begin
         Message, 'TAU0 must not have more than 2 elements!'
         return
      end 
   endcase
 
   ; Save elements of TRACER to individual variables
   case ( N_Elements( Tracer ) ) of
      0: begin  ; Default to Ox
         Tracer_1 = 2 
         Tracer_2 = 2
      end 
 
      1: begin
         Tracer_1 = Tracer[0]
         Tracer_2 = Tracer[0] 
      end
 
      2: begin
         Tracer_1 = Tracer[0]
         Tracer_2 = Tracer[1] 
      end
 
      else: begin
         Message, 'TRACER must not have more than 2 elements!'
         return
      end 
   endcase
 
  ; Save elements of LEV
   case ( N_Elements( Lev ) ) of
      0: begin  ; Default to Ox
         Lev_1 = 1
         Lev_2 = 1
      end 
 
      1: begin
         Lev_1 = Lev[0]
         Lev_2 = Lev[0] 
      end
 
      2: begin
         Lev_1 = Lev[0]
         Lev_2 = Lev[1] 
      end
 
      else: begin
         Message, 'LEV must not have more than 2 elements!'
         return
      end 
   endcase
 
 
   ; Default title for first data block
   if ( not Keyword_Set( Title_1 ) ) then begin
      Title_1 = '%MODEL%: ' + Extract_Filename( File_1 ) + $
                '!C!C%TRACERNAME% %DATE% %LEV% (%ALT%)'
   endif
 
   ; Default title for second data block
   if ( not Keyword_Set( Title_2 ) ) then begin
      Title_2 = '%MODEL%: ' + Extract_Filename( File_2 ) + $
                '!C!C%TRACERNAME% %DATE% %LEV% (%ALT%)'
   endif
 
   ; Store original color table
   TvLct, R_Orig, G_Orig, B_Orig, /Get
 
   ; Store original !MYCT structure (phs, 8/16/07)
   MyCt_Orig = !MYCT
 
   ;====================================================================
   ; Read data blocks
   ;====================================================================
 
   ; Data from first file (cut down to size)
   Success = CTM_Get_DataBlock( Data_1, DiagN,              $
                                Lon          = Lon,         $
                                Lat          = Lat,         $
                                Lev          = Lev_1,       $
                                Tracer       = Tracer_1,    $
                                Tau0         = Tau0_1,      $
                                FileName     = File_1,      $
                                ThisDataInfo = DataInfo_1,  $
                                ModelInfo    = ModelInfo_1, $
                                GridInfo     = GridInfo_1,  $
                                XMid         = XXMid,       $
                                Ymid         = YYMid,       $
                                ZMid         = ZZMid,       $
                                /Quiet,                     $
                                /NoPrint,                   $
                                _EXTRA=e )
   
   ; Exit upon error
   if ( not Success ) then Message, 'Could not read data from first file!'
 
   ; Data from second file (cut down to size)
   Success = CTM_Get_DataBlock( Data_2, DiagN,              $
                                Lon          = Lon,         $
                                Lat          = Lat,         $
                                Lev          = Lev_2,       $
                                Tracer       = Tracer_2,    $
                                Tau0         = Tau0_2,      $
                                FileName     = File_2,      $
                                ThisDataInfo = DataInfo_2,  $
                                ModelInfo    = ModelInfo_2, $
                                GridInfo     = GridInfo_2,  $
                                /Quiet,                     $
                                /NoPrint,                   $
                                _EXTRA=e )
   
   ; Exit upon error
   if ( not Success ) then Message, 'Could not read 2nd data block!'
 
   ; If DATARANGE isn't passed, then compute the data range
   ; based on the min & max of both data blocks
   if ( N_Elements( DataRange ) eq 0 ) then begin
      Min_1     = Min( Data_1, Max=Max_1 )
      Min_2     = Min( Data_2, Max=Max_2 )
      MinData   = Min( [ Min_1, Min_2 ]  ) 
      MaxData   = Max( [ Max_1, Max_2 ]  )      
      DataRange = [ MinData, MaxData ]
   endif
 
   ; Make sure data blocks have similar units
   Unit_1 = DataInfo_1.Unit
   Unit_2 = DataInfo_2.Unit
 
   if ( StrUpCase( StrTrim( Unit_1 ) )   ne $
        StrUpCase( StrTrim( Unit_2 ) ) ) then begin
      Message, 'Units of data blocks do not match!'
   endif
 
   ;====================================================================
   ; Compute absolute difference and range
   ;====================================================================
   
   ; Absolute difference
   Diff = Data_2 - Data_1

   ; If DIFFRANGE is not set, then compute default values
   if ( N_Elements( DiffRange ) eq 0 ) then begin
 
      ; Min and max from the difference array
      Min_1     = Min( Diff, Max=Max_1 )
   
      ; Make the range symmetric around zero
      if ( Abs( Min_1 ) gt Abs( Max_1 ) )                  $
         then DiffRange = [ -Abs( Min_1 ), Abs( Min_1 ) ]  $
         else DiffRange = [ -Abs( Max_1 ), Abs( Max_1 ) ]
 
   endif

   ;====================================================================
   ; Compute percent difference and range
   ;====================================================================

   ; Reset success keyword
   Success = 0

   ; Percent Difference
   Pct  = ( Diff / Data_1 ) * 100.
 
   ; Set to zero if diff is already 0
   Double_Zero = where( Diff eq 0., count)
   if ( Count ne 0 ) then Pct[Double_zero] = 0.
 
   ; MASK data below threshold
   if ( Keyword_Set( Mask ) ) then begin
         
      Pct = Abs( Pct ) ; switch to absolute %
         
      ; mask good data w/ "-99"
      Good = Where( ( Abs( Diff ) le Diff_Thr ) OR $
                    ( Pct         le Pc_Thr   ),   $
                      Count, ncomplement=nBad )
 
      ; Succes
      if ( Count ne 0 ) then Pct[good] = -99.
 
      ; Success status
      Success = ( nBad eq 0L )
 
   endif
 
   ; check for Nan and +/-Infinite
   i_nan = Where( ~Finite( Pct ),  nNan, ncomplement=nFinite )
 
   if ( nFinite eq 0 ) then begin
      S =  'WARNING: Cannot produce a % difference since ' + $
           'DATA1 is zero everywhere or one data set is NaN everywhere!'
      Message, S, /Continue
      return
   endif  
 
   if ( nNan ne 0 ) then Pct[i_nan] = 9.99e30 ; !values.f_infinity

   ; If PCRANGE is not set, then compute default min/max values
   if ( N_Elements( PCRange ) eq 0 ) then begin
 
      ; Min and max from the difference array
      Min_1     = Min( Pct, Max=Max_1 )
   
      ; Make the range symmetric around zero
      if ( Abs( Min_1 ) gt Abs( Max_1 ) )                $
         then PcRange = [ -Abs( Min_1 ), Abs( Min_1 ) ]  $
         else PcRange = [ -Abs( Max_1 ), Abs( Max_1 ) ]
 
   endif

   ;====================================================================
   ; Create plot titles
   ;====================================================================
 
   ;-------------------
   ; 1st panel
   ;-------------------
 
   ; Get altitude corresponding to this level
   if ( ChkStru( GridInfo_1,  'ZMID' ) ) then begin
      Alt_1   = GridInfo_1.Zmid[ Lev-1 ]
   endif else begin 
      Alt_1   = 0.0
   endelse

   ; Get structure of labels
   Label_1    = CTM_Label( Datainfo_1, Modelinfo_1, $
                           Lon=Lon, Lat=Lat, Lev=Lev_1, Alt=Alt_1 )
   
   ; Replace tokens in title string
   Title_1    = StrTrim( Title_1 )
   Title_1    = Replace_Token( Title_1, Label_1 )
 
   ;-------------------
   ; 2nd panel
   ;-------------------
 
   ; Get altitude corresponding to this level
   if ( ChkStru( GridInfo_2, "ZMID" ) ) then begin
      Alt_2   = GridInfo_2.Zmid[ Lev-1 ]
   endif else begin
      Alt_2   = 0.0
   endelse

   ; Get structure of labels
   Label_2    = CTM_Label( Datainfo_2, Modelinfo_2, $
                           Lon=Lon, Lat=Lat, Lev=Lev_1, Alt=Alt_2 )
   
   ; Replace tokens in title string
   Title_2    = StrTrim( Title_2 )
   Title_2    = Replace_Token( Title_2, Label_2 )
 
   ;-------------------
   ; 3rd & 4th panels
   ;-------------------
 
   ; Title for abs diff plot
   if ( N_Elements( DiffTitle ) eq 1 ) then begin
      Title_3 = DiffTitle
   endif else begin
      Title_3 = Extract_FileName( File_2 ) + ' - ' + $
                Extract_FileName( File_1 ) + '!C!CAbsolute difference'
   endelse
 
   ; Title for % diff plot
   if ( N_Elements( PcTitle ) eq 1 ) then begin
      Title_4 = PcTitle
   endif else begin
      Title_4 = Extract_FileName( File_2 ) + ' - ' + $
                Extract_FileName( File_1 ) + '!C!CPercent difference'
   endelse
 
   ;====================================================================
   ; Plot the data
   ;====================================================================
 
   ; 4 plot panels by default
   if ( MultiPan ) then MultiPanel, 4

   ; Open window or PS device
   if ( Dev ) then begin
      Open_Device, /Color, Bits=8, PS=PS, FileName=OutFileName, _EXTRA=e 
   endif

   ;---------------------------
   ; Plot data blocks
   ;---------------------------
 
   ; Plot 1st data block
   TvMap, Data_1, XXMid, YYMid,                                      $
      /Sample,               /CBar,                                  $
      Divisions=Div,         /Grid,          /Countries,             $
      /Coasts,               Title=Title_1,  Iso=Iso,                $
      /Quiet,                /NoPrint,       MinData=DataRange[0],   $
      MaxData=DataRange[1],  Unit=Unit_1,    _EXTRA=e                
                                                                   
   ; Plot 2nd data block                                           
   TvMap, Data_2, XXMid, YYMid,                                      $
      /Sample,               /CBar,                                  $
      Divisions=Div,         /Grid,          /Countries,             $
      /Coasts,               Title=Title_2,  Iso=Iso,                $
      /Quiet,                /NoPrint,       MinData=DataRange[0],   $
      MaxData=DataRange[1],  Unit=Unit_2,    _EXTRA=e
 
   ;---------------------------
   ; Plot abs difference
   ;---------------------------
 
   ; Use difference colortable w/ doubled midrange color for even DIFFNCOLORS
   MyCt, 'RdBu', NColors=DiffNColors, /MidCol, /Yello, /Reverse, _EXTRA=e
 
   ; Plot absolute diff
   TvMap, Diff, XXMid, YYMid,                                        $
      /Sample,               /CBar,                                  $
      Divisions=DiffDiv,     /Grid,          /Countries,             $
      /Coasts,               Title=Title_3,  Iso=Iso,                $
      /Quiet,                /NoPrint,       MinData=DiffRange[0],   $
      MaxData=DiffRange[1],  Unit=Unit_1,    _EXTRA=e
 
   ;---------------------------
   ; Plot percent difference
   ;---------------------------
 
   ; Use difference colortable w/ doubled midrange color for even DIFFNCOLORS
   MyCt, 'RdBy', NColors=PcNcolors, /MidCol, /Yello, /Reverse, _EXTRA=e
 
   ; Plot percent diff
   TvMap, Pct, XXMid, YYMid,                                         $
      /Sample,               /CBar,                                  $
      Divisions=PCDiv,       /Grid,          /Countries,             $
      /Coasts,               Title=Title_4,  Iso=Iso,                $
      /Quiet,                /NoPrint,       MinData=PCRange[0],     $
      MaxData=PCRange[1],    Unit='[%]',     _EXTRA=e
 
   ;====================================================================
   ; Cleanup and quit
   ;====================================================================
Quit:
 
   ; Clean up arrays
   UnDefine, Data1
   UnDefine, Data2
   UnDefine, Diff
   UnDefine, Pct
 
   ; Close the device
   if ( N_Elements( Dev ) ne 0 ) then begin
      if ( Dev ) then Close_Device
   endif

   ; Restore original color table
   if ( N_Elements( R_Orig ) ne 0 ) then TvLct, R_Orig, G_Orig, B_Orig
 
   ; Restore original !MYCT system variables
   if ( ChkStru( MyCT_Orig ) ) then !MYCT = MYCT_Orig 
 
   ; Turn off multipanel
   if ( MultiPan ) then Multipanel, /Off

end
