;-----------------------------------------------------------------------
;+
; NAME:
;        FIND_TRACER_INDEX
;
; PURPOSE:
;        Given the diagnostic category and tracer name, returns the
;        tracer number as specified in the "tracerinfo.dat" file.
;
; CATEGORY:
;        GAMAP Utilities, GAMAP Data Manipulation
;
; CALLING SEQUENCE:
;        RESULT = FIND_TRACER_INDEX( DIAGN, NAME )
;
; INPUTS:
;        DIAGN -> Diagnostic category name as specified in the
;             "diaginfo.dat" file (e.g. "IJ-AVG-$", "GMAO-2D", etc.).
;
;        NAME -> Tracer name as specified in the "tracerinfo.dat"
;              file (e.g. NOx, Ox, CO, PS, TAUCLI, etc.)
;
; KEYWORD PARAMETERS:
;        None
;
; OUTPUTS:
;        RESULT -> Tracer number corresponding to NAME and DIAGN.
;
; SUBROUTINES:
;        External subroutines required:
;        ------------------------------
;        CTM_DIAGINFO   CTM_TRACERINFO
;
; REQUIREMENTS:
;        Requires routines from the GAMAP package.
;
; NOTES:
;        Here, "tracer" is used in the wider sense.  It can be can be 
;        any quantity saved out from a CTM diagnostic output, as long
;        as it is specified by "diaginfo.dat" and "tracerinfo.dat".
;
; EXAMPLE:
;        PRINT, FIND_TRACER_INDEX( 'GMAO-2D', 'PS' )
;        IDL prints   5931
;
;             ; Returns the tracer index number for the "PS"
;             ; quantity in the "GMAO-2D" diagnostic category.
;             
; MODIFICATION HISTORY:
;        bmy, 06 Aug 2010: VERSION 1.00
;
;-
; Copyright (C) 2010, Bob Yantosca, Harvard University
; This software is provided as is without any warranty whatsoever.
; It may be freely used, copied or distributed for non-commercial
; purposes.  This copyright notice must be kept with any copy of
; this software. If this software shall be used commercially or
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to yantosca@seas.harvard.edu
; with subject "IDL routine find_tracer_index"
;-----------------------------------------------------------------------


function Find_Tracer_Index, DiagN, Name
 
   ; Keywords
   if ( N_Elements( DiagN ) ne 1 ) then Message, 'Must specify DiagN!'
   If ( N_Elements( Name  ) ne 1 ) then Message, 'Must specify tracer name!'
 
   ; Get the category offset and spacing from "diaginfo.dat"
   CTM_Diaginfo, DiagN, Offset=Offset, Spacing=Spacing
 
   ; Get the list of all tracer names and tracer numbers
   CTM_TracerInfo, Name=AllNames, Index=AllTracers,  /All_Tracers
 
   ; Only consider tracers for the given category
   Ind = Where( AllTracers ge Offset AND AllTracers lt Offset+Spacing )
 
   ; Exit if no matches found
   if ( Ind[0] lt 0 ) then return, -1L
 
   ; List of names & tracers for the given category 
   CatNames   = StrUpCase( AllNames[Ind] )
   CatTracers = AllTracers[Ind] 
 
   ; Look for the tracer name in the list of names for this category
   Ind = Where( CatNames eq StrUpCase( Name ) )
 
   ; Return
   if ( Ind[0] ge 0 )             $
     then return, CatTracers[Ind] $
     else return,  -1L
end
