; $Id: percentiles.pro,v 1.2 2009/10/22 15:40:54 phs Exp $
;-----------------------------------------------------------------------
;+
; NAME:
;        PERCENTILES
;
; PURPOSE:
;        Compute percentiles of a data array (both ways: data that
;        correspond to %, and % that correspond to data)
;
; CATEGORY:
;        Math & Units
;
; CALLING SEQUENCE:
;        Y = PERCENTILES( DATA [,VALUE=value-array] )
;
; INPUTS:
;        DATA -> the vector containing the data
;
; KEYWORD PARAMETERS:
; 
;        VALUE --> array or scalar that specify percentile(s) to
;             compute.  If /REVERSE is set, a percentage that
;             correspond to Value is return.
;             
;             Default percentile to compute is standard set of min
;             (0%), 25%, median (=50%), 75%, and max(100%) which can
;             be used for box- and whisker plots.  The values in the
;             VALUE array must lie between 0. and 1.
;
;             If /REVERSE, default value is mean(data) 
;
;        INTERPOLATE --> Behaves like EVEN keyword for MEDIAN. 
;             If no element of the data falls exactly on the requested
;             percentile value, then the 2 adjacent data elements are 
;             linearly interpolated to the requested percentile value.
;             When using the INTERPOLATE keyword, returned values may
;             not be elements of the input array. 
;             
;        /NAN  --> if set, ignores NaN values. You must use that
;                  keyword if your dataset may have NaN.
;        
;        /REVERSE --> to get % corresponding to data value, instead of
;                    data corresponding to %
;
; OUTPUTS:
;        Y -> The function returns an array with the percentile 
;             values or -1 if no data was passed or value contains 
;             invalid numbers.
;
; SUBROUTINES:
;        None
;
; REQUIREMENTS:
;        None
;
; NOTES:
;        None
;
; EXAMPLE:
;      x = (findgen(31)-15.)*0.2     ; create sample data
;      y = exp(-x^2)/3.14159         ; compute some Gauss distribution
;      p = percentiles(y,value=[0.05,0.1,0.9,0.95])
;      print,p
;
;      IDL prints :  3.92826e-05  0.000125309     0.305829     0.318310
;
; MODIFICATION HISTORY:
;        mgs, 03 Aug 1997: VERSION 1.00
;        mgs, 20 Feb 1998: - improved speed and memory usage
;                (after tip from Stein Vidar on newsgroup)
;  bmy & phs, 13 Jul 2007: GAMAP VERSION 2.10
;  cdh & jaf, 21 Oct 2009: GAMAP VERSION 2.13
;                          - fixed incorrect values for small sample sizes
;                          - removed unnecessary loop
;			   - added NaN keyword
;        phs, 22 Oct 2009: - added REVERSE keyword
;			   - updated handling of NaN
;        cdh, 19 Jun 2012: - added INTERPOLATE keyword
;       
;-
; Copyright (C) 1997-2007, Martin Schultz,
; Bob Yantosca and Philippe Le Sager, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.as.harvard.edu with subject "IDL routine percentiles"
;-----------------------------------------------------------------------


function percentiles, data, value=value, nan=nan, reverse=reverse,$
                      interpolate=interpolate

   result = -1
   n = n_elements(data)
   if (n le 0) then return,result ; error : data not defined

   ; check if speficic percentiles requested - if not: set standard
   if(not keyword_set(value)) then $
      value = keyword_set(reverse) ? mean(data, nan=keyword_set(nan) ) $
                                   : [ 0., 0.25, 0.5, 0.75, 1.0 ]

   ; Add functionality to ignore NaN entries (jaf,10/21/09)
   ; create a temporary copy of the data and sort
;--prior to 22/10/09
;   if keyword_set(nan) then tmp = data[where(finite(data))] else tmp = data

   ; Check for NaN or Infinite data, and update number of finite elements
   IF keyword_set(NaN) then begin 
      good = where( finite(data), ncomplement=nbad)
      n = n - nbad  
      if n eq 0 then return, result
   endif
   
   ; sort data
   ix  = sort(data)

   if keyword_set(reverse) then begin

      iv = value_locate(data[ix[0:n-1]], value)
      return, float(iv+1) / float(n)
      
   endif else begin

      ; OLD VERSION
      ;
      ;; loop through percentile values, get indices and add to result
      ;; This is all we need since computing percentiles is nothing more
      ;; than counting in a sorted array.
      ;for i=0L,n_elements(value)-1 do begin
      ;
      ;   if(value(i) lt 0. OR value(i) gt 1.) then return,-1
      ;   
      ;   ;if(value(i) le 0.5) then ind = long(value(i)*n)    $
      ;   ;else ind = long(value(i)*(n+1))
      ;   ;if (ind ge n) then ind = n-1L ; small fix for small n
      ;   ;                              ; (or value eq 1.)
      ;  
      ;   ;  if(i eq 0) then result = tmp(ind)  $
      ;   ;  else result = [result, tmp(ind) ]
      ;   ; ## change number 2
      ;   if(i eq 0) then result = data(ix(ind))  $
      ;              else result = [result, data(ix(ind)) ]
      ;endfor


      if keyword_set( interpolate ) then begin

         ; Interpolate data to requested percentiles 
         ; (cdh, 6/19/2012)
         ind = value * (n-1)
         return, interpol( data[ix[0:n-1]], indgen(n), ind )

      endif else begin
   
         ; NEW VERSION 
         ;  Updated to give accurate results for small N.
         ;  The ith percentile is located at index position value*(N-1). 
         ;  We round to get the nearest integer, then convert to long.
         ;  Loop was unneccessary. (cdh, jaf 10/21/2009)
         ind = long( round( value * (n-1) ) )
         return, data[ ix[ ind ] ]

      endelse

   endelse


end

