;-----------------------------------------------------------------------
;+
; NAME:
;        FREQ_DIST   
;
; PURPOSE:
;        Creates frequency distribution and percentile plots
;        for GEOS-CHEM tracers from benchmark simulations.
;
; CATEGORY:
;        Benchmarking
;
; CALLING SEQUENCE:
;        FREQ_DIST, FILES, TAUS, TRACERS, VERSIONS [, Keywords ]
;
; INPUTS:
;        FILES -> A 2-element vector containing the names of files
;             from the "old" and "new" GEOS-Chem model versions
;             that are to be compared. 
;
;        TAUS -> A 2-element vector contaning TAU values (hours GMT
;             from /1/1985) corresponding to the "old" and "new"
;             GEOS-Chem model versions.
;
;        TRACERS -> The list of transported tracers (i.e. diagnostic
;             category "IJ-AVG-$").
;
;        VERSIONS -> A 2-element vector containing the version
;             numbers for the "old" and "new" GEOS-Chem model
;             versions.
;
; KEYWORD PARAMETERS:
;        /DO_FULLCHEM -> Set this switch to plot the chemically
;             produced OH and aerosol optical depths in addition 
;             to the advected tracers.
;
;        /PS -> Set this switch to generate PostScript output.
;
;        OUTFILENAME -> If /PS is set, will write PostScript output 
;             to a file whose name is specified by this keyword.
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        External Subroutines Required:
;        ==========================================
;        OPEN_DEVICE    CLOSE_DEVICE
;        MULTIPANEL     EXTRACT_FILENAME (function)        
;        CTM_GET_DATA   
;
; REQUIREMENTS:
;        References routines from both GAMAP and TOOLS packages.
;
; NOTES:
; 
;        (2) Assumes the version # is located at the end of FILENEW.
;
;        (3) Assumes the following diagnostics are archived in FILENEW:
;            (a) ND43 ("CHEM-L=$"): Fullchem benchmark
;            (b) ND45 ("IJ-AVG-$"): Radon and Fullchem benchmarks
;        
; EXAMPLE:
;        FILES    = [ 'ctm.bpch.v7-04-10', 'ctm.bpch.v7-04-11' ]
;        TAUS     = [ NYMD2TAU( 20010701 ), NYMD2TAU( 20010701 ) ]
;        VERSIONS = [ 'v7-04-10', 'v7-04-11' ]
; 
;        FREQ_DIST, FILES, LEVELS, TAUS, VERSIONS, $
;             /DO_FULLCHEM, /PS, OUTFILENAME='myplot.ps'
;
;             ; Creates a frequency-ratio plot from the data
;             ; files for GEOS-Chem versions v7-04-10 and v7-04-11
;
; MODIFICATION HISTORY:
;        bmy, 12 Aug 2002: VERSION 1.01
;                          - adapted from Isabelle Bey's "comparison.pro"
;        bmy, 21 Jan 2003: VERSION 1.02
;                          - increased from 24 to 31 tracers  
;        bmy, 28 Apr 2004: VERSION 1.03
;                          - increased from 31 to 35 tracers
;        bmy, 03 May 2004: VERSION 1.04
;                          - increased from 35 to 39 tracers
;        bmy, 21 May 2004: VERSION 1.05
;                          - increased from 39 to 41 tracers
;        bmy, 02 Nov 2004: VERISION 1.06
;                          - bug fix: now print out top title on each
;                            page (when it is the first panel)
;        bmy, 06 May 2005: VERSION 1.07
;                          - Now use -9.99e30 to flag strat boxes
;        bmy, 08 Jul 2005: VERSION 1.08
;                          - Increased from 41 to 43 tracers
;        bmy, 09 Nov 2007: VERSION 1.09
;                          - Modified argument list and some 
;                            internal variable names.
;                          - Removed Radon keyword
;        bmy, 25 May 2011: VERSION 1.10
;                          - Added /DO_FULLCHEM keyword
;        bmy, 17 Apr 2012: GAMAP VERSION 2.16
;                          - Omit modelname error check
;        
;-
; Copyright (C) 2007,
; Bob Yantosca and Philippe Le Sager, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.as.harvard.edu with subject "IDL routine freq_dist"
;-----------------------------------------------------------------------


pro Freq_Dist, Files, Taus, Tracers, Versions,             $
               LVarTrop=LVarTrop, Do_FullChem=Do_FullChem, $
               PS=PS,             OutFileName=OutFileName, $
               _EXTRA=e
 
   ;====================================================================
   ; Initialization
   ;====================================================================
   
   ; External functions
   ;FORWARD_FUNCTION 

   ; Keyword Settings
   LVarTrop = Keyword_Set( LVarTrop )
   if ( N_Elements( Files       ) ne 2 ) then Message, 'Invalid FILES!'
   if ( N_Elements( Taus        ) ne 2 ) then Message, 'Invalid TAUS!'
   if ( N_Elements( Versions    ) ne 2 ) then Message, 'Invalid VERSIONS!'
   if ( N_Elements( OutFileName ) ne 1 ) then OutFileName = 'freq_dist.ps'
   
   ; Only do certain plots for full-chemistry benchmarks
   Do_FullChem = Keyword_Set( Do_FullChem )

   ; Define title for top of page
   TopTitle = 'GEOS-Chem ' + Versions[1]   + ' Frequency Distribution'
   TopTitle = TopTitle                     + '!C!C'  + $
              Extract_FileName( Files[1] ) +  ' / '  + $
              Extract_FileName( Files[0] )

   ; First-time flag
   First = 1L

   ;====================================================================
   ; Read data from the files
   ;====================================================================

   ;--------------------------------
   ; From FILE_1: the "old" file
   ;--------------------------------

   ; Read transported tracers from "old" file
   CTM_Get_Data, DataInfo_1, 'IJ-AVG-$', $
      File=Files[0], Tau0=Taus[0], Tracer=Tracers, /Quiet

   if ( Do_FullChem ) then begin

      ; Append to OH to "old" file
      CTM_Get_Data, DataInfo, 'CHEM-L=$', $
                    File=Files[0], Tau0=Taus[0], Tracer=1, /Quiet
      DataInfo_1 = [ DataInfo_1, DataInfo ]
      UnDefine, DataInfo

      ; Append aerosol OD's (Dust SO4 BC OC SALA SALC) to "old"
      CTM_Get_Data, DataInfo, 'OD-MAP-$', $
                    File=Files[0], Tau0=Taus[0], $
                    Tracer=[ 4, 6, 9, 12, 15, 18 ], /Quiet
      DataInfo_1 = [ DataInfo_1, DataInfo ]
      UnDefine, DataInfo

   endif

   ; Check if the "old" file has the variable tropopause turned on
   CTM_Get_Data, DataInfo, 'TR-PAUSE', $
      File=Files[0], Tau0=Taus[0], Tracer=1, /Quiet
   if ( N_Elements( DataInfo ) gt 0 ) then LPause_1 = *( DataInfo[0].Data )
   UnDefine, DataInfo

   ;--------------------------------
   ; From FILE_2: the "new" file
   ;--------------------------------

   ; Read transported tracers from "new" file
   CTM_Get_Data, DataInfo_2, 'IJ-AVG-$', $
      File=Files[1], Tau0=Taus[1], Tracer=Tracers, /Quiet

   if ( Do_FullChem ) then begin

      ; Append to OH "new" file
      CTM_Get_Data, DataInfo, 'CHEM-L=$', $
                    File=Files[1], Tau0=Taus[1], Tracer=1, /Quiet
      DataInfo_2 = [ DataInfo_2, DataInfo ]
      UnDefine, DataInfo
      
      ; Append aerosol OD's (Dust SO4 BC OC SALA SALC) to "old"
      CTM_Get_Data, DataInfo, 'OD-MAP-$', $
                    File=Files[1], Tau0=Taus[1], $
                    Tracer=[ 4, 6, 9, 12, 15, 18 ], /Quiet
      DataInfo_2 = [ DataInfo_2, DataInfo ]
      UnDefine, DataInfo
      
   endif

   ; Check if the "new" file has the variable tropopause turned on
   CTM_Get_Data, DataInfo, 'TR-PAUSE', $
      File=Files[1], Tau0=Taus[1], Tracer=1, /Quiet
   if ( N_Elements( DataInfo ) gt 0 ) then LPause_2 = *( DataInfo[0].Data )
   UnDefine, DataInfo

   ;------------------------------
   ; Error checks!
   ;------------------------------

   ; Stop if both DATAINFOs are incompatible
   if ( N_Elements( DataInfo_1 ) ne N_Elements( DataInfo_2 ) ) $
      then Message, 'Files have different numbers of tracers in them!'

; Temporarily comment out (bmy, 1/19/10)
;   ; Check if the variable tropopause data arrays are the same
;   if ( N_Elements( LPause_1 ) gt 0   AND $
;        N_Elements( LPause_2 ) gt 0 ) then begin
;
;      ; If both var trop level arrays are identical
;      if ( Long( Total( LPause_2 - LPause_1 ) ) eq 0 ) then begin
;         LVarTrop = 1
;         Message, 'Var trop is used in both new & old versions!', /Info
;      endif else begin
;         LVarTrop = 0
;         Message, 'Var trop is not used; Using ann mean tropopause!', /Info
;      endelse
;
;   endif

   ;====================================================================
   ; Process data and create frequency distribution!
   ;====================================================================
   
   ; Number of rows & cols per page
   Rows = 5
   Cols = 1

   ; Use Postscript font
   !p.font = 0

   ; Open the plot device and initialize the page
   Open_Device, /Color, Bits=8, /LandScape, PS=PS, File=OutFileName

   ; Multiple panels per page
   MultiPanel, Rows=Rows, Cols=Cols, Margin=[ 0.03, 0.03, 0.03, 0.03 ]

   ; Loop over all data blocks
   for D = 0L, N_Elements( DataInfo_1 ) - 1L do begin
   
      ;-----------------------------------------------------------------
      ; Error check grid, tracer name, and data block sizes 
      ;-----------------------------------------------------------------

      ; Get MODELINFO and GRIDINFO structures on both grids
      GetModelAndGridInfo, DataInfo_1[D], ModelInfo_1, GridInfo_1
      GetModelAndGridInfo, DataInfo_2[D], ModelInfo_2, GridInfo_2

;-----------------------------------------------------------------------------
; Prior to 4/17/12:
; Now only stop the run if the data arrays are differently sized.
; This will allow comparison between GEOS-5.2.0/MERRA/GEOS-5.7.2 data,
; all of which have the same vertical grid structure. (bmy, 4/17/12)
;      ; Make sure grids are compatible
;      if ( ( ModelInfo_1.Name ne ModelInfo_2.Name ) OR $
;           ( GridInfo_1.IMX   ne GridInfo_2.IMX   ) OR $
;           ( GridInfo_1.JMX   ne GridInfo_2.JMX   ) OR $
;           ( GridInfo_1.LMX   ne GridInfo_2.LMX   ) )  $
;         then Message, 'Model and resolution mismatch!'
;-----------------------------------------------------------------------------

      ; Stop the run if the data arrays have different dimensions
      if ( ( GridInfo_1.IMX ne GridInfo_2.IMX ) OR $
           ( GridInfo_1.JMX ne GridInfo_2.JMX ) OR $
           ( GridInfo_1.LMX ne GridInfo_2.LMX ) )  $
         then Message, 'Resolution mismatch!'

      ; Make sure the tracers correspond to each other
      TracerName_1 = DataInfo_1[D].TracerName
      TracerName_2 = DataInfo_2[D].TracerName
      if ( TracerName_1 ne TracerName_2 ) then Message, 'Tracer mismatch!'

      ; Define tracer name for plots below
      ; (rename aerosol optical depth tracer names)
      case ( StrUpCase( StrTrim( TracerName_1,  2 ) ) ) of
         'MOPD'  : TracerName = 'DUST Optical Depth'
         'OPSO4' : TracerName = 'SO4 Optical Depth'
         'OPBC'  : TracerName = 'BLACK CARBON Optical Depth'
         'OPOC'  : TracerName = 'ORGANIC CARBON Optical Depth'
         'OPSSA' : TracerName = 'ACCUM SEA SALT Optical Depth'
         'OPSSC' : TracerName = 'COARSE SEA SALT Optical Depth'
         else    : TracerName = StrTrim( TracerName_1, 2 )
      endcase

      ;-----------------------------------------------------------------
      ; Extract data arrays for surface and 500 hPa
      ;-----------------------------------------------------------------

      ; Data blocks from "old" and "new" models
      Data_1 = *( DataInfo_1[D].Data )
      Data_2 = *( DataInfo_2[D].Data )

      ; First-time initialization
      if ( First ) then begin
         
         ; Test if we are using the variable tropopause
         if ( LVarTrop ) then begin

            ; Copy tropopause level array
            LPause = LPause_2

            ; Undefine prior arrays
            UnDefine, LPause_1
            UnDefine, LPause_2

         endif else begin

            ; Define filename based on the grid 
            File = '/data/ctm/GEOS_4x5/ann_mean_trop_200202/ann_mean_trop.'
            File = File + CTM_NamExt( ModelInfo_2 ) + '.' 
            File = File + CTM_ResExt( ModelInfo_2 ) 

            ; Read annual mean tropopause
            CTM_Get_Data, DataInfo, 'TR-PAUSE', $
               File=File, Tau0=0D, Tracer=1, /Quiet
            LPause = *( DataInfo.Data )
            UnDefine, DataInfo
 
         endelse

         ; Reset first-time flag
         First = 0L
      endif

      ; Get the vertical dimensions of the data arrays
      Size_1 = Size( Data_1, /Dim )
      Size_2 = Size( Data_2, /Dim )

      ; Stop if the data block dimensions do not match
      if ( Size_1[0] ne Size_2[0] ) then Message, 'Longitude mismatch!'
      if ( Size_1[1] ne Size_2[1] ) then Message, 'Latitude mismatch!'
      if ( Size_1[2] ne Size_2[2] ) then Message, 'Level mismatch!'

      ;-----------------------------------------------------------------
      ; Set data to zero in the stratosphere
      ;-----------------------------------------------------------------
      for J = 0L, Size_1[1]-1L do begin
      for I = 0L, Size_1[0]-1L do begin

         ; LTROP is the first stratospheric level
         ; Convert from FORTRAN to IDL notation
         Ltrop = Fix( Lpause[I,J] ) - 1L

         ; Zero array elements higher than LTROP
         if ( Size_1[2]-1L gt Ltrop ) then Data_1[ I, J, LTrop:* ] = -9.99e30
         if ( Size_2[2]-1L gt Ltrop ) then Data_2[ I, J, LTrop:* ] = -9.99e30
      endfor
      endfor

      ;-----------------------------------------------------------------
      ; Compute the ratio of "New" / "Old" for CTM tracers or OH
      ; Prevent "divide by zero" errors
      ;-----------------------------------------------------------------

      ; Define ratio array
      Ratio = FltArr( Size_1[0], Size_1[1], Size_1[2] )
 
      ; IND, IND1 denote where DATA_1 and DATA_2 are zero
      Ind   = Where( Data_1 eq -9.99e30 )
      Ind1  = Where( Data_2 eq -9.99e30 )

      ; IND2 denotes places where DATA_1 is both non-zero and tropospheric
      Ind2  = Where( Data_1 ne 0. AND Data_1 ne -9.99e30 )

      ; Stop if DATA_1 and DATA_2 have a different # of "good" data pts
      if ( n_elements( Ind ) ne n_elements( Ind1 ) ) then begin
         S = 'IND is different than IND1! for ' + $
             StrTrim( DataInfo_2[D].TracerName, 2 )
         Message, S, /Info, /Continue
      endif

      ; If entire array is DATA_1 is zero then set DATA_2 to zero
      if ( Ind[0] ne -1 ) then Data_2[Ind] = 0.
     
      ; Compute ratio( new/old ) -- avoid divide-by-zero errors
      Ratio[Ind2] = Data_2[Ind2] / Data_1[Ind2]

      ; Compute number of elements of RATIO that are nonzero
      Ind = Where( Ratio gt 0, NTotal )
 
      ;-----------------------------------------------------------------
      ; Sort the RATIO array into bins for HISTOGRAM plotting
      ; Compute frequency distribution
      ;-----------------------------------------------------------------
      DeltaR    = 5e-3
      NB        = 40 
      S_Below   = 1.0 - ( DeltaR / 2.0 )
      S_Above   = 1.0 + ( DeltaR / 2.0 )
      Bin_Below = S_Below - ( FIndGen( NB ) * DeltaR ) 
      Bin_Above = S_Above + ( FIndGen( NB ) * DeltaR )
      NBins     = N_Elements( Bin_Below ) + N_Elements( Bin_Above ) + 1

      ; Define binning arrays
      Bins      = FltArr( NBins + 1 )
      Freq      = FltArr( NBins     )
      Bin_Mid   = FltArr( NBins     )
      Perc      = FltArr( NBins     )

      ; Initialize bin edges < 1
      for K = 0L, N_Elements( Bin_Below ) - 1L do begin
         Bins( N_Elements( Bin_Below ) - K ) = Bin_Below[K]
      endfor

      ; Initialize bin edges > 1
      for K = 0L, N_Elements( Bin_Above ) - 1L do begin
         Bins( N_Elements( Bin_Below ) + K + 1 ) = Bin_Above[K]
      endfor

      ; Capture points falling outside of the bin range in 
      ; the first and last bins -- so that we add up to 100%!
      Bins[0]     = 1e-5
      Bins[Nbins] = 1e5

      ;-----------------------------------------------------------------
      ; Compute frequency distribution and percentiles
      ;-----------------------------------------------------------------
      for K = 0L, NBins - 1L do begin

         ; Compute the center of the Kth bin
         Bin_Mid[K] = ( Bins[K] + Bins[K+1] ) / 2

         ; Find out which elements of RATIO fall into the Kth bin
         Ind = Where( Ratio gt Bins[K] and Ratio le Bins[K+1] )

         ; Compute frequency, percentile for Kth bin 
         if ( Ind[0] ge 0 ) then begin
            Freq[K] = N_Elements( Ind )
            Perc[K] = ( Freq[K] / NTotal ) * 100.0
         endif else begin
            Freq[K] = 0
         endelse

      endfor

      ; Handle endpoints
      Bin_Mid[0]       = Bin_Mid[1]       - DeltaR
      Bin_Mid[nbins-1] = Bin_Mid[nbins-2] + DeltaR

      ;-----------------------------------------------------------------
      ; Create HISTOGRAM plot
      ;-----------------------------------------------------------------
      Multipanel, Position=PPosition, FirstPanel=FirstPanel

      ; Plot Title
      Title = StrTrim( TracerName, 2 )

      ; X-axis settings
      XTickV = [ 0.8, 0.85, 0.9, 0.95, 1.0, 1.05, 1.1, 1.15, 1.2 ]
      XTicks = N_Elements( XTickV ) - 1L
      XRange = [ Bin_Mid[0], Bin_Mid[NBins-1] ]

      ; Y-axis settings
      YTitle = 'percent (%)'
      YRange = [ 0, 100 ]

      ; Plot histogram!
      Plot, Bin_Mid, Perc, Psym=10,                            $
         Position=PPosition, Color=!MYCT.BLACK, Title=Title,   $ 
         XRange=XRange,      XTickV=XTickV,     XTicks=XTicks, $
         XMinor=5,           /XStyle,           Thick=1,       $
         YRange=YRange,      /YStyle,           YTitle=YTitle
          
      ; Add in the "slashes" to make it look more like a bar graph
      for K = 0L, NBins - 1L do begin
         OPlot, [ Bins[K+1], Bins[K+1] ], [ 0, Perc(K) ], $
            Color=!MYCT.BLACK, Thick=1
      endfor
 
      ; Overplot percentage strings
      for K = 0L, NBins - 1L do begin

         ; If the percentage is less than 0.01%, then print
         ; the actual frequency in Times-Roman font (djj request)
         if ( Perc[K] lt 0.01 ) then begin
            Hx = '!6' + StrTrim( String((Freq[K]), Format='(i14)'), 2 ) + '!3'
         endif else begin
            Hx = StrTrim( String( ( Perc[K] ), Format='(f6.2)' ),  2 )        
         endelse
 
         ; Pick the height (in data coordinates)
         ; at which the string will be displayed
         case ( K mod 4 ) of
            0: Yx = 60
            1: Yx = 50
            2: Yx = 40
            3: Yx = 30
         endcase

         ; Print percentages!
         if ( K eq 0 ) then begin 

            ; KLUDGE: make sure the percentage string for
            ; the first bin is printed properly
            XYOutS, XTickV[0], Yx, Hx, $
               /Data, Color=!MYCT.BLACK, CharSize=0.6, Align=0.0

         endif else begin

            ; Plot the percentage string for each of
            ; the remaining bins
            XYOutS, Bins[K], Yx, Hx, $
               /Data, Color=1, CharSize=0.6, Align=0.0

         endelse
      endfor
 
      ;-----------------------------------------------------------------
      ; Create top title -- only plot when there is a new page 
      ;-----------------------------------------------------------------
      if ( FirstPanel ) then begin      
         XYOutS, 0.5, 1.05 , TopTitle, $
            /Normal, Color=!MYCT.BLACK, CharSize=1.0, Align=0.5
      endif

      ; Advance to next frame
      MultiPanel, /Advance, /NoErase
   endfor

   ;====================================================================
   ; Cleanup & Quit
   ;====================================================================

   ; Cancel previous MULTIPANEL settings
   MultiPanel, /Off

   ; Close device & quit
   Close_Device

end
 
