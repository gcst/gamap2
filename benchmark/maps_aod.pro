;-----------------------------------------------------------------------
;+
; NAME:
;        MAPS_AOD
;
; PURPOSE:
;        Creates column maps of aerosol optical depths from 1-month
;        GEOS-Chem benchmark simulation output.
;
; CATEGORY:
;        Benchmarking
;
; CALLING SEQUENCE:
;        MAPS_AOD_1YR, FILES, TRACERS, VERSIONS, [, Keywords ]
;
; INPUTS:
;        FILES -> A 2-element vector containing the names of files
;             from the "red", 'green", and "blue" GEOS-Chem model 
;             versions that are to be compared. 
;
;        TAUS -> A 2-element vector contaning TAU values (hours GMT
;             from /1/1985) corresponding to the "old" and "new"
;             GEOS-Chem model versions.
;
;        TRACERS -> The list of transported tracers (i.e. diagnostic
;             category "IJ-AVG-$") to be plotted.
;
;        VERSIONS ->  A 2-element vector containing the model version
;             names from the "red", 'green", and "blue" simulations. 
;
; KEYWORD PARAMETERS:
;        /DYNRANGE -> Set this switch to create plots using the entire
;             dynamic range of the data (centered around zero).  The
;             default is to use pre-defined data ranges.
;
;        /PS -> Set this switch to generate PostScript output.
;
;        OUTFILENAME -> If /PS is set, will write PostScript output 
;             to a file whose name is specified by this keyword.
;             Default is "tracer_ratio.pro".
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        Internal Subroutines Provided:
;        =========================================
;        PLOTDIFF
;
;        External Subroutines Required:
;        =========================================
;        OPEN_DEVICE     CLOSE_DEVICE
;        MULTIPANEL      COLORBAR_NDIV (function)
;        CTM_PLOT        CHKSTRU       (function)
;     
; REQUIREMENTS:
;        References routines from both GAMAP and TOOLS packages.
;        
; NOTES:
;        (1) Meant to be called from BENCHMARK_1MON.
;
; EXAMPLE:
;        FILES    = [ 'ctm.bpch.v9-02p', 'ctm.bpch.v9-02q' ]
;        TAUS     = [ NYMD2TAU( 20050701 ), NYMD2TAU( 20050701 ) ]
;        TRACERS  = [ 1, 2, 4 ]
;        VERSIONS = [ 'v9-02p', 'v9-02q' ]
;
;        DIFFERENCES_1YR, FILES, TAUS, TRACERS, VERSIONS, $
;             /PS, OUTFILENAME=myplot.ps'
;
;             ; Creates maps from 2 different model versions
;             ; using output from GEOS-Chem 1-month benchmark simulations.
;
;        DIFFERENCES_1YR, FILES, TAUS, TRACERS, VERSIONS, /DYNRANGE, $
;             /PS, OUTFILENAME=PSNAME
;
;             ; Same as above, but will create difference maps  using
;             ; the full dynamic range of the data (centered around zero)
;             ; instead of using pre-defined min & max values.

;
; MODIFICATION HISTORY:
;        bmy, 05 Sep 2012: VERSION 1.01
;                          - Initial version
;        mps, 16 Sep 2013: - Modified for 1-month benchmark output
;        bmy, 24 Jan 2014: GAMAP VERSION 2.17
;                          - Updated comments
;-
; Copyright (C) 2012-2014, Bob Yantosca, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.as.harvard.edu with subject "IDL routine differences"
;-----------------------------------------------------------------------


pro PlotAODMap, Data,     Version,  TracerName, $
                GridInfo, DynRange, Month,      $
                _EXTRA=e
   
   ;====================================================================
   ; Internal routine PLOTAODMAP plots AOD concentrations
   ;
   ; Initialize color table
   ;====================================================================

   ; Save original color table and !MYCT sysvar
   TvLct, R, G, B, /Get
   Myct_Orig = !MYCT

   ; Load White-Red colortable
   MyCt, /WhGrYlRd, _EXTRA=e

   ;====================================================================
   ; Plot settings
   ;====================================================================

   ; Version string
   VerStr = Version

   ; Pick new tracer name
   case ( StrUpCase( StrTrim( TracerName, 2 ) ) ) of 
      'OPD'   : NewTracerName = 'Dust: Column AOD map'
      'OPSO4' : NewTracerName = 'SO4: Column AOD map'
      'OPBC'  : NewTracerName = 'BC: Column AOD map'
      'OPOC'  : NewTracerName = 'OC: Column AOD map'
      'OPSSA' : NewTracerName = 'SALA: Column AOD map'
      'OPSSC' : NewTracerName = 'SALC: Column AOD map'
      ; For v10-01i and later
      'OPSO4550' : NewTracerName = 'SO4: Column AOD diff'
      'OPBC550'  : NewTracerName = 'BC: Column AOD diff'
      'OPOC550'  : NewTracerName = 'OC: Column AOD diff'
      'OPSSA550' : NewTracerName = 'SALA: Column AOD diff'
      'OPSSC550' : NewTracerName = 'SALC: Column AOD diff'
    endcase

   ; Plot title
   Title = VerStr + '!C!C' + NewTracerName

   ; Number of colorbar tickmarks
   Divisions = ColorBar_NDiv( 8 )

   ; Compute difference, but don't plot the polar latitudes
   XMid = GridInfo.XMid
   YMid = GridInfo.YMid[ 1:GridInfo.JMX-2 ]

   ; Get the difference range and unit
   ; NOTE: For dynamic range, plots, the RANGE will be ignored
   Get_Conc_Range, TracerName, Lo_Bound, Hi_Bound, Unit

   if ( DynRange ) then begin

      ;=================================================================
      ; Create plots using the full dynamic range of the data (centered
      ; around zero) if the /DYNRANGE keyword is set.
      ;=================================================================
      MinData  =  Min( Data, Max=MaxData )
      Triangle =  0
      NoGap    =  0
      Upos     =  1.1
      CbPos    =  [ 0.10, 0.01, 0.90, 0.04 ]

   endif else begin

      ;=================================================================
      ; Create plots using the pre-defined min & max values (from
      ; function GET_DIFF_RANGE).  This is the default.
      ;=================================================================
      MinData  =  Lo_Bound
      MaxData  =  Hi_Bound
      BotOut   =  !MYCT.BOTTOM
      Triangle =  1
      NoGap    =  1
      UPos     =  1.02
      CbPos    =  [ 0.05, 0.01, 0.95, 0.04 ]

   endelse

   ; Plot differences over a world map
   TvMap, Data[ *, 1:GridInfo.JMX-2 ], XMid, Ymid,          $
      /Countries,         /Coasts,         /Cbar,           $
      Division=Divisions, /Sample,         /Grid,           $
      Title=Title,        MinData=MinData, MaxData=MaxData, $
      CBFormat='(f13.3)', BOR_Label=' ',   Unit=Unit,       $
      Triangle=Triangle,  NoGap=NoGap,     BotOut=BotOut,   $
      CBPosition=CBPos,   TcsFac=1.0,      CsFac=0.8,       $
      UPos=UPos,          _EXTRA=e

   ; Restore original color table
   TvLct, R, G, B

   ; Restore !MYCT sysvar to defaults
   if ( ChkStru( Myct_Orig ) ) then !MYCT = Myct_Orig

end

;------------------------------------------------------------------------------

pro Maps_AOD, Files,  Taus,      Tracers, Versions,                $
              DynRange=DynRange, PS=PS,   OutFileName=OutFileName, $
              Month=Month,       _EXTRA=e
 
   ;====================================================================
   ; Initialization
   ;====================================================================
   
   ; External functions
   FORWARD_FUNCTION ChkStru, ColorBar_NDiv, Extract_FileName

   ; Arguments
   if ( N_Elements( Files    ) ne 2 ) then Message, 'Invalid FILES!'
   if ( N_Elements( Taus     ) ne 2 ) then Message, 'Invalid TAUS!'
   if ( N_Elements( Versions ) ne 2 ) then Message, 'Invalid VERSIONS!'

   ; Arguments
   DynRange = Keyword_Set( DynRange )
   if ( N_Elements( OutFileName ) ne 1 ) then OutFileName = 'aod_maps.ps'

   ; Title for the top of the plot
   TopTitle = 'GEOS-Chem ' + Versions[1] + ' Column AOD Maps!C!C'
   
   ;====================================================================
   ; Read data from the files
   ;====================================================================
   
   ; Read tracers from the 1st file
   CTM_Get_Data, DataInfo_1, 'OD-MAP-$', $
      File=Files[0], Tau0=Taus[0], Tracer=Tracers, /Quiet

   ; Read tracers from the 2nd file
   CTM_Get_Data, DataInfo_2, 'OD-MAP-$', $
      File=Files[1], Tau0=Taus[1], Tracer=Tracers, /Quiet

   ;------------------------------
   ; Error checks!
   ;------------------------------

   ; Stop if both DATAINFOs are incompatible
   if ( N_Elements( DataInfo_1 ) ne N_Elements( DataInfo_2 ) ) $
      then Message, 'Files have different numbers of tracers in them!'

   ;====================================================================
   ; Process data and create profile plots with CTM_PLOT!
   ;====================================================================

   ; Number of rows & colums on the plot
   Rows = 2
   Cols = 2

   ; Use Postscript font
   !p.font = 0

   ; Open the plot device and initialize the page
   Open_Device, /Color, Bits=8, /Portrait, PS=PS, File=OutFileName, _EXTRA=e
  
   ; Multiple panels per page
   MultiPanel, Rows=Rows, Cols=Cols, Margin=[ 0.03, 0.03, 0.03, 0.03 ]

   ; Max # of colorbar divisions
   Divisions = Colorbar_NDiv( 6 )

   ; Loop over the data blocks
   for D = 0L, N_Elements( DataInfo_1 )-1L do begin

      ;-----------------------------------------------------------------
      ; Error check grid, tracer name, and data block sizes 
      ;-----------------------------------------------------------------

      ; Get MODELINFO and GRIDINFO structures
      GetModelAndGridInfo, DataInfo_1[D], ModelInfo_1, GridInfo_1
      GetModelAndGridInfo, DataInfo_2[D], ModelInfo_2, GridInfo_2

      ; Make sure grids are compatible
      if ( GridInfo_1.IMX ne GridInfo_2.IMX  OR $
           GridInfo_1.JMX ne GridInfo_2.JMX )   $
         then Message, 'Resolution mismatch!'

      ; Make sure the tracers correspond to each other
      TracerName_1 = DataInfo_1[D].TracerName
      TracerName_2 = DataInfo_2[D].TracerName
      if ( TracerName_1 ne TracerName_2 ) then Message, 'Tracer mismatch!'

      ; Get full-sized data arrays
      Data_1 = *( DataInfo_1[D].Data )
      Data_2 = *( DataInfo_2[D].Data )

      ; Get the dimensions of the data arrays
      Size_1 = Size( Data_1, /Dim )
      Size_2 = Size( Data_2, /Dim )

      ; Stop the run if the data block sizes don't agree
      if ( Size_1[0] ne Size_2[0] ) then Message, 'Longitude mismatch!'
      if ( Size_1[1] ne Size_2[1] ) then Message, 'Latitude mismatch!'

      ; Get unit of data
      Unit = DataInfo_1[D].Unit

      ;-----------------------------------------------------------------
      ; Extract column data arraysys for surface and 500 hPa
      ;-----------------------------------------------------------------

      ; Sum arrays in the vertical
      Data_Col_1 = Total( Data_1, 3 )
      Data_Col_2 = Total( Data_2, 3 )

      ; We no longer need the large arrays
      UnDefine, Data_1
      UnDefine, Data_2

      ;-----------------------------------------------------------------
      ; Plot the data!
      ;-----------------------------------------------------------------

      ; Version 1
      PlotAODMap, Data_Col_1, Versions[0], TracerName_1, $
                  GridInfo_1, DynRange,    Month,        $
                  _EXTRA = e

      ; Version 2
      PlotAODMap, Data_Col_2, Versions[1], TracerName_1, $
                  GridInfo_1, DynRange,    Month,        $
                  _EXTRA = e

      ; Plot the top title on each page  
      if ( D*4 mod ( Rows * Cols ) eq 0 ) then begin
         XYoutS, 0.5, 1.03, TopTitle, $
            /Normal, Color=!MYCT.BLACK, CharSize=1.0, Align=0.5
      endif
   endfor

   ;====================================================================
   ; Cleanup and quit
   ;====================================================================

   ; Turn off multi-panel settings
   Multipanel, /Off

   ; Close plot device
   Close_Device

   ; Quit
   return
end
 
