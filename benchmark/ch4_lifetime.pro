;-----------------------------------------------------------------------
;+
; NAME:
;        CH4_LIFETIME
;
; PURPOSE:
;        Computes the methane (CH4) lifetime w/r/t tropospheric OH from
;        monthly-mean GEOS-Chem archived fields. 
;
; CATEGORY:
;        Benchmarking
;
; CALLING SEQUENCE:
;        CH4_LIFETIME, FILE, [ Keywords ]
;
; INPUTS:
;        FILE -> Specifies the name of the file with GEOS-Chem
;            diagnostic output (bpch format or otherwise).
;      
;
; KEYWORD PARAMETERS:
;        LON -> A 2-element vector containing the minimum and maximum
;             longitude values to include in the computation of the CH4
;             lifetime.  Default is [ -180, 180 ].
;
;        LAT -> A 2-element vector containing the minimum and maximum
;             latitude values to include in the computation of the CH4
;             lifetime.  Default is [ -90, 90 ].
;
;        LEV_a -> A 2-element vector containing the minimum and maximum
;             levels (i.e. all vertical levels) to include in the
;             computation of the CH4 lifetime.  Default is [ 1, 47 ] 
;             (corresponding to the GEOS-5 47-level simulation).
;
;        LEV_t -> a 2-element vector containing the minimum and
;             maximum tropospheric levels to include in the
;             computation of the CH4 lifetime.  Default is [ 1, 38 ]
;             (corresponding to the value of the max tropospheric
;             level  LLTROP for a GEOS-5 47-level simulation.)
;
;        TAU0 -> TAU value (hours from 0 GMT on 1985/01/01) with which
;             the GEOS-Chem diagnostic data blocks are timestamped.  
;             Default is 179664.00 (corresponding to 0 GMT on 
;             2005/07/01).
;
;        /VERBOSE -> Will cause CH4_LIFETIME to print the methyl
;             chlofororm lifetime to the screen.  The default is to
;             suppress printing.
;             
; OUTPUTS:
;        LIFE_YEARS -> Returns the CH4 lifetime w/r/t tropopsheric
;             OH in years.
;
; SUBROUTINES:
;        External subroutines required:
;        ==============================
;        CTM_BoxSize       (function)
;        CTM_Get_DataBlock (function)
;        Nymd2Tau          (function)
;
; REQUIREMENTS:
;        CH4_LIFETIME requires that the following GEOS-Chem
;        diagnostics be present in the input file:
; 
;          (a) Grid box surface area    [m2       ] (DXYP,     tracer #1)
;          (b) Grid box heights         [m        ] (BXHGHT-$, tracer #1)
;          (c) Air number density       [molec/m3 ] (BXHGHT-$, tracer #4)
;          (d) Chemically produced OH   [molec/cm3] (CHEM-L=$, tracer #1)
;          (e) Time spent in tropopause [fraction ] (TIME-TPS, tracer #1)
;          (f) Temperature              [K        ] (DAO-3D-$, tracer #5)
;
;        Requires routines from the GAMAP package.
;
; NOTES:
;        Derivation of CH4 lifetime w/r/t tropospheric OH
;        ----------------------------------------------------------
;
;        We assume that CH4 has a uniform mixing ratio in air (=1).  
;        Thus the density of air can be substituted for the density
;        of CH4 in the equations below.
;
;
;        The lifetime of CH4 w/ respect to tropospheric OH is:
;
;           T_OH = Atmospheric Burden / Tropospheric loss rate
;
;
;        The atmospheric burden of CH4 is:
;
;           SUM{ AIRDEN(I,J,L) * VOLUME(I,J,L) }
;
;           where:
;              I        = longitudes
;              J        = latitudes
;              L        = levels from the surface to top of atmosphere
;              AIRDEN   = Air density at ([I,J,L) in molec/cm3
;              VOLUME   = Grid box volume at (I,J,L) in cm3
; 
;
;        The tropospheric loss rate of CH4 is:
;
;           SUM{ K(I,J,X) * OH(I,J,X) * AIRDEN(I,J,X) * VOLUME(I,J,X) }
;    
;           where
;              I        = longitudes
;              J        = latitudes
;              X        = levels from the surface to the 
;                          location of the tropopause at (I,J)
;                          (we only count boxes fully in the tropopause)
;              K(I,J,X) = 2.45e-12 * EXP( -1775 / T(I,J,X) )
;              T(I,J,X) = Temperature at grid box (I,J,X)
;   
;
;        T_OH has several reported values in the literature:
;      
;           (a) Spivakovsky et al (2000) : 5.7 years
;           (b) Prinn et al (2001)       : 6.0 years
;
;        References:
;        ----------------------------------------------------------
;
;        (1) Prather, M. and C. Spivakovsky, "Tropospheric OH and
;             the lifetimes of hydrochlorofluorocarbons", JGR,
;             Vol 95, No. D11, 18723-18729, 1990.
;
;        (2) Lawrence, M.G, Joeckel, P, and von Kuhlmann, R., "What
;             does the global mean OH concentraton tell us?",
;             Atm. Chem. Phys, 1, 37-49, 2001.
;
;        (3) WMO/UNEP Scientific Assessment of Ozone Depletion: 2010
;            
; EXAMPLE:
;        LIFE = CH4_LIFETIME( 'ctm.bpch',       $
;                              LON=[-180,-180], $
;                              LAT=[ -90,  90], $
;                              LEV_A=[ 1, 47 ], $
;                              LEV_T=[ 1, 38 ], $
;                              TAU0=NYMD2TAU( 20050101 ) )
;
;             ; Will return the CH4 lifetime w/r/t tropospheric OH
;             ; (in years) based on GEOS-Chem diagnostic output at 
;             ; model date 2005/01/01.
;                                   
; MODIFICATION HISTORY:
;        mps, 09 Nov 2017: Initial version based on mcf_lifetime.pro
;
;-
; Copyright (C) 2011, Bob Yantosca, Harvard University
; This software is provided as is without any warranty whatsoever.
; It may be freely used, copied or distributed for non-commercial
; purposes.  This copyright notice must be kept with any copy of
; this software. If this software shall be used commercially or
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to yantosca@seas.harvard.edu
; or with subject "IDL routine mcf_lifetime"
;-----------------------------------------------------------------------


function CH4_Lifetime, File,                                     $
                       Lon=Lon,     Lat=Lat,    Lev_a=Lev_a,     $
                       Lev_t=Lev_t, Tau0=Tau0,  Verbose=Verbose, $
                       _EXTRA=e
 
   ;----------------------------------------------------------------
   ;### DEBUG: set inputs for debugging
   ;File    = '~/debug/v9-01-01/run.v9-01-01/ctm.bpch.v9-01-01'  
   ;----------------------------------------------------------------
 
   ;====================================================================
   ; Initialization
   ;==================================================================== 
 
   ; Declare external functions
   FORWARD_FUNCTION CTM_BoxSize, CTM_Get_DataBlock, Nymd2Tau
 
   ; Arguments
   if ( N_Elements( File  ) ne 1 ) then Message, 'FILE not passed!'
 
   ; Keywords
   if ( N_Elements( Lon   ) ne 2 ) then Lon   = [ -180, 180 ]
   if ( N_Elements( Lat   ) ne 2 ) then Lat   = [  -90,  90 ]
   if ( N_Elements( Lev_t ) ne 2 ) then Lev_t = [    1,  38 ]
   if ( N_Elements( Lev_a ) ne 2 ) then Lev_a = [    1,  47 ]
   if ( N_Elements( Tau0  ) ne 2 ) then Tau0  = Nymd2Tau( 20050701 )
 
   ; Index array for boxes up to max tropospheric level
   Ind_t   = LIndGen( Lev_t[1] )+1
 
   ;====================================================================
   ; Get OH [molec/cm3] : trop boxes only
   ;====================================================================
   Success = CTM_Get_DataBlock( OH_t,      'CHEM-L=$',    $
                                File=File,  Lon=Lon,      $
                                Lat=Lat,    Lev=Lev_t,    $
                                Tracer=1,   /Quiet,       $
                                /NoPrint,   GridInfo=Grid, $
                                _EXTRA=e )
 
   if ( not Success ) then Message, 'Could not read OH!'
 
   ; Cast from REAL*4 to REAL*8
   OH_t = OH_t * 1d0
 
   ;====================================================================
   ; Get grid box surface area [cm2] : surface only
   ;====================================================================

   ; First get grid box heights [m]
   Success = CTM_Get_DataBlock( AreaCm2,   'DXYP',         $
                                File=File,  Lon=Lon,       $
                                Lat=Lat,    Lev=1,         $
                                Tracer=1,   /Quiet,        $
                                /NoPrint,   _EXTRA=e )
 
   ; If the data file does not have the surface area archived,
   ; then compute it with CTM_BOXSIZE
   if ( not Success ) then begin
      AreaCm2 = CTM_BoxSize( Grid, /Geos, /Cm2 )
   endif

   ;====================================================================
   ; Get grid box volume [cm3] : trop + strat boxes
   ;====================================================================
 
   ; First get grid box heights [m]
   Success = CTM_Get_DataBlock( BxHt_a,    'BXHGHT-$',     $
                                File=File,  Lon=Lon,       $
                                Lat=Lat,    Lev=Lev_a,     $
                                Tracer=1,   /Quiet,        $
                                /NoPrint,   _EXTRA=e )
 
   if ( not Success ) then Message, 'Could not read BOXHEIGHT!'
 
   ; Convert from [m] to [cm3]
   BxHt_a = BxHt_a * 1d2
   
   ; Multiply areas by box heights to get volumes [cm3]
   Vol_a = BxHt_a
   for L=0L, Lev_a[1]-1L do begin
      Vol_a[*,*,L]= Vol_a[*,*,L] * AreaCm2
   endfor
 
   ; Make a separate array for grid box volumes in troposphere only
   ; This will facilitate IDL array operations
   Vol_t = Vol_a[*,*,Ind_t]
 
   ;====================================================================
   ; Get CH4 density [molec/m3] : trop + strat boxes
   ;
   ; NOTE: Assume that CH4 density is evenly distributed in air, with
   ; a mixing ratio of 1.  Thus CH4 density = air density.
   ;====================================================================
   Success = CTM_Get_DataBlock( CH4_a,     'BXHGHT-$',    $
                                File=File,  Lon=Lon,      $
                                Lat=Lat,    Lev=Lev_a,    $
                                Tracer=4,   /Quiet,       $
                                /NoPrint,   _EXTRA=e )
                               
   if ( not Success ) then Message, 'Could not read TIME IN TROPOSPHERE!'
 
   ; NOTE: Assume that methyl cholo
   ; Convert from [molec/m3] to [molec/cm3]
   CH4_a = CH4_a / 1d6
 
   ; Make a separate array for CH4 concentrations in troposphere only
   ; This will facilitate IDL array operations
   CH4_t = CH4_a[*,*,Ind_t]
 

   ;====================================================================
   ; Get time spent in troposphere: [fraction] : trop boxes only
   ;====================================================================
   Success = CTM_Get_DataBlock( Time_t,    'TIME-TPS',    $
                                File=File,  Lon=Lon,      $ 
                                Lat=Lat,    Lev=Lev_t,    $
                                Tracer=1,   /Quiet,       $
                                /NoPrint,   _EXTRA=e )
                               
   if ( not Success ) then Message, 'Could not read TIME IN TROPOSPHERE!'
 
   ; Cast from REAL*4 to REAL*8
   Time_t = Time_t * 1d0
 
   ; GEOS-Chem versions prior to v9-01-02 had a bug in the TIME-TPS 
   ; (time spent in the tropopause) diagnostic.  The diagnostic was 
   ; computed with a scale factor twice as large as it should have
   ; been.  Thus, instead of having the maximum value as 1.0, the 
   ; maximum value was 0.5.  In this case we can just multiply the
   ; time in the tropopause by 2 to fix the problem. (bmy, 5/24/11)
   if ( Max( Time_t ) lt 0.99 ) then Time_t = Time_t * 2d0
 
   ;====================================================================
   ; Get Arrhenius parameter K [cm3/molec/s] : Trop boxes only 
   ;====================================================================
 
   ; First get temperature 
   Success = CTM_Get_DataBlock( T_t,       'DAO-3D-$',     $  
                                File=File,  Lon=Lon,       $
                                Lat=Lat,    Lev=Lev_t,     $
                                Tracer=3,   /Quiet,        $
                                /NoPrint,   _EXTRA=e )
                               
   if ( not Success ) then Message, 'Could not read TEMPERATURE!'
 
   ; Compute reaction rate [cm3/molec/s]
   ; JPL evaluation v18, page 1-91
   K_t = 2.45d-12 * EXP( -1775d0 / T_t )
 
   ;====================================================================
   ; Compute the CH4 lifetime
   ;====================================================================
   
   ; Find all boxes that have are purely tropospheric.  
   ; This excludes influence from the stratosphere.
   Trop = Where( Time_t eq 1d0 )
  
   if ( Trop[0] ge 0 ) then begin
 
      ;-------------------------------------------
      ; If we find tropopsheric boxes ... 
      ;-------------------------------------------

      ; Numerator: Total atmospheric burden
      Num = Total( CH4_a * Vol_a  ) 
 
      ; Denominator: Loss rate in troposphere
      Den = Total( K_t[Trop] * OH_t[Trop] *  CH4_t[Trop] * Vol_t[Trop] ) 
 
      ; Compute MCL lifetime
      if ( Den gt 0 ) then begin
         Life_Sec   = Num      / Den                     ; in seconds
         Life_Years = Life_Sec / ( 365.25d0 * 86400d0 )  ; in years

         ; Print CH4 lifetime
         if ( Keyword_Set( Verbose ) ) then begin
            print, Life_Years, $
                   Format='("CH4 lifetime w/r/t/ trop OH: ", f7.4, " years")'
         endif

         ; Return lifetime to the calling program
         return, Life_Years

      endif else begin
 
         ; Print error message
         Message, 'Tropospheric loss rate is zero!  Cannot print CH4 lifetime!'
         
      endelse
     
   endif else begin
 
      ;-------------------------------------------
      ; If we don't find tropopsheric boxes ... 
      ;-------------------------------------------

      ; Exit w/ error message, we can't do computation
      Message, 'No tropospheric boxes!  Cannot compute CH4 lifetime!'
      
   endelse
   
end
