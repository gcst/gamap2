;-----------------------------------------------------------------------
;+
; NAME:
;        RATIOS
;
; PURPOSE:
;        Creates ratio plots ( New/Old ) for GEOS-Chem tracers and OH. 
;
; CATEGORY:
;        Benchmarking
;
; CALLING SEQUENCE:
;        RATIOS, FILES, LEVELS, TAUS, VERSIONS, [, Keywords ]
;
; INPUTS:
;        FILES -> A 2-element vector containing the names of files
;             from the "old" and "new" GEOS-Chem model versions
;             that are to be compared. 
;
;        LEVELS -> A 4-element vector containing the level indices
;             for the GEOS-Chem surface layer and 500 hPa layer.
;             for both models (e.g. SFC_1, SFC_2, 500_1, 500_2).
;             NOTE: This is in Fortran notation (starting from 1!)
;
;        TAUS -> A 2-element vector contaning TAU values (hours GMT
;             from /1/1985) corresponding to the "old" and "new"
;             GEOS-Chem model versions.
;
;        TRACERS -> The list of transported tracers (i.e. diagnostic
;             category "IJ-AVG-$").
;
;        VERSIONS -> A 2-element vector containing the version
;             numbers for the "old" and "new" GEOS-Chem model
;             versions.
;
; KEYWORD PARAMETERS:
;        /DO_FULLCHEM -> Set this switch to plot the chemically
;             produced OH in addition to the advected tracers.
;
;        /DYNRANGE -> Set this switch to create plots using the whole
;             dynamic range of the data.  Default is to restrict
;             the plot range from 0.5 to 2.0.
;
;        /PS -> Set this switch to generate PostScript output.
;
;        OUTFILENAME -> If /PS is set, will write PostScript output 
;             to a file whose name is specified by this keyword.
;             Default is "tracer_ratio.pro".
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        Internal Subroutines Included:
;        ===========================================
;        ComputeRatios   PlotRatio
;
;        External Subroutines Required:
;        ============================================
;        OPEN_DEVICE     CLOSE_DEVICE
;        MULTIPANEL      COLORBAR_NDIV    (function)
;        TVMAP           UNDEFINE 
;        CTM_GET_DATA,   EXTRACT_FILENAME (function)     
;     
; REQUIREMENTS:
;        References routines from both GAMAP and TOOLS packages.
;        
; NOTES:
;        (1) Meant to be called from BENCHMARK_1MON.
;
; EXAMPLES:
;        FILES    = [ 'ctm.bpch.v7-04-10', 'ctm.bpch.v7-04-11' ]
;        LEVELS   = [ 1, 1, 13, 13 ]
;        TAUS     = [ NYMD2TAU( 20010701 ), NYMD2TAU( 20010701 ) ]
;        TRACERS  = INDGEN( 43 ) + 1
;        VERSIONS = [ 'v7-04-10', 'v7-04-11' ]
; 
;        RATIOS, FILES, LEVELS, TAUS, TRACERS, VERSIONS, $
;             /DO_FULLCHEM, /PS, OUTFILENAME='myplot.ps'
;
;             ; Creates ratio plots of two GEOS-CHEM versions
;             ; (in this case v7-04-11 / v7-04-11) for July 2001.
;             ; Output is sent to PostScript file "myplot.ps".
;             ; The min & max of the data will be fixed at -/+ 5%.
;
;        RATIOS, FILES, LEVELS, TAUS, TRACERS, VERSIONS, $
;             /DYNRANGE, /PS, OUTFILENAME='myplot.ps'
;
;             ; Same as above, but this time the full dynamic range
;             ; of the data will be displayed.
;
;
; MODIFICATION HISTORY:
;        bmy, 14 Nov 2007: VERSION 1.01
;                          - based on older routine "tracer_ratio.pro"
;        bmy, 20 Nov 2007: VERSION 1.02
;                          - Now draw out-of-bounds triangles for
;                            the colorbar when using the "small"
;                            data ranges.  New feature of TVMAP.;
;        bmy, 07 May 2008: VERSION 1.03
;                          - Now allow for comparing models on 2
;                            different vertical grids.
;        bmy, 08 Feb 2011: VERSION 1.04
;                          - Now display in the top-of-plot title
;                            if the dynamic range option is used.
;        bmy, 08 Jun 2011: VERSION 1.05
;                          - Now create log plots in range 0.5 - 2.0
;                          - Added /DO_FULLCHEM keyword
;                          - Adjust colorbar so that the 0.9 - 1.1
;                            range shows up in white.
;                          - Now restore !MYCT sysvar to previous
;                            settings upon exiting the program
;                          - Better adjust colorbar position for /PS
;        mps, 29 Mar 2013: - Now plot HO2 ratios
; 
;-
; Copyright (C) 2007-2011,                            
; Bob Yantosca and Philippe Le Sager, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.as.harvard.edu with subject "IDL routine ratios"
;-----------------------------------------------------------------------


pro ComputeRatios, Size, Data_Sfc_1, Data_Sfc_2, Ratio_Sfc, $
                         Data_500_1, Data_500_2, Ratio_500

   ;====================================================================
   ; Internal routine COMPUTERATIOS computes the ratio of 
   ; "new" / "old" at both the surface level and the 500 hPa level. 
   ; (bmy, 11/9/07, 6/8/11)
   ;====================================================================

   ;----------------------
   ; %%%%% SURFACE %%%%%
   ;----------------------

   ; Compute the ratio 
   Ratio_Sfc = Data_Sfc_2 / Data_Sfc_1

   ; Replace non-finite values with a ratio of 1.0 where Data_Sfc_1 is 0
   Ind_zeroDenom =  Where ( Data_Sfc_1 eq 0 )
   if ( Ind_zeroDenom[0] ge 0 ) then Ratio_Sfc[Ind_zeroDenom] = 1.0e0

   ; Replace any remaining non-finite values with a missing value
   Ind_inf = Where( ~Finite( Ratio_Sfc ) ) 
   if ( Ind_inf[0] ge 0 ) then Ratio_Sfc[Ind_inf] = -9.99e30

   ;----------------------
   ; %%%%% 500 HPa %%%%%
   ;----------------------
   
   ; Compute the ratio
   Ratio_500 = Data_500_2 / Data_500_1

   ; Replace non-finite values with a ratio of 1.0 where Data_Sfc_1 is 0
   Ind_zeroDenom =  Where ( Data_500_1 eq 0 )
   if ( Ind_zeroDenom[0] ge 0 ) then Ratio_500[Ind_zeroDenom] = 1.0e0

   ; Replace any remaining non-finite values with a missing value
   Ind_inf = Where( ~Finite( Ratio_500 ) ) 
   if ( Ind_inf[0] ge 0 ) then Ratio_500[Ind_inf] = -9.99e30

end

;------------------------------------------------------------------------------

pro PlotRatio, Data, Level, TracerName, GridInfo, DynRange, PS=PS, _EXTRA=e
   
   ;====================================================================
   ; Internal routine PLOTRATIO plots either the surface or 500 hPa
   ; ratio of tracer between old and new versions (bmy, 11/9/07)
   ;====================================================================

   ; Plot title
   if ( Level gt 1 )                                    $
      then Title = 'Ratio @ 500 hPa for ' + TracerName $
      else Title = 'Ratio @ Surface for ' + TracerName  

   ; Number of colorbar tickmarks
   Divisions = ColorBar_NDiv( 6 )
      
   ; Don't plot the polar latitudes
   XMid      = GridInfo.XMid
   YMid      = GridInfo.YMid[ 1:GridInfo.JMX-2 ]
   Data      = Data[ *, 1:GridInfo.JMX-2 ]

   ; GOOD is the index of "good" data points
   Good      = Where( Data gt -9.99e30 )
   if ( Good[0] lt 0 ) then Message, 'No valid data points found!'

   ; If DYNRANGE is set, then plot the dynamic range of the data
   ; Otherwise, fix the min & max of the data as +/- 5% ratio
   if ( DynRange ) then begin

      ;=================================================================
      ; Plot ratio data dusing the dynamic range of the data 
      ; Center the plot range symmetrically around zero
      ;=================================================================

      MinData  =  Min( Data, Max=MaxData )

      ; For now, comment this out...
      ; COMMENT OUT -- center around zero 
      ;Extreme =  Max( [ Abs( MinData ), Abs( MaxData ) ] )
      ;MinData = -Extreme
      ;MaxData =  Extreme

      ; Settings for color table #63
      Log        = 1                               
      Div        = 8
      Annotation = [ '0.50', '0.61', '0.74', '0.90',                   $
                     '1.10', '1.34', '1.64', '2.00'  ]

      ; We need to set the colorbar a little bit lower for PostScript
      if ( Keyword_Set( PS ) )                                         $
         then CBPos = [ 0.05, -0.02, 0.95, 0.01 ]                      $
         else CBPos = [ 0.05,  0.05, 0.95, 0.08 ]

      ; Plot ratio over a world map
      TvMap, Data, XMid, Ymid,                                         $
             /Countries,         /Coasts,         /Cbar,               $
             Division=Div,       /Sample,         /Grid,               $
             Title=Title,        MinData=MinData, MaxData=MaxData,     $
             CBFormat='(f13.3)', BOR_Label=' ',   Triangle=Triangle,   $
             NoGap=NoGap,        BotOut=BotOut,   Log=Log,             $
             CbPosition=CbPos,   _EXTRA=e

   endif else begin

      ;=================================================================
      ; Plot ratio data w/in limits of 0.5 - 2.0 (default)
      ; Values in the range of 0.9 - 1.1 will show up as white
      ;=================================================================

      ; Settings for plot
      MinData  = 0.5
      MaxData  = 2.0
      BotOut   = !MYCT.BOTTOM
      Triangle = 1
      NoGap    = 1
      Log      = 1                               
      Div      = 8
      Annote   = [ '0.50', '0.61', '0.74', '0.90',                     $
                   '1.10', '1.34', '1.64', '2.00'  ]                   
                                                                       
      ; We need to set the colorbar a little bit lower for PostScript  
      if ( Keyword_Set( PS ) )                                         $
         then CBPos = [ 0.05, -0.02, 0.95, 0.01 ]                      $
         else CBPos = [ 0.05,  0.05, 0.95, 0.08 ]                      
                                                                       
      ; Plot ratio over a world map                                    
      TvMap, Data, XMid, Ymid,                                         $
             /Countries,         /Coasts,          /Cbar,              $
             Division=Div,       /Sample,          /Grid,              $
             Title=Title,        MinData=MinData,  MaxData=MaxData,    $
             CBFormat='(f13.3)', BOR_Label=' ',    Triangle=Triangle,  $
             NoGap=NoGap,        BotOut=BotOut,    Log=Log,            $
             Annotation=Annote,  CbPosition=CbPos, _EXTRA=e

   endelse

end

;------------------------------------------------------------------------------

pro Ratios, Files, Levels, Taus, Tracers, Versions,     $
            DynRange=DynRange, Do_FullChem=Do_FullChem, $
            PS=PS,             OutFileName=OutFileName, $
            _EXTRA=e
 
   ;====================================================================
   ; Initialization
   ;====================================================================
   
   ; External functions
   FORWARD_FUNCTION ChkStru, ColorBar_NDiv, Extract_FileName

   ; Arguments
   if ( N_Elements( Files       ) ne 2 ) then Message, 'Invalid FILES!'
   if ( N_Elements( Levels      ) ne 4 ) then Message, 'Invalid LEVELS'
   if ( N_Elements( Taus        ) ne 2 ) then Message, 'Invalid TAUS!'
   if ( N_Elements( Versions    ) ne 2 ) then Message, 'Invalid VERSIONS!'

   ; Keywords
   Do_FullChem = Keyword_Set( Do_FullChem )
   DynRange    = Keyword_Set( DynRange    )
   if ( N_Elements( OutFileName ) ne 1 ) then OutFileName = 'ratios.ps'

   ; Title for the top of the plot
   if ( DynRange ) then begin
      TopTitle = 'GEOS-Chem '                 + Versions[1]              + $
                 ' Tracer Ratios (dyn range) at Surface and 500 hPa!C!C' + $
                 Extract_FileName( Files[1] ) +  ' / '                   + $
                 Extract_FileName( Files[0] )
    
   endif else begin
      TopTitle = 'GEOS-Chem '                 + Versions[1]              + $
                 ' Tracer Ratios at Surface and 500 hPa!C!C'             + $
                 Extract_FileName( Files[1] ) +  ' / '                   + $
                 Extract_FileName( Files[0] )
   endelse

   ; Save original color table
   TvLct, R, G, B, /Get
   
   ; Save the original settings of the !MYCT sysvar
   if ( ChkStru( !MYCT ) ) then Myct_Orig = !MYCT

   ; This colortable will show 0.9 - 1.0 in the white, and anything
   ; outside that in the red or blue.  Use 8 colorbar divisions to line
   ; everything up properly. (bmy, 5/21/10)
   ;
   ; NOTE: It is really 0.9057 - 1.0104.  Close enough for gov't work.
   MyCt, 'RdBu', NColors=14, /MidCol, /White, /Reverse, _EXTRA=e

   ;====================================================================
   ; Read data from the files
   ;====================================================================
   
   ;------------------------------
   ; From FILE_1: the "old" file
   ;------------------------------

   ; Read tracers from the old file
   CTM_Get_Data, DataInfo_1, 'IJ-AVG-$', $
      File=Files[0], Tau0=Taus[0], Tracer=Tracers, /Quiet

   ; Read OH from the "new" file
   if ( Do_FullChem ) then begin
      CTM_Get_Data, DataInfo, 'CHEM-L=$', $
                    File=Files[0], Tau0=Taus[0], Tracer=1, /Quiet
      DataInfo_1 = [ DataInfo_1, DataInfo ]
      UnDefine, DataInfo
   endif

   ; Read HO2 from the "new" file
   if ( Do_FullChem ) then begin
      CTM_Get_Data, DataInfo, 'CHEM-L=$', $
                    File=Files[0], Tau0=Taus[0], Tracer=3, /Quiet
      DataInfo_1 = [ DataInfo_1, DataInfo ]
      UnDefine, DataInfo
   endif

   ;------------------------------
   ; From FILE_2: the "new" file
   ;------------------------------

   ; Read tracers from the "new" file
   CTM_Get_Data, DataInfo_2, 'IJ-AVG-$', $
      File=Files[1], Tau0=Taus[1], Tracer=Tracers, /Quiet

   ; Read OH from the "new" file
   if ( Do_FullChem ) then begin
      CTM_Get_Data, DataInfo, 'CHEM-L=$', $
                    File=Files[1], Tau0=Taus[1], Tracer=1, /Quiet
      DataInfo_2 = [ DataInfo_2, DataInfo ]
      UnDefine, DataInfo
   endif

   ; Read HO2 from the "new" file
   if ( Do_FullChem ) then begin
      CTM_Get_Data, DataInfo, 'CHEM-L=$', $
                    File=Files[1], Tau0=Taus[1], Tracer=3, /Quiet
      DataInfo_2 = [ DataInfo_2, DataInfo ]
      UnDefine, DataInfo
   endif

   ;------------------------------
   ; Error checks!
   ;------------------------------

   ; Stop if both DATAINFOs are incompatible
   if ( N_Elements( DataInfo_1 ) ne N_Elements( DataInfo_2 ) ) $
      then Message, 'Files have different numbers of tracers in them!'

   ;====================================================================
   ; Process data and create ratio plots!
   ;====================================================================

   ; Number of rows & colums on the plot
   Rows = 3
   Cols = 2

   ; Use Postscript font
   !p.font = 0

   ; Open the plot device and initialize the page
   Open_Device, /Color, Bits=8, /Portrait, PS=PS, File=OutFileName, _EXTRA=e

   ; Open text file for printing max ratios
   Open_File, 'Ratios_Sfc.txt', IlunSfc, /Get_LUN, /Write
   Open_File, 'Ratios_500.txt', Ilun500, /Get_LUN, /Write
   
   ; Multiple panels per page
   MultiPanel, Rows=Rows, Cols=Cols, Margin=[ 0.03, 0.03, 0.03, 0.03 ]

   ; Loop over all data blocks
   for D = 0L,  N_Elements( DataInfo_1 )-1L do begin
   
      ;-----------------------------------------------------------------
      ; Error check grid, tracer name, and data block sizes 
      ;-----------------------------------------------------------------

      ; Get MODELINFO and GRIDINFO structures
      GetModelAndGridInfo, DataInfo_1[D], ModelInfo_1, GridInfo_1
      GetModelAndGridInfo, DataInfo_2[D], ModelInfo_2, GridInfo_2

      ; Make sure grids are compatible
      if ( GridInfo_1.IMX ne GridInfo_2.IMX OR  $
           GridInfo_1.JMX ne GridInfo_2.JMX   ) $
         then Message, 'Resolution mismatch!'

      ; Make sure the tracers correspond to each other
      TracerName_1 = DataInfo_1[D].TracerName
      TracerName_2 = DataInfo_2[D].TracerName
      if ( TracerName_1 ne TracerName_2 ) then Message, 'Tracer mismatch!'

      ; Get full-sized data arrays
      Data_1 = *( DataInfo_1[D].Data )
      Data_2 = *( DataInfo_2[D].Data )

      ; Get the dimensions of the data arrays
      Size_1 = Size( Data_1, /Dim )
      Size_2 = Size( Data_2, /Dim )

      ; Stop the run if the data block sizes don't agree
      if ( Size_1[0] ne Size_2[0] ) then Message, 'Longitude mismatch!'
      if ( Size_1[1] ne Size_2[1] ) then Message, 'Latitude mismatch!'

      ;-----------------------------------------------------------------
      ; Extract data arrays for surface and 500 hPa
      ;-----------------------------------------------------------------

      ; Surface data arrays
      Data_Sfc_1 = Data_1[ *, *, Levels[0]-1 ]
      Data_Sfc_2 = Data_2[ *, *, Levels[1]-1 ]

      ; 500 hPa data arrays
      Data_500_1 = Data_1[ *, *, Levels[2]-1 ]
      Data_500_2 = Data_2[ *, *, Levels[3]-1 ]

      ; We no longer need the 3-D data arrays
      UnDefine, Data_1
      UnDefine, Data_2

      ;-----------------------------------------------------------------
      ; Compute the ratios and plot the data
      ;-----------------------------------------------------------------

      ; Get the "new" / "old" ratios at surface and 500 hPa
      ComputeRatios, Size_1, Data_Sfc_1, Data_Sfc_2, Ratio_Sfc, $
                             Data_500_1, Data_500_2, Ratio_500

      ; Print species that differ by more than 10% to file
      if ( Max(Abs(Ratio_Sfc)) ge 1.1 or Max(Abs(Ratio_Sfc)) le 0.9 ) then begin
         PrintF, IlunSfc, TracerName_1
      endif
      if ( Max(Abs(Ratio_500)) ge 1.1 or Max(Abs(Ratio_500)) le 0.9 ) then begin
         PrintF, Ilun500, TracerName_1
      endif
         
      ; Plot the Surface ratios
      PlotRatio, Ratio_Sfc,  Levels[0], TracerName_1, $
                 GridInfo_1, DynRange,  PS=PS,        $
                 _EXTRA=e

      ; Plot the 500 hPa ratios
      PlotRatio, Ratio_500,  Levels[2], TracerName_1, $
                 GridInfo_1, DynRange,  PS=PS,        $
                 _EXTRA=e
 
      ; Plot the top title on each page  
      if ( D*2 mod ( Rows * Cols ) eq 0 ) then begin
         XYoutS, 0.5, 1.03, TopTitle, $
            /Normal, Color=!MYCT.BLACK, CharSize=1.0, Align=0.5
      endif

      ;-----------------------------------------------------------------
      ; Undefine stuff for next iteration
      ;-----------------------------------------------------------------
      UnDefine, GridInfo_1
      UnDefine, GridInfo_2
      UnDefine, ModelInfo_1
      UnDefine, ModelInfo_2
      UnDefine, Size_1
      UnDefine, Size_2
      UnDefine, Ratio_Sfc
      UnDefine, Ratio_500
      UnDefine, TracerName_1
      UnDefine, TracerName_2

   endfor

   ;====================================================================
   ; Cleanup and quit
   ;====================================================================

   ; Close previous MULTIPANEL settings
   MultiPanel, /Off

   ; Close plot device
   Close_Device

   ; Close file
   Close,    IlunSfc
   Close,    Ilun500
   Free_LUN, IlunSfc
   Free_LUN, Ilun500
   
   ; Restore original color table
   TvLct, R, G, B

   ; Restore !MYCT sysvar to original settings
   if ( ChkStru( Myct_Orig ) ) then !MYCT = Myct_Orig

   ; Quit
   return
end
 
