;-----------------------------------------------------------------------
;+
; NAME:
;        STRAT
;
; PURPOSE:
;        Creates zonal-mean curtain plots of GEOS-Chem tracers in the
;        stratosphere (~400 hPa to max lev) from the GEOS-Chem
;        1-month benchmark simulations.
;
; CATEGORY:
;        Benchmarking
;
; CALLING SEQUENCE:
;        STRAT, FILE, LONS, TAUS, TRACERS, VERSION, [, Keywords ]
;
; INPUTS:
;        FILE -> The name of the file containing data to be plotted.
;
;        LONS -> Longitudes to plot
;
;        TAU -> The TAU value (hours GMT from /1/1985) corresponding
;             to the data to be plotted.
;
;        TRACERS -> The list of transported tracers (i.e. diagnostic
;             category "IJ-AVG-$").
;
;        VERSION -> The model version number corresponding to the
;             data to be plotted.
;
; KEYWORD PARAMETERS:
;        /DO_FULLCHEM -> Set this switch to plot the chemically
;             produced OH in addition to the advected tracers.
;
;        /PS -> Set this switch to generate PostScript output.
;
;        OUTFILENAME -> If /PS is set, will write PostScript output 
;             to a file whose name is specified by this keyword.
;             Default is "tracer_ratio.pro".
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        Internal Subroutines Provided:
;        ==================================================
;        PlotStrat
;
;        External Subroutines Required:
;        ==================================================
;        CLOSE_DEVICE          COLORBAR_NDIV    (function)
;        CTM_GET_DATA          EXTRACT_FILENAME (function)
;        GETMODELANDGRIDINFO   MULTIPANEL
;        MYCT                  OPEN_DEVICE
;        TVMAP                 CHKSTRU          (function)
;        UNDEFINE
;        
; REQUIREMENTS:
;        References routines from the GAMAP package.
;        
; NOTES:
;        (1) Meant to be called from BENCHMARK_1MON.
;
; EXAMPLES:
;        FILE     = 'ctm.bpch.v7-04-11'
;        LEVELS   = [ 1, 1, 13, 13 ]
;        TAUS     = NYMD2TAU( 20010701 )
;        TRACERS  = INDGEN( 43 ) + 1
;        VERSIONS = 'v7-04-11'
;
;        STRAT, FILE, LONS, TAU, TRACERS, VERSION, $
;             /DO_FULLCHEM, /PS, OUTFILENAME='myplot.ps'
;
; MODIFICATION HISTORY:
;        mps, 11 Sep 2015: - Initial version, based on zonal.pro
;        mps, 14 Mar 2018: - Now plot data above 400 hPa to capture entire
;                            stratosphere
;
;-
; Copyright (C) 2012-2013,
; Bob Yantosca and Philippe Le Sager, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.as.harvard.edu with subject "IDL routine maps"
;-----------------------------------------------------------------------


pro PlotStrat, Data, Lons, TracerName, Unit, GridInfo, _EXTRA=e
   
   ;====================================================================
   ; Internal routine PLOTSTRAT plots tracer concentrations in the
   ; stratosphere (mps, 9/11/15)
   ;====================================================================

   ; Plot title
   if ( Lons ge 0 )                                      $
      then Title = 'Tracer Curtain @ '+ string(GridInfo.XMid[Lons])+' - ' + TracerName  $
      else Title = 'Tracer Curtain Zonal mean - ' + TracerName

   ; Index arrays
   S             = Size( Data, /Dim )
   Lats          = GridInfo.YMid

   ; Set Y-axis coordinate for pressure (~400 hPa to max lev)
   if ( Lons ge 0 )                                                         $
   then YMid     = GridInfo.PMid[0:S[2]-1L]                                 $
   else YMid     = GridInfo.PMid[0:S[1]-1L]

   ; Select data for the stratosphere
   if ( TracerName eq 'OH' or TracerName eq 'HO2' ) then begin
      YStrat     = YMid[26:58]
      if ( Lons ge 0 )                                                      $
      then Strat = Data[*,*,26:58]                                          $
      else Strat = Data[*,26:58]
   endif else begin
      YStrat     = YMid[26:71]
      if ( Lons ge 0 )                                                      $
      then Strat = Data[*,*,26:71]                                          $
      else Strat = Data[*,26:71]
   endelse

   ; Parameters common to both dyn range and pre-defined range plots
   XTickV        = [ -90, -60, -30, 0, 30, 60, 90 ]
   XTicks        = N_Elements( XTickV )-1L
   XMinor        = 6
   XTitle        = 'Latitude'
   yTitle        = 'Pressure (hPa)'
   Div           = Colorbar_Ndiv( 6 )

   ; For OH, let's rescale the unit for clarity
   if ( TracerName eq 'OH' ) then begin
      Strat = Strat / 1e5
      Unit = '1e5 molec/cm3'
   endif

   ; For HO2, let's rescale the unit for clarity
   if ( TracerName eq 'HO2' ) then begin
      Strat = Strat / 1e-12
      Unit = 'pptv'
   endif

   ; Use exponents to avoid colorbars with ranges 0.0000-0.0000
   if ( Max(Strat) lt 0.0001 ) then begin
      Format = '(e13.3)'
   endif else if ( Max(Strat) gt 10000.0 ) then begin
      Format = '(f13.0)'
   endif else begin
      Format = '(f13.4)'
   endelse

   ; Plot data w/ country boundaries
   TvPlot, Strat, Lats, YStrat,                               $
           /Cbar,         Division=Div,       /Sample,       $
           Title=Title,   CBFormat=Format, Unit=Unit,     $
           /XStyle,       XTickV=XTickV,      XTicks=XTicks, $
           XMinor=XMinor, XTitle=Xtitle,      YTitle=YTitle, $
           _EXTRA=e

end

;------------------------------------------------------------------------------

pro Strat, File, Lons, Tau, Tracers, Version, $
          Do_FullChem=Do_FullChem,  PS=PS,     $
          OutFileName=OutFileName, _EXTRA=e
   
   ;====================================================================
   ; Initialization
   ;====================================================================
   
   ; External functions
   FORWARD_FUNCTION ChkStru, ColorBar_NDiv, Extract_FileName

   ; Arguments
   if ( N_Elements( File    ) ne 1 ) then Message, 'Invalid FILES!'
   if ( N_Elements( Lons    ) ne 1 ) then Message, 'Invalid LONS'
   if ( N_Elements( Tau     ) ne 1 ) then Message, 'Invalid TAUS!'
   if ( N_Elements( Version ) ne 1 ) then Message, 'Invalid VERSIONS!'

   ; Keywords
   Do_FullChem = Keyword_Set( Do_FullChem )
   if ( N_Elements( OutFileName ) ne 1 ) then OutFileName = 'strat.ps'

   ; Title for the top of the plot
   TopTitle = 'GEOS-Chem ' + Version            + $
      ' Stratospheric zonal mean and curtain at 180E !C!C' + Extract_FileName( File )

   ; Save original color table information
   TvLct, R, G, B, /Get

   ; Save the original settings of the !MYCT sysvar
   if ( ChkStru( !MYCT ) ) then Myct_Orig = !MYCT

   ; Load modified spectrum, extended to 12 colors
   MyCt, /ModSpec, NColors=12

   ;====================================================================
   ; Read data from the files
   ;====================================================================
   
   ; Read transported tracers
   CTM_Get_Data, DataInfo, 'IJ-AVG-$', $
      File=File, Tau0=Tau, Tracer=Tracers, /Quiet

   ; Read OH and append to DATAINFO
   if ( Do_FullChem ) then begin
      CTM_Get_Data, TmpDataInfo, 'CHEM-L=$', $
                    File=File, Tau0=Taus, Tracer=1, /Quiet
      DataInfo = [ DataInfo, TmpDataInfo ]
      UnDefine, TmpDataInfo

      ; Read HO2 data
      CTM_Get_Data, TmpDataInfo, 'CHEM-L=$', $
                    File=File, Tau0=Taus, Tracer=3, /Quiet
      DataInfo = [ DataInfo, TmpDataInfo ]
      UnDefine, TmpDataInfo
   endif

   ;====================================================================
   ; Read data from the files
   ;====================================================================

   ; Number of rows & columns on the plot
   Rows = 3
   Cols = 2

   ; Use Postscript font
   !p.font = 0

   ; Open the plot device and initialize the page
   Open_Device, /Color, Bits=8, /Portrait, PS=PS, File=OutFileName, _EXTRA=e
 
   ; Multiple panels per page
   MultiPanel, Rows=Rows, Cols=Cols, Margin=[ 0.03, 0.03, 0.03, 0.03 ]

   ; Loop over all data blocks
   for D = 0L, N_Elements( DataInfo )-1L do begin
   
      ;-----------------------------------------------------------------
      ; Extract data 
      ;-----------------------------------------------------------------

      ; Get MODELINFO and GRIDINFO structures
      GetModelAndGridInfo, DataInfo[D], ModelInfo, GridInfo

      ; Get tracername and unit strings
      TracerName = DataInfo[D].TracerName
      Unit       = DataInfo[D].Unit
      
      ; Get data array
      Data       = *( DataInfo[D].Data )

      ;--------------------------------------------------------------
      ; Prior to 5/29/13:
      ; Can't seem to find the MEAN2 function in IDL, which may
      ; be part of the JHU astro library.  Reverse-engineer it
      ; with a simple command here. (bmy, 5/29/13)
      ;Data_Zonal   = Mean2( Data, 1 )
      ;--------------------------------------------------------------

      ; Get dimensions of data
      SData      = Size( Data, /Dim )

      ; Split into zonal and cross section at 180E
      Data_Zonal = Total( Data, 1 ) / Float( SData[0] )
      Data_Lon   = Data[ Lons, *, * ]

      ; We no longer need the large data araray
      UnDefine, Data

      ;-----------------------------------------------------------------
      ; Plot the data!
      ;-----------------------------------------------------------------      

      ; Plot the surface data
      PlotStrat, Data_Zonal, -1, TracerName, Unit, GridInfo, _EXTRA=e
      
      ; Plot the 500hPa data
      PlotStrat, Data_Lon, Lons, TracerName, Unit, GridInfo, _EXTRA=e
      
      ; Plot the top title on each page  
      if ( D*2 mod ( Rows * Cols ) eq 0 ) then begin
         XYoutS, 0.5, 1.03, TopTitle, $
            /Normal, Color=!MYCT.BLACK, CharSize=1.0, Align=0.5
      endif

      ;-----------------------------------------------------------------
      ; Undefine stuff for next iteration
      ;-----------------------------------------------------------------
      UnDefine, Data_Zonal
      UnDefine, Data_Lon
      UnDefine, ModelInfo
      UnDefine, GridInfo
      UnDefine, TracerName
      UnDefine, Unit

   endfor

   ;====================================================================
   ; Cleanup & quit
   ;====================================================================

   ; Cancel previous MultiPanel Settings
   MultiPanel, /Off

   ; Close plot device
   Close_Device

   ; Restore original color table
   TvLct, R, G, B

   ; Restore !MYCT sysvar to original settings
   if ( ChkStru( Myct_Orig ) ) then !MYCT = Myct_Orig

   ; Quit
   return

end
 
