;-----------------------------------------------------------------------
;+
; NAME:
;        FULLCHEM_BUDGET
;
; PURPOSE:
;        Computes the budgets of Ox and CO from the GEOS-CHEM model.
;        for full chemistry benchmark simulations.
;
; CATEGORY:
;        Benchmarking
;
; CALLING SEQUENCE:
;        FULLCHEM_BUDGET [, Keywords ]
;
; INPUTS:
;        None
;
; KEYWORD PARAMETERS:
;        TAU0 -> Time index of the data to plot.  Units are hours 
;             since 0 GMT on 1/1/1985.  Default is 144600D (July 1, 2001).
;
;        FILENEW -> Name of a binary punch file containing model
;             output from a "New" version of the GEOS-CHEM. 
;             If omitted, a dialog box will prompt the user to
;             supply a file name.   
;
;        OUTFILENAME -> Name of the file where budget information
;             will be written to.  Default is "(VERSION).budget.fullchem", 
;             where VERSION is the version number contained w/in
;             FILENEW.
;
;        INITIALFILE -> Name of a binary file containing the mass of
;             Ox [in kg] at the start of a GEOS-CHEM model run.
;
;        FINALFILE -> Name of a binary file containing the mass of
;             Ox [in kg] at the end of a GEOS-CHEM model run.
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        External Subroutines Required:
;        ====================================
;        OPEN_FILE     CTM_BOXSIZE (function)   
;        CTM_GET_DATA  TAU2YYMMDD  (function)
;        UNDEFINE      GETMODELANDGRIDINFO 
;
; REQUIREMENTS:
;        References routines from both GAMAP and TOOLS packages.
;
; NOTES:
;        (1) Assumes the version # is located at the end of FILENEW.
;
;        (2) Assumes the following GEOS-CHEM diagnostics 
;            have been archived in the input files:
;            (a) ND24 ("EW-FLX-$")   (f) ND65 ("PORL-L=$")
;            (b) ND25 ("NS-FLX-$")   (g) ND66 ("DAO-3D-$")
;            (c) ND26 ("UP-FLX-$")   (h) ND68 ("BXHGHT-$")
;            (d) ND44 ("DRYD-FLX")   (i)      ("TCMASS-$") 
;            (e) ND45 ("CHEM-L=$")   (j)      ("TR-PAUSE")
;
; EXAMPLE:
;        FULLCHEM_BUDGET, TAU0=144600D,
;                         FILENEW='ctm.bpch.v5-01'
;                         INITIALFILE='fullchem.mass.initial.v5-01',  $
;                         FINALFILE='fullchem.mass.final.v5-01',      $
;                         OUTFILENAME='v5-01.budget.fullchem'
;
; MODIFICATION HISTORY:
;        bmy, 15 Aug 2002: VERSION 1.01
;                          - adapted from Isabelle Bey's "budget.pro"
;        bmy, 14 Jan 2003: VERSION 1.02
;                          - In GEOS-CHEM v5-03+, ND44 saves out tracers
;                            using the CTM tracer number 
;        bmy, 30 Oct 2003: VERSION 1.03
;                          - now call PTR_FREE to free the heap memory
;                            so that we don't run out of memory
;                          - now compute mean mass-weighted OH instead
;                            of methyl chloroform lifetime
;  ccc & bmy, 11 Aug 2010: VERSION 1.04
;                          - Updated computation of Ox budget
;        bmy, 10 Jan 2011: VERSION 1.05
;                          - Updated 200hPa level for MERRA
;        bmy, 08 Jun 2011: - Also print out MCF lifetime
;        bmy, 11 May 2012: GAMAP VERSION 2.16
;                          - Modified for GEOS-5.7.2 met
;        mps, 06 Jan 2014: - Fix bug in calculation of WetYear so that wetdep
;                            from convective updrafts is not double counted
;                            (P. Kasibhatla)
;        mps, 04 Mar 2016: - Modified for MERRA2 met
;        mps, 09 Nov 2017: - Also print out CH4 lifetime
;
;-
; Copyright (C) 2002-2010, Bob Yantosca, Harvard University
; This software is provided as is without any warranty
; whatsoever. It may be freely used, copied or distributed
; for non-commercial purposes. This copyright notice must be
; kept with any copy of this software. If this software shall
; be used commercially or sold as part of a larger package,
; please contact the author.
; Bugs and comments should be directed to bey@io.harvard.edu
; or bmy@io.harvard.edu with subject "IDL routine fullchem_budget"
;-----------------------------------------------------------------------


pro FullChem_Budget, Tau0=Tau0,               FileNew=FileNew,     $
                     InitialFile=InitialFile, FinalFile=FinalFile, $
                     OutFileName=OutFileName, _EXTRA=e

   ;====================================================================
   ; Keyword Settings / Initialization
   ;====================================================================

   ; External functions
   FORWARD_FUNCTION CTM_BoxSize, Tau2YYMMDD

   ; Extract the version number from the file name
   Ind     = StrPos( FileNew, '.', /Reverse_Search )
   Version = StrMid( FileNew, Ind+1, StrLen( FileNew ) - Ind + 1 )

   ; Keyword Settings
   if ( N_Elements( FileNew     ) ne 1 ) then Message, 'FILENEW not passed!'
   if ( N_Elements( Tau0        ) ne 1 ) then Tau0 = 144600D
   if ( N_Elements( OutFileName ) ne 1 ) $
      then OutFileName = Version + '.budget.fullchem.txt'

   ; Avogadro's number
   Avo     = 6.023d23

   ;==================================================================== 
   ; Read grid box heights, compute grid box volumes
   ;====================================================================
   CTM_Get_Data, DataInfo_N, 'BXHGHT-$', File=FileNew, Tau0=Tau0, Tracer=1
 
   ; Boxheight [m]
   BoxHt = *( DataInfo_N[0].Data )

   ; Get model & grid info
   GetModelAndGridInfo, DataInfo_N[0], ModelInfo_N, GridInfo_N

   ; Surface area [cm2]
   AreaCm2 = Ctm_BoxSize( GridInfo_N, /GEOS, /CM2 )

   ; Box volume [cm3]
   Size_N = Size( BoxHt, /Dim )
   BoxVol = FltArr( Size_N[0], Size_N[1], Size_N[2] )

   for L = 0L, Size_N[2]-1L do begin 
      BoxVol[*,*,L] = BoxHt[*,*,L] * AreaCm2[*,*] * 100e0
   endfor

   ; Undefine stuff
   UnDefine, BoxHt

   ;==================================================================== 
   ; Grid parameters
   ;====================================================================

   ; Get the model and grid information from the DATAINFO strucutre
   GetModelAndGridInfo, DataInfo_N[0], InType, InGrid

   ; Find the level nearest 200hPa, plus the LEV and LEV_t inputs
   ; for the MCF_LIFETIME routine for each model grid (bmy, 5/11/12)
   case ( InType.Name ) of 
      'GEOSFP': begin
         L200hPa = 31
         Lev_a   = [ 1, 72 ]
         Lev_t   = [ 1, 40 ]
      end

      'GEOSFP_47L': begin
         L200hPa = 31
         Lev_a   = [ 1, 47 ]
         Lev_t   = [ 1, 38 ]
      end

      'MERRA2': begin
         L200hPa = 31
         Lev_a   = [ 1, 72 ]
         Lev_t   = [ 1, 40 ]
      end

      'MERRA2_47L': begin
         L200hPa = 31
         Lev_a   = [ 1, 47 ]
         Lev_t   = [ 1, 38 ]
      end

      'MERRA': begin
         L200hPa = 31
         Lev_a   = [ 1, 72 ]
         Lev_t   = [ 1, 40 ]
      end

      'MERRA_47L': begin
         L200hPa = 31
         Lev_a   = [ 1, 47 ]
         Lev_t   = [ 1, 38 ]
      end

      'GEOS5': begin
         L200hPa = 31
         Lev_a   = [ 1, 72 ]
         Lev_t   = [ 1, 40 ]
      end

      'GEOS5_47L': begin
         L200hPa = 31
         Lev_a   = [ 1, 47 ]
         Lev_t   = [ 1, 38 ]
      end

      'GEOS4_30L': begin
         L200hPa = 14
         Lev_a   = [ 1, 30 ]
         Lev_t   = [ 1, 22 ]
      end

      'GEOS3_30L': begin
         L200hPa = 18
         Lev_a   = [ 1, 30 ]
         Lev_t   = [ 1, 24 ]
      end
   endcase

   ; Convert from Fortran to IDL notation
   L200hPa = L200hPa - 1L

   ;====================================================================
   ; Time quantities
   ;====================================================================

   ; Get TAU0 and TAU1
   ThisTau0 = DataInfo_N[0].Tau0 
   ThisTau1 = DataInfo_N[0].Tau1 

   ; Number of seconds and days in diagnostic interval
   Seconds  = ( ThisTau1 - ThisTau0 ) * 3600D0
   Days     = Seconds / 86400D0

   ; Scaling factor to [1/year]
   ; NOTE: benchmark run is done in a non-leap-year (2001)
   OneYear  = 365d0 / Days

   ; Undefine stuff
   UnDefine, DataInfo_N

   ;====================================================================
   ; Dry deposition of Ox = (O3 + NO2 + 2*NO3) + PAN + PPN + PMN 
   ;==================================================================== 

   ; Drydep flux tracers: NO2  O3  PAN  HNO3  PMN  PPN
   Tracer = [              64,  2,   3,    7,  15,  16 ]

   ; Read all drydep tracers
   CTM_Get_Data, DataInfo_N, 'DRYD-FLX', File=FileNew, Tau0=Tau0, Tracer=Tracer

   ; Sum drydep fluxes from each tracer
   for D = 0L, N_Elements( DataInfo_N ) - 1L do begin

      ; Convert drydep fluxes from [molec/cm2/s] to [kg/s]
      DryD = *(  DataInfo_N[D].Data )
      DryD = ( DryD * AreaCm2 ) * ( 48d-3 / Avo ) 

      ; Sum fluxes from all drydep tracers into DRYDT
      if ( D eq 0 )               $ 
         then DrydT = DryD        $ 
         else DrydT = DrydT + DryD

   endfor

   ; Compute total drydep loss in [Tg] and [Tg/year]
   Dry     = ( Total( DrydT ) ) * Seconds / 1d9
   DryYear = Dry * OneYear
 
   ; Undefine stuff
   UnDefine, DataInfo_N
   UnDefine, Dryd
   UnDefine, DrydT

   ;====================================================================
   ; Wet deposition and wet scavenging of HNO3 
   ; Convert to Ox by multiplying by the ratio of mol wts
   ;==================================================================== 

   ; Wetdep tracers: HNO3 
   Tracer = [           7 ]

   ;------------------------------------------------
   ; Wet scavenging of HNO3 in convective updrafts
   ;------------------------------------------------
   CTM_Get_Data, DataInfo_N, 'WETDCV-$', File=FileNew, Tau0=Tau0, Tracer=Tracer

   ; Convert from [kg/s] to [Tg] and [Tg/year]
   Tmp     = *( DataInfo_N.Data )
   Wet     = Total( Tmp ) * ( Seconds / 1d9 ) * ( 48D / 63D )
   WetYear = Wet * OneYear
   
   ; Undefine stuff
   UnDefine, DataInfo_N
   UnDefine, Tmp
   
   ;------------------------------------------------
   ; Wet deposition of HNO3 in for conv & LS clouds
   ;------------------------------------------------
   CTM_Get_Data, DataInfo_N, 'WETDLS-$', File=FileNew, Tau0=Tau0, Tracer=Tracer

   ; Convert from [kg/s] to [Tg] and [Tg/year]
   Tmp     = *( DataInfo_N.Data )
   Wet     = Wet + ( Total( Tmp ) * ( Seconds / 1d9 ) * ( 48D / 63D ) )
   WetYear = Wet * OneYear

   ; Undefine stuff
   UnDefine, DataInfo_N
   UnDefine, Tmp

   ;====================================================================
   ; Get mean tropopause level
   ;====================================================================
   ; Mean tropopause level
   CTM_Get_Data, DataInfo_N, 'TR-PAUSE', File=FileNew, Tracer=1, /First
   LPause  = *( DataInfo_N[0].Data )
   LPause  = round(LPause)

   ;====================================================================
   ; Chemical production & loss of Ox
   ;====================================================================
   CTM_Get_Data, DataInfo_N, 'PORL-L=$', File=FileNew, Tau0=Tau0, Tracer=[1,2]
   
   ;--------------
   ; P(Ox)
   ;--------------
   Prod    = *( DataInfo_N[0].Data )
   Size_N  = Size( Prod, /Dim )
   
   ; Convert from [molec/cm3/s] to [kg/s]
   Prod    = ( Prod * BoxVol[*,*,0:Size_N[2]-1L] ) * ( 48d-3 / Avo )

   ; Initialize
   POx     = 0D

   for J = 0L, GridInfo_N.JMX - 1L do begin
   for I = 0L, GridInfo_N.IMX - 1L do begin
   
      ; First tropopause level
      Ltrop = LPause[I, J] - 1L

      ; Compute total tropospheric P(Ox) in [kg]
      POx = POx + Total( Prod[ I, J, 0:LTrop-1] ) * Seconds / 1d9

   endfor
   endfor

   ; Compute total tropospheric P(Ox) in [Tg/year]
   POxYear = POx * OneYear
   
   ;--------------
   ; L(Ox)
   ;--------------
   Loss    = *( DataInfo_N[1].Data )
   Size_N  = Size( Loss, /Dim )
   
   ; Convert [molec/cm3/s] to [kg/s]
   Loss    = ( Loss * BoxVol[*,*,0:Size_N[2]-1L] ) * ( 48d-3 / Avo )
   
   ; Initialize
   LOx     = 0D

   for J = 0L, GridInfo_N.JMX - 1L do begin
   for I = 0L, GridInfo_N.IMX - 1L do begin

      ; First tropopause level
      Ltrop = LPause[I, J] - 1L

      ; Compute total tropospheric L(Ox) in [kg]
      LOx = LOx + Total( Loss[ I, J, 0:LTrop-1] ) * Seconds / 1d9

   endfor
   endfor

   ; Compute total tropospheric L(O3) in [Tg/year]
   LOxYear = LOx * OneYear
   
   ;--------------
   ; Net P-L
   ;--------------
   NetOx     = POx     - LOx
   NetOxYear = POXYear - LOxYear
   
   ; Undefine stuff
   UnDefine, DataInfo_N
   UnDefine, Prod
   UnDefine, Loss
   UnDefine, Size_N

   ;====================================================================
   ; Get initial and final masses of O3
   ;====================================================================

   ; Initial O3 mass [kg]
   CTM_Get_Data, DataInfo_N, 'TCMASS-$', File=InitialFile, Tracer=2, /First
   InitOx  = *( DataInfo_N[0].Data )
   UnDefine, DataInfo_N
   
   ; Final O3 mass [kg]
   CTM_Get_Data, DataInfo_N, 'TCMASS-$', File=FinalFile, Tracer=2, /First
   FinalOx = *( DataInfo_N[0].Data )
   UnDefine, DataInfo_N
   
   ; Summing variables
   TotInitOx  = 0D
   TotFinalOx = 0D

   for J = 0L, GridInfo_N.JMX - 1L do begin
   for I = 0L, GridInfo_N.IMX - 1L do begin
   
      ; First tropopause level (convert to IDL notation)
      Ltrop      = LPause[I, J] - 1L
      
      ; Sum of Ox mass below tropopause, start & end
      TotInitOx  = TotInitOx  + Total( InitOx[  I, J, 0:LTrop-1] )
      TotFinalOx = TotFinalOx + Total( FinalOx[ I, J, 0:LTrop-1] )
   endfor
   endfor

   ; Convert from [kg] to [Tg]
   TotInitOx  = TotInitOx  / 1d9
   TotFinalOx = TotFinalOx / 1d9

   ;====================================================================
   ; Stratospheric flux of Ox
   ;====================================================================

   ; Calculate stratospheric Ox here as residual source in [Tg]
   StOx     = ( TotFinalOx - TotInitOx ) - ( NetOx - Dry - Wet )
   StOxYear = StOx * OneYear
   
   ;====================================================================
   ; Chemical production & loss of CO
   ;====================================================================
   CTM_Get_Data, DataInfo_N, 'PORL-L=$', File=FileNew, Tau0=Tau0, Tracer=[3,4]
   
   ;--------------
   ; P(CO)
   ;--------------
   Prod      = *( DataInfo_N[0].Data )
   Size_N    = Size( Prod, /Dim )
   
   ; Convert from [molec/cm3/s] to [kg/s]
   Prod      = ( Prod * BoxVol[*,*,0:Size_N[2]-1L] ) * ( 28d-3 / Avo )
   
   ; Initialize
   PCO   = 0D

   for J = 0L, GridInfo_N.JMX - 1L do begin
   for I = 0L, GridInfo_N.IMX - 1L do begin

      ; First tropopause level
      Ltrop = LPause[I, J] - 1L

      ; Compute total tropospheric P(CO) in [kg]
      PCO = PCO + Total( Prod[ I, J, 0:LTrop-1] ) * Seconds / 1d9

   endfor
   endfor

   ; Compute total tropospheric P(CO) in [kg/year]
   PCOYear = PCO * OneYear
   
   ;--------------
   ; L(CO)
   ;--------------
   Loss      = *( DataInfo_N[1].Data )
   Size_N    = Size( Loss, /Dim )
   
   ; Convert [molec/cm3/s] to [kg/s]
   Loss      = ( Loss * BoxVol[*,*,0:Size_N[2]-1L] ) * ( 28d-3 / Avo )
   
   ; Initialize
   LCO   = 0D

   for J = 0L, GridInfo_N.JMX - 1L do begin
   for I = 0L, GridInfo_N.IMX - 1L do begin

      ; First tropopause level
      Ltrop = LPause[I, J] - 1L

      ; Compute total tropospheric L(CO) in [kg]
      LCO = LCO + Total( Loss[ I, J, 0:LTrop-1] ) * Seconds / 1d9

   endfor
   endfor

   ; Compute total tropospheric L(CO) in [kg/year]
   LCOYear = LCO * OneYear
   
   ;--------------
   ; Net P-L
   ;--------------
   NetCO     = PCO     - LCO
   NetCOYear = PCOYear - LCOYear
   
   ; Undefine stuff
   UnDefine, DataInfo_N
   UnDefine, Prod
   UnDefine, Loss
   UnDefine, Size_N

   ;====================================================================
   ; Mean OH lifetime
   ;====================================================================

   ; Get the air mass
   CTM_Get_Data, DataInfo_A, 'BXHGHT-$', Tracer=2, File=FileNew
   AirMass = *( DataInfo_A[0].Data )
   UnDefine, DataInfo_A
   
   ; Get the OH concentration
   CTM_Get_Data, DataInfo_O, 'CHEM-L=$', Tracer=1, File=FileNew
   OH = *( DataInfo_O[0].Data )
   UnDefine, DataInfo_O
      
   ; Sum mass-weighted OH and airmass below the tropopause
   TotOH   = 0d0
   TotMass = 0d0
     
   ; Loop over all boxes
   for L = 0L, GridInfo_N.LMX-1L do begin
   for J = 0L, GridInfo_N.JMX-1L do begin
   for I = 0L, GridInfo_N.IMX-1L do begin
         
      ; Only process tropospheric boxes
      ; LPAUSE-2L means that we have to subtract since LPAUSE-1
      ; is the level where the tropopause occurs, and then we have
      ; to subtract another 1 to be in IDL notation (start from zero)
      if ( L le LPause[I,J]-2L ) then begin
         TotOH   = TotOH   + ( OH[I,J,L] * AirMass[I,J,L] )
         TotMass = TotMass + AirMass[I,J,L]
      endif
   endfor
   endfor
   endfor
      
   ; Compute Mean OH [1e5 molec OH/cm3]
   MeanOH = ( TotOH / TotMass ) / 1d5

   ; Cleanup
   UnDefine, OH
   UnDefine, AirMass
   UnDefine, TotOH
   UnDefine, TotMass

   ;====================================================================
   ; Compute methyl chloroform lifetime (offline)
   ;==================================================================== 
   
   ; Get MCF lifetime (years), computed from monthly mean fields.
   MCF_Life = MCF_LifeTime( FileNew,              $
                            Lon   = [-180, 180],  $
                            Lat   = [-90, 90],    $
                            Lev_a = Lev_a,        $
                            Lev_t = Lev_t,        $
                            Tau0  = Tau0,         $
                            _EXTRA=e            )

   ;====================================================================
   ; Compute methane lifetime (offline)
   ;==================================================================== 
   
   ; Get CH4 lifetime (years), computed from monthly mean fields.
   CH4_Life = CH4_LifeTime( FileNew,              $
                            Lon   = [-180, 180],  $
                            Lat   = [-90, 90],    $
                            Lev_a = Lev_a,        $
                            Lev_t = Lev_t,        $
                            Tau0  = Tau0,         $
                            _EXTRA=e            )

   ;====================================================================
   ; Write to file
   ;==================================================================== 

   ; Compute net Ox for printing
   TotOx     = NetOx     + StOx      - Dry     - Wet
   TotOxYear = NetOxYear + StOxYear  - DryYear - WetYear
   DeltaOx   = TotFinalOx - TotInitOx

   ; Open file
   Open_File, OutFileName, Ilun, /Get_LUN, /Write

   ; Compute start & end dates for the file
   Result = Tau2YYMMDD( ThisTau0, /NFormat )
   Nymd0  = Result[0]
   Nhms0  = Result[1] / 10000L
   Result = Tau2YYMMDD( ThisTau1, /NFormat )
   Nymd1  = Result[0]
   Nhms1  = Result[1] / 10000L

   ; Format strings
   Fs = '(''Start time: '', i2.2, '' GMT on '', i8.8 )'
   Fe = '(''End   time: '', i2.2, '' GMT on '', i8.8 )'
   F0 = '(a20, f12.3, 3x, f12.3 )'
   F1 = '(a20, f12.3 )'
   F2 = '(a20, f11.4, ''  [1e5 molec OH/cm3]'' )'
   F3 = '(a20, f11.4, ''  [years]'' )'

   ; Header string
   S = '======= GEOS-CHEM ' + Version + ' Full Chemistry Benchmark ======='

   ; Print values
   PrintF, Ilun, S
   PrintF, Ilun
   PrintF, Ilun, Nhms0, Nymd0, Format=Fs
   PrintF, Ilun, Nhms1, Nymd1, Format=Fe
   PrintF, Ilun
   PrintF, Ilun, '====================== Ox Budget ======================'
   PrintF, Ilun
   PrintF, Ilun, ' MASS ACCUMULATION         Tg Ox       Tg Ox/yr'
   PrintF, Ilun, ' -----------------         -----       --------'
   PrintF, Ilun
   PrintF, Ilun, 'Initial Mass(Ox): ',    TotInitOx,          Format=F1
   PrintF, Ilun, 'Final Mass(Ox): ',      TotFinalOx,         Format=F1
   PrintF, Ilun, '                      ----------'
   PrintF, Ilun, 'Final - Initial: ',     DeltaOx,            Format=F1
   PrintF, Ilun
   PrintF, Ilun
   PrintF, Ilun, ' SOURCES AND SINKS         Tg Ox       Tg Ox/yr'
   PrintF, Ilun, ' -----------------         -----       --------'
   PrintF, Ilun                           
   PrintF, Ilun, '* Dry Deposition'
   PrintF, Ilun, 'Total Drydep(Ox): ',    Dry,    DryYear,    Format=F0
   PrintF, Ilun 
   PrintF, Ilun, '* Wet Deposition'
   PrintF, Ilun, 'Total WetDep(Ox): ',    Wet,    WetYear,    Format=F0
   PrintF, Ilun 
   PrintF, Ilun, '* Chemistry'
   PrintF, Ilun, 'Total P(Ox): ',         POx,    POxYear,    Format=F0
   PrintF, Ilun, 'Total L(Ox): ',         LOx,    LOxYear,    Format=F0
   PrintF, Ilun, '                      ----------     ----------'
   PrintF, Ilun, 'Net P(Ox)-L(Ox): ',     NetOx,  NetOxYear,  Format=F0
   PrintF, Ilun
   PrintF, Ilun, '* Dynamics'
   PrintF, Ilun, 'Strat Flux(Ox): ',      StOx,   StOxYear,   Format=F0
   PrintF, Ilun
   PrintF, Ilun, '* DELTA Ox            ----------     ----------'
   PrintF, Ilun, 'Chem+Dyn-Dry-Wet: ',    TotOx,  TotOxYear,  Format=F0
   PrintF, Ilun
   PrintF, Ilun
   PrintF, Ilun, '====================== CO Budget ======================'
   PrintF, Ilun
   PrintF, Ilun, ' SOURCES AND SINKS         Tg CO       Tg CO/yr'
   PrintF, Ilun, ' -----------------         -----       --------'
   PrintF, Ilun
   PrintF, Ilun, '* Chemistry'
   PrintF, Ilun, 'Total P(CO): ',         PCO,    PCOYear,    Format=F0
   PrintF, Ilun, 'Total L(CO): ',         LCO,    LCOYear,    Format=F0
   PrintF, Ilun, '                      ----------     ----------'
   PrintF, Ilun, 'Net P(CO)-L(CO): ',     NetCO,  NetCOYear,  Format=F0
   PrintF, Ilun
   PrintF, Ilun
   PrintF, Ilun, '======================  OH Sink  ======================'
   PrintF, Ilun
   PrintF, Ilun, 'Mean OH           :',   MeanOH,             Format=F2
   PrintF, Ilun, 'T(MCF) wrt trop OH:',   MCF_Life,           Format=F3
   PrintF, Ilun, 'T(CH4) wrt trop OH:',   CH4_Life,           Format=F3

   ; Close file
   Close,    Ilun
   Free_LUN, Ilun

   ; Undefine stuff
   UnDefine, ModelInfo_N
   UnDefine, GridInfo_N

   ; Quit
   return
end
