;-----------------------------------------------------------------------
;+
; NAME:
;        PROFILES
;
; PURPOSE:
;        Creates longitudinal difference profiles of tracers along 
;        15S latitude and 42N latitude.
;
; CATEGORY:
;        Benchmarking
;
; CALLING SEQUENCE:
;        PROFILES, FILES, ALTRANGE, TAUS, TRACERS, VERSIONS, [, Keywords ]
;
; INPUTS:
;        FILES -> A 2-element vector containing the names of files
;             from the "old" and "new" GEOS-Chem model versions
;             that are to be compared. 
;
;        ALTRANGE -> A 2-element vector containing the altitude range
;             (in km) of the data to be plotted.  ALTRANGE will be 
;             passed to CTM_EXTRACT.  
;
;        TAUS -> A 2-element vector contaning TAU values (hours GMT
;             from /1/1985) corresponding to the "old" and "new"
;             GEOS-Chem model versions.
;
;        TRACERS -> The list of transported tracers (i.e. diagnostic
;             category "IJ-AVG-$").
;
;        VERSIONS -> A 2-element vector containing the version
;             numbers for the "old" and "new" GEOS-Chem model
;             versions.
;
; KEYWORD PARAMETERS:
;        /DO_FULLCHEM -> Set this switch to plot the chemically
;             produced OH in addition to the advected tracers.
;
;        /DYNRANGE -> Set this switch to create plots using the whole
;             dynamic range of the data.  Default is to restrict
;             the plot range to predetermined values as returned
;             by routine GET_DIFF_RANGE.
;
;        /PS -> Set this switch to generate PostScript output.
;
;        OUTFILENAME -> If /PS is set, will write PostScript output 
;             to a file whose name is specified by this keyword.
;             Default is "tracer_ratio.pro".
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        Internal Subroutines Included:
;        =======================================================
;        PlotProfile
;
;        External Subroutines Required:
;        =======================================================
;        CLOSE_DEVICE                  COLORBAR_NDIV (function)   
;        CTM_EXTRACT      (function)   CTM_GET_DATA 
;        GET_DIFF_RANGE   (function)   GETMODELANDGRIDINFO   
;        EXTRACT_FILENAME (function)   MULTIPANEL                 
;        CHKSTRU          (function)   MYCT                          
;        OPEN_DEVICE                   TVMAP                         
;        UNDEFINE   
;     
; REQUIREMENTS:
;        References routines from the GAMAP package.
;        
; NOTES:
;        (1) Meant to be called from BENCHMARK_1MON.
;
; EXAMPLE:
;        FILES    = [ 'ctm.bpch.v7-04-10', 'ctm.bpch.v7-04-11' ]
;        ALTRANGE = [ 0, 20 ]
;        TAUS     = [ NYMD2TAU( 20010701 ), NYMD2TAU( 20010701 ) ]
;        TRACERS  = INDGEN( 43 ) + 1
;        VERSIONS = [ 'v7-04-10', 'v7-04-11' ]
;
;        PROFILES, FILES, ALTRANGE, TAUS, TRACERS, VERSIONS, $
;             /DO_FULLCHEM, /PS, OUTFILENAME='myplot.ps'
;
;             ; Creates ratio plots of two GEOS-CHEM versions
;             ; (in this case v7-04-11 / v7-04-10) for July 2001.
;             ; Output is sent to PostScript file "myplot.ps".
;             ; The min and max of the data on each plot panel is 
;             ; restricted to pre-defined values returned by
;             ; function GET_DIFF_RANGE.
;             
;        PROFILES, FILES, ALTRANGE, TAUS, TRACERS, VERSIONS, $
;             /DYNRANGE, /PS, OUTFILENAME='myplot.ps'
;
;             ; Same as the above example, but the min & max of 
;             ; each plot panel corresponds to the dynamic range
;             ; of the data (centered around zero)
;
; MODIFICATION HISTORY:
;        bmy, 14 Nov 2007: VERSION 1.01
;                          - Initial version
;        bmy, 20 Nov 2007: VERSION 1.02
;                          - Now draw out-of-bounds triangles for
;                            the colorbar when using the "small"
;                            data ranges.  New feature of TVMAP.
;        bmy, 08 Feb 2011: VERSION 1.03
;                          - Now display in the top-of-plot title
;                            if the dynamic range option is used.
;        bmy, 08 Jun 2011: VERSION 1.04
;                          - Added /DO_FULLCHEM keyword
;                          - Now call COLORBAR with the UPOS keyword 
;                            to place the colorbar unit string properly
;                          - Now use appropriate settings for creating
;                            plots w/ the full dynamic range (/DYNRANGE)
;                          - Now restore !MYCT sysvar to previous
;                            settings upon exiting the program
;                          - Better adjust colorbar position for /PS
;        bmy, 11 May 2012: GAMAP VERSION 2.16
;                          - Modify the error check to allow
;                            comparison of equivalent vertical grids
;                            (e.g. GEOS-5, MERRA, GEOS-5.7) even if
;                            the model names differ
;        mps, 04 Mar 2016: - Include MERRA2 in the check for equivalent
;                            vertical grids
; 
;-
; Copyright (C) 2007-2012,
; Bob Yantosca and Philippe Le Sager, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.as.harvard.edu with subject "IDL routine differences"
;-----------------------------------------------------------------------

pro PlotProfile, Data1, Data2,    TracerName, GridInfo, $
                 Unit,  DynRange, S15=S15,    N42=N42,  $
                 PS=PS, _EXTRA=e
   
   ;====================================================================
   ; Internal routine PLOTPROFILE plots either the surface or 500 hPa
   ; ratio of tracer between old and new versions (bmy, 11/9/07)
   ;====================================================================

   ; Plot title for 15S
   if ( Keyword_Set( S15 ) ) $
      then Title = 'Difference along 15S - ' + TracerName

   ; Plot title for 42N
   if ( Keyword_Set( N42 ) ) $
      then Title = 'Difference along 42N - ' + TracerName  

   ; Number of colorbar tickmarks
   Divisions = ColorBar_NDiv( 6 )

   ; Define plot ranges
   S    = Size( Data1, /Dim )
   XMid = GridInfo.XMid
   Zmid = GridInfo.ZMid[0:S[1]-1L]

   ; Compute difference
   Diff = Data2 - Data1

   if ( DynRange ) then begin

      ;=================================================================
      ; Create plots using the full dynamic range of the data (centered
      ; around zero) if the /DYNRANGE keyword is set.
      ;=================================================================
      MinData  =  Min( Diff, Max=MaxData )
      Extreme  =  Max( [ Abs( MinData ), Abs( MaxData ) ] )
      MinData  = -Extreme
      MaxData  =  Extreme
      Triangle =  0
      NoGap    =  0
      Upos     =  1.1

      ; We need to set the colorbar a little bit lower for PostScript
      if ( Keyword_Set( PS ) )                                         $
         then CBPos = [ 0.10, -0.02, 0.90, 0.01 ]                      $
         else CBPos = [ 0.05,  0.01, 0.95, 0.04 ]

   endif else begin

      ;=================================================================
      ; Create plots using the pre-defined min & max values from
      ; function GET_DIFF_RANGE.  This is the default.
      ;=================================================================
      Range    =  Get_Diff_Range( TracerName )
      MinData  =  Range[0] 
      MaxData  =  Range[1]
      BotOut   =  !MYCT.BOTTOM
      Triangle =  1
      NoGap    =  1
      UPos     =  1.02

      ; We need to set the colorbar a little bit lower for PostScript  
      if ( Keyword_Set( PS ) )                                         $
         then CBPos = [ 0.05, -0.02, 0.95, 0.01 ]                      $
         else CBPos = [ 0.05,  0.05, 0.95, 0.08 ]          

   endelse

   ; For OH, let's rescale the unit for clarity
   if ( TracerName eq 'OH' ) then begin
      Diff    = Diff    / 1e5
      MinData = MinData / 1e5
      MaxData = MaxData / 1e5
      Unit    = '1e5 molec/cm3'
   endif

   ; X-axis varibles
   XTickV = [ -180, -120, -60, 0, 60, 120, 180 ]
   XTicks = N_Elements( XTickV )-1L
   XMinor = 6

   ; Plot profile difference
   TvPlot, Diff, XMid, ZMid,                                          $
      /Cbar,              Division=Divisions, /Sample,                $
      Title=Title,        MinData=MinData,    MaxData=MaxData,        $
      CBFormat='(f13.3)', BOR_Label=' ',      Unit=Unit,              $
      /XStyle,            XTickV=XTickV,      XTicks=XTicks,          $
      XMinor=XMinor,      XTitle='Longitude', YTitle='Altitude (km)', $
      /YStyle,            Triangle=Triangle,  NoGap=NoGap,            $
      CBPosition=CBPos,   UPos=UPos,          BotOut=BotOut,          $
      _EXTRA=e

end

;------------------------------------------------------------------------------

pro Profiles, Files, AltRange, Taus, Tracers, Versions,     $
              DynRange=Dynrange, Do_FullChem=Do_FullChem,   $
              PS=PS,             OutFileName=OutFileName,   $
              _EXTRA=e
 
   ;====================================================================
   ; Initialization
   ;====================================================================
   
   ; External functions
   FORWARD_FUNCTION ColorBar_NDiv, Get_Diff_Range, Extract_FileName

   ; Arguments
   if ( N_Elements( Files    ) ne 2 ) then Message, 'Invalid FILES!'
   if ( N_Elements( AltRange ) ne 2 ) then Message, 'Invalid ALTRANGE!'
   if ( N_Elements( Taus     ) ne 2 ) then Message, 'Invalid TAUS!'
   if ( N_Elements( Versions ) ne 2 ) then Message, 'Invalid VERSIONS!'

   ; Keywords
   Do_FullChem = Keyword_Set( Do_FullChem )
   DynRange    = Keyword_Set( DynRange    )
   if ( N_Elements( OutFileName ) ne 1 ) then OutFileName = 'profiles.ps'

   ; Title for the top of the plot
   if ( DynRange ) then begin
      TopTitle = 'GEOS-Chem '                 + Versions[1]              + $
                 ' Longitudinal Profiles (dyn range) at 15S and 42N!C!C' + $
                 Extract_FileName( Files[1] ) + ' - '                    + $
                 Extract_FileName( Files[0] )
   endif else begin
      TopTitle = 'GEOS-Chem '                 + Versions[1]              + $
                 ' Longitudinal Profiles at 15S and 42N!C!C'             + $
                 Extract_FileName( Files[1] ) + ' - '                    + $
                 Extract_FileName( Files[0] )
   endelse


   ; Save original color table
   TvLct, R, G, B, /Get

   ; Save the original settings of the !MYCT sysvar
   if ( ChkStru( !MYCT ) ) then Myct_Orig = !MYCT

   ; Load Blue-White-White-Red colortable
   MyCt, /BuWhWhRd
 
   ;====================================================================
   ; Read data from the files
   ;====================================================================
   
   ;------------------------------
   ; From FILE_1: the "old" file
   ;------------------------------

   ; Read tracers from the old file
   CTM_Get_Data, DataInfo_1, 'IJ-AVG-$', $
      File=Files[0], Tau0=Taus[0], Tracer=Tracers, /Quiet

   ; Read OH from the "new" file
   if ( Do_FullChem ) then begin
      CTM_Get_Data, DataInfo, 'CHEM-L=$', $
                    File=Files[0], Tau0=Taus[0], Tracer=1, /Quiet
      DataInfo_1 = [ DataInfo_1, DataInfo ]
      UnDefine, DataInfo
   endif

   ;------------------------------
   ; From FILE_2: the "new" file
   ;------------------------------

   ; Read tracers from the "new" file
   CTM_Get_Data, DataInfo_2, 'IJ-AVG-$', $
      File=Files[1], Tau0=Taus[1], Tracer=Tracers, /Quiet

   ; Read "OH" from the "new" file
   if ( Do_FullChem ) then begin
      CTM_Get_Data, DataInfo, 'CHEM-L=$', $
                    File=Files[1], Tau0=Taus[1], Tracer=1, /Quiet
      DataInfo_2 = [ DataInfo_2, DataInfo ]
      UnDefine, DataInfo
   endif

   ;------------------------------
   ; Error checks!
   ;------------------------------

   ; Stop if both DATAINFOs are incompatible
   if ( N_Elements( DataInfo_1 ) ne N_Elements( DataInfo_2 ) ) $
      then Message, 'Files have different numbers of tracers in them!'

   ;====================================================================
   ; Process data and create difference plots!
   ;====================================================================

   ; Number of rows & colums on the plot
   Rows = 3
   Cols = 2

   ; Use Postscript font
   !p.font = 0

   ; Open the plot device and initialize the page
   Open_Device, /Color, Bits=8, /Portrait, PS=PS, File=OutFileName, _EXTRA=e

   ; Multiple panels per page
   MultiPanel, Rows=Rows, Cols=Cols, Margin=[ 0.07, 0.07, 0.07, 0.07 ]

   ; Loop over the # of categories
   for D = 0L, N_Elements( DataInfo_1 )-1L do begin
      
      ;-----------------------------------------------------------------
      ; Error check grid, tracer name, and data block sizes 
      ;-----------------------------------------------------------------

      ; Get MODELINFO and GRIDINFO structures
      GetModelAndGridInfo, DataInfo_1[D], ModelInfo_1, GridInfo_1
      GetModelAndGridInfo, DataInfo_2[D], ModelInfo_2, GridInfo_2
      
      ; Can't plot if the vertical grids are different!
      if ( Modelinfo_1.Name ne ModelInfo_2.Name ) then begin
         
         ; First determine if both grids are equivalent even if the
         ; model names differ (e.g. GEOS-5.2, MERRA, GEOS-5.7)
         Models = [ Modelinfo_1.Name, ModelInfo_2.Name ]
         F0     = ( Where( Models eq 'GEOS5_47L'  ) ge 0 )
         F1     = ( Where( Models eq 'MERRA_47L'  ) ge 0 )
         F2     = ( Where( Models eq 'GEOSFP_47L' ) ge 0 )
         F3     = ( Where( Models eq 'MERRA2_47L' ) ge 0 )

         ; If both grids are not equivalent, then stop w/ error message
         if ( ( F0 + F1 + F2 + F3 ) ne 2 ) then  begin

            ; Now check models using native grid
            F4     = ( Where( Models eq 'GEOS5'  ) ge 0 )
            F5     = ( Where( Models eq 'MERRA'  ) ge 0 )
            F6     = ( Where( Models eq 'GEOSFP' ) ge 0 )
            F7     = ( Where( Models eq 'MERRA2' ) ge 0 )

            if ( ( F4 + F5 + F6 + F7 ) ne 2 ) then  begin
               Message, $
               'The two models have different vertical grids!  Cannot plot!'
            endif
         endif
      endif

      ; Make sure grids are compatible
      if ( ( GridInfo_1.IMX   ne GridInfo_2.IMX   ) OR $
           ( GridInfo_1.JMX   ne GridInfo_2.JMX   ) OR $
           ( GridInfo_1.LMX   ne GridInfo_2.LMX   ) )  $
         then Message, 'Resolution mismatch!'

      ; Make sure the tracers correspond to each other
      TracerName_1 = DataInfo_1[D].TracerName
      TracerName_2 = DataInfo_2[D].TracerName
      if ( TracerName_1 ne TracerName_2 ) then Message, 'Tracer mismatch!'

      ; Get full-sized data arrays
      Data_1 = *( DataInfo_1[D].Data )
      Data_2 = *( DataInfo_2[D].Data )

      ; Get the dimensions of the data arrays
      Size_1 = Size( Data_1, /Dim )
      Size_2 = Size( Data_2, /Dim )

      ; Stop the run if the data block sizes don't agree
      if ( Size_1[0] ne Size_2[0] ) then Message, 'Longitude mismatch!'
      if ( Size_1[1] ne Size_2[1] ) then Message, 'Latitude mismatch!'

      ; Get unit of data
      Unit = DataInfo_1[D].Unit

      ;-----------------------------------------------------------------
      ; Extract data arrays for surface and 500 hPa
      ;-----------------------------------------------------------------

      ; Extract data along 15S -- 1st model
      Data_15S_1 = CTM_Extract( Data_1,                                     $
                                ModelInfo=ModelInfo_1, GridInfo=GridInfo_1, $
                                Lon=[-180,180], Lat=-15, Alt=AltRange )

      ; Extract data along 15S -- 2nd model
      Data_15S_2 = CTM_Extract( Data_2,                                     $
                                ModelInfo=ModelInfo_1, GridInfo=GridInfo_1, $
                                Lon=[-180,180], Lat=-15, Alt=AltRange )

      ; Extract data along 42N -- 1st model
      Data_42N_1 = CTM_Extract( Data_1,                                     $
                                ModelInfo=ModelInfo_1, GridInfo=GridInfo_1, $
                                Lon=[-180,180], Lat=42,  Alt=AltRange )

      ; Extract data along 42N -- 2nd model
      Data_42N_2 = CTM_Extract( Data_2,                                     $
                                ModelInfo=ModelInfo_1, GridInfo=GridInfo_1, $
                                Lon=[-180,180], Lat=42,  Alt=AltRange )

      ; We no longer need the 3-D data arrays
      UnDefine, Data_1
      UnDefine, Data_2

      ;-----------------------------------------------------------------
      ; Plot the data!
      ;-----------------------------------------------------------------

      ; Profile along 15S
      PlotProfile, Data_15S_1, Data_15S_2, TracerName_1, GridInfo_1, $
                   Unit,       DynRange,   /S15,         PS=PS,      $
                   _EXTRA=e

      ; Profile along 42N
      PlotProfile, Data_42N_1, Data_42N_2, TracerName_1, GridInfo_1, $
                   Unit,       DynRange,   /N42,         PS=PS,      $
                   _EXTRA=e

      ; Plot the top title on each page  
      if ( D*2 mod ( Rows * Cols ) eq 0 ) then begin
         XYoutS, 0.5, 1.03, TopTitle, $
            /Normal, Color=!MYCT.BLACK, CharSize=1.0, Align=0.5
      endif

      ;-----------------------------------------------------------------
      ; Undefine stuff for next iteration
      ;-----------------------------------------------------------------
      UnDefine, Data_15S_1
      UnDefine, Data_15S_2
      UnDefine, Data_42N_1      
      UnDefine, Data_42N_1
      UnDefine, GridInfo_1
      UnDefine, GridInfo_2
      UnDefine, ModelInfo_1
      UnDefine, ModelInfo_2
      UnDefine, Size_1
      UnDefine, Size_2
      UnDefine, TracerName_1
      UnDefine, TracerName_2
      
   endfor

   ;====================================================================
   ; Cleanup and quit
   ;====================================================================

   ; Cancel previous MULTIPANEL settings
   MultiPanel, /Off

   ; Close plot device
   Close_Device

   ; Restore original color table
   TvLct, R, G, B

   ; Restore !MYCT sysvar to original settings
   if ( ChkStru( Myct_Orig ) ) then !MYCT = Myct_Orig

   ; Quit
   return
end
 
