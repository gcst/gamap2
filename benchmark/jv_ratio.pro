;-----------------------------------------------------------------------
;+
; NAME:
;        JV_RATIO
;
; PURPOSE:
;        Creates ratio (new/old) plots of J-values at both the surface
;        and 500 hPa. 
;       
; CATEGORY:
;        Benchmarking
;
; CALLING SEQUENCE:
;        JV_RATIO, FILES, LEVELS, TAUS, VERSIONS, [, Keywords ]
;
; INPUTS:
;        FILES -> A 2-element vector containing the names of files
;             from the "old" and "new" GEOS-Chem model versions
;             that are to be compared. 
;
;        LEVELS -> A 4-element vector containing the level indices
;             for the GEOS-Chem surface layer and 500 hPa layer.
;             for both models (e.g. SFC_1, SFC_2, 500_1, 500_2).
;             NOTE: This is in Fortran notation (starting from 1!)
;
;        TAUS -> A 2-element vector contaning TAU values (hours GMT
;             from /1/1985) corresponding to the "old" and "new"
;             GEOS-Chem model versions.
;
;        VERSIONS -> A 2-element vector containing the version
;             numbers for the "old" and "new" GEOS-Chem model
;             versions.
;
; KEYWORD PARAMETERS:
;        /DYNRANGE -> Set this switch to create plots using the whole
;             dynamic range of the data.  Default is to restrict
;             the plot range to +/- 5%.
;
;        /PS -> Set this switch to generate PostScript output.
;
;        OUTFILENAME -> If /PS is set, will write PostScript output
;             to a file whose name is specified by this keyword.
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        External Subroutines Required:
;        ==================================================
;        ComputeJVRatios       PlotJVRatios
;
;        External Subroutines Required:
;        ==================================================
;        CLOSE_DEVICE          COLORBAR_NDIV    (function)
;        CTM_GET_DATA          EXTRACT_FILENAME (function)       
;        GETMODELANDGRIDINFO   MULTIPANEL         
;        MYCT                  OPEN_DEVICE   
;        TVMAP  
;
; REQUIREMENTS:
;        References routines from the GAMAP package.
;
; NOTES:
;        (1) Meant to be called from BENCHMARK_1MON
;
; EXAMPLE:
;        FILES    = [ 'ctm.bpch.v7-04-10', 'ctm.bpch.v7-04-11' ]
;        LEVELS   = [ 1, 1, 13, 13 ]
;        TAUS     = [ NYMD2TAU( 20010701 ), NYMD2TAU( 20010701 ) ]
;        VERSIONS = [ 'v7-04-10', 'v7-04-11' ]
; 
;        JV_RATIO, FILES, LEVELS, TAUS, VERSIONS, $
;             /PS, OUTFILENAME='myplot.ps'
;
;             ; Creates J-value ratio plots [ v7-04-11 / v7-04-10 ]
;             ; for July 2001 at the surface and 500 hPa.
;             ; The max & min of the data will be fixed at +/- 5%.
;
;        JV_RATIO, FILES, LEVELS, TAUS, VERSIONS, $
;             /DYNRANGE, /PS, OUTFILENAME='myplot.ps'
;
;             ; Same as above, but this time the full dynamic
;             ; range of the data will be displayed.
;
; MODIFICATION HISTORY:
;        bmy, 14 Aug 2002: VERSION 1.01
;                          - Adapted from Isabelle Bey's "cat_plot.pro"
;                          - you may now specify different TAU values
;        bmy, 13 Sep 2002: VERSION 1.02
;                            for FILENEW and FILEOLD via the TAU0 keyword
;        bmy, 04 Oct 2004: VERSION 1.03
;                          - Remove MIN_VALID from call to TVMAP
;        bmy, 09 Nov 2007: VERSION 1.04
;                          - Modified argument list and some 
;                            internal variable names.
;        bmy, 20 Nov 2007: VERSION 1.05
;                          - Now draw out-of-bounds triangles for
;                            the colorbar when using the "small"
;                            data ranges.  New feature of TVMAP.
;        bmy, 07 May 2008: VERSION 1.06
;                          - Now allow for comparing models on 2
;                            different vertical grids.
;        bmy, 08 Feb 2011: VERSION 1.07
;                          - Now display in the top-of-plot title
;                            if the dynamic range option is used.
;        bmy, 08 Jun 2011: VERSION 1.08
;                          - Now create log plots in range 0.5 - 2.0
;                          - Added /DO_FULLCHEM keyword
;                          - Adjust colorbar so that the 0.9 - 1.1
;                            range shows up in white.
;                          - Now restore !MYCT sysvar to previous
;                            settings upon exiting the program
;  mps & bmy, 29 May 2014: GAMAP VERSION 2.18
;                          - Compatible with extra J-Value tracers
;                            in v10-01c and higher versions; 
;-
; Copyright (C) 2007-2011,
; Bob Yantosca and Philippe Le Sager, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.as.harvard.edu with subject "IDL routine ratios"
;-----------------------------------------------------------------------

pro ComputeJVRatios, Size, Data_Sfc_1, Data_Sfc_2, Ratio_Sfc, $
                           Data_500_1, Data_500_2, Ratio_500

   ;====================================================================
   ; Internal routine COMPUTEJVRATIOS computes the ratio of 
   ; "new"/"old" at both the surface level and the 500 hPa level. 
   ; (bmy, 11/9/07, 6/3/11)
   ;====================================================================

   ;----------------------
   ; %%%%% SURFACE %%%%%
   ;----------------------

   ; Compute the ratio
   Ratio_Sfc = Data_Sfc_2 / Data_Sfc_1

   ; Replace non-finite values with a missing data value
   Ind = Where( ~Finite( Ratio_Sfc ) ) 
   if ( Ind[0] ge 0 ) then Ratio_Sfc[Ind] = -9.99e30

   ;----------------------
   ; %%%%% 500 HPa %%%%%
   ;----------------------

   ; Compute the ratio
   Ratio_500 = Data_500_2 / Data_500_1

   ; Replace non-finite values with a missing data value
   Ind = Where( ~Finite( Ratio_500 ) ) 
   if ( Ind[0] ge 0 ) then Ratio_500[Ind] = -9.99e30

end

;------------------------------------------------------------------------------

pro PlotJVRatio, Data, Level, TracerName, GridInfo, DynRange, PS=PS, _EXTRA=e
   
   ;====================================================================
   ; Internal routine PLOTRATIO plots either the surface or 500 hPa
   ; ratio of tracer between old and new versions (bmy, 11/9/07)
   ;====================================================================

   ; Plot title
   if ( Level gt 1 )                                           $
      then Title = 'J-Value Ratio @ 500 hPa for ' + TracerName $
      else Title = 'J-Value Ratio @ Surface for ' + TracerName  

   ; Number of colorbar tickmarks
   Divisions = ColorBar_NDiv( 6 )
      
   ; Don't plot the polar latitudes
   XMid      = GridInfo.XMid
   YMid      = GridInfo.YMid[ 1:GridInfo.JMX-2 ]
   Data      = Data[ *, 1:GridInfo.JMX-2 ]
  
   if ( DynRange ) then begin

      ;=================================================================
      ; Plot ratio data dusing the dynamic range of the data 
      ; Center the plot range symmetrically around zero
      ;=================================================================

      MinData  = Min( Data, Max=MaxData )
      ; Comment this out -- center around 1
      ;Extreme  =  Max( [ Abs( MinData ), Abs( MaxData ) ] )
      ;MinData  = -Extreme
      ;MaxData  =  Extreme
      BotOut   = !MYCT.BOTTOM
      Triangle = 1
      NoGap    = 1
      Div      = 8

      ; We need to set the colorbar a little bit lower for PostScript
      if ( Keyword_Set( PS ) )                    $
         then CBPos = [ 0.05, 0.01, 0.95, 0.04 ] $
         else CBPos = [ 0.05, 0.05, 0.95, 0.08 ]

      ; Plot ratio over world map boundaries
      TvMap, Data, XMid, Ymid,                                       $
             /Countries,         /Coasts,         /Cbar,             $ 
             Division=Div,       /Sample,         /Grid,             $
             Title=Title,        MinData=MinData, MaxData=MaxData,   $
             CBFormat='(f13.3)', BOR_Label=' ',   Triangle=Triangle, $
             NoGap=NoGap,        BotOut=BotOut,   _EXTRA=e

   endif else begin

      ;=================================================================
      ; Plot ratio data dusing the dynamic range of the data 
      ; Center the plot range symmetrically around zero
      ;=================================================================

      ; Settings for plot
      MinData  = 0.5
      MaxData  = 2.0
      BotOut   = !MYCT.BOTTOM
      Triangle = 1
      NoGap    = 1
      Log      = 1
      Div      = 8
      Annote   = [ '0.50', '0.61', '0.74', '0.90',  $
                   '1.10', '1.34', '1.64', '2.00'  ]

      ; We need to set the colorbar a little bit lower for PostScript
      if ( Keyword_Set( PS ) )                    $
         then CBPos = [ 0.05, 0.01, 0.95, 0.04 ] $
         else CBPos = [ 0.05, 0.05, 0.95, 0.08 ]
      
      ; Plot ratio over world map
      TvMap, Data, XMid, Ymid,                                       $
             /Countries,         /Coasts,         /Cbar,             $ 
             Divisions=Div,      /Sample,         /Grid,             $
             Title=Title,        MinData=MinData, MaxData=MaxData,   $
             CBFormat='(f13.2)', BOR_Label=' ',   Triangle=Triangle, $
             CbPosition=CBPos,   NoGap=NoGap,     BotOut=BotOut,     $
             Annotation=Annote,  Log=Log,         _EXTRA=e

   endelse

end

;------------------------------------------------------------------------------

pro JV_Ratio, Files, Levels, Taus, Versions,    $
              DynRange=DynRange,       PS=PS,   $
              OutFileName=OutFileName, _EXTRA=e

   ;====================================================================
   ; Initialization
   ;====================================================================
   
   ; External functions
   FORWARD_FUNCTION ColorBar_NDiv, Extract_FileName

   ; Arguments
   if ( N_Elements( Files    ) ne 2 ) then Message, 'Invalid FILES!'
   if ( N_Elements( Levels   ) ne 4 ) then Message, 'Invalid LEVELS'
   if ( N_Elements( Taus     ) ne 2 ) then Message, 'Invalid TAUS!'
   if ( N_Elements( Versions ) ne 2 ) then Message, 'Invalid VERSIONS!'

   ; Keywords
   DynRange = Keyword_Set( DynRange )
   if ( N_Elements( OutFileName ) ne 1 ) then OutFileName = 'ratios.ps'

   ; Define title for top of page
   if ( DynRange ) then begin
      TopTitle = 'GEOS-Chem '                 + Versions[1]         + $
                 ' J-Values (dyn range) at Surface and 500 hPa!C!C' + $
                 Extract_FileName( Files[1] ) + ' / '               + $
                 Extract_FileName( Files[0] )

   endif else begin
      TopTitle = 'GEOS-Chem '                 + Versions[1]         + $
                 ' J-Values at Surface and 500 hPa!C!C'             + $
                 Extract_FileName( Files[1] ) + ' / '               + $
                 Extract_FileName( Files[0] )
   endelse

   ; Save original color table
   TvLct, R, G, B, /Get

   ; Save the original settings of the !MYCT sysvar
   if ( ChkStru( !MYCT ) ) then Myct_Orig = !MYCT

   ; This colortable will show 0.9 - 1.0 in the white, and anything
   ; outside that in the red or blue.  Use 8 colorbar divisions to line
   ; everything up properly. (bmy, 5/21/10)
   ;
   ; NOTE: It is really 0.9057 - 1.0104.  Close enough for gov't work.
   MyCt, 'RdBu', NColors=14, /MidCol, /White, /Reverse, _EXTRA=e

   ;====================================================================
   ; Read data from the files
   ;====================================================================

;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;%%% Use this for benchmarks prior to v10-01c_trop (bmy, mps, 5/29/14)
;%%%   ; Tracer list prior to v10-01c_UCX:                            
;%%%   Tracer = [  1,  2,  3,  4,  5,  6, 23, 24, 25, 26, 27, 28     ]
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
;%%% Use this to compare v10-01c_trop to v10-01c_UCX (bmy, mps, 5/29/14)
;%%%   Tracer = [  1,  2,  3,  4,  5,  6,  9, 10, 11, 12, $
;%%%              13, 14, 17, 18, 23, 24, 25, 26, 27, 28 ]
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
;%%% Tracer list for v10-01c_UCX and higher versions
;%%%   Tracer = [  1,  2,  3,  4,  5,  6,  9, 10, 11, 12, 13, 14, 15, $ 
;%%%              16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28 ]
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   ; Tracer list for v11-02c and higher versions (mps, 8/29/17)
   Tracer = [  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, $ 
              16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, $
              31, 32, 33, 34, 35, 36, 37  ]
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;   ; Tracer list for v11-02d and higher versions (mps, 10/5/17)
;   Tracer = [  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, $ 
;              16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, $
;              31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, $
;              46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, $
;              61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, $
;              76  ]

   ; Read data from "old" file
   CTM_Get_Data, DataInfo_1, 'JV-MAP-$', $
                 File = Files[0], Tau0 = Taus[0], $
                 Tracer = Tracer, /Quiet

   CTM_Get_Data, DataInfo_2, 'JV-MAP-$', $
                 File = Files[1], Tau0 = Taus[1], $
                 Tracer = Tracer, /Quiet

   ; Error check
   if ( N_Elements( DataInfo_1 ) ne N_Elements( DataInfo_2 ) ) $
      then Message, 'Files have different numbers of tracers in them!'

   ;====================================================================
   ; Process tracers
   ;====================================================================

   ; Number of plots per page
   Rows = 2
   Cols = 2

   ; Use Postscript font
   !p.font = 0

   ; Initialize the plot device
   Open_Device, /Color, Bits=8, /Portrait, PS=PS, FileName=OutFileName
 
   ; Number of panels per page
   MultiPanel, Rows=Rows, Cols=Cols, Margin=[ 0.03, 0.03, 0.03, 0.03 ]
  
   ; Loop over tracers
   for D = 0L, N_Elements( DataInfo_1 ) - 1L do begin

      ;-----------------------------------------------------------------
      ; Error check grid, tracer name, and data block sizes 
      ;-----------------------------------------------------------------

      ; Get MODELINFO and GRIDINFO structures
      GetModelAndGridInfo, DataInfo_1[D], ModelInfo_1, GridInfo_1
      GetModelAndGridInfo, DataInfo_2[D], ModelInfo_2, GridInfo_2

      ; Make sure grids are compatible
      if ( GridInfo_1.IMX ne GridInfo_2.IMX OR $
           GridInfo_1.JMX ne GridInfo_2.JMX )  $ 
         then Message, 'Resolution mismatch!'

      ; Make sure the tracers correspond to each other
      TracerName_1 = DataInfo_1[D].TracerName
      TracerName_2 = DataInfo_2[D].TracerName

      ; Debug
      ;Print, TracerName_1, ' ', TracerName_2

      if ( TracerName_1 ne TracerName_2 ) then Message, 'Tracer mismatch!'

      ; Get data arrays
      Data_1 = *( DataInfo_1[D].Data )
      Data_2 = *( DataInfo_2[D].Data )

      ; Get dimensions of new & old arrays
      Size_1 = Size( Data_1, /Dim )
      Size_2 = Size( Data_2, /Dim )

      ; Stop the run if the data block sizes don't agree
      if ( Size_1[0] ne Size_2[0] ) then Message, 'Longitude mismatch!'
      if ( Size_1[1] ne Size_2[1] ) then Message, 'Latitude mismatch!'

      ;-----------------------------------------------------------------
      ; Extract data arrays for surface and 500 hPa
      ;-----------------------------------------------------------------

      ; Surface data arrays
      Data_Sfc_1 = Data_1[ *, *, Levels[0]-1 ]
      Data_Sfc_2 = Data_2[ *, *, Levels[1]-1 ]

      ; 500 hPa data arrays
      Data_500_1 = Data_1[ *, *, Levels[2]-1 ]
      Data_500_2 = Data_2[ *, *, Levels[3]-1 ]

      ; We no longer need the 3-D data arrays
      UnDefine, Data_1
      UnDefine, Data_2

      ;-----------------------------------------------------------------
      ; Compute the ratios and plot the data
      ;-----------------------------------------------------------------

      Print, 'Creating J-value ratio maps for ', TracerName_1

      ; Get the "new" / "old" ratios at surface and 500 hPa
      ComputeJVRatios, Size_1, Data_Sfc_1, Data_Sfc_2, Ratio_Sfc, $
                               Data_500_1, Data_500_2, Ratio_500

      ; Plot the Surface ratios
      PlotJVRatio, Ratio_Sfc,  Levels[0], TracerName_1, $
                   GridInfo_1, DynRange,  PS=PS,        $
                   _EXTRA=e

      ; Plot the 500 hPa ratios
      PlotJVRatio, Ratio_500,  Levels[2], TracerName_1, $
                   GridInfo_1, DynRange,  PS=PS,        $
                   _EXTRA=e

      ;-----------------------------------------------------------------
      ; Print top title -- only plot when there is a new page
      ;-----------------------------------------------------------------

      if ( D mod ( Rows * Cols ) eq 0 ) then begin
         XYOutS, 0.5, 1.03, TopTitle, $
            /Normal, Color=!MYCT.BLACK, CharSize=1.0, Align=0.5
      endif
 
      ; Undefine stuff
      UnDefine, Data_500_1
      UnDefine, Data_500_2
      UnDefine, Data_Sfc_1
      UnDefine, Data_Sfc_2
      UnDefine, GridInfo_1
      UnDefine, GridInfo_2
      UnDefine, ModelInfo_1
      UnDefine, ModelInfo_2
      UnDefine, Size_1
      UnDefine, Size_2

   endfor

   ;====================================================================
   ; Cleanup and quit
   ;====================================================================
Quit:

   ; Cancel previous MULTIPANEL settings
   MultiPanel, /Off

   ; Close device
   Close_Device

   ; Restore original color table
   TvLct, R, G, B

   ; Restore !MYCT sysvar to original settings
   if ( ChkStru( Myct_Orig ) ) then !MYCT = Myct_Orig

   ; Quit
   return

end
 
