;-----------------------------------------------------------------------
;+
; NAME:
;        STRAT_DIFF
;
; PURPOSE:
;        Creates zonal mean absolute and percent difference plots
;        of tracers in the stratosphere (~400 hPa to max lev) from the GEOS-Chem
;        1-month benchmark simulations.
;
; CATEGORY:
;        Benchmarking
;
; CALLING SEQUENCE:
;        STRAT_DIFF, FILES, TAUS, TRACERS, VERSIONS, [, Keywords ]
;
; INPUTS:
;        FILES -> A 2-element vector containing the names of files
;             from the "old" and "new" GEOS-Chem model versions
;             that are to be compared. 
;
;        TAUS -> A 2-element vector contaning TAU values (hours GMT
;             from /1/1985) corresponding to the "old" and "new"
;             GEOS-Chem model versions.
;
;        TRACERS -> The list of transported tracers (i.e. diagnostic
;             category "IJ-AVG-$").
;
;        VERSIONS -> A 2-element vector containing the version
;             numbers for the "old" and "new" GEOS-Chem model
;             versions.
;
; KEYWORD PARAMETERS:
;        /DO_FULLCHEM -> Set this switch to plot the chemically
;             produced OH in addition to the advected tracers.
;
;        /DYNRANGE -> Set this switch to create plots using the whole
;             dynamic range of the data.  Default is to restrict
;             the plot range to predetermined values as returned
;             by routine GET_DIFF_RANGE.
;
;        /PS -> Set this switch to generate PostScript output.
;
;        OUTFILENAME -> If /PS is set, will write PostScript output 
;             to a file whose name is specified by this keyword.
;             Default is "tracer_ratio.pro".
;
;        ZDFORMAT -> This keyword passes a colorbar format string
;             (Fortran-style) to the COLORBAR routine (via TVPLOT).
;             This keyword is purposely not named CBFORMAT, in order
;             to avoid passing this quantity to other routines.             
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        Internal Subroutines Included:
;        ==================================================
;        PlotStratAbsDiff      PlotStratPctDiff
;
;        External Subroutines Required:
;        ==================================================
;        CLOSE_DEVICE          COLORBAR_NDIV    (function)   
;        CTM_GET_DATA          GET_DIFF_RANGE   (function)   
;        GETMODELANDGRIDINFO   EXTRACT_FILENAME (function)   
;        MULTIPANEL            CHKSTRU          (function)   
;        MYCT                  OPEN_DEVICE                   
;        TVPLOT                UNDEFINE   
;     
; REQUIREMENTS:
;        References routines from the GAMAP package.
;        
; NOTES:
;        (1) Meant to be called from BENCHMARK_1MON.
;
; EXAMPLE:
;        FILES    = [ 'ctm.bpch.v9-01-01', 'ctm.bpch.v9-01-02a' ]
;        TAUS     = [ NYMD2TAU( 20050701 ), NYMD2TAU( 20050701 ) ]
;        TRACERS  = INDGEN( 43 ) + 1
;        VERSIONS = [ 'v9-01-01', 'v9-01-02a' ]
;
;        STRAT_DIFF, FILES, ALTRANGE, TAUS, TRACERS, VERSIONS, $
;             /DO_FULLCHEM, /PS, OUTFILENAME='myplot.ps'
;
;             ; Creates zonal mean difference plots of two GEOS-Chem 
;             ; versions; (in this case v9-01-01 / v9-01-01a) for 
;             ; July 2001.  Output is sent to the PostScript file 
;             ; "myplot.ps".  The min and max of the data on each plot 
;             ; panel is restricted to pre-defined values returned by
;             ; function GET_DIFF_RANGE.
;             
;        STRAT_DIFF, FILES, ALTRANGE, TAUS, TRACERS, VERSIONS, $
;             /DYNRANGE, /PS, OUTFILENAME='myplot.ps'
;
;             ; Same as the above example, but the min & max of 
;             ; each plot panel corresponds to the dynamic range
;             ; of the data (centered around zero).
;
; MODIFICATION HISTORY:
;        mps, 11 Sep 2015: - Initial version, based on zonal_diff.pro
;        mps, 04 Jan 2016: - Include MERRA2 in the check for equivalent
;                            vertical grids
;        mps, 14 Mar 2018: - Now plot data above 400 hPa to capture entire
;                            stratosphere
; 
;-
; Copyright (C) 2011-2012, Bob Yantosca, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to yantosca@seas.harvard.edu
; or with subject "IDL routine strat_diff"
;-----------------------------------------------------------------------

pro PlotStratAbsDiff, Data1,             Data2,   TracerName,               $
                      GridInfo,          Unit,    DynRange,                 $
                      TimeTrop=TimeTrop, PS=PS,   Format=Format,            $
                      _EXTRA=e
   
   ;====================================================================
   ; Internal routine PlotStratAbsDiff plots the zonal mean absolute
   ; difference for a given tracer between two benchmark simulations.
   ;====================================================================

   ; Keywords
   Percent     = Keyword_Set( Percent  )
   PS          = Keyword_Set( PS       )
   if ( N_Elements( Format ) eq 0 ) then Format = '(e13.3)' 

   ; Grid dimension parameters
   S           = Size( Data1, /Dim )
   Lats        = GridInfo.YMid
   N_Lon       = Double( S[0] )

   ; Set Y-axis coordinate for pressure
   YMid        = GridInfo.PMid[0:S[2]-1L]

   ; Select data for the stratosphere (~400 hPa to max lev)
   if ( TracerName eq 'OH' or TracerName eq 'HO2' ) then begin
      YStrat      = YMid[26:58]
      Strat1      = Data1[*,*,26:58]
      Strat2      = Data2[*,*,26:58]
   endif else begin
      YStrat      = YMid[26:71]
      Strat1      = Data1[*,*,26:71]
      Strat2      = Data2[*,*,26:71]
   endelse

   ; Missing data value
   FILLVALUE   = -9.99e30

   ;-----------------------------------------------------------------
   ; GEOS-Chem advected tracers are defined at all vertical levels,
   ; so we don't have to neglect the stratospheric boxes.
   ;-----------------------------------------------------------------

   ; Compute absolute differences
   AbsDiff     = Strat2 - Strat1

   ; Compute zonal mean absolute differences
   ZAbsDiff    = Total( AbsDiff, 1 ) / N_Lon

   ; Undefine
   Undefine, BotOut
   UnDefine, AbsDiff

   ; Replace denormal data with a missing value 
   Ind         = Where( ~Finite( ZAbsDiff ) )
   if ( Ind[0] ge 0 ) then ZAbsDiff[Ind] = FILLVALUE

   ;====================================================================
   ; Set plot parameters
   ;====================================================================

   ; Parameters common to both dyn range and pre-defined range plots
   XTickV         = [ -90, -60, -30, 0, 30, 60, 90 ]
   XTicks         = N_Elements( XTickV )-1L
   XMinor         = 3
   XTitle         = 'Latitude'
   yTitle         = 'Pressure (hPa)'
   Title          = 'Zonal mean abs diff: ' + TracerName  
   Div            = Colorbar_Ndiv( 6 )

   if ( DynRange ) then begin

      ;-----------------------------------------------------------------
      ; Set plot parameters for the full dynamic range of the data
      ; Center the range around zero symmetrically
      ;-----------------------------------------------------------------
      Good     =  Where( ZAbsDiff gt FILLVALUE )
      MinData  =  Min( ZAbsDiff[Good], Max=MaxData )
      Extreme  =  Max( [ Abs( MinData ), Abs( MaxData ) ] )
      MinData  = -Extreme
      MaxData  =  Extreme
      BotOut   =  !MYCT.BOTTOM
      Triangle =  1
      NoGap    =  1
      Upos     =  1.1
      
      ; We need to set the colorbar a little bit lower for PostScript
      if ( PS )                                                             $
         then CBPos = [ 0.10, -0.02, 0.90, 0.01 ]                           $
         else CBPos = [ 0.05,  0.01, 0.95, 0.04 ]

   endif else begin

      ;-----------------------------------------------------------------
      ; Set plot parameters using the pre-defined min & max values from
      ; function GET_DIFF_RANGE.  This is the default.
      ;-----------------------------------------------------------------
      Range    =  Get_Diff_Range( TracerName )
      MinData  =  Range[0] 
      MaxData  =  Range[1]
      BotOut   =  !MYCT.BOTTOM
      Triangle =  1
      NoGap    =  1
      UPos     =  1.02
      
      ; We need to set the colorbar a little bit lower for PostScript  
      if ( PS )                                                             $
         then CBPos = [ 0.05, -0.02, 0.95, 0.01 ]                           $
         else CBPos = [ 0.05,  0.01, 0.95, 0.04 ]          

   endelse

   ; For OH, let's rescale the unit for clarity
   if ( TracerName eq 'OH' ) then begin
      ZAbsDiff = ZAbsDiff / 1e5
      MinData  = MinData  / 1e5
      MaxData  = MaxData  / 1e5
      Unit     = '1e5 molec/cm3'
   endif

   ; For HO2, let's rescale the unit for clarity
   if ( TracerName eq 'HO2' ) then begin
      ZAbsDiff = ZAbsDiff / 1e-12
      MinData  = MinData  / 1e-12
      MaxData  = MaxData  / 1e-12
      Unit     = 'pptv'
   endif

   ; Use exponents to avoid colorbars with ranges 0.0000-0.0000
   if ( MaxData lt 0.0001 ) then begin
      Format = '(e13.3)'
   endif else if ( MaxData gt 10000.0 ) then begin
      Format = '(f13.0)'
   endif else begin
      Format = '(f13.4)'
   endelse

   ;====================================================================
   ; Create the plot!
   ;====================================================================

   ; Plot zonal mean differences
   TvPlot, ZAbsDiff, Lats, YStrat,                                          $
           /Cbar,             Division=Div,      /Sample,                   $
           Title=Title,       MinData=MinData,   MaxData=MaxData,           $
           CBFormat=Format,   BOR_Label=' ',     Unit=Unit,                 $
           /XStyle,           XTickV=XTickV,     XTicks=XTicks,             $
           XMinor=XMinor,     XTitle=Xtitle,     YTitle=YTitle,             $
           Triangle=Triangle, NoGap=NoGap,                                  $
           CBPosition=CBPos,  UPos=UPos,         BotOut=BotOut,             $
           Min_Valid=MinData, _EXTRA=e

end

;------------------------------------------------------------------------------

pro PlotStratPctDiff, Data1,             Data2,   TracerName,               $
                      GridInfo,          Unit,    DynRange,                 $
                      TimeTrop=TimeTrop, PS=PS,   Format=Format,            $
                      _EXTRA=e
   
   ;====================================================================
   ; Internal routine PlotStratPctDiff plots the zonal mean percent
   ; difference for a given tracer between two benchmark simulations.
   ;====================================================================

   ; Keywords
   Percent     = Keyword_Set( Percent )
   PS          = Keyword_Set( PS      )
   if ( N_Elements( Format ) eq 0 ) then Format = '(f14.4)' 

   ; Index arrays
   S           = Size( Data1, /Dim )
   Lats        = GridInfo.YMid
   N_Lon       = Double( S[0] )

   ; Set Y-axis coordinate for pressure
   YMid        = GridInfo.PMid[0:S[2]-1L]

   ; Select data for the stratosphere (~400 hPa to max lev)
   if ( TracerName eq 'OH' or TracerName eq 'HO2' ) then begin
      YStrat      = YMid[26:58]
      Strat1      = Data1[*,*,26:58]
      Strat2      = Data2[*,*,26:58]
   endif else begin
      YStrat      = YMid[26:71]
      Strat1      = Data1[*,*,26:71]
      Strat2      = Data2[*,*,26:71]
   endelse

   ; Missing data value
   FILLVALUE   = -9.99e30

   ;-----------------------------------------------------------------
   ; GEOS-Chem advected tracers are defined at all vertical levels,
   ; so we don't have to neglect the stratospheric boxes.
   ;-----------------------------------------------------------------

   ; Compute percent differences
   PctDiff     = ( ( Strat2 - Strat1 ) / Strat1 ) * 100d0

   ; Compute zonal mean percent differences
   ZPctDiff    = Total( PctDiff, 1 ) / N_Lon

;   endelse

   ; Replace denormal data with a missing value 
   Ind         = Where( ~Finite( ZPctDiff ) )
   if ( Ind[0] ge 0 ) then ZPctDiff[Ind] = FILLVALUE

   ;=================================================================
   ; Plot percent difference
   ;=================================================================
   
   ; Parameters common to both dyn range and pre-defined range plots
   XTickV      = [ -90, -60, -30, 0, 30, 60, 90 ]
   XTicks      = N_Elements( XTickV )-1L
   XMinor      = 6
   XTitle      = 'Latitude'
   yTitle      = 'Pressure (hPa)'
   Title       = 'Zonal mean % diff: ' + TracerName  
   Div         = Colorbar_Ndiv( 6 )

   if ( DynRange ) then begin

      ;-----------------------------------------------------------------
      ; Parameters specific to the dynamic range plots
      ; Center the range around zero symmetrically
      ;-----------------------------------------------------------------
      Good     =  Where( ZPctDiff gt FILLVALUE )

      MinData  =  Min( ZPctDiff[Good], Max=MaxData )
      Extreme  =  Max( [ Abs( MinData ), Abs( MaxData ) ] )

      ; Prevent ratios from going out of range by capping at 1000%
      ; (mps, 8/7/13)
      if ( Extreme gt 1000d0 ) then Extreme = 1000d0
      MinData  = -Extreme
      MaxData  =  Extreme

      BotOut   =  !MYCT.BOTTOM
      Triangle =  1
      NoGap    =  1
      Upos     =  1.1
      
      ; We need to set the colorbar a little bit lower for PostScript
      if ( PS )                                                             $
         then CBPos = [ 0.10, -0.02, 0.90, 0.01 ]                           $
         else CBPos = [ 0.05,  0.01, 0.95, 0.04 ]

   endif else begin

      ;-----------------------------------------------------------------
      ; Parameters specific to the plots w/ pre-defined min & max 
      ; limits (i.e. returned by function GET_DIFF_RANGE.
      ;-----------------------------------------------------------------
      MinData  = -30
      MaxData  = +30
      BotOut   =  !MYCT.BOTTOM
      Triangle =  1
      NoGap    =  1
      UPos     =  1.02
      
      ; We need to set the colorbar a little bit lower for PostScript  
      if ( PS )                                                             $
         then CBPos = [ 0.05, -0.02, 0.95, 0.01 ]                           $
         else CBPos = [ 0.05,  0.01, 0.95, 0.04 ]                             
                    
   endelse
         
   ;====================================================================
   ; Create the plot!
   ;====================================================================
  
   ; Plot percent difference 
   TvPlot, ZPctDiff, Lats, YStrat,                                           $
           /Cbar,              Division=Div,      /Sample,                   $
           Title=Title,        MinData=MinData,   MaxData=MaxData,           $
           CBFormat='(f14.4)', BOR_Label=' ',     Unit='%',                  $
           /XStyle,            XTickV=XTickV,     XTicks=XTicks,             $
           XMinor=XMinor,      XTitle=XTitle,     YTitle=YTitle,             $
           Triangle=Triangle,  NoGap=NoGap,                                  $
           CBPosition=CBPos,   UPos=UPos,         BotOut=BotOut,             $
           Min_Valid=MinData,  _EXTRA=e
           
end

;------------------------------------------------------------------------------

pro Strat_Diff, Files, Taus, Tracers, Versions,                             $
                DynRange=Dynrange, Do_FullChem=Do_FullChem,                 $
                PS=PS,             OutFileName=OutFileName,                 $
                ZDFormat=ZDFormat, _EXTRA=e
 
   ;====================================================================
   ; Initialization
   ;====================================================================
   
   ;--------------------------------------------------------------------
   ;### DEBUG: Hardwire inputs for testing
   ;Files    = [ '~/SR/v9-01-02a/ctm.bpch.v9-01-02a', $
   ;             '~/SR/v9-01-02b/ctm.bpch.v9-01-02b' ]
   ;Taus     = [ 179664D, 179664D ]
   ;Tracers  = [1 ] ; LindGen( 43 ) +1L
   ;Versions = [ 'v9-01-02a', 'v9-01-02b' ]
   ;--------------------------------------------------------------------

   ; External functions
   FORWARD_FUNCTION ColorBar_NDiv, Get_Diff_Range, Extract_FileName

   ; Arguments
   if ( N_Elements( Files    ) ne 2 ) then Message, 'Invalid FILES!'
   if ( N_Elements( Taus     ) ne 2 ) then Message, 'Invalid TAUS!'
   if ( N_Elements( Versions ) ne 2 ) then Message, 'Invalid VERSIONS!'

   ; Keywords
   Do_FullChem = Keyword_Set( Do_FullChem )
   DynRange    = Keyword_Set( DynRange    )
   if ( N_Elements( OutFileName ) ne 1 )                                    $
      then OutFileName = 'Strat_Differences.ps'

   ; Title for the top of the plot
   if ( DynRange ) then begin
      TopTitle = 'GEOS-Chem '                 + Versions[1]              +  $
                 ' Stratospheric zonal differences (dyn range)!C!C'      + $
                 Extract_FileName( Files[1] ) + ' - '                    +  $
                 Extract_FileName( Files[0] )
   endif else begin
      TopTitle = 'GEOS-Chem '                 + Versions[1]              +  $
                 ' Stratospheric zonal differences!C!C'                  + $
                 Extract_FileName( Files[1] ) + ' - '                    +  $
                 Extract_FileName( Files[0] )
   endelse


   ; Save original color table
   TvLct, R, G, B, /Get

   ; Save the original settings of the !MYCT sysvar
   if ( ChkStru( !MYCT ) ) then Myct_Orig = !MYCT

   ; Load Blue-White-White-Red colortable w/ 12 colors
   MyCt, /BuWhWhRd, NColors=12

   ;====================================================================
   ; Read data from the files
   ;====================================================================
   
   ;------------------------------
   ; From FILE_1: the "old" file
   ;------------------------------

   ; Read tracers from the old file
   CTM_Get_Data, DataInfo_1, 'IJ-AVG-$', $
      File=Files[0], Tau0=Taus[0], Tracer=Tracers, /Quiet

   ; Get full-chemistry quantities from the old file
   ; Read OH from the "new" file
   if ( Do_FullChem ) then begin

      ; Read OH data
      CTM_Get_Data, DataInfo, 'CHEM-L=$', $
                    File=Files[0], Tau0=Taus[0], Tracer=1, /Quiet
      DataInfo_1 = [ DataInfo_1, DataInfo ]
      UnDefine, DataInfo

      ; Read the time each grid box spent in the tropopause
      CTM_Get_Data, DataInfo, 'TIME-TPS', $
                    File=Files[0], Tau0=Taus[0], Tracer=1, /Quiet
      TimeTrop_1 = *( DataInfo[0].Data )
      UnDefine, DataInfo

      ; Read HO2 data
      CTM_Get_Data, DataInfo, 'CHEM-L=$', $
                    File=Files[0], Tau0=Taus[0], Tracer=3, /Quiet
      DataInfo_1 = [ DataInfo_1, DataInfo ]
      UnDefine, DataInfo

   endif

   ;------------------------------
   ; From FILE_2: the "new" file
   ;------------------------------

   ; Read tracers from the "new" file
   CTM_Get_Data, DataInfo_2, 'IJ-AVG-$', $
      File=Files[1], Tau0=Taus[1], Tracer=Tracers, /Quiet

   ; Get full-chemistry quantities from the new file
   if ( Do_FullChem ) then begin

      ; Read OH data
      CTM_Get_Data, DataInfo, 'CHEM-L=$', $
                    File=Files[1], Tau0=Taus[1], Tracer=1, /Quiet
      DataInfo_2 = [ DataInfo_2, DataInfo ]
      UnDefine, DataInfo

      ; Read the time each grid box spent in the tropopause
      CTM_Get_Data, DataInfo, 'TIME-TPS', $
                    File=Files[0], Tau0=Taus[0], Tracer=1, /Quiet
      TimeTrop_2 = *( DataInfo[0].Data )
      UnDefine, DataInfo

      ; Read HO2 data
      CTM_Get_Data, DataInfo, 'CHEM-L=$', $
                    File=Files[1], Tau0=Taus[1], Tracer=3, /Quiet
      DataInfo_2 = [ DataInfo_2, DataInfo ]
      UnDefine, DataInfo

   endif

   ;------------------------------
   ; Error checks!
   ;------------------------------

   ; Stop if both DATAINFOs are incompatible
   if ( N_Elements( DataInfo_1 ) ne N_Elements( DataInfo_2 ) ) $
      then Message, 'Files have different numbers of tracers in them!'

   ;====================================================================
   ; Process data and create difference plots!
   ;====================================================================

   ; Number of rows & colums on the plot
   Rows = 3
   Cols = 2

   ; Use Postscript font
   !p.font = 0

   ; Open the plot device and initialize the page
   Open_Device, /Color, Bits=8, /Portrait, PS=PS, File=OutFileName, _EXTRA=e

   ; Multiple panels per page
   MultiPanel, Rows=Rows, Cols=Cols, Margin=[ 0.07, 0.07, 0.07, 0.07 ]

   ; Loop over the # of categories
   for D = 0L, N_Elements( DataInfo_1 )-1L do begin
      
      ;-----------------------------------------------------------------
      ; Error check grid, tracer name, and data block sizes 
      ;-----------------------------------------------------------------

      ; Get MODELINFO and GRIDINFO structures
      GetModelAndGridInfo, DataInfo_1[D], ModelInfo_1, GridInfo_1
      GetModelAndGridInfo, DataInfo_2[D], ModelInfo_2, GridInfo_2

      ; Can't plot if the vertical grids are different!
      if ( Modelinfo_1.Name ne ModelInfo_2.Name ) then begin
         
         ; First determine if both grids are equivalent even if the
         ; model names differ (e.g. GEOS-5.2, MERRA, GEOS-5.7)
         Models = [ Modelinfo_1.Name,  ModelInfo_2.Name ]
         F0     = ( Where( Models eq 'GEOS5_47L'  ) ge 0 )
         F1     = ( Where( Models eq 'MERRA_47L'  ) ge 0 )
         F2     = ( Where( Models eq 'GEOSFP_47L' ) ge 0 )
         F3     = ( Where( Models eq 'MERRA2_47L' ) ge 0 )

         ; If both grids are not equivalent, then stop w/ error message
         if ( ( F0 + F1 + F2 ) ne 2 ) then  begin

            ; Now check models using native grid
            F4     = ( Where( Models eq 'GEOS5'  ) ge 0 )
            F5     = ( Where( Models eq 'MERRA'  ) ge 0 )
            F6     = ( Where( Models eq 'GEOSFP' ) ge 0 )
            F7     = ( Where( Models eq 'MERRA2' ) ge 0 )

            if ( ( F4 + F5 + F6 + F7 ) ne 2 ) then  begin
               Message, $
               'The two models have different vertical grids!  Cannot plot!'
            endif
         endif
      endif

      ; Make sure grids are compatible
      if ( ( GridInfo_1.IMX   ne GridInfo_2.IMX   ) OR $
           ( GridInfo_1.JMX   ne GridInfo_2.JMX   ) OR $
           ( GridInfo_1.LMX   ne GridInfo_2.LMX   ) )  $
         then Message, 'Resolution mismatch!'

      ; Make sure the tracers correspond to each other
      TracerName_1 = DataInfo_1[D].TracerName
      TracerName_2 = DataInfo_2[D].TracerName
      if ( TracerName_1 ne TracerName_2 ) then Message, 'Tracer mismatch!'

      ; Get full-sized data arrays
      Data_1 = *( DataInfo_1[D].Data )
      Data_2 = *( DataInfo_2[D].Data )

      ; Get the dimensions of the data arrays
      Size_1 = Size( Data_1, /Dim )
      Size_2 = Size( Data_2, /Dim )

      ; Stop the run if the data block sizes don't agree
      if ( Size_1[0] ne Size_2[0] ) then Message, 'Longitude mismatch!'
      if ( Size_1[1] ne Size_2[1] ) then Message, 'Latitude mismatch!'

      ; Get unit of data
      Unit = DataInfo_1[D].Unit

      ;-----------------------------------------------------------------
      ; Plot the data!
      ;-----------------------------------------------------------------

      ; Absolute diff
      PlotStratAbsDiff, Data_1,              Data_2, TracerName_1,          $
                        GridInfo_1,          Unit,   DynRange,              $
                        TimeTrop=TimeTrop_2, PS=PS,  Format=ZDFormat,       $
                        _EXTRA=e

      ; Percent diff
      PlotStratPctDiff, Data_1,              Data_2, TracerName_1,          $
                        GridInfo_1,          Unit,   DynRange,              $
                        TimeTrop=TimeTrop_2, PS=PS,  Format=ZDFormat,       $
                        _EXTRA=e

      ; Plot the top title on each page  
      if ( D*2 mod ( Rows * Cols ) eq 0 ) then begin
         XYoutS, 0.5, 1.03, TopTitle, $
            /Normal, Color=!MYCT.BLACK, CharSize=1.0, Align=0.5
      endif

      ;-----------------------------------------------------------------
      ; Undefine stuff for next iteration
      ;-----------------------------------------------------------------
      UnDefine, Data_1
      UnDefine, Data_2
      UnDefine, GridInfo_1
      UnDefine, GridInfo_2
      UnDefine, ModelInfo_1
      UnDefine, ModelInfo_2
      UnDefine, Size_1
      UnDefine, Size_2
      UnDefine, TracerName_1
      UnDefine, TracerName_2
      
   endfor

   ;====================================================================
   ; Cleanup and quit
   ;====================================================================

   ; Cancel previous MULTIPANEL settings
   MultiPanel, /Off

   ; Close plot device
   Close_Device

   ; Restore original color table
   TvLct, R, G, B

   ; Restore !MYCT sysvar to original settings
   if ( ChkStru( Myct_Orig ) ) then !MYCT = Myct_Orig

   ; Quit
   return
end
 
