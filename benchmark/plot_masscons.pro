;-----------------------------------------------------------------------
;+
; NAME:
;        PLOT_MASSCONS
;
; PURPOSE:
;        Plots the evolution of total mass vs. time from the
;        geosfp_2x25_masscons simulation.
;       
; CATEGORY
;        Benchmarking
;
; CALLING SEQUENCE:
;        PLOT_MASSCONS, FILENAME, _EXTRA=e
;
; INPUTS:
;        FILENAME -> Name of the file containing the total mass
;             printed every 6 hours from the geosfp_2x25_masscons
;             simulation.  Default is "tracer_mass_kg.dat".
; 
; OUTPUT:
;        None (creates a plot)
;
; KEYWORD PARAMETERS:
;        /VERBOSE -> Print extra information (min and max of time
;             and total mass) to the screen.
; 
;        _EXTRA =e -> Passes extra keywords to PLOT and OPLOT routins.
;
; SUBROUTINES:
;        External Subroutines Required:
;        ===============================
;        NYMD2TAU   (function)
;        STRBREAK   (function)
;        STRSCI     (function)
;
; REQUIREMENTS:
;        None
;
; NOTES:
;        This could probably be written a little more efficiently.
;        Also, plotting output has been kept very basic, as we are
;        mostly using this for quick validation plots, which do not
;        need to be fancy.
;
; EXAMPLE:
;        PLOT_MASSCONS, 'tracer_mass_kg_2017.dat'
;
;        ; Creates a plot from the data in the given file name.
;
; MODIFICATION HISTORY:
;        bmy, 22 Feb 2017: GAMAP VERSION 2.19
;                          - Initial version
;
;-
; Copyright (C) 2017, GEOS-Chem Support Team, Harvard University.
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.harvard.edu with subject "IDL routine ctm_grid"
;-----------------------------------------------------------------------

; Plot evolution of mass over time
pro Plot_Masscons, FileName, Verbose = Verbose, _EXTRA = e

   ;====================================================================
   ; Initialize
   ;====================================================================

   ; Resolve other GAMAP functions
   FORWARD_FUNCTION Nymd2Tau, StrBreak, StrSci

   ; Default values
   if ( N_Elements( FileName ) ne 1 ) then FileName = 'tracer_mass_kg.dat'

   ; Strings
   Line    = ''
   Date    = ''
   Time    = ''
   Mass    = ''

   ; Scalars
   C       = 0L

   ; Arrays
   TauArr  = DblArr( 500000 )
   MassArr = DblArr( 500000 )

   ;====================================================================
   ; Read data from file
   ;====================================================================

   ; Open file
   Open_File, FileName, Ilun, /Get_LUN

   ; Read data
   while ( not EOF( Ilun ) ) do begin
      
      ; Read each line
      ReadF, Ilun, Line

      ; Split the string
      Result = StrBreak( Line,  ' ' )

      ; Get, date, time, and mass
      Date = Result[0]
      Time = Result[1]
      Mass = Double( Result[3] )

      ; Get YYYYMMDD
      Result = StrBreak( Date, '/' )
      NYMD   = Long( Result[0] ) *  10000L + $
               Long( Result[1] ) *  100L   + $
               Long( Result[2] )

      ; Get hhmmss
      Result = StrBreak( Time, ':' )
      NHMS   = Long( Result[0] ) *  10000L + $
               Long( Result[1] ) *  100L
      
      ; Get TAU value
      Tau    = Nymd2Tau( NYMD, NHMS )

      ; Save into arrays
      TauArr [C] = Tau[0]
      MassArr[C] = Mass

      ; Increment
      C =  C + 1L

   endwhile
   
   ; Close file
   Close, Ilun
   Free_Lun, Ilun

   ;====================================================================
   ; Prepare arrays and plot data
   ;====================================================================

   ; Time array
   TauArr      = TauArr [0:C-1L]
   MinTau      = Min( TauArr, Max = MaxTau )  
   TauArr      = TauArr - TauArr[0]

   ; Mass arrays
   MassArr     = MassArr[0:C-1L]
   InitialMass = DblArr( N_Elements( MassArr ) ) + MassArr[0]

   ; Get the X-axis range
   MinTime     = Min( TauArr, Max = MaxTime )
   XRange      = [ MinTime, MaxTime ]

   ; Top of plot title
   Title       = 'Evolution of total mass with time (Initial mass = ' +  $
                  String( MassArr[0], Format = '(e15.9)' ) +  ' kg)'

   ; Y-axis title
   YTitle = 'Mass [' +  StrSci( 1e15, /POT_Only ) + ' kg]'

   ; Print some information if /VERBOSE is set
   if ( Keyword_Set( Verbose ) ) then begin

      ; Get min and max of the mass arrayt
      MinMass  = Min( MassArr, Max = MaxMass )

      ; Print tau range
      F1 =  '("TAU range    [hrs since 1/1/1985] : ", 2f20.5 )'
      print, Format = F1, MinTau, MaxTau

      ; Print initial mass
      F2 =  '("Initial mass [kg                ] : ", e20.14)'
      print, Format = F2, MassArr[0]

      ; Print mass range
      F3 =  '("Mass range   [kg                ] : ", e20.14,1x,e20.14)'
      print, Format = F3, MinMass, MaxMass
   endif

   ; Divide by 1e15 to make it easier to see the axis values (after VERBOSE)
   MassArr     = MassArr     * 1e-15
   InitialMass = InitialMass * 1e-15

   ; Plot data
   Plot, TauArr, MassArr,               $
         Color      = !MYCT.BLACK,      $
         /XStyle,                       $
         Xtitle     = 'Hours',          $
         XRange     = Xrange,           $
         XMargin    = [ 18, 5 ],        $
         /YStyle,                       $
         Ytitle     = YTitle,           $
         Title      = Title,            $
         _EXTRA     = e

   ; Plot initial mass for reference as a line
   Oplot, TauArr,  InitialMass,         $
          Color     = !MYCT.RED,        $
          Thick     = 2,                $
          LineStyle = 0,                $
          _EXTRA    = e

   return
end
