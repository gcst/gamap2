;-----------------------------------------------------------------------
;+
; NAME:
;        JV_MAP
;
; PURPOSE:
;        Creates plots of J-values at both the surface and 500 hPa. 
;       
; CATEGORY:
;        Benchmarking
;
; CALLING SEQUENCE:
;        JV_MAP, FILE, LEVELS, TAU, VERSION, [, Keywords ]
;
; INPUTS:
;        FILE -> The name of the file containing data to be plotted.
;
;        LEVELS -> A 4-element vector containing the level indices
;             for the GEOS-Chem surface layer and 500 hPa layer.
;             for both models (e.g. SFC_1, SFC_2, 500_1, 500_2).
;             NOTE: This is in Fortran notation (starting from 1!)
;
;        TAU -> The TAU value (hours GMT from /1/1985) corresponding
;             to the data to be plotted.
;
;        VERSION -> The model version number corresponding to the
;             data to be plotted.
;
; KEYWORD PARAMETERS:
;        /DYNRANGE -> Set this switch to create plots using the whole
;             dynamic range of the data.  Default is to restrict
;             the plot range to +/- 5%.
;
;        /PS -> Set this switch to generate PostScript output.
;
;        OUTFILENAME -> If /PS is set, will write PostScript output
;             to a file whose name is specified by this keyword.
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        Internal Subroutines Required:
;        ==================================================
;        PlotJVMap
;
;        External Subroutines Required:
;        ==================================================
;        CLOSE_DEVICE          COLORBAR_NDIV    (function)
;        CTM_GET_DATA          EXTRACT_FILENAME (function)       
;        GETMODELANDGRIDINFO   MULTIPANEL         
;        MYCT                  OPEN_DEVICE   
;        TVMAP  
;
; REQUIREMENTS:
;        References routines from the GAMAP package.
;
; NOTES:
;        (1) Meant to be called from BENCHMARK_1MON
;
; EXAMPLE:
;        FILE     = 'ctm.bpch.v10-01i'
;        LEVELS   = [ 1, 1, 13, 13 ]
;        TAUS     = NYMD2TAU( 20130701 )
;        VERSIONS = 'v10-01i'
; 
;        JV_MAP, FILES, LEVELS, TAUS, VERSIONS, $
;             /PS, OUTFILENAME='myplot.ps'
;
;             ; Creates J-value plots for v10-01i for July 2013
;             ; at the surface and 500 hPa.
;             ; The max & min of the data will be fixed at +/- 5%.
;
;        JV_MAP, FILE, LEVELS, TAU, VERSION, $
;             /DYNRANGE, /PS, OUTFILENAME='myplot.ps'
;
;             ; Same as above, but this time the full dynamic
;             ; range of the data will be displayed.
;
; MODIFICATION HISTORY:
;        mps, 19 Apr 2014: VERSION 1.01
;                          - Adapted from jv_ratio.pro
;  mps & bmy, 29 May 2014: GAMAP VERSION 2.18
;                          - Compatible with extra J-Value tracers
;                            in v10-01c and higher versions
; 
;-
; Copyright (C) 2007-2014,
; Bob Yantosca and Philippe Le Sager, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.as.harvard.edu with subject "IDL routine JV maps"
;-----------------------------------------------------------------------

pro PlotJVMap, Data, Level, TracerName, Unit, GridInfo, PS=PS, _EXTRA=e
   
   ;====================================================================
   ; Internal routine PlotJVMap plots J-values at either the surface
   ; or 500 hPa (bmy, 11/9/07)
   ;====================================================================

   ; Plot title
   if ( Level gt 1 )                                     $
      then Title = 'J-Value @ 500 hPa for ' + TracerName $
      else Title = 'J-Value @ Surface for ' + TracerName  

   ; Number of colorbar tickmarks
   Divisions = ColorBar_NDiv( 6 )
      
   ; Don't plot the polar latitudes
   XMid      = GridInfo.XMid
   YMid      = GridInfo.YMid[ 1:GridInfo.JMX-2 ]
   Data      = Data[ *, 1:GridInfo.JMX-2 ]
  
   ; Determine Min and Max of data
   MinData  =  Min( Data, Max=MaxData )

   ; We need to set the colorbar a little bit lower for PostScript
   if ( Keyword_Set( PS ) )                   $
      then CBPos = [ 0.05, 0.01, 0.95, 0.04 ] $
      else CBPos = [ 0.05, 0.05, 0.95, 0.08 ]
   
   ; Plot J-values over world map
   TvMap, Data, XMid, Ymid,                                   $
          /Countries,          /Coasts,            /Cbar,     $ 
          Divisions=Div,       /Sample,            /Grid,     $
          Title=Title,         CBFormat='(e10.2)', Unit=Unit, $
          MinData=MinData,     MaxData=MaxData,    _EXTRA=e

end

;------------------------------------------------------------------------------

pro JV_Map, File, Levels, Tau, Version,    $
            PS=PS, OutFileName=OutFileName, _EXTRA=e

   ;====================================================================
   ; Initialization
   ;====================================================================
   
   ; External functions
   FORWARD_FUNCTION ColorBar_NDiv, Extract_FileName

   ; Arguments
   if ( N_Elements( File    ) ne 1 ) then Message, 'Invalid FILES!'
   if ( N_Elements( Levels  ) ne 4 ) then Message, 'Invalid LEVELS'
   if ( N_Elements( Tau     ) ne 1 ) then Message, 'Invalid TAUS!'
   if ( N_Elements( Version ) ne 1 ) then Message, 'Invalid VERSIONS!'

   ; Keywords
   if ( N_Elements( OutFileName ) ne 1 ) then OutFileName = 'jv_maps.ps'

   ; Define title for top of page
   TopTitle = 'GEOS-Chem ' + Version         + $
      ' J-Values at Surface and 500 hPa!C!C' + Extract_FileName( File )

   ; Save original color table
   TvLct, R, G, B, /Get

   ; Save the original settings of the !MYCT sysvar
   if ( ChkStru( !MYCT ) ) then Myct_Orig = !MYCT

   ; Load modified spectrum, extended to 12 colors
   MyCt, /ModSpec, NColors=12

   ;====================================================================
   ; Read data from the files
   ;====================================================================
 
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;%%% Use this for benchmarks prior to v10-01c_trop (bmy, mps, 5/29/14)
;%%%   ; Tracer list prior to v10-01c_UCX:                            
;%%%   Tracer = [  1,  2,  3,  4,  5,  6, 23, 24, 25, 26, 27, 28     ]
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
;%%% Use this to compare v10-01c_trop to v10-01c_UCX (bmy, mps, 5/29/14)
;%%%   Tracer = [  1,  2,  3,  4,  5,  6,  9, 10, 11, 12, $
;%%%              13, 14, 17, 18, 23, 24, 25, 26, 27, 28 ]
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;%%% Tracer list for v10-01c_UCX and higher versions
;%%%   Tracer = [  1,  2,  3,  4,  5,  6,  9, 10, 11, 12, 13, 14, 15, $ 
;%%%              16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28 ]
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;%%%   ; Tracer list for v11-02c and higher versions (mps, 8/29/17)
;%%%   Tracer = [  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, $ 
;%%%              16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, $
;%%%              31, 32, 33, 34, 35, 36, 37  ]
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   ; Tracer list for v11-02d and higher versions (mps, 10/5/17)
   Tracer = [  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, $ 
              16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, $
              31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, $
              46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, $
              61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, $
              76  ]

   ; Read data from "new" file
   CTM_Get_Data, DataInfo, 'JV-MAP-$',    $
                 File=File,     Tau0=Tau, $
                 Tracer=Tracer, /Quiet

   ;====================================================================
   ; Process tracers
   ;====================================================================

   ; Number of plots per page
   Rows = 2
   Cols = 2

   ; Use Postscript font
   !p.font = 0

   ; Initialize the plot device
   Open_Device, /Color, Bits=8, /Portrait, PS=PS, FileName=OutFileName
 
   ; Number of panels per page
   MultiPanel, Rows=Rows, Cols=Cols, Margin=[ 0.03, 0.03, 0.03, 0.03 ]
  
   ; Loop over tracers
   for D = 0L, N_Elements( DataInfo ) - 1L do begin

      ;-----------------------------------------------------------------
      ; Extract data
      ;-----------------------------------------------------------------

      ; Get MODELINFO and GRIDINFO structures
      GetModelAndGridInfo, DataInfo[D], ModelInfo, GridInfo

      ; Get tracername and unit strings
      TracerName = DataInfo[D].TracerName
      Unit       = DataInfo[D].Unit

      ; Get data arrays
      Data       = *( DataInfo[D].Data )

      ; Split into sfc and 500hPa levels
      Data_Sfc   = Data[ *, *, Levels[0]-1L ]
      Data_500   = Data[ *, *, Levels[2]-1L ]

      ; We no longer need the large data array
      UnDefine, Data

      ;-----------------------------------------------------------------
      ; Plot the data!
      ;-----------------------------------------------------------------

      Print, 'Creating J-value maps for ', TracerName

      ; Plot the Surface maps
      PlotJVMap, Data_Sfc, Levels[0], TracerName, Unit, GridInfo, _EXTRA=e

      ; Plot the 500 hPa maps
      PlotJVMap, Data_500, Levels[2], TracerName, Unit, GridInfo, _EXTRA=e

      ; Plot the top title on each page  
      if ( D mod ( Rows * Cols ) eq 0 ) then begin
         XYOutS, 0.5, 1.03, TopTitle, $
            /Normal, Color=!MYCT.BLACK, CharSize=1.0, Align=0.5
      endif
 
      ; Undefine stuff
      UnDefine, Data_500
      UnDefine, Data_Sfc
      UnDefine, ModelInfo
      UnDefine, GridInfo
      UnDefine, TracerName
      UnDefine, Unit

   endfor

   ;====================================================================
   ; Cleanup and quit
   ;====================================================================
Quit:

   ; Cancel previous MULTIPANEL settings
   MultiPanel, /Off

   ; Close device
   Close_Device

   ; Restore original color table
   TvLct, R, G, B

   ; Restore !MYCT sysvar to original settings
   if ( ChkStru( Myct_Orig ) ) then !MYCT = Myct_Orig

   ; Quit
   return

end
 
