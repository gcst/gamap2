;-----------------------------------------------------------------------
;+
; NAME:
;        BENCHMARK_1MON
;
; PURPOSE:
;        Produces maps of tracers and other quantities from two
;        GEOS-Chem benchmark simulations (for model validation).
;
; CATEGORY:
;        Benchmarking
;
; CALLING SEQUENCE:
;        BENCHMARK_1MON, INPUTFILE, [, Keywords ]
;
; INPUTS:
;        INPUTFILE -> A file containing default values for version 
;             numbers, directories, model names, resolutions, etc.
;             Default is "input_bm.1mon"
;
; KEYWORD PARAMETERS:
;        By default, BENCHMARK_1MON will produce the following types
;        of output:
;
;          (a) Table of Ox and CO budgets, mean OH conc. and CH3CCL3 lifetime 
;          (b) Table of emissions totals
;          (c) Frequency distribution histogram
;          (d) Vertical profiles of tracer differences along 15S and 42N 
;          (e) Maps of tracer concentration @ surface and 500 hPa
;          (f) Maps of tracer differences   @ surface and 500 hPa
;          (g) Maps of J-value differences  @ surface and 500 hPa
;          (h) Maps of tracer ratios        @ surface and 500 hPa
;
;        Each of these types of output can be turned off individually
;        with the following keywords:
; 
;        /NO_AOD_DIFFS -> Do not create difference maps of aerosol optical
;             depths.
;
;        /NO_AOD_MAPS -> Do not create concentration plots for aerosol
;             optical depths.
;
;        /NO_BUDGET -> Do not create the table of Ox and CO budgets,
;             mean OH concentration and CH3CCl3 lifetime.
;
;        /NO_CONC_MAPS -> Do not create the plot the maps of tracer
;             concentrations @ sfc and 500 hPa altitude.
;
;        /NO_DIFFS -> Do not create the maps of tracer differences
;             at the surface and 500 hPa altitude.
;
;        /NO_EMISSIONS -> Do not create the table of emissions totals. 
;
;        /NO_FREQ_DIST -> Do not create the the frequency distribution
;             histogram plot.
;        
;        /NO_JVALUES -> Do not create the maps of J-value ratios
;             at the surface and 500 hPa altitude.
;
;        /NO_JVDIFFS -> Do not create the maps of J-value differences
;             at the surface and 500 hPa altitude.
;
;        /NO_JVMAPS -> Do not create the maps of J-values 
;             at the surface and 500 hPa altitude.
;
;        /NO_PROFILES -> Do not create the plot of vertical profiles 
;             of tracer differences along 15S and 42N.
;
;        /NO_RATIOS -> Do not create the maps of tracer ratios at
;             the surface and 500 hPa altitude.
;
;        /NO_STRATDIFF -> Do not create the maps of zonal mean differences
;             in the stratosphere (100hPa-0.01hPa)
;
;        /NO_STRATCONC -> Do not create the maps of zonal mean concentrations
;             in the stratosphere (100hPa-0.01hPa)
;
;        /NO_ZONALDIFF -> Do not create the maps of zonal mean differences
;
;        /NO_ZONALCONC -> Do not create the maps of zonal tracer concentrations
;
;        /NO_CLOUDDIFF -> Do not create difference plots of cloud optical depth
;
;        /NO_2D_MET -> Do not create difference plots for 2-D met fields
;
;        /NO_3D_MET -> Do not create difference plots for 3-D met fields
;
;        Additional keywords:
;        --------------------
;
;        /DEBUG -> Set this switch to print the values of the various
;             input variables.  Use this to make sure that all values
;             have been created corectly.
;
;        /DYNRANGE -> Set this switch to create additional difference 
;             plots, ratio plots, and profile plots using the whole
;             dynamic range of the data.
;
;        /NO_FULLCHEM -> Set this switch to only plot the advected 
;             tracers and omit full-chemistry quantities such as OH
;             and aerosol optical depths.  This is useful if you wish
;             to compare output from offline GEOS-Chem simulations
;             (e.g. Rn-Pb-Be, CH4).
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        Internal Subroutines:
;        ====================================================
;        DynOutFile (function)    GetSfc500Levels (function)  
;
;        External Subroutines Required:
;        ====================================================
;        CTM_NAMEXT (function)    CTM_TYPE (function) 
;        DIFFERENCES              FREQ_DIST         
;        FULLCHEM_BUDGET          FULLCHEM_EMISSIONS
;        JV_RATIO                 PROFILES
;        MAPS                     NYMD2TAU (function)
;        RATIOS                   REPLACE_TOKEN
;        STRUADDVAR (function)    MCF_LIFETIME (function)
;
; REQUIREMENTS:
;        References routines from the GAMAP package.
;
; NOTES:
;        BENCHMARK_1MON assumes that the following GEOS-Chem
;        diagnostic quantities have been archived for the 
;        "old" and "new" model versions:
;
;          (a) ND22 ("JV-MAP-$")   (h) ND44 ("DRYD-FLX")
;          (b) ND24 ("EW-FLX-$")   (i) ND45 ("IJ-AVG-$")
;          (c) ND25 ("NS-FLX-$")   (j) ND65 ("PORL-L=$")
;          (d) ND26 ("UP-FLX-$")   (k) ND66 ("DAO-3D-$")
;          (e) ND31 ("PEDGE-$" )   (l) ND67 ("DAO-FLDS")
;          (f) ND43 ("CHEM-L=$")   (m) ND69 ("DXYP"    )
;          (g) ND32 (""NOX-AC-$", "NOX-AN-$", "NOX-BIOB", 
;                     "NOX-FERT", "NOX-LI-$", "NOX-SOIL")
;
; EXAMPLES:
;        BENCHMARK_1MON, 'input.1mon'
;
;            ; Produces the full suite of benchmark output plots.
;
;        BENCHMARK_1MON, 'input.1mon', /DYNRANGE
;
;            ; Produces the full suite of benchmark output plots.
;            ; Will also produce an additional set of difference and
;            ; ratio maps using the full dynamic range of the data.
;
;        BENCHMARK_1MON, 'input.1mon', /DEBUG, /NO_FREQ_DIST
;
;            ; Will produce the standard plots except for the
;            ; frequency distribution histogram.  Also will cause
;            ; extra information to be echoed to the screen.
;
; MODIFICATION HISTORY:
;        bmy, 09 Nov 2007: VERSION 1.01
;                          - based on "benchmark.pro"
;        bmy, 10 Jan 2011: VERSION 1.02
;                          - Now set proper symbolic links to
;                            diaginfo.dat and tracerinfo.dat 
;                          - Set 500hPa level for MERRA
;                          - Added /NO_PROFILES keyword to suppress
;                            printing of vertical profiles
;        bmy, 08 Jun 2011: VERSION 1.03
;                          - Updated comments
;                          - Added /NO_BUDGET, /NO_EMISSIONS, 
;                            /NO_PROFILES, /NO_CONC_MAPS, /NO_DIFFS, 
;                            /NO_JVALUES, /NO_RATIOS, /NO_FULLCHEM 
;                            keywords.
;                          - Now pass _EXTRA=e to all routines
;                          - Now use FILE_WHICH to locate the 
;                            diff_range.1mon file
;                          - Now look for diaginfo.dat and 
;                            tracerinfo.dat in RUNDIR_2.  Do not
;                            use symbolic links anymore.
;        bmy, 10 Jun 2011: - Now call EMISSION_RATIOS
;        bmy, 23 Jun 2011  - Now call ZONAL_DIFF
;        bmy, 18 Jul 2011  - Now pass /PRESSURE keyword to ZONAL_DIFF
;                            to create plots w/ pressure on Y-axis
;        bmy, 11 May 2012: GAMAP VERSION 2.16
;                          - Now do not stop the run if the two model 
;                            grids are equivalent.  This allows
;                            comparisons between GEOS-5, MERRA,
;                            GEOS-5.7 data.  
;                          - Return 500hPa level for GEOS-5.7 met
;        cdh, 18 Mar 2013: GAMAP VERSION 2.17
;                          - Added Zonal concentration plots for !
;                            1-month benchmarks
;        mps, 16 Sep 2013: - Now create AOD difference plots
;                          - Now create AOD map plots
;        mps, 18 Nov 2013: - Read in Model_1 and Model_2 as printed in input
;                            file. Previously, model names were converted to
;                            filename extensions using CTM_NamExt.
;                          - Update GetSfc500Levels to accept model names
;                          - Rename all instances of GEOS57_47L to GEOSFP_47L
;        mps, 02 Dec 2013: - Now create difference plots for 2-D and 3-D
;                            met fields
;        mps, 03 Dec 2013: - Now create absolute difference plots for J-values
;        mps, 21 Apr 2015: - Now create emission maps and emission difference
;                            plots
;        mps, 11 Sep 2015: - Now create stratospheric benchmark plots showing
;                            zonal mean differences and concentrations for
;                            100-0.01 hPa
;        mps, 04 Mar 2016: - Include MERRA2 in the check for equivalent
;                            vertical grids
;        mps, 29 Mar 2016: - Add plots for cloud optical depth
;
;-
; Copyright (C) 2007-2013,
; Bob Yantosca and Philippe Le Sager, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.harvard.edu with subject "IDL routine benchmark_1mon"
;-----------------------------------------------------------------------

function DynOutFile, File

   ;====================================================================
   ; Internal function DynOutFile appends the the extension 
   ; '.dyn_range' to a PostScript filename in order to denote
   ; that the filename contains the dynamic range of data, as opposed
   ; to the "small" range of data. (bmy, 11/14/07)
   ;====================================================================

   ; Add text and return
   return, StrMid( File, 0, StrLen( File )-3L ) + '.dyn_range.ps' 
end

;------------------------------------------------------------------------------

function GetSfc500Levels, ModelName

   ;====================================================================
   ; Internal function GetSfc500Level returns the surface and 500hPa 
   ; levels, depending on the met field type (bmy, 11/14/07)
   ;====================================================================

   ; Surface level
   Level_Sfc  = 1

   ; 500hPa level
   case ( ModelName ) of
      'GEOSFP'     : Level_500 = 23
      'GEOSFP_47L' : Level_500 = 23
      'MERRA'      : Level_500 = 23
      'MERRA_47L'  : Level_500 = 23
      'MERRA2'     : Level_500 = 23
      'MERRA2_47L' : Level_500 = 23
      'GEOS5'      : Level_500 = 23
      'GEOS5_47L'  : Level_500 = 23
      'GEOS4_30L'  : Level_500 = 8
      else         : Level_500 = -1
   endcase

   ; Return to calling program
   return, [ Level_Sfc, Level_500 ]
end

;-----------------------------------------------------------------------------

pro BenchMark_1Mon, InputFile,                    $
                    Debug        = Debug,         $
                    DynRange     = DynRange,      $
                    No_Budget    = No_Budget,     $
                    No_Conc_Maps = No_Conc_Maps,  $
                    No_Diffs     = No_Diffs,      $
                    No_Emissions = No_Emissions,  $
                    No_Freq_Dist = No_Freq_Dist,  $
                    No_FullChem  = No_FullChem,   $
                    No_Jvalues   = No_Jvalues,    $
                    No_JVDiffs   = No_JVDiffs,    $
                    No_JVMaps    = No_JVMaps,     $
                    No_Profiles  = No_Profiles,   $
                    No_Ratios    = No_Ratios,     $ 
                    No_StratDiff = No_StratDiff,  $
                    No_StratConc = No_StratConc,  $
                    No_ZonalDiff = No_ZonalDiff,  $
                    No_ZonalConc = No_ZonalConc,  $
                    No_AOD_Diffs = No_AOD_Diffs,  $
                    No_AOD_Maps  = No_AOD_Maps,   $                    
                    No_CloudDiff = No_CloudDiff,  $
                    No_2D_Met    = No_2D_Met,     $
                    No_3D_Met    = No_3D_Met,     $
                    _EXTRA=e
   
   ;====================================================================
   ; Initialization 
   ;====================================================================

   ; External Functions
   FORWARD_FUNCTION CTM_NamExt,    CTM_Type,  Nymd2Tau, $
                    Replace_Token, StruAddVar

   ; Arguments
   if ( N_Elements( InputFile ) eq 0 ) then InputFile = 'input_bm.1mon'

   ; Keywords
   Debug        =  Keyword_Set( Debug        )
   DynRange     =  Keyword_Set( DynRange     )
   Do_Budget    = ~Keyword_Set( No_Budget    )
   Do_Conc_Maps = ~Keyword_Set( No_Conc_Maps ) 
   Do_Diffs     = ~Keyword_Set( No_Diffs     )
   Do_Emissions = ~Keyword_Set( No_Emissions )
   Do_Freq_Dist = ~Keyword_Set( No_Freq_Dist )
   Do_FullChem  = ~Keyword_Set( No_FullChem  )
   Do_Jvalues   = ~Keyword_Set( No_Jvalues   )
   Do_JVDiffs   = ~Keyword_Set( No_JVDiffs   )
   Do_JVMaps    = ~Keyword_Set( No_JVMaps    )
   Do_Profiles  = ~Keyword_Set( No_Profiles  )
   Do_Ratios    = ~Keyword_Set( No_Ratios    )
   Do_StratDiff = ~Keyword_Set( No_StratDiff )
   Do_StratConc = ~Keyword_Set( No_StratConc )
   Do_Zonal     = ~Keyword_Set( No_ZonalDiff )
   Do_ZonalConc = ~Keyword_Set( No_ZonalConc )
   Do_AOD_Diffs = ~Keyword_Set( No_AOD_Diffs )
   Do_AOD_Maps  = ~Keyword_Set( No_AOD_Maps  )
   Do_CloudDiff = ~Keyword_Set( No_CloudDiff )
   Do_2D_Met    = ~Keyword_set( No_2D_Met    )
   Do_3D_Met    = ~Keyword_set( No_3D_Met    )

   ; Read the default ranges for difference plots
   Read_Diff_Range, File_Which( 'diff_range.1mon' )
   
   ;====================================================================
   ; Read default settings from INPUTFILE
   ;====================================================================

   ; Open the input file
   Open_File, InputFile, Ilun, /Get_Lun

   ; Define string variable
   Line = ''
   
   ; Loop thru file
   while ( not EOF( Ilun ) ) do begin

      ; Read line and break on colon
      ReadF, Ilun, Line
      Result = StrTrim( StrBreak( Line, ':' ), 2 )

      ; Parse value of line into individual variables
      case ( StrUpCase( Result[0] ) ) of 
         'VERSION_1'   : Version_1   = Result[1]
         'VERSION_2'   : Version_2   = Result[1]
         'MODEL_1'     : Model_1     = Result[1]
         'MODEL_2'     : Model_2     = Result[1]
         'DATE_1'      : Tau0_1      = Nymd2Tau( Long( Result[1] ) )
         'DATE_2'      : Tau0_2      = Nymd2Tau( Long( Result[1] ) )
         'N_TRACERS_1' : N_Tracers_1 = Long( Result[1] )
         'N_TRACERS_2' : N_Tracers_2 = Long( Result[1] )
         'RUNDIR_1'    : RunDir_1    = Result[1]
         'RUNDIR_2'    : RunDir_2    = Result[1]
         'FILE_1'      : File_1      = Result[1]
         'FILE_2'      : File_2      = Result[1]
         'INITFILE_2'  : InitFile_2  = Result[1]
         'FINALFILE_2' : FinalFile_2 = Result[1]
         'OUTPUTDIR'   : OutPutDir   = Result[1]
         'MAX_ALT_KM'  : Max_Alt_Km  = Long( Result[1] )
         'BUDGET'      : Budget      = Result[1] 
         'CONC_MAPS'   : Conc_Plots  = Result[1]
         'DIFFERENCES' : Diff_Plots  = Result[1]
         'EMISSIONS'   : Emissions   = Result[1] 
         'FREQ_DIST'   : Freq_Dist   = Result[1]
         'J-VALUES'    : J_Values    = Result[1]
         'JV_DIFFS'    : JV_Diffs    = Result[1]
         'JV_MAPS'     : JV_Maps     = Result[1]
         'PROFILES'    : Prof_Plots  = Result[1] 
         'RATIOS'      : Ratio_Plots = Result[1]
         'STRAT_DIFFS' : Strat_Plots = Result[1] 
         'STRAT_CONC'  : StratC_Plots= Result[1] 
         'ZONAL_DIFFS' : Zonal_Plots = Result[1] 
         'ZONAL'       : Zonal_Plots = Result[1] 
         'ZONALCONC'   : ZonalC_Plots= Result[1] 
         'ZONAL_CONC'  : ZonalC_Plots= Result[1] 
         'AOD_DIFFS'   : AOD_Diffs   = Result[1]
         'AOD_MAPS'    : AOD_Maps    = Result[1]
         'CLOUD_DIFFS' : Cloud_Diffs = Result[1]
         'DIFF_2D_MET' : Diff_2D_Met = Result[1]
         'DIFF_3D_MET' : Diff_3D_Met = Result[1]
         else          : ; Do nothing
      endcase
   endwhile

   ; Close input filename
   Close,    Ilun
   Free_Lun, Ilun

   ;----------------
   ; Error checks!
   ;----------------

   ; Skip error check if we are comparing two different model types
   if ( Do_Freq_Dist ) then begin

      ; Can't plot if the vertical grids are different!
      if ( Model_1 ne Model_2 ) then begin
         
         ; First determine if both grids are equivalent even if the
         ; model names differ (e.g. GEOS-5.2, MERRA, GEOS-5.7)
         Models = [ Model_1, Model_2 ]
         F0     = ( Where( Models eq 'GEOS5_47L'  ) ge 0 )
         F1     = ( Where( Models eq 'MERRA_47L'  ) ge 0 )
         F2     = ( Where( Models eq 'GEOSFP_47L' ) ge 0 )
         F3     = ( Where( Models eq 'MERRA2_47L' ) ge 0 )

         ; If both grids are not equivalent, then stop w/ error message
         if ( ( F0 + F1 + F2 + F3 ) ne 2 ) then  begin

            ; Now check models using native grid
            F4     = ( Where( Models eq 'GEOS5'  ) ge 0 )
            F5     = ( Where( Models eq 'MERRA'  ) ge 0 )
            F6     = ( Where( Models eq 'GEOSFP' ) ge 0 )
            F7     = ( Where( Models eq 'MERRA2' ) ge 0 )

            if ( ( F4 + F5 + F6 + F7 ) ne 2 ) then  begin
               Message, $
               'The two models have different vertical grids!  Cannot plot!'
            endif
         endif
      endif

       ; Can't plot if the # of tracers are different!
      if ( N_Tracers_1 ne N_Tracers_2 ) then begin
         Message, 'The two models have a different # of tracers!  Cannot plot!'
      endif

   endif

   ;====================================================================
   ; Create directory, filename, and other relevant variables
   ;====================================================================

   ; Define structure for token replacement
   Token       = { VERSION_1:Version_1,  VERSION_2:Version_2, $
                   MODEL_1  :Model_1,    MODEL_2  :Model_2   }

   ; Replace tokens in run directory variables
   RunDir_1    = Replace_Token( RunDir_1, Token )
   RunDir_2    = Replace_Token( RunDir_2, Token )

   ; Add run directory variables to TOKEN structure
   Token       = StruAddVar( Token, { RUNDIR_1:RunDir_1, RUNDIR_2:RunDir_2 } )

   ; Replace tokens in output directory variable
   OutputDir   = Replace_Token( OutputDir,   Token )

   ; Add output directory to TOKEN structure
   Token       = StruAddVar( Token, { OUTPUTDIR:OutputDir } )

   ; Replace tokens in the rest of the variables
   File_1      = Replace_Token( File_1,      Token ) 
   File_2      = Replace_Token( File_2,      Token ) 
   InitFile_2  = Replace_Token( InitFile_2,  Token ) 
   FinalFile_2 = Replace_Token( FinalFile_2, Token ) 
   Budget      = Replace_Token( Budget,      Token )
   Conc_Plots  = Replace_Token( Conc_Plots,  Token )
   Diff_Plots  = Replace_Token( Diff_Plots,  Token )
   Emissions   = Replace_Token( Emissions,   Token )
   Freq_Dist   = Replace_Token( Freq_Dist,   Token )
   J_Values    = Replace_Token( J_Values,    Token )
   JV_Diffs    = Replace_Token( JV_Diffs,    Token )
   JV_Maps     = Replace_Token( JV_Maps,     Token )
   Prof_Plots  = Replace_Token( Prof_Plots,  Token )
   Ratio_Plots = Replace_Token( Ratio_Plots, Token )
   Strat_Plots = Replace_Token( Strat_Plots, Token )
   StratC_Plots= Replace_Token( StratC_Plots,Token )
   Zonal_Plots = Replace_Token( Zonal_Plots, Token )
   ZonalC_Plots= Replace_Token( ZonalC_Plots,Token )
   AOD_Diffs   = Replace_Token( AOD_Diffs,   Token )
   AOD_Maps    = Replace_Token( AOD_Maps,    Token )
   Cloud_Diffs = Replace_Token( Cloud_Diffs, Token )
   Diff_2D_Met = Replace_Token( Diff_2D_Met, Token )
   Diff_3D_Met = Replace_Token( Diff_3D_Met, Token )

   ; Altitude range (km) for difference profile plots
   AltRange    = [ 0L, Max_Alt_Km ]

   ; Surface and 500hPa levels from both models ( sfc1, sfc2, 500_1, 500_2 )
   Levels_1    = GetSfc500Levels( Model_1 )
   Levels_2    = GetSfc500Levels( Model_2 )
   Levels      = [ Levels_1[0], Levels_2[0], Levels_1[1], Levels_2[1] ]

   ; TAU0 values
   Taus        = [ Tau0_1, Tau0_2 ]

   ; File names
   Files       = [ File_1, File_2 ]

   ; Tracer list
   Tracers     = LindGen( N_Tracers_1 ) + 1L

   ; Model versions
   Versions    = [ Version_1, Version_2 ]

   ;====================================================================
   ; Debug output
   ;====================================================================
   if ( Debug ) then begin

      print, '%%% 1st Model %%%'
      print, 'Version_1   : ', Version_1
      print, 'Model_1     : ', Model_1     
      print, 'Tau0_1      : ', Tau0_1       
      print, 'N_Tracers_1 : ', N_Tracers_1
      print, 'RunDir_1    : ', RunDir_1    
      print, 'File_1      : ', File_1      
      print
      print, '%%% 2nd Model %%%'      
      print, 'Version_2   : ', Version_2
      print, 'Model_2     : ', Model_2     
      print, 'Tau0_2      : ', Tau0_2       
      print, 'N_Tracers_2 : ', N_Tracers_2 
      print, 'RunDir_2    : ', RunDir_2    
      print, 'File_2      : ', File_2      
      print, 'InitFile_2  : ', InitFile_2  
      print, 'FinalFile_2 : ', FinalFile_2
      print
      print, '%%% For Plotting %%%'
      print, 'OutputDir   : ', OutputDir   
      print, 'Altrange    : ', Altrange
      print, 'Budget      : ', Budget  
      print, 'Conc_Plots  : ', Conc_Plots
      print, 'Diff_Plots  : ', Diff_Plots
      print, 'Emissions   : ', Emissions  
      print, 'Freq_Dist   : ', Freq_Dist  
      print, 'J_Values    : ', J_Values   
      print, 'JV_Diffs    : ', JV_Diffs   
      print, 'JV_Maps     : ', JV_Maps   
      print, 'Prof_Plots  : ', Prof_Plots
      print, 'Ratio Plots : ', Ratio_Plots 
      print, 'Strat_Diffs : ', Strat_Plots
      print, 'StratC_Plots: ', StratC_Plots
      print, 'Zonal_Diffs : ', Zonal_Plots
      print, 'ZonalC_Plots: ', ZonalC_Plots
      print, 'AOD_Diffs   : ', AOD_Diffs
      print, 'AOD_Maps    : ', AOD_Maps
      print, 'Cloud_Diffs : ', Cloud_Diffs
      print, 'Diff_2D_Met : ', Diff_2D_Met
      print, 'Diff_3D_Met : ', Diff_3D_Met

   endif

   ;====================================================================
   ; Use the metadata from the diaginfo.dat and tracerinfo.dat
   ; files contained in the run directory.  Refresh every time.
   ;====================================================================

   ; File names 
   DInfo = StrTrim( RunDir_2, 2 ) + '/diaginfo.dat'
   TInfo = StrTrim( RunDir_2, 2 ) + '/tracerinfo.dat' 

   ; Load metadata from diaginfo.dat (if we find it)
   if ( File_Exist( Dinfo ) )                                           $
      then CTM_DiagInfo, FileName=DInfo, /Force                         $ 
      else Message, 'Could not find the proper diaginfo.dat file!'

   ; Load metadata from tracerinfo.dat (if we find it)
   if ( File_Exist( Tinfo ) )                                           $
      then CTM_TracerInfo, FileName=Tinfo, /Force                       $ 
      else Message, 'Could not find the proper diaginfo.dat file!'

   ;====================================================================
   ; Make the plots
   ;====================================================================

   ;--------------------------------------------------------------------
   ; Frequency distribution
   ;--------------------------------------------------------------------
   if ( Do_Freq_Dist ) then begin

      ; Echo info
      Message, 'Generating frequency distributions ...', /Info

      ; Create frequency distribution plot
      Freq_Dist, Files, Taus, Tracers, Versions,                        $
                 /LVarTrop,  Do_FullChem=Do_FullChem,                   $
                 /PS,        OutFile=Freq_Dist,                         $
                 _EXTRA=e                                               
                                                                        
   endif                                                                

   ;--------------------------------------------------------------------
   ; J-value ratios @ sfc & 500hPa                                      
   ;--------------------------------------------------------------------
   if ( Do_Jvalues AND Do_FullChem ) then begin
                                                  
      ; Echo info                                                       
      Message, 'Generating J-value ratio maps ...', /Info               
                                                                        
      ; Create J-value plots with "small" data range                    
      JV_Ratio, Files, Levels, Taus, Versions,                          $
                /PS, OutFile=J_Values                                   
      
      ;-----------------------------------------------------------------------
      ;%%% Until further notice, don't create dynamic range ratio plots
      ;%%% (bmy, 6/8/11)
      ;
      ;; Create J-value plots with full dynamic range (if necessary)     
      ;if ( DynRange ) then begin                                        
      ;   JV_Ratio, Files, Levels, Taus, Versions,                       $
      ;             /DynRange,                                           $
      ;             /PS,                                                 $
      ;             OutFile = DynOutFile( J_Values )
      ;endif
      ;-----------------------------------------------------------------------
      
   endif

   ;--------------------------------------------------------------------
   ; J-value differences @ sfc & 500hPa                                      
   ;--------------------------------------------------------------------
   if ( Do_JVDiffs AND Do_FullChem ) then begin
                                                  
      ; Echo info                                                       
      Message, 'Generating J-value difference maps ...', /Info               
                                                                        
      ; Create J-value difference plots
      JV_Diff, Files, Levels, Taus, Versions, /DynRange,                 $
                /PS, OutFile=JV_Diffs                                   
      
   endif

   ;--------------------------------------------------------------------
   ; J-value maps @ sfc & 500hPa                                      
   ;--------------------------------------------------------------------
   if ( Do_JVMaps AND Do_FullChem ) then begin
                                                  
      ; Echo info                                                       
      Message, 'Generating J-value maps ...', /Info               
                                                                        
      ; Create J-value difference plots
      JV_Map, Files[1], Levels, Taus[1], Versions[1],  $
              /PS, OutFile=JV_Maps                                   
      
   endif

   ;--------------------------------------------------------------------
   ; Tracer ratios @ sfc & 500hPa
   ;--------------------------------------------------------------------
   if ( Do_Ratios ) then begin
   
      ; Echo info
      Message, 'Generating tracer ratio maps ...', /Info
      
      ; Create ratio plots with "small" data range
      Ratios, Files, Levels, Taus, Tracers, Versions, /PS,               $
               OutFile=Ratio_Plots, Do_FullChem=Do_FullChem, _EXTRA=e
                 
      ;-----------------------------------------------------------------------
      ;%%% Until further notice, don't create dynamic range ratio plots 
      ;%%% (bmy, 6/8/11)
      ;
      ;; Create ratio plots with full dynamic range (if necessary)       
      ;if ( DynRange ) then begin                                        
      ;   Ratios, Files, Levels, Taus, Tracers, Versions,                $
      ;           /DynRange, Do_FullChem=Do_FullChem,                    $
      ;           /PS,       OutFile=DynOutFile( Ratio_Plots ),          $
      ;           _EXTRA=e
      ;endif
      ;-----------------------------------------------------------------------

   endif

   ;--------------------------------------------------------------------
   ; Tracer abs diff @ sfc & 500hPa
   ;--------------------------------------------------------------------
   if ( Do_Diffs ) then begin

      ; Echo info
      Message, 'Generating tracer difference maps ...', /Info
   
      ; Create diff plots with "small" data range
      Differences, Files, Levels, Taus, Tracers, Versions,              $
                   Do_FullChem=Do_FullChem, /PS,                        $
                   OutFile=Diff_Plots,      _EXTRA=e                    
                                                                        
      ; Create diff plots with full dynamic range (if necessary)        
      if ( DynRange ) then begin                                        
         Differences, Files, Levels, Taus, Tracers, Versions,           $
                      /DynRange, Do_FullChem=Do_FullChem,               $
                      /PS,       OutFile=DynOutFile( Diff_Plots )
      endif

   endif
   
   ;--------------------------------------------------------------------
   ; Tracer concentration maps  @ sfc & 500hPa
   ;--------------------------------------------------------------------
   if ( Do_Conc_Maps ) then begin

      ; Echo info
      Message, 'Generating tracer concentration maps ...', /Info

      ; Create tracer concentration maps at surface and 500 hPa
      Maps, Files[1], Levels, Taus[1], Tracers, Versions[1],            $
            Do_FullChem=Do_FullChem,  /PS,                              $
            OutFile=Conc_Plots,       _EXTRA=e

   endif

   ;--------------------------------------------------------------------
   ; Tracer profiles along 15S and 42N
   ;--------------------------------------------------------------------
   if ( Do_Profiles ) then begin
   
      ; Echo info
      Message, 'Generating difference profiles along 15S and 42N ...', /Info
   
      ; Create lon diff profile plots w/ "small" data range
      Profiles, Files, AltRange, Taus, Tracers, Versions,               $
                Do_FullChem=Do_FullChem, /PS,                           $
                OutFile=Prof_Plots,      _EXTRA=e                       
                                                                        
      ; Create diff plots with full dynamic range (if necessary)        
      if ( DynRange ) then begin                                        
         Profiles, Files, AltRange, Taus, Tracers, Versions,            $
                   /DynRange, Do_FullChem=Do_FullChem,                  $
                   /PS,       OutFile=DynOutFile( Prof_Plots ),         $
                   _EXTRA=e
      endif
   
   endif

   ;--------------------------------------------------------------------
   ; Zonal mean difference maps 
   ;--------------------------------------------------------------------
   if ( Do_Zonal ) then begin

      ; Echo info
      Message, 'Generating zonal mean difference maps ...', /Info

      ; Create lon diff profile plots w/ "small" data range
      Zonal_Diff, Files, Taus, Tracers, Versions,                       $
                  Do_FullChem=Do_FullChem, /PS,                         $
                  OutFile=Zonal_Plots,     /Pressure,                   $
                  _EXTRA=e                       
                                                            
      ; Create diff plots with full dynamic range (if necessary)        
      if ( DynRange ) then begin   
         Zonal_Diff, Files, Taus, Tracers, Versions,                    $
                    /DynRange, Do_FullChem=Do_FullChem,                 $
                    /PS,       OutFile=DynOutFile( Zonal_Plots ),       $
                    /Pressure, _EXTRA=e
      endif

   endif

   ;--------------------------------------------------------------------
   ; Zonal mean concentration plots
   ;--------------------------------------------------------------------
   if ( Do_ZonalConc ) then begin

      ; Echo info
      Message, 'Generating zonal mean concentration plots ...', /Info

      Lons = 0

      ; Create zonal mean concentration plots
      Zonal, Files[1], Lons, Taus[1], Tracers, Versions[1],             $
                  Do_FullChem=Do_FullChem, /PS,                         $
                  OutFile=ZonalC_Plots,    /Pressure,                   $
                  _EXTRA=e
                                                            
   endif

   ;--------------------------------------------------------------------
   ; Stratospheric zonal mean difference maps 
   ;--------------------------------------------------------------------
   if ( Do_StratDiff ) then begin

      ; Echo info
      Message, 'Generating stratospheric difference maps ...', /Info

      ; Create lon diff profile plots w/ "small" data range
      Strat_Diff, Files, Taus, Tracers, Versions,                       $
                  Do_FullChem=Do_FullChem, /PS,                         $
                  OutFile=Strat_Plots,     _EXTRA=e

      ; Create diff plots with full dynamic range (if necessary)
      if ( DynRange ) then begin
         Strat_Diff, Files, Taus, Tracers, Versions,                    $
                    /DynRange, Do_FullChem=Do_FullChem,                 $
                    /PS,       OutFile=DynOutFile( Strat_Plots ),       $
                    _EXTRA=e
      endif

   endif

   ;--------------------------------------------------------------------
   ; Stratospheric zonal mean concentration plots
   ;--------------------------------------------------------------------
   if ( Do_StratConc ) then begin

      ; Echo info
      Message, 'Generating stratospheric concentration plots ...', /Info

      Lons = 0

      ; Create zonal mean concentration plots
      Strat, Files[1], Lons, Taus[1], Tracers, Versions[1],             $
                  Do_FullChem=Do_FullChem, /PS,                         $
                  OutFile=StratC_Plots,    _EXTRA=e

   endif

   ;--------------------------------------------------------------------
   ; Emission totals and maps
   ;--------------------------------------------------------------------
   if ( Do_Emissions AND Do_FullChem ) then begin

      ; Echo info
      Message, 'Generating emission totals ...', /Info
      
      ; Compute table of emissions sums
      FullChem_Emissions, FileNew=File_2,          FileOld=File_1,          $
                          Version_New=Versions[1], Version_Old=Versions[0], $
                          OutFileName=Emissions

      ; Echo info
      Message, 'Generating emission maps ...', /Info

      Emission_Maps, Files[1], Taus[1],          Versions[1],        $
                     /PS,      OutDir=OutputDir, _EXTRA=e


      ; Echo info
      Message, 'Generating emission ratio maps ...', /Info

      ; Compute emission ratio maps
      Emission_Ratios, Files, Taus,             Versions,            $
                       /PS,   OutDir=OutputDir, _EXTRA=e


      ; Echo info
      Message, 'Generating emission difference maps ...', /Info

      Emission_Differences, Files, Taus,             Versions,       $
                            /PS,   OutDir=OutputDir, _EXTRA=e

   endif

   ;--------------------------------------------------------------------
   ; Budget printout
   ;--------------------------------------------------------------------
   if ( Do_Budget AND Do_FullChem ) then begin
   
      ; Echo info
      Message, 'Generating budgets ...', /Info

      ; Create budget of Ox & CO, mean OH concentration, and MCF lifetime
      FullChem_Budget, Tau0=Taus[1],           FileNew=File_2,          $
                       InitialFile=InitFile_2, FinalFile=FinalFile_2,   $
                       OutFile=Budget,         _EXTRA=e

   endif

   ;--------------------------------------------------------------------
   ; AOD difference maps at surface and 500 hPa
   ;--------------------------------------------------------------------

   if ( Do_AOD_Diffs ) then begin

      ; Echo info
      Message, 'Generating AOD difference maps ...',  /Info

      ; Read the default plot ranges 
      Read_Diff_Range, File_Which( 'aod_diff_range.1mon' )

      ; Define tracers
      Tracers  = [ 4, 6, 9, 12, 15, 18 ]

      ; Create the profile plots
      Differences_AOD, Files, Taus, Tracers, Versions,          $
                       /PS, OutFileName=AOD_Diffs, _EXTRA=e

      ; Create diff plots with full dynamic range (if necessary)        
      if ( DynRange ) then begin                                        
         Differences_AOD, Files, Taus, Tracers, Versions,       $
                          /DynRange, /PS, OutFile=DynOutFile( AOD_Diffs )
      endif

   endif

   ;--------------------------------------------------------------------
   ; AOD difference maps at surface and 500 hPa
   ;--------------------------------------------------------------------

   if ( Do_AOD_Maps ) then begin

      ; Echo info
      Message, 'Generating AOD concentration maps ...',  /Info

      ; Read the default plot ranges 
      Read_Conc_Range, File_Which( 'aod_conc_range.1mon' )

      ; Define tracers
      Tracers  = [ 4, 6, 9, 12, 15, 18 ]

      ; Create the profile plots
      Maps_AOD, Files, Taus, Tracers, Versions,                 $
                /PS, OutFileName=AOD_Maps, _EXTRA=e

   endif

   ;--------------------------------------------------------------------
   ; Cloud optical depth differences
   ;--------------------------------------------------------------------
   if ( Do_CloudDiff ) then begin

      ; Echo info
      Message, 'Generating cloud optical depth difference maps ...', /Info

      ; Define tracers
      Tracers = [ 1, 2 ]
      
      ; Create diff plots with full dynamic range
      Diff_Clouds, Files, Taus, Tracers, Versions,                   $
                   /DynRange, Do_FullChem=Do_FullChem,               $
                   /PS,       OutFile=Cloud_Diffs

   endif
   
   ;--------------------------------------------------------------------
   ; 2D Met field differences
   ;--------------------------------------------------------------------
   if ( Do_2D_Met ) then begin

      ; Echo info
      Message, 'Generating 2D met field difference maps ...', /Info

      ; Define tracers
      Tracers = LindGen( 23 ) + 1L
      
      ; Create diff plots with full dynamic range
      Diff_2D_Met, Files, Taus, Tracers, Versions,                   $
                   /DynRange, /PS, OutFile=DynOutFile( Diff_2D_Met )

   endif

   ;--------------------------------------------------------------------
   ; 3D Met field diffs @ sfc & 500 hPa
   ;--------------------------------------------------------------------
   if ( Do_3D_Met ) then begin

      ; Echo info
      Message, 'Generating 3D met field difference maps ...', /Info

      ; Define tracers
      Tracers = LindGen( 6 ) + 1L
      
      ; Create diff plots with full dynamic range
      Diff_3D_Met, Files, Levels, Taus, Tracers, Versions,           $
                   /DynRange, Do_FullChem=Do_FullChem,               $
                   /PS,       OutFile=DynOutFile( Diff_3D_Met )

   endif

   ;====================================================================
   ; Cleanup and Quit
   ;====================================================================
Quit:

end
