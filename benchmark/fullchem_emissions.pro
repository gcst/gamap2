;-----------------------------------------------------------------------
;+
; NAME:
;        FULLCHEM_EMISSIONS
;
; PURPOSE:
;        Prints totals of GEOS-CHEM emission species for two different
;        model versions.  Also prints the difference in emission 
;        totals between the two model versions.
;
; CATEGORY:
;        Benchmarking
;
; CALLING SEQUENCE:
;        FULLCHEM_EMISSIONS [ , Keywords ]
;
; INPUTS:
;        None
;
; KEYWORD PARAMETERS:
;        FILENEW -> Name of a binary punch file containing model
;             output from a "New" version of the GEOS-Chem   
;
;        FILEOLD -> Name of a binary punch file containing model
;             output from a "Old" version of the GEOS-Chem. 
;
;        VERSION_NEW -> String that specifies the GEOS-Chem version 
;             number pertaining to FILENEW.  If not specified, then 
;             FULLCHEM_EMISSIONS will look for this at the end of
;             the filename FILENEW.
;
;        VERSION_OLD -> String that specifies the GEOS-Chem version 
;             number pertaining to FILEOLD  If not specified, then 
;             FULLCHEM_EMISSIONS will look for this at the end of
;             the filename FILEOLD.
;
;        OUTFILENAME -> Name of the text file where emission totals
;             and differences will be sent.  Default is "emissions.txt".
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        Internal Subroutines:
;        ==================================
;        WriteHeader
;        WriteTracers
;
;        External Subroutines Required:
;        ==================================
;        CTM_SUM_EMISSIONS 
;        UNDEFINE
;        STRPAD (function)
; 
; REQUIREMENTS:
;        References routines from both GAMAP and TOOLS packages.
;
; NOTES:
;        (1) Assumes the version number is located at the end of
;            both FILENEW and FILEOLD
;
;        (2) Assumes that both FILENEW and FILEOLD contain the
;            following GEOS-CHEM diagnostic categories:
;            (a) ND06: DUSTSRCE
;            (b) ND07: BC-ANTH,  BC-BIOB,  BC-BIOF,  PL-BC=$,
;                      OC-ANTH,  OC-BIOB,  OC-BIOF,  PL-OC=$
;            (c) ND08: SALTSRCE
;            (d) ND11: ACETSRCE
;            (e) ND13: DMS-BIOG, SO2-AC-$, SO2-AN-$, SO2-EV-$, 
;                      SO2-NV-$, SO4-AN-$, NH3-ANTH, NH3-BIOB, 
;                      NH3-BIOG, NH3-NATU, SO2-SHIP
;            (f) ND28: BIOBSRCE
;            (g) ND29: CO--SRCE
;            (h) ND32: NO-AC-$,  NO-AN-$,  NO-BIOB,  NO-FERT, 
;                      NO-LI-$,  NO-SOIL
;            (i) ND34: BIOFSRCE
;            (j) ND36: ANTHSRCE
;            (k) ND46  BIOGSRCE
;
; EXAMPLE:
;        FULLCHEM_EMISSIONS, FILENEW='ctm.bpch.v10-01e', $
;                            FILEOLD='ctm.bpch.v10-01d', $
;                            OUTFILENAME='emissions.txt'
;
;             ; Prints emissions & differences between 
;             ; versions v10-01d and v10-01e
;                           
; MODIFICATION HISTORY:
;        bmy, 18 Jun 2001: VERSION 1.00
;        bmy, 20 Jun 2001: VERSION 1.01
;                          - now omit ALD2 (#11) from ANTHROPOGENIC
;        bmy, 20 Sep 2001: VERSION 1.02
;                          - now print ND11 acetone sources, sinks
;        bmy, 15 Aug 2002: VERSION 1.03
;                          - renamed to FULLCHEM_EMISSIONS
;                          - renamed FILE_NEW to FILENEW and 
;                            FILE_OLD to FILEOLD
;        bmy, 17 Jan 2003: VERSION 1.04
;                          - also sum up sulfate emission categories
;        bmy, 27 Mar 2003: VERSION 1.05
;                          - adjust FORMAT string for branch versions
;                          - now also print out NH3-NATU source
;        bmy, 09 Apr 2004: VERSION 1.06
;                          - Now print out emissions of BC/OC tracers
;                          - Now print out hydrophilic BC/OC which
;                            came from hydrophobic BC/OC
;        bmy, 28 Apr 2004: VERSION 1.07
;                          - Now print out dust emissions
;        bmy, 03 May 2004: VERSION 1.08
;                          - Now print out seasalt emissions
;        bmy, 21 May 2004: VERSION 1.09
;                          - Now print out ship exhaust SO2 emissions
;        bmy, 08 Jul 2005: VERSION 1.10
;                          - Updated for 43 tracers
;        bmy, 10 Jan 2011: VERSION 1.11
;                          - Now make numeric fields 13 chars wide to
;                            allow for wider title headers
;        bmy, 16 Dec 2011: GAMAP VERSION 2.16
;                          - Remove ACET from dryleaf and ACET from
;                            grass; these are obsolete GEIA quantities
;        mps, 23 Jan 2014: - Now report NH3 emissions in Tg N
;        bmy, 18 Aug 2014: GAMAP VERSION 2.18
;                          - Now display Anthro + Biofuels together
;                            which facilitates use w/ HEMCO emissions
;        bmy, 18 Aug 2014: - Now pass VERSION_NEW and VERSION_OLD as
;                            keywords.  If these are not specified, 
;                            then FULLCHEM_EMISSIONS will obtain these
;                            from the filenames FILENEW and FILEOLD.
;        ewl, 18 Mar 2015: - Replace SO2-BF-$ and SO4-BF-$ with SO2-BIOF
;                            and SO4-BIOF in anthro+biofuel section.
;
;-
; Copyright (C) 2001-2014, Bob Yantosca, Harvard University
; This software is provided as is without any warranty
; whatsoever. It may be freely used, copied or distributed
; for non-commercial purposes. This copyright notice must be
; kept with any copy of this software. If this software shall
; be used commercially or sold as part of a larger package,
; please contact the author.
; Bugs and comments should be directed to bmy@io.harvard.edu
; with subject "IDL routine fullchem_emissions"
;-----------------------------------------------------------------------

;-----------------------------------------------------------------------------
 
pro WriteHeader, Ilun, Title, Version_New, Version_Old 

   ;====================================================================
   ; Internal subroutine WriteHeader writes a header for each emissions
   ; category with a top title and also the version string information.
   ;====================================================================  

   ; Now use wider format string (bmy, 1/10/11)
   Format = '('' Tracer    '',a11,3x,a11,3x,a11,'' - '',a)'

   PrintF, Ilun, Title
   PrintF, Ilun, Version_New, Version_Old, Version_New, Version_Old, $
                 Format=Format
   PrintF, Ilun, '==============================================================='
 
   ; Return to main program
   return
end
 
;-----------------------------------------------------------------------------
 
pro WriteTracers, Ilun, New, Old, Advance=Advance

   ;====================================================================
   ; Internal subroutine WriteTracers writes tracer sums and
   ; differences for a given emissions category.
   ;====================================================================  

   ; Error check
   if ( N_Elements( New ) ne N_Elements( Old ) ) then begin
      Message, 'NEW and OLD do not have the same # of elements!'
   endif
 
   ; Write totals & difference to OUTFILENAME
   for N = 0L, N_Elements( New ) - 1L do begin

      ; Pad Name to 8 spaces
      Name = StrPad( New[N].Name, 8 )

      ; Print the data
      PrintF, Ilun, Name, New[N].Sum, Old[N].Sum,  $
                    New[N].Sum - Old[N].Sum, New[N].Unit, $
                    Format='(a8,2(1x,f13.6),3x,f13.6,3x,a6)'
   endfor

   ; Write some spacers -- if /ADVANCE is set
   if ( Keyword_Set( Advance ) ) then begin
      PrintF, Ilun
      PrintF, Ilun
   endif

   ; Return to main program
   return
end
 
;-----------------------------------------------------------------------------
  
pro FullChem_Emissions, FileNew     = FileNew,     $
                        FileOld     = FileOld,     $
                        Version_New = Version_New, $
                        Version_Old = Version_Old, $
                        OutFileName = OutFileName

   ;====================================================================
   ; Initialization
   ;====================================================================
 
   ; Keyword settings
   if ( N_Elements( FileNew ) ne 1 ) then Message, 'FILENEW not passed!'
   if ( N_Elements( FileOld ) ne 1 ) then Message, 'FILEOLD not passed!'

   ; Get new version number (if it is not specified)
   if ( N_Elements( Version_New ) eq 0 ) then Message, 'VERSION_NEW not passed!'

   ; Get old version number (if it is not specified)
   if ( N_Elements( Version_Old ) eq 0 ) then Message, 'VERSION_OLD not passed!'
 
   ; Create default output file name (if it is not specified)
   if ( N_Elements( OutFileName ) ne 1 ) $
      then OutFileName = Version_New + '.emissions.txt'
 
   ; Open ouptut file for printing emissions table data
   Open_File, OutFileName, Ilun, /Write

   ;====================================================================
   ; ANTHROPOGENIC + BIOFUELS
   ;
   ; NOTE: HEMCO lumps anthropogenic & biofuels together, so it is
   ; better to compare the sum of these rather than individual totals.
   ;====================================================================

   ;%%%%%% Anthropogenic + Biofuel: ND36 tracers %%%%%%

   Tracer = [ 1, 4, 5, 9, 10, 11, 18, 19, 20, 21, 67, 68, 69, 94 ]
 
   CTM_Sum_Emissions, 'ANTHSRCE', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=ANew   ; Anthro, new

   CTM_Sum_Emissions, 'ANTHSRCE', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=AOld   ; Anthro, old

   CTM_Sum_Emissions, 'BIOFSRCE', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=BNew   ; Biofuel, new

   CTM_Sum_Emissions, 'BIOFSRCE', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=BOld   ; Biofuel, old

   ; Sum anthro + biofuel together
   New = ANew
   Old = AOld
   for N = 0L, N_Elements( Tracer )-1L do begin
      New[N].Sum  = ANew[N].Sum + BNew[N].Sum
      Old[N].Sum  = AOld[N].Sum + BOld[N].Sum
   endfor
 
   ; Write to log file
   WriteHeader,  Ilun, 'ANTHROPOGENIC + BIOFUEL', Version_New, Version_Old 
   WriteTracers, Ilun, New, Old
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old
   UnDefine, ANew
   UnDefine, AOld
   UnDefine, BNew
   UnDefine, BOld

   ;%%%%%% Anthro SO2 + Biofuel SO2 %%%%%%
   Tracer = [ 26 ]

   CTM_Sum_Emissions, 'SO2-AN-$',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=ANew,  /No_Sec,       /Kg                      ; Anthro SO2, new

   CTM_Sum_Emissions, 'SO2-AN-$',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=AOld,  /No_Sec,       /Kg                      ; Anthro SO2, old
 
   CTM_Sum_Emissions,  'SO2-BIOF',            $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=BNew,  /No_Sec,       /Kg                      ; Biofuel SO2, new 

   CTM_Sum_Emissions,  'SO2-BIOF',            $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=BOld,  /No_Sec,       /Kg                      ; Biofuel SO2, old
 
   ; Sum anthro + biofuel together
   New        = ANew
   Old        = AOld
   New[0].Sum = ANew[0].Sum + BNew[0].Sum
   Old[0].Sum = AOld[0].Sum + BOld[0].Sum
   
   ; Write to log file
   WriteTracers, Ilun, New, Old
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old
   UnDefine, ANew
   UnDefine, AOld
   UnDefine, BNew
   UnDefine, BOld

   ;%%%%%% Anthro SO4 + Biofuel SO4 %%%%%%
   Tracer = [ 27 ]
 
   CTM_Sum_Emissions, 'SO4-AN-$',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=ANew,  /No_Sec,       /Kg                      ; Anthro SO4, new

   CTM_Sum_Emissions, 'SO4-AN-$',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result =AOld, /No_Sec,       /Kg                      ; Anthro SO4, old

   CTM_Sum_Emissions, 'SO4-BIOF',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=BNew,  /No_Sec,       /Kg                      ; Biofuel SO4, new
 
   CTM_Sum_Emissions, 'SO4-BIOF',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=BOld,  /No_Sec,       /Kg                      ; Biofuel SO4, old
 
   ; Sum anthro + biofuel together
   New        = ANew
   Old        = AOld
   New[0].Sum = ANew[0].Sum + BNew[0].Sum
   Old[0].Sum = AOld[0].Sum + BOld[0].Sum

   ; Write to log file
   WriteTracers, Ilun, New, Old
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old
   UnDefine, ANew
   UnDefine, AOld
   UnDefine, BNew
   UnDefine, BOld

   ;%%%%%% Anthro NH3 + Biofuel NH3 %%%%%%
   Tracer = [ 30 ]

   CTM_Sum_Emissions, 'NH3-ANTH',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=ANew,  /No_Sec,       /Kg                      ; Anthro NH3, new 

   CTM_Sum_Emissions, 'NH3-ANTH',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=AOld,  /No_Sec,       /Kg                      ; Anthro NH3, old

   CTM_Sum_Emissions, 'NH3-BIOF',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=BNew,  /No_Sec,       /Kg                      ; Biofuel NH3, new
 
   CTM_Sum_Emissions, 'NH3-BIOF',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=BOld,  /No_Sec,       /Kg                      ; Biofuel NH3, old
  
   ; Sum anthro + biofuel together
   New         = ANew
   Old         = AOld
   New[0].Sum  = ANew[0].Sum + BNew[0].Sum
   Old[0].Sum  = AOld[0].Sum + BOld[0].Sum

   ; Convert from Tg NH3 to Tg N
   New[0].Sum  = New[0].Sum * 14d0/17d0
   Old[0].Sum  = Old[0].Sum * 14d0/17d0
   New[0].Unit = 'Tg N'

   ; Write to log file
   WriteTracers, Ilun, New, Old, /Advance

   ; Clear memory
   UnDefine, New
   UnDefine, Old
   UnDefine, ANew
   UnDefine, AOld
   UnDefine, BNew
   UnDefine, BOld

   ;====================================================================
   ; Print biomass burning totals from old & new versions
   ;====================================================================

   ;%%%%%% ND28 Tracers %%%%%% 
   Tracer = [ 1,   4,  5,  9, 10, 11, 18, 19, 20, 21, 26, 30, 34, 35, $
              67, 68, 69, 70, 94, 95 ]
 
   CTM_Sum_Emissions, 'BIOBSRCE', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=New
 
   CTM_Sum_Emissions, 'BIOBSRCE', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=Old
 
   ; Write to log file
   WriteHeader,  Ilun, 'BIOMASS', Version_New, Version_Old 
   WriteTracers, Ilun, New, Old, /Advance
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;====================================================================
   ; Print biogenic and natural totals from old & new versions
   ;==================================================================== 

   ;%%%%%% Biogenic Emissions: ND46 Tracers %%%%%%
   Tracer = [  1,  2,  3,  4,  5,  7,  8,  9, 10, 11, 12, 13, $
              14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, $
              27, 28, 29  ]

   CTM_Sum_Emissions, 'BIOGSRCE', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=New
 
   CTM_Sum_Emissions, 'BIOGSRCE', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=Old
 
   ; Write to log file
   WriteHeader, Ilun, 'BIOGENIC AND NATURAL SOURCES', Version_New, Version_Old 
   WriteTracers, Ilun, New, Old

   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;%%%%%% Biogenic DMS %%%%%%
   Tracer = [ 25 ]
 
   CTM_Sum_Emissions, 'DMS-BIOG', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=New, /No_Sec, /Kg
 
   CTM_Sum_Emissions, 'DMS-BIOG', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=Old, /No_Sec, /Kg
 
   ; Write to log file
   WriteTracers, Ilun, New, Old
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old 

   ;%%%%%% Natural Source NH3 %%%%%%
   Tracer = [ 30 ]
   
   CTM_Sum_Emissions, 'NH3-NATU', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=New, /No_Sec, /Kg
 
   CTM_Sum_Emissions, 'NH3-NATU', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=Old, /No_Sec, /Kg
 
   ; Convert from Tg NH3 to Tg N
   New[0].Sum  = New[0].Sum * 14d0/17d0
   Old[0].Sum  = Old[0].Sum * 14d0/17d0
   New[0].Unit = 'Tg N'

   ; Write to log file
   WriteTracers, Ilun, New, Old, /Advance
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;====================================================================
   ; Print ACETONE sources from old & new versions
   ;==================================================================== 
   Tracer = [ 1, 2, 3, 4 ]
 
   CTM_Sum_Emissions, 'ACETSRCE', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=New
 
   CTM_Sum_Emissions, 'ACETSRCE', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=Old
 
   ; Write to log file
   WriteHeader,  Ilun, 'ACETONE SOURCES', Version_New, Version_Old 
   WriteTracers, Ilun, New, Old, /Advance
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;====================================================================
   ; Print ACETONE sinks from old & new versions
   ;
   ;%%%% NOTE: HEMCO NEEDS TO BE MODIFIED TO SAVE THIS %%%%
   ;==================================================================== 
;   Tracer = [ 5 ]
; 
;   CTM_Sum_Emissions, 'ACETSRCE', $
;      File=FileNew, Tracer=Tracer, /Cum_Only, Result=New
; 
;   CTM_Sum_Emissions, 'ACETSRCE', $
;      File=FileOld, Tracer=Tracer, /Cum_Only, Result=Old
; 
;   ; Write to log file
;   WriteHeader,  Ilun, 'ACETONE SINKS', Version_New, Version_Old 
;   WriteTracers, Ilun, New, Old, /Advance
; 
;   ; Clear memory
;   UnDefine, New
;   UnDefine, Old

   ;====================================================================
   ; Print CO source totals from old & new versions
   ;==================================================================== 
 
   ;%%%%%% Anthro CO + Biofuel CO %%%%%
   CTM_Sum_Emissions, 'CO--SRCE', $
      File=FileNew, Tracer=1, /Cum_Only, Result=ANew        ; Anthro CO, new
 
   CTM_Sum_Emissions, 'CO--SRCE', $
      File=FileOld, Tracer=1, /Cum_Only, Result=AOld        ; Anthro CO, old

   CTM_Sum_Emissions, 'CO--SRCE', $
      File=FileNew, Tracer=3, /Cum_Only, Result=BNew        ; Biofuel CO, new
 
   CTM_Sum_Emissions, 'CO--SRCE', $
      File=FileOld, Tracer=3, /Cum_Only, Result=BOld        ; Biofuel CO, old
 
   ; Sum anthro + biofuel together
   New         = ANew
   Old         = AOld
   New[0].Sum  = ANew[0].Sum + BNew[0].Sum
   Old[0].Sum  = AOld[0].Sum + BOld[0].Sum
   New[0].Name = 'COan+bf'

   ; Write to log file
   WriteHeader,  Ilun, 'CO SOURCES', Version_New, Version_Old 
   WriteTracers, Ilun, New, Old
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old
   UnDefine, ANew
   UnDefine, AOld
   UnDefine, BNew
   UnDefine, BOld
   
   ;%%%%%% Biomass CO %%%%%%
   CTM_Sum_Emissions, 'CO--SRCE', $
      File=FileNew, Tracer=2, /Cum_Only, Result=New
 
   CTM_Sum_Emissions, 'CO--SRCE', $
      File=FileOld, Tracer=2, /Cum_Only, Result=Old

   New[0].Name = 'CObb'

   ; Write to log file
   WriteTracers, Ilun, New, Old
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;%%%%%% CO from monoterpenes %%%%%%
   CTM_Sum_Emissions, 'CO--SRCE', $
      File=FileNew, Tracer=5, /Cum_Only, Result=New
 
   CTM_Sum_Emissions, 'CO--SRCE', $
      File=FileOld, Tracer=5, /Cum_Only, Result=Old

   New[0].Name = 'COmono'

   ; Write to log file
   WriteTracers, Ilun, New, Old
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;%%%%%% CO from ships %%%%%%
   CTM_Sum_Emissions, 'CO--SRCE', $
      File=FileNew, Tracer=6, /Cum_Only, Result=New
 
   CTM_Sum_Emissions, 'CO--SRCE', $
      File=FileOld, Tracer=6, /Cum_Only, Result=Old

   New[0].Name = 'COship'

   ; Write to log file
   WriteTracers, Ilun, New, Old, /Advance
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;====================================================================
   ; Print NO source totals from old & new versions -- ND32 diag
   ;==================================================================== 
   Tracer = [ 1 ]
 
   ;%%%%%% Aircraft NO %%%%%%
   CTM_Sum_Emissions, 'NO-AC-$', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=New
 
   CTM_Sum_Emissions, 'NO-AC-$', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=Old
 
   New[0].Name = 'NOac'
 
   WriteHeader,  Ilun, 'NOx SOURCES', Version_New, Version_Old 
   WriteTracers, Ilun, New, Old

   UnDefine, New
   UnDefine, Old
 
   ;%%%%%% Anthro NO + Biofuel NO %%%%%%
   CTM_Sum_Emissions, 'NO-AN-$', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=ANew   ; Anthro NO, new

   CTM_Sum_Emissions, 'NO-AN-$', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=AOld   ; Anthro NO, old

   CTM_Sum_Emissions, 'NO-BIOF', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=BNew   ; Biofuel NO, new
 
   CTM_Sum_Emissions, 'NO-BIOF', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=BOld   ; Biofuel NO, old
 
   ; Sum anthro + biofuel together
   New         = ANew
   Old         = AOld
   New[0].Sum  = ANew[0].Sum + BNew[0].Sum
   Old[0].Sum  = AOld[0].Sum + BOld[0].Sum
   New[0].Name = 'NOan+bf'
 
   ; Write to log file
   WriteTracers, Ilun, New, Old

   ; Clear memory
   UnDefine, New
   UnDefine, Old
   UnDefine, ANew
   UnDefine, AOld
   UnDefine, BNew
   UnDefine, BOld

   ;%%%%%% Biomass NO %%%%%%
   CTM_Sum_Emissions, 'NO-BIOB', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=New
 
   CTM_Sum_Emissions, 'NO-BIOB', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=Old
  
   New[0].Name = 'NObb'
 
   ; Write to log file
   WriteTracers, Ilun, New, Old

   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;%%%%%% Fertilizer NO %%%%%%
   CTM_Sum_Emissions, 'NO-FERT', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=New
 
   CTM_Sum_Emissions, 'NO-FERT', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=Old
 
   New[0].Name = 'NOfe'
 
   ; Write to log file
   WriteTracers, Ilun, New, Old

   ; Clear memory
   UnDefine, New
   UnDefine, Old
   
   ;%%%%%% Lightning NO %%%%%%
   CTM_Sum_Emissions, 'NO-LI-$', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=New
 
   CTM_Sum_Emissions, 'NO-LI-$', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=Old
 
   New[0].Name = 'NOli'
 
   ; Write to log file
   WriteTracers, Ilun, New, Old

   ; Clear memory
   UnDefine, New
   UnDefine, Old
 
   ;%%%%%% Soil NO %%%%%%
   CTM_Sum_Emissions, 'NO-SOIL', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=New
 
   CTM_Sum_Emissions, 'NO-SOIL', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=Old
 
   New[0].Name = 'NOso'
 
   ; Write to log file
   WriteTracers, Ilun, New, Old, /Advance
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;====================================================================
   ; DMS emissions
   ;====================================================================
   Tracer = [ 25 ]
 
   CTM_Sum_Emissions, 'DMS-BIOG', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=New, /No_Sec, /Kg
 
   CTM_Sum_Emissions, 'DMS-BIOG', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=Old, /No_Sec, /Kg
 
   New[0].Name = 'DMSbg'

   ; Write to log file
   WriteHeader,  Ilun, 'DMS SOURCES', Version_New, Version_Old 
   WriteTracers, Ilun, New, Old, /Advance
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;====================================================================
   ; SO2 emissions
   ;====================================================================
   Tracer = [ 26 ]

   ;%%%%%% Aircraft SO2 %%%%%%
   CTM_Sum_Emissions, 'SO2-AC-$',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=New,   /No_Sec,       /Kg
 
   CTM_Sum_Emissions, 'SO2-AC-$',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=Old,   /No_Sec,       /Kg
 
   New[0].Name = 'SO2ac'

   ; Write to log file
   WriteHeader,  Ilun, 'SO2 SOURCES', Version_New, Version_Old 
   WriteTracers, Ilun, New, Old
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;%%%%%% Anthro SO2 + Biofuel SO2 %%%%%%
   CTM_Sum_Emissions, 'SO2-AN-$',              $
      File=FileNew, Tracer=Tracer, /Cum_Only,  $
      Result=ANew,  /No_Sec,       /Kg                      ; Anthro SO2, new

   CTM_Sum_Emissions, 'SO2-AN-$',              $
      File=FileOld, Tracer=Tracer, /Cum_Only,  $
      Result=AOld,  /No_Sec,       /Kg                      ; Anthro SO2, old
 
   CTM_Sum_Emissions, 'SO2-BIOF',              $
      File=FileNew, Tracer=Tracer, /Cum_Only,  $
      Result=BNew,  /No_Sec,       /Kg                      ; Biofuel SO2, new
 
   CTM_Sum_Emissions, 'SO2-BIOF',              $
      File=FileOld, Tracer=Tracer, /Cum_Only,  $
      Result=BOld,  /No_Sec,       /Kg                      ; Biofuel SO2, old

   ; Sum anthro + biofuel together
   New         = ANew
   Old         = AOld
   New[0].Sum  = ANew[0].Sum + BNew[0].Sum
   Old[0].Sum  = AOld[0].Sum + BOld[0].Sum
   New[0].Name = 'SO2an+bf'

   ; Write to log file
   WriteTracers, Ilun, New, Old

   ; Clear memory
   UnDefine, New
   UnDefine, Old
   UnDefine, ANew
   UnDefine, AOld
   UnDefine, BNew
   UnDefine, BOld

   ;%%%%%% Biomass SO2 %%%%%%
   CTM_Sum_Emissions, 'SO2-BIOB', $
      File=FileNew, Tracer=Tracer, /Cum_Only, Result=New, /No_Sec, /Kg
 
   CTM_Sum_Emissions, 'SO2-BIOB', $
      File=FileOld, Tracer=Tracer, /Cum_Only, Result=Old, /No_Sec, /Kg
 
   New[0].Name = 'SO2bb'

   ; Write to log file
   WriteTracers, Ilun, New, Old
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;%%%%%% Volcano SO2 (eruptive + non-eruptive) %%%%%%
   
   CTM_Sum_Emissions, 'SO2-EV-$',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=ENew,  /No_Sec,       /Kg                      ; Eruptive, new
 
   CTM_Sum_Emissions, 'SO2-EV-$',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=EOld,  /No_Sec,       /Kg                      ; Eruptive, old

   CTM_Sum_Emissions, 'SO2-NV-$',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=NNew,  /No_Sec,       /Kg                      ; Noneruptive, new
 
   CTM_Sum_Emissions, 'SO2-NV-$',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=NOld,  /No_Sec,       /Kg                      ; Noneruptive, old
 
   ; Sum eruptive and noneruptive together
   New         = ENew
   Old         = EOld
   New[0].Sum  = ENew[0].Sum + NNew[0].Sum
   Old[0].Sum  = EOld[0].Sum + NOld[0].Sum
   New[0].Name = 'SO2volc'

   ; Write to log file
   WriteTracers, Ilun, New, Old
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old
   UnDefine, ENew
   UnDefine, EOld
   UnDefine, NNew
   UnDefine, NOld
 
   ;%%%%%% Ship exhaust SO2 %%%%%%
   CTM_Sum_Emissions, 'SO2-SHIP',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=New,   /No_Sec,       /Kg
 
   CTM_Sum_Emissions, 'SO2-SHIP',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=Old,   /No_Sec,       /Kg
 
   New[0].Name = 'SO2ship'

   ; Write to log file
   WriteTracers, Ilun, New, Old, /Advance

   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;====================================================================
   ; SO4 emissions
   ;====================================================================
   Tracer = [ 27 ]
 
   ;%%%%%% Anthro SO4 + Biofuel SO4 %%%%%
   CTM_Sum_Emissions, 'SO4-AN-$',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=ANew,  /No_Sec,       /Kg                      ; Anthro SO4, new  
 
   CTM_Sum_Emissions, 'SO4-AN-$',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=AOld,  /No_Sec,       /Kg                      ; Anthro SO4, old

   CTM_Sum_Emissions, 'SO4-BIOF',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=BNew,  /No_Sec,       /Kg                      ; Biofuel SO4, new
 
   CTM_Sum_Emissions, 'SO4-BIOF',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=BOld,  /No_Sec,       /Kg                      ; Biofuel SO4, old
 
   ; Sum anthro + biofuel together
   New         = ANew
   Old         = AOld
   New[0].Sum  = ANew[0].Sum + BNew[0].Sum
   Old[0].Sum  = AOld[0].Sum + BOld[0].Sum
   New[0].Name = 'SO4an+bf'

   ; Write to log file
   WriteHeader,  Ilun, 'SO4 SOURCES', Version_New, Version_Old 
   WriteTracers, Ilun, New, Old, /Advance
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old
   UnDefine, ANew
   UnDefine, AOld
   UnDefine, BNew
   UnDefine, BOld

   ;====================================================================
   ; NH3 emissions
   ;====================================================================
   Tracer = [ 30 ]

   ;%%%%%% Anthro NH3 + Biofuel NH3 %%%%%%
   CTM_Sum_Emissions, 'NH3-ANTH',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=ANew,  /No_Sec,       /Kg                      ; Anthro NH3, new
 
   CTM_Sum_Emissions, 'NH3-ANTH',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=AOld,  /No_Sec,       /Kg                      ; Anthro NH3, old
 
   CTM_Sum_Emissions, 'NH3-BIOF',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=BNew,  /No_Sec,       /Kg                      ; Biofuel NH3, new
 
   CTM_Sum_Emissions, 'NH3-BIOF',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=BOld,  /No_Sec,       /Kg                      ; Biofuel NH3, old
 
   ; Sum anthro + biofuel together (convert Tg NH3 to N)
   New         = ANew
   Old         = AOld
   New[0].Sum  = ( ANew[0].Sum + BNew[0].Sum ) * 14d0/17d0
   Old[0].Sum  = ( AOld[0].Sum + BOld[0].Sum ) * 14d0/17d0
   New[0].Name = 'NH3an+bf'
   New[0].Unit = 'Tg N'

   ; Write to log file
   WriteHeader,  Ilun, 'NH3 SOURCES', Version_New, Version_Old 
   WriteTracers, Ilun, New, Old

   ; Clear memory
   UnDefine, New
   UnDefine, Old
   UnDefine, ANew
   UnDefine, AOld
   UnDefine, BNew
   UnDefine, BOld

   ;%%%%%% Biomass NH3 %%%%%%
   CTM_Sum_Emissions, 'NH3-BIOB',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=New,   /No_Sec,       /Kg
 
   CTM_Sum_Emissions, 'NH3-BIOB',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=Old, /No_Sec, /Kg
 
   New[0].Name = 'NH3bb'

   ; Convert from Tg NH3 to Tg N
   New[0].Sum  = New[0].Sum * 14d0/17d0
   Old[0].Sum  = Old[0].Sum * 14d0/17d0
   New[0].Unit = 'Tg N'

   ; Write to log file
   WriteTracers, Ilun, New, Old
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;%%%%%% Natural Source NH3 %%%%%%
   CTM_Sum_Emissions, 'NH3-NATU',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $ 
      Result=New,   /No_Sec,       /Kg
 
   CTM_Sum_Emissions, 'NH3-NATU',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=Old,   /No_Sec,       /Kg
 
   ; Convert from Tg NH3 to Tg N
   New[0].Sum  = New[0].Sum * 14d0/17d0
   Old[0].Sum  = Old[0].Sum * 14d0/17d0
   New[0].Name = 'NH3na'
   New[0].Unit = 'Tg N'

   ; Write to log file
   WriteTracers, Ilun, New, Old, /Advance
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;====================================================================
   ; BLACK CARBON emissions (sum of Hydrophilic and Hydrophobic)
   ;====================================================================

   Tracer = [ 34 ]

   ;%%%%%% Anthro BC + Biofuel BC %%%%%%
   CTM_Sum_Emissions, 'BC-ANTH',              $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=ANew,  /No_Sec,       /Kg                      ; Anthro BC, new
 
   CTM_Sum_Emissions, 'BC-ANTH',              $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=AOld,  /No_Sec,       /Kg                      ; Anthro BC, old

   CTM_Sum_Emissions, 'BC-BIOF',              $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=BNew,  /No_Sec,       /Kg                      ; Biofuel BC, new
 
   CTM_Sum_Emissions, 'BC-BIOF', $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=BOld,  /No_Sec,       /Kg                      ; Biofuel BC, old

   ; Sum anthro + biofuel together
   New         = ANew
   Old         = AOld
   New[0].Sum  = ANew[0].Sum + BNew[0].Sum
   Old[0].Sum  = AOld[0].Sum + BOld[0].Sum
   New[0].Name = 'BCan+bf'

   ; Write to log file
   WriteHeader,  Ilun, 'BLACK CARBON SOURCES', Version_New, Version_Old 
   WriteTracers, Ilun, New, Old
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old
   UnDefine, ANew
   UnDefine, AOld
   UnDefine, BNew
   UnDefine, BOld

   ;%%%%%% Biomass BLKC %%%%%%
   CTM_Sum_Emissions, 'BC-BIOB',              $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=New,   /No_Sec,       /Kg
 
   CTM_Sum_Emissions, 'BC-BIOB', $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=Old,   /No_Sec,       /Kg
 
   New[0].Name = 'BCbb'

   WriteTracers, Ilun, New, Old, /Advance
 
   UnDefine, New
   UnDefine, Old

   ;====================================================================
   ; ORGANIC CARBON emissions (both Hydrophilic and Hydrophobic)
   ;====================================================================
 
   Tracer = [ 35 ]

   ;%%%%%% Anthro OC + Biofuel OC %%%%%%
   CTM_Sum_Emissions, 'OC-ANTH',              $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=ANew,  /No_Sec,       /Kg                      ; Anthro OC, new
 
   CTM_Sum_Emissions, 'OC-ANTH',              $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=AOld,  /No_Sec,       /Kg                      ; Anthro OC, old

   CTM_Sum_Emissions, 'OC-BIOF',              $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=BNew,  /No_Sec,       /Kg                      ; Biofuel OC, new
 
   CTM_Sum_Emissions, 'OC-BIOF',              $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=BOld,  /No_Sec,       /Kg                      ; Biofuel OC, old
 
   ; Sum anthro + biofuel together
   New         = ANew
   Old         = AOld
   New[0].Sum  = ANew[0].Sum + BNew[0].Sum
   Old[0].Sum  = AOld[0].Sum + BOld[0].Sum
   New[0].Name = 'OCan+bf'

   ; Write to log file
   WriteHeader,  Ilun, 'ORGANIC CARBON SOURCES', Version_New, Version_Old 
   WriteTracers, Ilun, New, Old
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old
   UnDefine, ANew
   UnDefine, AOld
   UnDefine, BNew
   UnDefine, BOld

   ;%%%%%% Biomass OC %%%%%%
   CTM_Sum_Emissions, 'OC-BIOB',              $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=New,   /No_Sec,       /Kg
 
   CTM_Sum_Emissions, 'OC-BIOB',              $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=Old,   /No_Sec,       /Kg

   New[0].Name = 'OCbb'

   ; Write to log file
   WriteTracers, Ilun, New, Old
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;%%%%%% Biogenic OC %%%%%%
   CTM_Sum_Emissions, 'OC-BIOG',              $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=New,   /No_Sec,       /Kg
 
   CTM_Sum_Emissions, 'OC-BIOG',              $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=Old,   /No_Sec,       /Kg
 
   New[0].Name = 'OCbg'

   WriteTracers, Ilun, New, Old, /Advance
 
   UnDefine, New
   UnDefine, Old

   ;====================================================================
   ; Hydrophilic from hydrophobic tracer
   ;====================================================================

   ;%%%%%% Converted BC %%%%%%
   Tracer = [ 34 ]

   CTM_Sum_Emissions, 'PL-BC=$',              $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=New,   /No_Sec,       /Kg
 
   CTM_Sum_Emissions, 'PL-BC=$',              $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=Old,   /No_Sec,       /Kg

   New[0].Name = 'BChp'

   ; Write to log file
   WriteHeader,  Ilun, 'HYDROPHILIC TRACER FROM HYDROPHOBIC TRACER', $
                 Version_New, Version_Old 
   WriteTracers, Ilun, New, Old
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;%%%%%% Converted OC %%%%%%
   Tracer = [ 35 ]

   CTM_Sum_Emissions, 'PL-OC=$',              $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=New,   /No_Sec,       /Kg
 
   CTM_Sum_Emissions, 'PL-OC=$',              $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=Old,   /No_Sec,       /Kg
 
   New[0].Name = 'OChp'

   ; Write to log file
   WriteTracers, Ilun, New, Old, /Advance
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;====================================================================
   ; DUST emissions (ND06)
   ;====================================================================
   
   ; Dust tracers
   Tracer = [ 38, 39, 40, 41 ]
 
   ;%%%%%% Dust emissions %%%%%%
   CTM_Sum_Emissions, 'DUSTSRCE',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=New,   /Kg,           /No_Sec
 
   CTM_Sum_Emissions, 'DUSTSRCE',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=Old,   /Kg,           /No_Sec
 
   ; Write to log file
   WriteHeader,  Ilun, 'DUST EMISSIONS', Version_New, Version_Old 
   WriteTracers, Ilun, New, Old, /Advance
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;====================================================================
   ; SEASALT emissions (ND08)
   ;====================================================================

   ; Sea salt tracers
   Tracer = [ 42, 43 ]
 
   ;%%%%%% Sea salt emissions %%%%%%
   CTM_Sum_Emissions, 'SALTSRCE',             $
      File=FileNew, Tracer=Tracer, /Cum_Only, $
      Result=New,   /Kg,           /No_Sec
 
   CTM_Sum_Emissions, 'SALTSRCE',             $
      File=FileOld, Tracer=Tracer, /Cum_Only, $
      Result=Old,   /Kg,           /No_Sec
 
   ; Write to log file
   WriteHeader,  Ilun, 'SEA SALT EMISSIONS', Version_New, Version_Old 
   WriteTracers, Ilun, New, Old, /Advance
 
   ; Clear memory
   UnDefine, New
   UnDefine, Old

   ;====================================================================
   ; Close file and quit
   ;====================================================================
quit:
   Close,    Ilun
   Free_LUN, Ilun
 
   return
end
   
   
 
 
   
