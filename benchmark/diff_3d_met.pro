;-----------------------------------------------------------------------
;+
; NAME:
;        DIFF_3D_MET
;
; PURPOSE:
;        Creates absolute difference plots ( New - Old ) 
;        for GEOS-Chem 3-D meteorology fields.
;
; CATEGORY:
;        Benchmarking
;
; CALLING SEQUENCE:
;        DIFF_3D_MET, FILES, LEVELS, TAUS, TRACERS, VERSIONS, [, Keywords ]
;
; INPUTS:
;        FILES -> A 2-element vector containing the names of files
;             from the "old" and "new" GEOS-Chem model versions
;             that are to be compared. 
;
;        LEVELS -> A 4-element vector containing the level indices
;             for the GEOS-Chem surface layer and 500 hPa layer.
;             for both models (e.g. SFC_1, SFC_2, 500_1, 500_2).
;             NOTE: This is in Fortran notation (starting from 1!)
;
;        TAUS -> A 2-element vector contaning TAU values (hours GMT
;             from /1/1985) corresponding to the "old" and "new"
;             GEOS-Chem model versions.
;
;        TRACERS -> The list of transported tracers (i.e. diagnostic
;             category "IJ-AVG-$").
;
;        VERSIONS -> A 2-element vector containing the version
;             numbers for the "old" and "new" GEOS-Chem model
;             versions.
;
; KEYWORD PARAMETERS:
;        /DYNRANGE -> Set this switch to create plots using the whole
;             dynamic range of the data.  Default is to restrict
;             the plot range to predetermined values as returned
;             by routine GET_DIFF_RANGE.
;
;        /PS -> Set this switch to generate PostScript output.
;
;        OUTFILENAME -> If /PS is set, will write PostScript output 
;             to a file whose name is specified by this keyword.
;             Default is "tracer_ratio.pro".
;
; OUTPUTS:
;        None
;
; SUBROUTINES:
;        Internal Subroutines Included:
;        ===================================================
;        PlotDiff
;
;        External Subroutines Required:
;        ===================================================
;        CLOSE_DEVICE           COLORBAR_NDIV    (function)
;        CTM_GET_DATA           GET_DIFF_RANGE  
;        GETMODELANDGRIDINFO    EXTRACT_FILENAME (function)
;        MULTIPANEL             MYCT                       
;        OPEN_DEVICE            TVMAP                      
;        UNDEFINE        
;        
; REQUIREMENTS:
;        References routines from the GAMAP package.
;        
; NOTES:
;        (1) Meant to be called from BENCHMARK_1MON.
;
; EXAMPLES:
;        FILES    = [ 'ctm.bpch.v7-04-10', 'ctm.bpch.v7-04-11' ]
;        LEVELS   = [ 1, 1, 13, 13 ]
;        TAUS     = [ NYMD2TAU( 20010701 ), NYMD2TAU( 20010701 ) ]
;        TRACERS  = INDGEN( 43 ) + 1
;        VERSIONS = [ 'v7-04-10', 'v7-04-11' ]
;
;        DIFF_3D_MET, FILES, LEVELS, TAUS, TRACERS, VERSIONS, $
;             /PS, OUTFILENAME='myplot.ps'
;
;             ; Creates ratio plots of two GEOS-CHEM versions
;             ; (in this case v7-04-11 / v7-04-10) for July 2001.
;             ; Output is sent to PostScript file "myplot.ps".
;             ; The min and max of the data on each plot panel is 
;             ; restricted to pre-defined values returned by
;             ; function GET_DIFF_RANGE.
;
;        DIFF_3D_MET, FILES, LEVELS, TAUS, TRACERS, VERSIONS, $
;             /DYNRANGE, /PS, OUTFILENAME='myplot.ps'
;
;             ; Same as the above example, but the min & max of 
;             ; each plot panel corresponds to the dynamic range
;             ; of the data.
;
; MODIFICATION HISTORY:
;        mps, 02 Dec 2013: - Initial version, based on differences.pro
; 
;-
; Copyright (C) 2007-2011,
; Bob Yantosca and Philippe Le Sager, Harvard University
; This software is provided as is without any warranty whatsoever. 
; It may be freely used, copied or distributed for non-commercial 
; purposes. This copyright notice must be kept with any copy of 
; this software. If this software shall be used commercially or 
; sold as part of a larger package, please contact the author.
; Bugs and comments should be directed to bmy@io.as.harvard.edu
; or phs@io.as.harvard.edu with subject "IDL routine differences"
;-----------------------------------------------------------------------


pro PlotDiff, Data1,    Data2, Level,    TracerName, $
              GridInfo, Unit,  DynRange, PS=PS,      $
              _EXTRA=e
   
   ;====================================================================
   ; Internal routine PLOTDIFF plots either the surface or 500 hPa
   ; ratio of tracer between old and new versions (bmy, 11/14/07)
   ;====================================================================

   ; Plot title
   if ( Level gt 1 )                                    $
      then Title = 'Abs Diff @ 500 hPa - ' + TracerName $
      else Title = 'Abs Diff @ Surface - ' + TracerName  

   ; Number of colorbar tickmarks
   Divisions = ColorBar_NDiv( 6 )
      
   ; Don't plot the polar latitudes
   XMid  = GridInfo.XMid
   YMid  = GridInfo.YMid[ 1:GridInfo.JMX-2 ]
   Data1 = Data1[ *, 1:GridInfo.JMX-2 ]
   Data2 = Data2[ *, 1:GridInfo.JMX-2 ]

   ; Compute "new" - "old" difference
   Diff  = Data2 - Data1

   if ( DynRange ) then begin

      ;=================================================================
      ; Create plots using the full dynamic range of the data (centered
      ; around zero) if the /DYNRANGE keyword is set.
      ;=================================================================
      MinData  =  Min( Diff, Max=MaxData )
      Extreme  =  Max( [ Abs( MinData ), Abs( MaxData ) ] )
      MinData  = -Extreme
      MaxData  =  Extreme
      Triangle =  0
      NoGap    =  0
      Upos     =  1.1

      ; We need to set the colorbar a little bit lower for PostScript
      if ( Keyword_Set( PS ) )                     $
         then CBPos = [ 0.10, -0.02, 0.90, 0.01 ]  $
         else CBPos = [ 0.05,  0.05, 0.95, 0.08 ]

   endif else begin

      ;=================================================================
      ; Create plots using the pre-defined min & max values from
      ; function GET_DIFF_RANGE.  This is the default.
      ;=================================================================
      Range    =  Get_Diff_Range( TracerName )
      MinData  =  Range[0] 
      MaxData  =  Range[1]
      BotOut   =  !MYCT.BOTTOM
      Triangle =  1
      NoGap    =  1
      UPos     =  1.02

      ; We need to set the colorbar a little bit lower for PostScript
      if ( Keyword_Set( PS ) )                     $
         then CBPos = [ 0.05, -0.02, 0.95, 0.01 ]  $
         else CBPos = [ 0.05,  0.05, 0.95, 0.08 ]

   endelse

   ; For OH, let's rescale the unit for clarity
   if ( TracerName eq 'OH' ) then begin
      Diff    = Diff    / 1e5
      MinData = MinData / 1e5
      MaxData = MaxData / 1e5
      Unit    = '1e5 molec/cm3'
   endif

   ; For HO2, let's rescale the unit for clarity
   if ( TracerName eq 'HO2' ) then begin
      Diff    = Diff    / 1e-12
      MinData = MinData / 1e-12
      MaxData = MaxData / 1e-12
      Unit    = 'pptv'
   endif

   ; Plot differences over a world map
   TvMap, Diff, XMid, Ymid,                                 $
      /Countries,         /Coasts,         /Cbar,           $
      Division=Divisions, /Sample,         /Grid,           $
      Title=Title,        MinData=MinData, MaxData=MaxData, $
      CBFormat='(f13.4)', BOR_Label=' ',   Unit=Unit,       $
      Triangle=Triangle,  NoGap=NoGap,     BotOut=BotOut,   $
      CbPosition=CbPos,   UPos=UPos,       _EXTRA=e

end

;------------------------------------------------------------------------------

pro Diff_3D_Met, Files, Levels, Taus, Tracers, Versions,      $
                 DynRange=DynRange,                           $
                 PS=PS,              OutFileName=OutFileName, $
                 _EXTRA=e
 
   ;====================================================================
   ; Initialization
   ;====================================================================
   
   ; External functions
   FORWARD_FUNCTION ColorBar_NDiv, Get_Diff_Range, Extract_FileName

   ; Arguments
   if ( N_Elements( Files    ) ne 2 ) then Message, 'Invalid FILES!'
   if ( N_Elements( Levels   ) ne 4 ) then Message, 'Invalid LEVELS'
   if ( N_Elements( Taus     ) ne 2 ) then Message, 'Invalid TAUS!'
   if ( N_Elements( Versions ) ne 2 ) then Message, 'Invalid VERSIONS!'

   ; Keywords
   DynRange    = Keyword_Set( DynRange )
   if ( N_Elements( OutFileName ) ne 1 ) then OutFileName = 'ratios.ps'

   ; Title for the top of the plot
   if ( DynRange ) then begin
      TopTitle = 'GEOS-Chem '                 + Versions[1]           + $
        ' Absolute Differences (dyn range) at Surface and 500 hPa!C!C'+ $
                 Extract_FileName( Files[1] ) +  ' - '                + $
                 Extract_FileName( Files[0] ) 
   endif else begin
      TopTitle = 'GEOS-Chem '                 + Versions[1]           + $
                 ' Absolute Differences at Surface and 500 hPa!C!C'   + $
                 Extract_FileName( Files[1] ) +  ' - '                + $
                 Extract_FileName( Files[0] ) 
   endelse

   ; Save original color table
   TvLct, R, G, B, /Get

   ; Save the original settings of the !MYCT sysvar
   if ( ChkStru( !MYCT ) ) then Myct_Orig = !MYCT

   ; Load Blue-White-White-Red colortable
   MyCt, /BuWhWhRd
   
   ;====================================================================
   ; Read data from the files
   ;====================================================================
   
   ;------------------------------
   ; From FILE_1: the "old" file
   ;------------------------------

   ; Read tracers from the old file
   CTM_Get_Data, DataInfo_1, 'DAO-3D-$', $
      File=Files[0], Tau0=Taus[0], Tracer=Tracers, /Quiet

   ;------------------------------
   ; From FILE_2: the "new" file
   ;------------------------------

   ; Read tracers from the "new" file
   CTM_Get_Data, DataInfo_2, 'DAO-3D-$', $
      File=Files[1], Tau0=Taus[1], Tracer=Tracers, /Quiet

   ;------------------------------
   ; Error checks!
   ;------------------------------

   ; Stop if both DATAINFOs are incompatible
   if ( N_Elements( DataInfo_1 ) ne N_Elements( DataInfo_2 ) ) $
      then Message, 'Files have different numbers of tracers in them!'

   ;====================================================================
   ; Process data and create difference plots!
   ;====================================================================

   ; Number of rows & columns on the plot
   Rows = 3
   Cols = 2

   ; Use Postscript font
   !p.font = 0

   ; Open the plot device and initialize the page
   Open_Device, /Color, Bits=8, /Portrait, PS=PS, File=OutFileName, _EXTRA=e

   ; Multiple panels per page
   MultiPanel, Rows=Rows, Cols=Cols, Margin=[ 0.03, 0.03, 0.03, 0.03 ]

   ; Loop over all data blocks
   for D = 0L, N_Elements( DataInfo_1 )-1L do begin
   
      ;-----------------------------------------------------------------
      ; Error check grid, tracer name, and data block sizes 
      ;-----------------------------------------------------------------

      ; Get MODELINFO and GRIDINFO structures
      GetModelAndGridInfo, DataInfo_1[D], ModelInfo_1, GridInfo_1
      GetModelAndGridInfo, DataInfo_2[D], ModelInfo_2, GridInfo_2

      ; Make sure grids are compatible
      if ( GridInfo_1.IMX ne GridInfo_2.IMX OR $
           GridInfo_1.JMX ne GridInfo_2.JMX )  $
         then Message, 'Resolution mismatch!'

      ; Make sure the tracers correspond to each other
      TracerName_1 = DataInfo_1[D].TracerName
      TracerName_2 = DataInfo_2[D].TracerName
      if ( TracerName_1 ne TracerName_2 ) then Message, 'Tracer mismatch!'

      ; Get full-sized data arrays
      Data_1 = *( DataInfo_1[D].Data )
      Data_2 = *( DataInfo_2[D].Data )

      ; Get the dimensions of the data arrays
      Size_1 = Size( Data_1, /Dim )
      Size_2 = Size( Data_2, /Dim )

      ; Stop the run if the data block sizes don't agree
      if ( Size_1[0] ne Size_2[0] ) then Message, 'Longitude mismatch!'
      if ( Size_1[1] ne Size_2[1] ) then Message, 'Latitude mismatch!'

      ; Get unit of data
      Unit = DataInfo_1[D].Unit

      ;-----------------------------------------------------------------
      ; Extract data arrays for surface and 500 hPa
      ;-----------------------------------------------------------------

      ; Surface data arrays
      Data_Sfc_1 = Data_1[ *, *, Levels[0]-1 ]
      Data_Sfc_2 = Data_2[ *, *, Levels[1]-1 ]

      ; 500 hPa data arrays
      Data_500_1 = Data_1[ *, *, Levels[2]-1 ]
      Data_500_2 = Data_2[ *, *, Levels[3]-1 ]

      ; We no longer need the 3-D data arrays
      UnDefine, Data_1
      UnDefine, Data_2

      ;-----------------------------------------------------------------
      ; Compute the absolute differences and plot the data
      ;-----------------------------------------------------------------
 
      ; Plot the surface differences
      PlotDiff, Data_Sfc_1, Data_Sfc_2, Levels[0], TracerName_1, $
                GridInfo_1, Unit,       DynRange,  PS=PS,        $
                _EXTRA=e

      ; Plot the 500 hPa differences
      PlotDiff, Data_500_1, Data_500_2, Levels[2], TracerName_1, $
                GridInfo_1, Unit,       DynRange,  PS=PS,        $
                _EXTRA=e
 
      ; Plot the top title on each page  
      if ( D*2 mod ( Rows * Cols ) eq 0 ) then begin
         XYoutS, 0.5, 1.03, TopTitle, $
            /Normal, Color=!MYCT.BLACK, CharSize=1.0, Align=0.5
      endif

      ;-----------------------------------------------------------------
      ; Undefine stuff for next iteration
      ;-----------------------------------------------------------------
      UnDefine, Data_Sfc_1
      UnDefine, Data_Sfc_2
      UnDefine, Data_500_1
      UnDefine, Data_500_2
      UnDefine, GridInfo_1
      UnDefine, GridInfo_2
      UnDefine, ModelInfo_1
      UnDefine, ModelInfo_2
      UnDefine, Ratio_Sfc
      UnDefine, Ratio_500
      UnDefine, Size_1
      UnDefine, Size_2
      UnDefine, TracerName_1
      UnDefine, TracerName_2

   endfor

   ;====================================================================
   ; Cleanup and quit
   ;====================================================================

   ; Cancel previous MULTIPANEL settings
   MultiPanel, /Off

   ; Close plot device
   Close_Device

   ; Restore original color table
   TvLct, R, G, B

   ; Restore !MYCT sysvar to original settings
   if ( ChkStru( Myct_Orig ) ) then !MYCT = Myct_Orig

   ; Quit
   return
end
 
